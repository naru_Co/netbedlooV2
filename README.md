# Netbedloo
Online application for exchange : books, DVD, video games, trocs. Based on an optimisation Engine which detect the best part (or path ) to exchange with. This application is a also a cultural network where users can exchange also their critics and opinions inside forums and talk in privacy with a private real time chat system.
This application was generated using JHipster 4.1.1.

## Copyright
2017 © Naru .inc , All rights reserved
## Development
This application was developped using for:
- Server Side : Java, Spring Boot, Spring Security, Maven, etc.. 
- Client Side : AngularJs, Jquery, HTML 5, CSS3, Bootstrap; WebSockets, etc..
- DataBase : PostgreSQL, Liquibase, Hazelcache, etc ... 




package com.naru.netbedloo.domain;

import java.time.Instant;

public class EkherHkeya {

    private long id;
    private Profile profile;
    private Message message;
    private Instant creationDate;
    private long type;
    private long unseen;

    public EkherHkeya() {

    }

    public EkherHkeya(long id, Profile profile, Message message, Instant creationDate, long type,long unseen) {
        this.id = id;
        this.profile = profile;
        this.message = message;
        this.creationDate = creationDate;
        this.unseen=unseen;
        this.type = type;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public long getUnseen() {
        return unseen;
    }

    public void setUnsen(long unseen) {
        this.unseen = unseen;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

}
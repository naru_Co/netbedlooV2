package com.naru.netbedloo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Matched.
 */
@Entity
@Table(name = "matched")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "matched")
public class Matched implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "matching_date")
    private Instant matchingDate;

    @Column(name = "statut")
    private Boolean statut;

    @Column(name = "reference")
    private Long reference;

    @Column(name = "distance")
    private Double distance;

    @ManyToOne
    private Profile owner;

    @ManyToOne
    private Profile wanter;

    @ManyToOne
    private Article owned;

    @ManyToOne
    private Article wanted;

    @ManyToOne
    private Tbedloo tbedloo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getMatchingDate() {
        return matchingDate;
    }

    public Matched matchingDate(Instant matchingDate) {
        this.matchingDate = matchingDate;
        return this;
    }

    public void setMatchingDate(Instant matchingDate) {
        this.matchingDate = matchingDate;
    }

    public Boolean isStatut() {
        return statut;
    }

    public Matched statut(Boolean statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Long getReference() {
        return reference;
    }

    public Matched reference(Long reference) {
        this.reference = reference;
        return this;
    }

    public void setReference(Long reference) {
        this.reference = reference;
    }

    public Double getDistance() {
        return distance;
    }

    public Matched distance(Double distance) {
        this.distance = distance;
        return this;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Profile getOwner() {
        return owner;
    }

    public Matched owner(Profile profile) {
        this.owner = profile;
        return this;
    }

    public void setOwner(Profile profile) {
        this.owner = profile;
    }

    public Profile getWanter() {
        return wanter;
    }

    public Matched wanter(Profile profile) {
        this.wanter = profile;
        return this;
    }

    public void setWanter(Profile profile) {
        this.wanter = profile;
    }

    public Article getOwned() {
        return owned;
    }

    public Matched owned(Article article) {
        this.owned = article;
        return this;
    }

    public void setOwned(Article article) {
        this.owned = article;
    }

    public Article getWanted() {
        return wanted;
    }

    public Matched wanted(Article article) {
        this.wanted = article;
        return this;
    }

    public void setWanted(Article article) {
        this.wanted = article;
    }

    public Tbedloo getTbedloo() {
        return tbedloo;
    }

    public Matched tbedloo(Tbedloo tbedloo) {
        this.tbedloo = tbedloo;
        return this;
    }

    public void setTbedloo(Tbedloo tbedloo) {
        this.tbedloo = tbedloo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Matched matched = (Matched) o;
        if (matched.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, matched.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Matched{" +
            "id=" + id +
            ", matchingDate='" + matchingDate + "'" +
            ", statut='" + statut + "'" +
            ", reference='" + reference + "'" +
            ", distance='" + distance + "'" +
            '}';
    }
}

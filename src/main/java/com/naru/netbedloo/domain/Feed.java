package com.naru.netbedloo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Feed.
 */
@Entity
@Table(name = "feed")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "feed")
public class Feed implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "time")
    private Instant time;

    @Column(name = "type")
    private Integer type;

    @Column(name = "statut")
    private Boolean statut;

    @Column(name = "content")
    private String content;

    @ManyToOne
    private MatchingHistory matches;

    @OneToOne
    @JoinColumn(unique = true)
    private OwnedHistory owned;

    @OneToOne
    @JoinColumn(unique = true)
    private WantedHistory wanted;

    @OneToOne
    @JoinColumn(unique = true)
    private Follow follow;

    @ManyToOne
    private Profile owner;

    @OneToOne
    @JoinColumn(unique = true)
    private Article article;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "feed_visible_to",
               joinColumns = @JoinColumn(name="feeds_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="visible_tos_id", referencedColumnName="id"))
    private Set<Profile> visibleTos = new HashSet<>();

    @OneToOne
    @JoinColumn(unique = true)
    private Rating rating;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTime() {
        return time;
    }

    public Feed time(Instant time) {
        this.time = time;
        return this;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Integer getType() {
        return type;
    }

    public Feed type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean isStatut() {
        return statut;
    }

    public Feed statut(Boolean statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public String getContent() {
        return content;
    }

    public Feed content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MatchingHistory getMatches() {
        return matches;
    }

    public Feed matches(MatchingHistory matchingHistory) {
        this.matches = matchingHistory;
        return this;
    }

    public void setMatches(MatchingHistory matchingHistory) {
        this.matches = matchingHistory;
    }

    public OwnedHistory getOwned() {
        return owned;
    }

    public Feed owned(OwnedHistory ownedHistory) {
        this.owned = ownedHistory;
        return this;
    }

    public void setOwned(OwnedHistory ownedHistory) {
        this.owned = ownedHistory;
    }

    public WantedHistory getWanted() {
        return wanted;
    }

    public Feed wanted(WantedHistory wantedHistory) {
        this.wanted = wantedHistory;
        return this;
    }

    public void setWanted(WantedHistory wantedHistory) {
        this.wanted = wantedHistory;
    }

    public Follow getFollow() {
        return follow;
    }

    public Feed follow(Follow follow) {
        this.follow = follow;
        return this;
    }

    public void setFollow(Follow follow) {
        this.follow = follow;
    }

    public Profile getOwner() {
        return owner;
    }

    public Feed owner(Profile profile) {
        this.owner = profile;
        return this;
    }

    public void setOwner(Profile profile) {
        this.owner = profile;
    }

    public Article getArticle() {
        return article;
    }

    public Feed article(Article article) {
        this.article = article;
        return this;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Set<Profile> getVisibleTos() {
        return visibleTos;
    }

    public Feed visibleTos(Set<Profile> profiles) {
        this.visibleTos = profiles;
        return this;
    }

    public Feed addVisibleTo(Profile profile) {
        this.visibleTos.add(profile);
        profile.getFeeds().add(this);
        return this;
    }

    public Feed removeVisibleTo(Profile profile) {
        this.visibleTos.remove(profile);
        profile.getFeeds().remove(this);
        return this;
    }

    public void setVisibleTos(Set<Profile> profiles) {
        this.visibleTos = profiles;
    }

    public Rating getRating() {
        return rating;
    }

    public Feed rating(Rating rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Feed feed = (Feed) o;
        if (feed.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), feed.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Feed{" +
            "id=" + getId() +
            ", time='" + getTime() + "'" +
            ", type=" + getType() +
            ", statut='" + isStatut() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}

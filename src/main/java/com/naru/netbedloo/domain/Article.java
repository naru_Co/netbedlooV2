package com.naru.netbedloo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Article.
 */
@Entity
@Table(name = "article")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "cover_image")
    private String coverImage;

    @Column(name = "normal_image")
    private String normalImage;

    @Column(name = "medium_image")
    private String mediumImage;

     @Column(name = "thumbnail_image")
    private String thumbnailImage;

    @Column(name = "description")
    private String description;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "popularity")
    private Long popularity;

    @Column(name = "average_rate")
    private Double averageRate;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Spec spec;

    @ManyToOne
    private Profile profile;

    @OneToMany(mappedBy = "owned")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Matched> matchedowners = new HashSet<>();

    @OneToMany(mappedBy = "wanted")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Matched> matchedwanters = new HashSet<>();

    @OneToMany(mappedBy = "article")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Owned> owneds = new HashSet<>();

    @OneToMany(mappedBy = "article")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Wanted> wanteds = new HashSet<>();

    @OneToMany(mappedBy = "owned")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MatchingHistory> matchingownedHistories = new HashSet<>();

    @OneToMany(mappedBy = "wanted")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MatchingHistory> matchingwantedHistories = new HashSet<>();

    @OneToMany(mappedBy = "article")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OwnedHistory> ownedHistories = new HashSet<>();

    @OneToMany(mappedBy = "article")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<WantedHistory> wantedHistories = new HashSet<>();

    @OneToMany(mappedBy = "rated",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Rating> ratings = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "article_tag",
               joinColumns = @JoinColumn(name="articles_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="tags_id", referencedColumnName="id"))
    private Set<Tag> tags = new HashSet<>();

    @OneToOne(orphanRemoval = true)
    @JoinColumn(unique = true)
    private Forum forum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

          public String getNormalImage() {
        return normalImage;
    }

     public Article normalImage(String normalImage) {
        this.normalImage = normalImage;
        return this;
    }

    public void setNormalImage(String normalImage) {
        this.normalImage = normalImage;
    }

      public String getMediumImage() {
        return mediumImage;
    }

     public Article mediumImage(String mediumImage) {
        this.mediumImage = mediumImage;
        return this;
    }

    public void setMediumImage(String mediumImage) {
        this.mediumImage = mediumImage;
    }

         public String getThumbnailImage() {
        return thumbnailImage;
    }

     public Article thumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
        return this;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getTitle() {
        return title;
    }

    public Article title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public Article author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public Article coverImage(String coverImage) {
        this.coverImage = coverImage;
        return this;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getDescription() {
        return description;
    }

    public Article description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Article creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Long getPopularity() {
        return popularity;
    }

    public Article popularity(Long popularity) {
        this.popularity = popularity;
        return this;
    }

    public void setPopularity(Long popularity) {
        this.popularity = popularity;
    }
    
    public Double getAverageRate() {
        return averageRate;
    }

    public Article averageRate(Double averageRate) {
        this.averageRate = averageRate;
        return this;
    }

    public void setAverageRate(Double averageRate) {
        this.averageRate = averageRate;
    }

    public Category getCategory() {
        return category;
    }

    public Article category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Spec getSpec() {
        return spec;
    }

    public Article spec(Spec spec) {
        this.spec = spec;
        return this;
    }

    public void setSpec(Spec spec) {
        this.spec = spec;
    }

    public Profile getProfile() {
        return profile;
    }

    public Article profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Set<Matched> getMatchedowners() {
        return matchedowners;
    }

    public Article matchedowners(Set<Matched> matcheds) {
        this.matchedowners = matcheds;
        return this;
    }

    public Article addMatchedowner(Matched matched) {
        this.matchedowners.add(matched);
        matched.setOwned(this);
        return this;
    }

    public Article removeMatchedowner(Matched matched) {
        this.matchedowners.remove(matched);
        matched.setOwned(null);
        return this;
    }

    public void setMatchedowners(Set<Matched> matcheds) {
        this.matchedowners = matcheds;
    }

    public Set<Matched> getMatchedwanters() {
        return matchedwanters;
    }

    public Article matchedwanters(Set<Matched> matcheds) {
        this.matchedwanters = matcheds;
        return this;
    }

    public Article addMatchedwanter(Matched matched) {
        this.matchedwanters.add(matched);
        matched.setWanted(this);
        return this;
    }

    public Article removeMatchedwanter(Matched matched) {
        this.matchedwanters.remove(matched);
        matched.setWanted(null);
        return this;
    }

    public void setMatchedwanters(Set<Matched> matcheds) {
        this.matchedwanters = matcheds;
    }

    public Set<Owned> getOwneds() {
        return owneds;
    }

    public Article owneds(Set<Owned> owneds) {
        this.owneds = owneds;
        return this;
    }

    public Article addOwned(Owned owned) {
        this.owneds.add(owned);
        owned.setArticle(this);
        return this;
    }

    public Article removeOwned(Owned owned) {
        this.owneds.remove(owned);
        owned.setArticle(null);
        return this;
    }

    public void setOwneds(Set<Owned> owneds) {
        this.owneds = owneds;
    }

    public Set<Wanted> getWanteds() {
        return wanteds;
    }

    public Article wanteds(Set<Wanted> wanteds) {
        this.wanteds = wanteds;
        return this;
    }

    public Article addWanted(Wanted wanted) {
        this.wanteds.add(wanted);
        wanted.setArticle(this);
        return this;
    }

    public Article removeWanted(Wanted wanted) {
        this.wanteds.remove(wanted);
        wanted.setArticle(null);
        return this;
    }

    public void setWanteds(Set<Wanted> wanteds) {
        this.wanteds = wanteds;
    }

    public Set<MatchingHistory> getMatchingownedHistories() {
        return matchingownedHistories;
    }

    public Article matchingownedHistories(Set<MatchingHistory> matchingHistories) {
        this.matchingownedHistories = matchingHistories;
        return this;
    }

    public Article addMatchingownedHistory(MatchingHistory matchingHistory) {
        this.matchingownedHistories.add(matchingHistory);
        matchingHistory.setOwned(this);
        return this;
    }

    public Article removeMatchingownedHistory(MatchingHistory matchingHistory) {
        this.matchingownedHistories.remove(matchingHistory);
        matchingHistory.setOwned(null);
        return this;
    }

    public void setMatchingownedHistories(Set<MatchingHistory> matchingHistories) {
        this.matchingownedHistories = matchingHistories;
    }

    public Set<MatchingHistory> getMatchingwantedHistories() {
        return matchingwantedHistories;
    }

    public Article matchingwantedHistories(Set<MatchingHistory> matchingHistories) {
        this.matchingwantedHistories = matchingHistories;
        return this;
    }

    public Article addMatchingwantedHistory(MatchingHistory matchingHistory) {
        this.matchingwantedHistories.add(matchingHistory);
        matchingHistory.setWanted(this);
        return this;
    }

    public Article removeMatchingwantedHistory(MatchingHistory matchingHistory) {
        this.matchingwantedHistories.remove(matchingHistory);
        matchingHistory.setWanted(null);
        return this;
    }

    public void setMatchingwantedHistories(Set<MatchingHistory> matchingHistories) {
        this.matchingwantedHistories = matchingHistories;
    }

    public Set<OwnedHistory> getOwnedHistories() {
        return ownedHistories;
    }

    public Article ownedHistories(Set<OwnedHistory> ownedHistories) {
        this.ownedHistories = ownedHistories;
        return this;
    }

    public Article addOwnedHistory(OwnedHistory ownedHistory) {
        this.ownedHistories.add(ownedHistory);
        ownedHistory.setArticle(this);
        return this;
    }

    public Article removeOwnedHistory(OwnedHistory ownedHistory) {
        this.ownedHistories.remove(ownedHistory);
        ownedHistory.setArticle(null);
        return this;
    }

    public void setOwnedHistories(Set<OwnedHistory> ownedHistories) {
        this.ownedHistories = ownedHistories;
    }

    public Set<WantedHistory> getWantedHistories() {
        return wantedHistories;
    }

    public Article wantedHistories(Set<WantedHistory> wantedHistories) {
        this.wantedHistories = wantedHistories;
        return this;
    }

    public Article addWantedHistory(WantedHistory wantedHistory) {
        this.wantedHistories.add(wantedHistory);
        wantedHistory.setArticle(this);
        return this;
    }

    public Article removeWantedHistory(WantedHistory wantedHistory) {
        this.wantedHistories.remove(wantedHistory);
        wantedHistory.setArticle(null);
        return this;
    }

    public void setWantedHistories(Set<WantedHistory> wantedHistories) {
        this.wantedHistories = wantedHistories;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public Article ratings(Set<Rating> ratings) {
        this.ratings = ratings;
        return this;
    }

    public Article addRating(Rating rating) {
        this.ratings.add(rating);
        rating.setRated(this);
        return this;
    }

    public Article removeRating(Rating rating) {
        this.ratings.remove(rating);
        rating.setRated(null);
        return this;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Article tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Article addTag(Tag tag) {
        this.tags.add(tag);
        tag.getArticles().add(this);
        return this;
    }

    public Article removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getArticles().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Forum getForum() {
        return forum;
    }

    public Article forum(Forum forum) {
        this.forum = forum;
        return this;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Article article = (Article) o;
        if (article.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, article.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Article{" +
            "id=" + id +
            ", title='" + title + "'" +
            ", author='" + author + "'" +
            ", coverImage='" + coverImage + "'" +
            ", description='" + description + "'" +
            ", creationDate='" + creationDate + "'" +
            ", popularity='" + popularity + "'" +
            '}';
    }
}

package com.naru.netbedloo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tbedloo.
 */
@Entity
@Table(name = "tbedloo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tbedloo")
public class Tbedloo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tbedloo_date")
    private Instant tbedlooDate;

    @OneToMany(orphanRemoval=true,mappedBy = "tbedloo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Matched> matcheds = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTbedlooDate() {
        return tbedlooDate;
    }

    public Tbedloo tbedlooDate(Instant tbedlooDate) {
        this.tbedlooDate = tbedlooDate;
        return this;
    }

    public void setTbedlooDate(Instant tbedlooDate) {
        this.tbedlooDate = tbedlooDate;
    }

    public Set<Matched> getMatcheds() {
        return matcheds;
    }

    public Tbedloo matcheds(Set<Matched> matcheds) {
        this.matcheds = matcheds;
        return this;
    }

    public Tbedloo addMatched(Matched matched) {
        this.matcheds.add(matched);
        matched.setTbedloo(this);
        return this;
    }

    public Tbedloo removeMatched(Matched matched) {
        this.matcheds.remove(matched);
        matched.setTbedloo(null);
        return this;
    }

    public void setMatcheds(Set<Matched> matcheds) {
        this.matcheds = matcheds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tbedloo tbedloo = (Tbedloo) o;
        if (tbedloo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tbedloo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tbedloo{" +
            "id=" + getId() +
            ", tbedlooDate='" + getTbedlooDate() + "'" +
            "}";
    }
}

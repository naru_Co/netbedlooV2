package com.naru.netbedloo.domain;

public class ArticleImage {

    private Article article;
    private byte [] file;
    private String name;
    private String type;
   
    public ArticleImage(){
        
    }

    public ArticleImage(Article article, byte [] file,String name,String type){
     
        this.article=article;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Article getArticle() {
        return article;
    }
   public void setArticle(Article article) {
        this.article = article;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
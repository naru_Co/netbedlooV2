package com.naru.netbedloo.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A WantedHistory.
 */
@Entity
@Table(name = "wanted_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "wantedhistory")
public class WantedHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "wanting_date")
    private Instant wantingDate;

    @Column(name = "erase_date")
    private Instant eraseDate;

    @ManyToOne
    private Article article;

    @ManyToOne
    private Profile wanter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getWantingDate() {
        return wantingDate;
    }

    public WantedHistory wantingDate(Instant wantingDate) {
        this.wantingDate = wantingDate;
        return this;
    }

    public void setWantingDate(Instant wantingDate) {
        this.wantingDate = wantingDate;
    }

    public Instant getEraseDate() {
        return eraseDate;
    }

    public WantedHistory eraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
        return this;
    }

    public void setEraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
    }

    public Article getArticle() {
        return article;
    }

    public WantedHistory article(Article article) {
        this.article = article;
        return this;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Profile getWanter() {
        return wanter;
    }

    public WantedHistory wanter(Profile profile) {
        this.wanter = profile;
        return this;
    }

    public void setWanter(Profile profile) {
        this.wanter = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WantedHistory wantedHistory = (WantedHistory) o;
        if (wantedHistory.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, wantedHistory.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "WantedHistory{" +
            "id=" + id +
            ", wantingDate='" + wantingDate + "'" +
            ", eraseDate='" + eraseDate + "'" +
            '}';
    }
}

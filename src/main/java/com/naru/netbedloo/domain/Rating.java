package com.naru.netbedloo.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
/**
 * A Rating.
 */
@Entity
@Table(name = "rating")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rating")
public class Rating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rate")
    private Integer rate;

    @Column(name = "rating_time")
    private Instant ratingTime;

    @ManyToOne
    private Article rated;

    @ManyToOne
    private Profile rater;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public Rating rate(Integer rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Instant getRatingTime() {
        return ratingTime;
    }

    public Rating ratingTime(Instant ratingTime) {
        this.ratingTime = ratingTime;
        return this;
    }

    public void setRatingTime(Instant ratingTime) {
        this.ratingTime = ratingTime;
    }

    public Article getRated() {
        return rated;
    }

    public Rating rated(Article article) {
        this.rated = article;
        return this;
    }

    public void setRated(Article article) {
        this.rated = article;
    }

    public Profile getRater() {
        return rater;
    }

    public Rating rater(Profile profile) {
        this.rater = profile;
        return this;
    }

    public void setRater(Profile profile) {
        this.rater = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rating rating = (Rating) o;
        if (rating.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, rating.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Rating{" +
            "id=" + id +
            ", rate='" + rate + "'" +
            ", ratingTime='" + ratingTime + "'" +
            '}';
    }
}

package com.naru.netbedloo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Profile.
 */
@Entity
@Table(name = "profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "profile")
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "player_id")
    private String playerId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "pseudo")
    private String pseudo;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "thumbnail_image")
    private String thumbnailImage;

    @Column(name = "normal_image")
    private String normalImage;

    @Column(name = "medium_image")
    private String mediumImage;

    @Column(name = "sexe")
    private String sexe;

    @Column(name = "adress")
    private String adress;

    @Column(name = "zip_code")
    private Integer zipCode;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "bio")
    private String bio;

    @OneToMany(mappedBy = "profile")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Article> articles = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Owned> owneds = new HashSet<>();

    @OneToMany(mappedBy = "wanter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Wanted> wanteds = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Matched> matchedowners = new HashSet<>();

    @OneToMany(mappedBy = "wanter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Matched> matchedwanters = new HashSet<>();

    @ManyToOne
    private Gouv gouv;

    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MatchingHistory> matchedownerhistories = new HashSet<>();

    @OneToMany(mappedBy = "wanter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MatchingHistory> matchedwanterhistories = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OwnedHistory> ownedHistories = new HashSet<>();

    @OneToMany(mappedBy = "wanter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<WantedHistory> wantedHistories = new HashSet<>();

    @OneToMany(mappedBy = "rater")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Rating> ratings = new HashSet<>();

    @OneToMany(mappedBy = "profile")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Forum> forums = new HashSet<>();

    @OneToMany(mappedBy = "profile")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Subject> subjects = new HashSet<>();

    @OneToMany(mappedBy = "profile")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(mappedBy = "profile")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Response> responses = new HashSet<>();

    @OneToMany(mappedBy = "followed")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Follow> tofollows = new HashSet<>();

    @OneToMany(mappedBy = "follower")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Follow> followings = new HashSet<>();

    @OneToMany(mappedBy = "sender")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Message> sentMessages = new HashSet<>();

    @OneToMany(mappedBy = "receiver")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Message> recievedMessages = new HashSet<>();

    @ManyToOne
    private Country country;
    @ManyToMany(mappedBy = "visibleTos",fetch = FetchType.EAGER)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Feed> feeds = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public Profile playerId(String playerId) {
        this.playerId = playerId;
        return this;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public Profile firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Profile lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPseudo() {
        return pseudo;
    }

    public Profile pseudo(String pseudo) {
        this.pseudo = pseudo;
        return this;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Profile birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Profile imagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public Profile thumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
        return this;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getNormalImage() {
        return normalImage;
    }

    public Profile normalImage(String normalImage) {
        this.normalImage = normalImage;
        return this;
    }

    public void setNormalImage(String normalImage) {
        this.normalImage = normalImage;
    }

    public String getMediumImage() {
        return mediumImage;
    }

    public Profile mediumImage(String mediumImage) {
        this.mediumImage = mediumImage;
        return this;
    }

    public void setMediumImage(String mediumImage) {
        this.mediumImage = mediumImage;
    }

    public String getSexe() {
        return sexe;
    }

    public Profile sexe(String sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAdress() {
        return adress;
    }

    public Profile adress(String adress) {
        this.adress = adress;
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public Profile zipCode(Integer zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public Profile email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Profile phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBio() {
        return bio;
    }

    public Profile bio(String bio) {
        this.bio = bio;
        return this;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Set<Article> getArticles() {
        return articles;
    }

    public Profile articles(Set<Article> articles) {
        this.articles = articles;
        return this;
    }

    public Profile addArticle(Article article) {
        this.articles.add(article);
        article.setProfile(this);
        return this;
    }

    public Profile removeArticle(Article article) {
        this.articles.remove(article);
        article.setProfile(null);
        return this;
    }

    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }

    public Set<Owned> getOwneds() {
        return owneds;
    }

    public Profile owneds(Set<Owned> owneds) {
        this.owneds = owneds;
        return this;
    }

    public Profile addOwned(Owned owned) {
        this.owneds.add(owned);
        owned.setOwner(this);
        return this;
    }

    public Profile removeOwned(Owned owned) {
        this.owneds.remove(owned);
        owned.setOwner(null);
        return this;
    }

    public void setOwneds(Set<Owned> owneds) {
        this.owneds = owneds;
    }

    public Set<Wanted> getWanteds() {
        return wanteds;
    }

    public Profile wanteds(Set<Wanted> wanteds) {
        this.wanteds = wanteds;
        return this;
    }

    public Profile addWanted(Wanted wanted) {
        this.wanteds.add(wanted);
        wanted.setWanter(this);
        return this;
    }

    public Profile removeWanted(Wanted wanted) {
        this.wanteds.remove(wanted);
        wanted.setWanter(null);
        return this;
    }

    public void setWanteds(Set<Wanted> wanteds) {
        this.wanteds = wanteds;
    }

    public Set<Matched> getMatchedowners() {
        return matchedowners;
    }

    public Profile matchedowners(Set<Matched> matcheds) {
        this.matchedowners = matcheds;
        return this;
    }

    public Profile addMatchedowner(Matched matched) {
        this.matchedowners.add(matched);
        matched.setOwner(this);
        return this;
    }

    public Profile removeMatchedowner(Matched matched) {
        this.matchedowners.remove(matched);
        matched.setOwner(null);
        return this;
    }

    public void setMatchedowners(Set<Matched> matcheds) {
        this.matchedowners = matcheds;
    }

    public Set<Matched> getMatchedwanters() {
        return matchedwanters;
    }

    public Profile matchedwanters(Set<Matched> matcheds) {
        this.matchedwanters = matcheds;
        return this;
    }

    public Profile addMatchedwanter(Matched matched) {
        this.matchedwanters.add(matched);
        matched.setWanter(this);
        return this;
    }

    public Profile removeMatchedwanter(Matched matched) {
        this.matchedwanters.remove(matched);
        matched.setWanter(null);
        return this;
    }

    public void setMatchedwanters(Set<Matched> matcheds) {
        this.matchedwanters = matcheds;
    }

    public Gouv getGouv() {
        return gouv;
    }

    public Profile gouv(Gouv gouv) {
        this.gouv = gouv;
        return this;
    }

    public void setGouv(Gouv gouv) {
        this.gouv = gouv;
    }

    public Set<MatchingHistory> getMatchedownerhistories() {
        return matchedownerhistories;
    }

    public Profile matchedownerhistories(Set<MatchingHistory> matchingHistories) {
        this.matchedownerhistories = matchingHistories;
        return this;
    }

    public Profile addMatchedownerhistory(MatchingHistory matchingHistory) {
        this.matchedownerhistories.add(matchingHistory);
        matchingHistory.setOwner(this);
        return this;
    }

    public Profile removeMatchedownerhistory(MatchingHistory matchingHistory) {
        this.matchedownerhistories.remove(matchingHistory);
        matchingHistory.setOwner(null);
        return this;
    }

    public void setMatchedownerhistories(Set<MatchingHistory> matchingHistories) {
        this.matchedownerhistories = matchingHistories;
    }

    public Set<MatchingHistory> getMatchedwanterhistories() {
        return matchedwanterhistories;
    }

    public Profile matchedwanterhistories(Set<MatchingHistory> matchingHistories) {
        this.matchedwanterhistories = matchingHistories;
        return this;
    }

    public Profile addMatchedwanterhistory(MatchingHistory matchingHistory) {
        this.matchedwanterhistories.add(matchingHistory);
        matchingHistory.setWanter(this);
        return this;
    }

    public Profile removeMatchedwanterhistory(MatchingHistory matchingHistory) {
        this.matchedwanterhistories.remove(matchingHistory);
        matchingHistory.setWanter(null);
        return this;
    }

    public void setMatchedwanterhistories(Set<MatchingHistory> matchingHistories) {
        this.matchedwanterhistories = matchingHistories;
    }

    public Set<OwnedHistory> getOwnedHistories() {
        return ownedHistories;
    }

    public Profile ownedHistories(Set<OwnedHistory> ownedHistories) {
        this.ownedHistories = ownedHistories;
        return this;
    }

    public Profile addOwnedHistory(OwnedHistory ownedHistory) {
        this.ownedHistories.add(ownedHistory);
        ownedHistory.setOwner(this);
        return this;
    }

    public Profile removeOwnedHistory(OwnedHistory ownedHistory) {
        this.ownedHistories.remove(ownedHistory);
        ownedHistory.setOwner(null);
        return this;
    }

    public void setOwnedHistories(Set<OwnedHistory> ownedHistories) {
        this.ownedHistories = ownedHistories;
    }

    public Set<WantedHistory> getWantedHistories() {
        return wantedHistories;
    }

    public Profile wantedHistories(Set<WantedHistory> wantedHistories) {
        this.wantedHistories = wantedHistories;
        return this;
    }

    public Profile addWantedHistory(WantedHistory wantedHistory) {
        this.wantedHistories.add(wantedHistory);
        wantedHistory.setWanter(this);
        return this;
    }

    public Profile removeWantedHistory(WantedHistory wantedHistory) {
        this.wantedHistories.remove(wantedHistory);
        wantedHistory.setWanter(null);
        return this;
    }

    public void setWantedHistories(Set<WantedHistory> wantedHistories) {
        this.wantedHistories = wantedHistories;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public Profile ratings(Set<Rating> ratings) {
        this.ratings = ratings;
        return this;
    }

    public Profile addRating(Rating rating) {
        this.ratings.add(rating);
        rating.setRater(this);
        return this;
    }

    public Profile removeRating(Rating rating) {
        this.ratings.remove(rating);
        rating.setRater(null);
        return this;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Set<Forum> getForums() {
        return forums;
    }

    public Profile forums(Set<Forum> forums) {
        this.forums = forums;
        return this;
    }

    public Profile addForum(Forum forum) {
        this.forums.add(forum);
        forum.setProfile(this);
        return this;
    }

    public Profile removeForum(Forum forum) {
        this.forums.remove(forum);
        forum.setProfile(null);
        return this;
    }

    public void setForums(Set<Forum> forums) {
        this.forums = forums;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public Profile subjects(Set<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }

    public Profile addSubject(Subject subject) {
        this.subjects.add(subject);
        subject.setProfile(this);
        return this;
    }

    public Profile removeSubject(Subject subject) {
        this.subjects.remove(subject);
        subject.setProfile(null);
        return this;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Profile comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Profile addComment(Comment comment) {
        this.comments.add(comment);
        comment.setProfile(this);
        return this;
    }

    public Profile removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setProfile(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Response> getResponses() {
        return responses;
    }

    public Profile responses(Set<Response> responses) {
        this.responses = responses;
        return this;
    }

    public Profile addResponse(Response response) {
        this.responses.add(response);
        response.setProfile(this);
        return this;
    }

    public Profile removeResponse(Response response) {
        this.responses.remove(response);
        response.setProfile(null);
        return this;
    }

    public void setResponses(Set<Response> responses) {
        this.responses = responses;
    }

    public Set<Follow> getTofollows() {
        return tofollows;
    }

    public Profile tofollows(Set<Follow> follows) {
        this.tofollows = follows;
        return this;
    }

    public Profile addTofollow(Follow follow) {
        this.tofollows.add(follow);
        follow.setFollowed(this);
        return this;
    }

    public Profile removeTofollow(Follow follow) {
        this.tofollows.remove(follow);
        follow.setFollowed(null);
        return this;
    }

    public void setTofollows(Set<Follow> follows) {
        this.tofollows = follows;
    }

    public Set<Follow> getFollowings() {
        return followings;
    }

    public Profile followings(Set<Follow> follows) {
        this.followings = follows;
        return this;
    }

    public Profile addFollowing(Follow follow) {
        this.followings.add(follow);
        follow.setFollower(this);
        return this;
    }

    public Profile removeFollowing(Follow follow) {
        this.followings.remove(follow);
        follow.setFollower(null);
        return this;
    }

    public void setFollowings(Set<Follow> follows) {
        this.followings = follows;
    }

    public Set<Message> getSentMessages() {
        return sentMessages;
    }

    public Profile sentMessages(Set<Message> messages) {
        this.sentMessages = messages;
        return this;
    }

    public Profile addSentMessage(Message message) {
        this.sentMessages.add(message);
        message.setSender(this);
        return this;
    }

    public Profile removeSentMessage(Message message) {
        this.sentMessages.remove(message);
        message.setSender(null);
        return this;
    }

    public void setSentMessages(Set<Message> messages) {
        this.sentMessages = messages;
    }

    public Set<Message> getRecievedMessages() {
        return recievedMessages;
    }

    public Profile recievedMessages(Set<Message> messages) {
        this.recievedMessages = messages;
        return this;
    }

    public Profile addRecievedMessage(Message message) {
        this.recievedMessages.add(message);
        message.setReceiver(this);
        return this;
    }

    public Profile removeRecievedMessage(Message message) {
        this.recievedMessages.remove(message);
        message.setReceiver(null);
        return this;
    }

    public void setRecievedMessages(Set<Message> messages) {
        this.recievedMessages = messages;
    }

    public Country getCountry() {
        return country;
    }

    public Profile country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Feed> getFeeds() {
        return feeds;
    }

    public Profile feeds(Set<Feed> feeds) {
        this.feeds = feeds;
        return this;
    }

    public Profile addFeed(Feed feed) {
        this.feeds.add(feed);
        feed.getVisibleTos().add(this);
        return this;
    }

    public Profile removeFeed(Feed feed) {
        this.feeds.remove(feed);
        feed.getVisibleTos().remove(this);
        return this;
    }

    public void setFeeds(Set<Feed> feeds) {
        this.feeds = feeds;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Profile profile = (Profile) o;
        if (profile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", playerId='" + getPlayerId() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", pseudo='" + getPseudo() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", imagePath='" + getImagePath() + "'" +
            ", thumbnailImage='" + getThumbnailImage() + "'" +
            ", normalImage='" + getNormalImage() + "'" +
            ", mediumImage='" + getMediumImage() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", adress='" + getAdress() + "'" +
            ", zipCode=" + getZipCode() +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", bio='" + getBio() + "'" +
            "}";
    }
}

package com.naru.netbedloo.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A Message.
 */
@Entity
@Table(name = "message")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "message")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "creation_date")
    private Instant creationDate;

    @Column(name = "seen_date")
    private Instant seenDate;

    @Column(name = "uid")
    private String uid;

    @ManyToOne
    private Profile sender;

    @ManyToOne
    private Profile receiver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public Message content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public Message creationDate(Instant creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Instant getSeenDate() {
        return seenDate;
    }

    public Message seenDate(Instant seenDate) {
        this.seenDate = seenDate;
        return this;
    }

    public void setSeenDate(Instant seenDate) {
        this.seenDate = seenDate;
    }

    public String getUid() {
        return uid;
    }

    public Message uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Profile getSender() {
        return sender;
    }

    public Message sender(Profile profile) {
        this.sender = profile;
        return this;
    }

    public void setSender(Profile profile) {
        this.sender = profile;
    }

    public Profile getReceiver() {
        return receiver;
    }

    public Message receiver(Profile profile) {
        this.receiver = profile;
        return this;
    }

    public void setReceiver(Profile profile) {
        this.receiver = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        if (message.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, message.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Message{" +
            "id=" + id +
            ", content='" + content + "'" +
            ", creationDate='" + creationDate + "'" +
            ", seenDate='" + seenDate + "'" +
            ", uid='" + uid + "'" +
            '}';
    }
}

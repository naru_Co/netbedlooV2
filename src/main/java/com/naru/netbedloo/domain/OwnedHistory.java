package com.naru.netbedloo.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A OwnedHistory.
 */
@Entity
@Table(name = "owned_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ownedhistory")
public class OwnedHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "owningdate")
    private Instant owningdate;

    @Column(name = "erase_date")
    private Instant eraseDate;

    @ManyToOne
    private Article article;

    @ManyToOne
    private Profile owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getOwningdate() {
        return owningdate;
    }

    public OwnedHistory owningdate(Instant owningdate) {
        this.owningdate = owningdate;
        return this;
    }

    public void setOwningdate(Instant owningdate) {
        this.owningdate = owningdate;
    }

    public Instant getEraseDate() {
        return eraseDate;
    }

    public OwnedHistory eraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
        return this;
    }

    public void setEraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
    }

    public Article getArticle() {
        return article;
    }

    public OwnedHistory article(Article article) {
        this.article = article;
        return this;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Profile getOwner() {
        return owner;
    }

    public OwnedHistory owner(Profile profile) {
        this.owner = profile;
        return this;
    }

    public void setOwner(Profile profile) {
        this.owner = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OwnedHistory ownedHistory = (OwnedHistory) o;
        if (ownedHistory.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, ownedHistory.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "OwnedHistory{" +
            "id=" + id +
            ", owningdate='" + owningdate + "'" +
            ", eraseDate='" + eraseDate + "'" +
            '}';
    }
}

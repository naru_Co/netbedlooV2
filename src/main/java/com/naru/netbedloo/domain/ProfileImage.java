package com.naru.netbedloo.domain;

public class ProfileImage {

    private Profile profile;
    private byte [] file;
    private String name;
    private String type;
    private String login;
   
    public ProfileImage(){
        
    }

    public ProfileImage(Profile profile, byte [] file,String name,String type,String login){
     
        this.profile=profile;
        this.file=file;
        this.name=name;
        this.type=type;
        this.login=login;
  }   
   public Profile getProfile() {
        return profile;
    }
   public void setProfile(Profile profile) {
        this.profile = profile;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
}
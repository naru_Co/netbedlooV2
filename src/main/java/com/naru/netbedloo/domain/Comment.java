package com.naru.netbedloo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Comment.
 */
@Entity
@Table(name = "comment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "comment")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "comment")
    private String comment;

    @Column(name = "publication_time")
    private Instant publicationTime;

    @ManyToOne
    private Subject subject;

    @ManyToOne
    private Profile profile;

    @OneToMany(mappedBy = "comment")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Response> responses = new HashSet<>();

    @ManyToOne
    private Feed feed;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public Comment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getPublicationTime() {
        return publicationTime;
    }

    public Comment publicationTime(Instant publicationTime) {
        this.publicationTime = publicationTime;
        return this;
    }

    public void setPublicationTime(Instant publicationTime) {
        this.publicationTime = publicationTime;
    }

    public Subject getSubject() {
        return subject;
    }

    public Comment subject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Profile getProfile() {
        return profile;
    }

    public Comment profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Set<Response> getResponses() {
        return responses;
    }

    public Comment responses(Set<Response> responses) {
        this.responses = responses;
        return this;
    }

    public Comment addResponse(Response response) {
        this.responses.add(response);
        response.setComment(this);
        return this;
    }

    public Comment removeResponse(Response response) {
        this.responses.remove(response);
        response.setComment(null);
        return this;
    }

    public void setResponses(Set<Response> responses) {
        this.responses = responses;
    }

    public Feed getFeed() {
        return feed;
    }

    public Comment feed(Feed feed) {
        this.feed = feed;
        return this;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comment comment = (Comment) o;
        if (comment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", publicationTime='" + getPublicationTime() + "'" +
            "}";
    }
}

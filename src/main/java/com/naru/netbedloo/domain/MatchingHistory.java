package com.naru.netbedloo.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A MatchingHistory.
 */
@Entity
@Table(name = "matching_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "matchinghistory")
public class MatchingHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "matching_date")
    private Instant matchingDate;

    @Column(name = "statut")
    private String statut;

    @Column(name = "distance")
    private Double distance;

    @Column(name = "tbedloo_date")
    private Instant tbedlooDate;

    @Column(name = "erase_date")
    private Instant eraseDate;

    @ManyToOne
    private Profile owner;

    @ManyToOne
    private Profile wanter;

    @ManyToOne
    private Article owned;

    @ManyToOne
    private Article wanted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getMatchingDate() {
        return matchingDate;
    }

    public MatchingHistory matchingDate(Instant matchingDate) {
        this.matchingDate = matchingDate;
        return this;
    }

    public void setMatchingDate(Instant matchingDate) {
        this.matchingDate = matchingDate;
    }

    public String getStatut() {
        return statut;
    }

    public MatchingHistory statut(String statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Double getDistance() {
        return distance;
    }

    public MatchingHistory distance(Double distance) {
        this.distance = distance;
        return this;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Instant getTbedlooDate() {
        return tbedlooDate;
    }

    public MatchingHistory tbedlooDate(Instant tbedlooDate) {
        this.tbedlooDate = tbedlooDate;
        return this;
    }

    public void setTbedlooDate(Instant tbedlooDate) {
        this.tbedlooDate = tbedlooDate;
    }

    public Instant getEraseDate() {
        return eraseDate;
    }

    public MatchingHistory eraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
        return this;
    }

    public void setEraseDate(Instant eraseDate) {
        this.eraseDate = eraseDate;
    }

    public Profile getOwner() {
        return owner;
    }

    public MatchingHistory owner(Profile profile) {
        this.owner = profile;
        return this;
    }

    public void setOwner(Profile profile) {
        this.owner = profile;
    }

    public Profile getWanter() {
        return wanter;
    }

    public MatchingHistory wanter(Profile profile) {
        this.wanter = profile;
        return this;
    }

    public void setWanter(Profile profile) {
        this.wanter = profile;
    }

    public Article getOwned() {
        return owned;
    }

    public MatchingHistory owned(Article article) {
        this.owned = article;
        return this;
    }

    public void setOwned(Article article) {
        this.owned = article;
    }

    public Article getWanted() {
        return wanted;
    }

    public MatchingHistory wanted(Article article) {
        this.wanted = article;
        return this;
    }

    public void setWanted(Article article) {
        this.wanted = article;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MatchingHistory matchingHistory = (MatchingHistory) o;
        if (matchingHistory.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, matchingHistory.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MatchingHistory{" +
            "id=" + id +
            ", matchingDate='" + matchingDate + "'" +
            ", statut='" + statut + "'" +
            ", distance='" + distance + "'" +
            ", tbedlooDate='" + tbedlooDate + "'" +
            ", eraseDate='" + eraseDate + "'" +
            '}';
    }
}

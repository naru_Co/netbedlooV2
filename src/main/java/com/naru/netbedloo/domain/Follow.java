package com.naru.netbedloo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Follow.
 */
@Entity
@Table(name = "follow")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "follow")
public class Follow implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "follow_time")
    private Instant followTime;

    @ManyToOne
    private Profile follower;

    @ManyToOne
    private Profile followed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFollowTime() {
        return followTime;
    }

    public Follow followTime(Instant followTime) {
        this.followTime = followTime;
        return this;
    }

    public void setFollowTime(Instant followTime) {
        this.followTime = followTime;
    }

    public Profile getFollower() {
        return follower;
    }

    public Follow follower(Profile profile) {
        this.follower = profile;
        return this;
    }

    public void setFollower(Profile profile) {
        this.follower = profile;
    }

    public Profile getFollowed() {
        return followed;
    }

    public Follow followed(Profile profile) {
        this.followed = profile;
        return this;
    }

    public void setFollowed(Profile profile) {
        this.followed = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Follow follow = (Follow) o;
        if (follow.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, follow.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Follow{" +
            "id=" + id +
            ", followTime='" + followTime + "'" +
            '}';
    }
}

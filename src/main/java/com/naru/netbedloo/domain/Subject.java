package com.naru.netbedloo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Subject.
 */
@Entity
@Table(name = "subject")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "subject")
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "publication")
    private String publication;

    @Column(name = "publication_time")
    private Instant publicationTime;

    @ManyToOne
    private Forum forum;

    @OneToMany(mappedBy = "subject",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();

    @ManyToOne
    private Profile profile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublication() {
        return publication;
    }

    public Subject publication(String publication) {
        this.publication = publication;
        return this;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public Instant getPublicationTime() {
        return publicationTime;
    }

    public Subject publicationTime(Instant publicationTime) {
        this.publicationTime = publicationTime;
        return this;
    }

    public void setPublicationTime(Instant publicationTime) {
        this.publicationTime = publicationTime;
    }

    public Forum getForum() {
        return forum;
    }

    public Subject forum(Forum forum) {
        this.forum = forum;
        return this;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Subject comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Subject addComment(Comment comment) {
        this.comments.add(comment);
        comment.setSubject(this);
        return this;
    }

    public Subject removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setSubject(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Profile getProfile() {
        return profile;
    }

    public Subject profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subject subject = (Subject) o;
        if (subject.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subject.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Subject{" +
            "id=" + id +
            ", publication='" + publication + "'" +
            ", publicationTime='" + publicationTime + "'" +
            '}';
    }
}

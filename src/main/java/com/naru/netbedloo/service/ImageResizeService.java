package com.naru.netbedloo.service;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.PixelGrabber;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ImageResizeService {

    private final Logger log = LoggerFactory.getLogger(ImageResizeService.class);

    private static final int[] RGB_MASKS = { 0xFF0000, 0xFF00, 0xFF };

    private static final ColorModel RGB_OPAQUE = new DirectColorModel(32, RGB_MASKS[0], RGB_MASKS[1], RGB_MASKS[2]);

    public void thumbailSizeforProfileImage(String filePath, String outputPath) {
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);

            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 50, 50,
                    null);

            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);

        } catch (Exception e) {
        }

    }

    public void mediumSizeforProfileImage(String filePath, String outputPath) {

        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 100, 100,
                    null);

            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);
        } catch (Exception e) {
        }

    }

    public void normalSizeforProfileImage(String filePath, String outputPath) {
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 400, 400,
                    null);
            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);

        } catch (Exception e) {
        }

    }

    public File thumbailSizeforArticleImage(String filePath, String outputPath) {
        File outputfile = new File(outputPath);
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 60, 100,
                    null);

            
            ImageIO.write(scaledImage, "JPEG", outputfile);
            return outputfile;
        } catch (Exception e) {
            return outputfile;
        }

    }

    public void thumbailSizeforArticleGameImage(String filePath, String outputPath) {
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 50, 63,
                    null);

            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);
        } catch (Exception e) {
        }

    }

    public File mediumSizeforArticleImage(String filePath, String outputPath) {
        File outputfile = new File(outputPath);
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 120, 200,
                    null);

            
            ImageIO.write(scaledImage, "JPEG", outputfile);
            return outputfile;
        } catch (Exception e) {
            return outputfile;
        }

    }

    public void mediumSizeforArticleGameImage(String filePath, String outputPath) {
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 200, 250,
                    null);

            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);
        } catch (Exception e) {
        }

    }

    public File normalSizeforArticleImage(String filePath, String outputPath) {
        File outputfile = new File(outputPath);
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 300, 500,
                    null);

           
            ImageIO.write(scaledImage, "JPEG", outputfile);
            return outputfile;
        } catch (Exception e) {
            return outputfile;
        }

    }

    public void normalSizeforArticleGameImage(String filePath, String outputPath) {
        try {
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

            PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
            pg.grabPixels();
            int width = pg.getWidth(), height = pg.getHeight();

            DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
            WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
            BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

            BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 400, 500,
                    null);

            File outputfile = new File(outputPath);
            ImageIO.write(scaledImage, "JPEG", outputfile);

        } catch (Exception e) {
        }

    }

}
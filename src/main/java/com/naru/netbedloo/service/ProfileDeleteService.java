package com.naru.netbedloo.service;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Rating;
import com.naru.netbedloo.domain.Signal;
import com.naru.netbedloo.domain.Subject;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Forum;
import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.domain.MatchingHistory;
import com.naru.netbedloo.domain.Message;
import com.naru.netbedloo.repository.MatchedRepository;
import com.naru.netbedloo.repository.MatchingHistoryRepository;
import com.naru.netbedloo.repository.MessageRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.RatingRepository;
import com.naru.netbedloo.repository.SignalRepository;
import com.naru.netbedloo.repository.SubjectRepository;
import com.naru.netbedloo.repository.WantedRepository;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.ForumRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileDeleteService {

    @Autowired
    private final WantedRepository wantedRepository;
    @Autowired
    private final OwnedRepository ownedRepository;
    @Autowired
    private final ArticleRepository articleRepository;
    @Autowired
    private final MatchedRepository matchedRepository;
    @Autowired
    private final MatchingHistoryRepository matchingHistoryRepository;
    @Autowired
    private final OwnedHistoryRepository ownedHistoryRepository;
    @Autowired
    private final WantedHistoryRepository wantedHistoryRepository;
    @Autowired
    private final ProfileRepository profileRepository;

    @Autowired
    private final FollowRepository followRepository;

    @Autowired
    private final MessageRepository messageRepository;

    @Autowired
    private final ForumRepository forumRepository;

    @Autowired
    private final SubjectRepository subjectRepository;

    @Autowired
    private final RatingRepository ratingRepository;

    @Autowired
    private final SignalRepository signalRepository;

    public ProfileDeleteService(WantedRepository wantedRepository, OwnedRepository ownedRepository,
            ArticleRepository articleRepository, MatchedRepository matchedRepository,
            MatchingHistoryRepository matchingHistoryRepository, OwnedHistoryRepository ownedHistoryRepository,
            WantedHistoryRepository wantedHistoryRepository, ProfileRepository profileRepository,
            FollowRepository followRepository, MessageRepository messageRepository, ForumRepository forumRepository,
            SubjectRepository subjectRepository, SignalRepository signalRepository, RatingRepository ratingRepository) {
        this.articleRepository = articleRepository;
        this.wantedRepository = wantedRepository;
        this.ownedRepository = ownedRepository;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.matchingHistoryRepository = matchingHistoryRepository;
        this.matchedRepository = matchedRepository;
        this.profileRepository = profileRepository;
        this.followRepository = followRepository;
        this.messageRepository = messageRepository;
        this.forumRepository = forumRepository;
        this.subjectRepository = subjectRepository;
        this.signalRepository = signalRepository;
        this.ratingRepository = ratingRepository;

    }

    public void profiledelete(long id) {
        /******************** Find The Naru Profile ******************************/
        long profile_id = 9;
        Profile naru = profileRepository.findOne(profile_id);

        /******************** Configure Reports *************************/
        List<Signal> reports = signalRepository.findAllByProfileId(id);

        if (reports != null) {
            for (Signal report : reports) {
                report.setProfile(naru);
                signalRepository.save(report);
            }
        }

        /******************** Configure Sent Message to stock it *************************/
        List<Message> sentmessages = messageRepository.findAllBySenderId(id);
        if (sentmessages != null) {
            for (Message msg : sentmessages) {
                msg.setReceiver(naru);
                msg.setSender(naru);
                messageRepository.save(msg);
            }

        }
        /******************** Configure Received Message to stock it  *********************/
        List<Message> receivedmessages = messageRepository.findAllByReceiverId(id);
        if (receivedmessages != null) {
            for (Message rmsg : receivedmessages) {
                rmsg.setReceiver(naru);
                rmsg.setSender(naru);
                messageRepository.save(rmsg);
            }

        }

        /******************** Delete followers ************************************/
        List<Follow> followers = followRepository.findAllByFollowed_Id(id);
        if (followers != null) {
            for (Follow follower : followers) {
                followRepository.delete(follower.getId());
            }
        }
        /******************** Delete followeds ************************************/
        List<Follow> followeds = followRepository.findAllByFollower_Id(id);
        if (followeds != null) {
            for (Follow followed : followeds) {
                followRepository.delete(followed.getId());
            }
        }
        /******************** Set Naru Profile for Articles ***********************/
        List<Article> articles = articleRepository.findAllByProfileId(id);
        if (articles != null) {
            for (Article article : articles) {

                article.setProfile(naru);
                Forum forum = forumRepository.findOne(article.getForum().getId());
                forum.setProfile(naru);
                forumRepository.save(forum);
                articleRepository.save(article);

            }
        }
        /******************** Delete Forum Comments *************************/
        List<Subject> subjects = subjectRepository.findAllByProfileId(id);

        if (subjects != null) {
            for (Subject subj : subjects) {
                subjectRepository.delete(subj.getId());
            }

        }
        /******************** Configure Rating *************************/
        List<Rating> ratings = ratingRepository.findAllByRaterId(id);

        if (ratings != null) {
            for (Rating rate : ratings) {
                rate.setRater(naru);
                ratingRepository.save(rate);
            }

        }

        /******************** Delete Wanteds *************************************/

        List<Wanted> wanteds = wantedRepository.findAllWithArticleByWanter_Id(id);
        if (wanteds != null) {
            for (Wanted wanted : wanteds) {
                wantedRepository.delete(wanted);
            }
        }
        /******************** Delete Wanteds History  ***************************/
        List<WantedHistory> wantedsHistories = wantedHistoryRepository.findAllByWanter_Id(id);
        if (wantedsHistories != null) {
            for (WantedHistory wantedHistory : wantedsHistories) {
                wantedHistoryRepository.delete(wantedHistory.getId());
            }
        }

        /**************************** Delete Owneds  ***************************/
        List<Owned> owneds = ownedRepository.findAllWithArticleByOwner_Id(id);
        if (owneds != null) {
            for (Owned owned : owneds) {
                ownedRepository.delete(owned);
            }
        }

        /******************** Delete Owneds History ****************************/
        List<OwnedHistory> ownedHistories = ownedHistoryRepository.findAllByOwner_id(id);
        if (ownedHistories != null) {
            for (OwnedHistory ownedHistory : ownedHistories) {
                ownedHistoryRepository.delete(ownedHistory.getId());
            }
        }
        List<Matched> matcheds = matchedRepository.findAllByOwner_Id(id);
        if (matcheds != null) {
            for (Matched matched : matcheds) {
                matchedRepository.delete(matched);
            }
        }
        List<Matched> matchedsW = matchedRepository.findAllByWanter_Id(id);
        if (matchedsW != null) {
            for (Matched matchedw : matchedsW) {
                matchedRepository.delete(matchedw);
            }
        }
        List<MatchingHistory> matchingHistories = matchingHistoryRepository.findAllByOwner_Id(id);
        if (matchingHistories != null) {
            for (MatchingHistory matchingHistory : matchingHistories) {
                matchingHistoryRepository.delete(matchingHistory);
            }
        }
        List<MatchingHistory> matchingHistoriesW = matchingHistoryRepository.findAllByWanter_Id(id);
        if (matchingHistoriesW != null) {
            for (MatchingHistory matchingHistoryW : matchingHistoriesW) {
                matchingHistoryRepository.delete(matchingHistoryW);
            }
        }

    }
}
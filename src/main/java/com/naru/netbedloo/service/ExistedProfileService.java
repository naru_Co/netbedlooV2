package com.naru.netbedloo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.naru.netbedloo.domain.ProfileImage;
import com.naru.netbedloo.repository.UserRepository;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@Service
public class ExistedProfileService {

    @Autowired
    private final UserRepository userRepository;

    public ExistedProfileService(UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    
    
    public ResponseEntity CheckIfExist(ProfileImage profileIm) {
        ResponseEntity exist = null;
        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
        String login = profileIm.getLogin();
        String email = profileIm.getProfile().getEmail();
        return userRepository.findOneByLoginIgnoreCase(login).map(
                user ->  new ResponseEntity<>("login already in use", textPlainHeaders, HttpStatus.BAD_REQUEST))
                .orElseGet(() -> userRepository.findOneByEmail(email)
                        .map(user -> new ResponseEntity<>("email address already in use", textPlainHeaders,HttpStatus.BAD_REQUEST))
                        .orElseGet(() -> {
                            return exist;
                        }));

    }

}
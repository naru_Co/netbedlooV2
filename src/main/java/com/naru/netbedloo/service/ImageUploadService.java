package com.naru.netbedloo.service;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.naru.netbedloo.domain.ProfileImage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;








/**
 * Service class for managing users.
 */
@Service
@Transactional
public class ImageUploadService {
 @Autowired
    private HttpServletRequest request;
 public String uploadImage(ProfileImage prImage) {


                    byte[] image = prImage.getFile();


                    String uploadsDir = "/app/profil_photos/";
                    String realPathtoUpload =  request.getServletContext().getRealPath(uploadsDir);

                    if(! new File(realPathtoUpload).exists()){
                        new File(realPathtoUpload).mkdir();
                    }
                    //debut de la boucle

                    String orgName = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())+prImage.getName();
                    String filePath = realPathtoUpload + "/" + orgName;
                    File dest = new File(filePath);

                    try{
                    new FileOutputStream(dest).write(image);
                    }
                    catch (Exception e){}
                    String imagePath = "/app/profil_photos/" + orgName;

                    return imagePath;
           }
}

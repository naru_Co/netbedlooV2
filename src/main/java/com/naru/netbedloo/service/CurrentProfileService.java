package com.naru.netbedloo.service;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.User;
import com.naru.netbedloo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
@Service
public class CurrentProfileService {

    @Autowired
    private final UserRepository userRepository;
   

    public CurrentProfileService(UserRepository userRepository){
        this.userRepository=userRepository;
        

    }
    public Profile getCurrentProfile() {
        Object principal= SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username= ((UserDetails)principal).getUsername();
        User user=userRepository.findOneWithProfileByLogin(username);
        Profile profile=user.getProfile();
        return profile;
    }

}
package com.naru.netbedloo.service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Category;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Gouv;
import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.domain.MatchingHistory;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.GouvRepository;
import com.naru.netbedloo.repository.MatchedRepository;
import com.naru.netbedloo.repository.MatchingHistoryRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.WantedRepository;
import com.naru.netbedloo.web.websocket.MatchingNotificationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class MatchingService {

    private final Logger log = LoggerFactory.getLogger(MatchingService.class);

    private final ProfileRepository profileRepository;

    private final WantedRepository wantedRepository;

    private final OwnedRepository ownedRepository;

    private final ArticleRepository articleRepository;

    private final GouvRepository gouvRepository;

    private final MatchedRepository matchedRepository;

    private final FollowRepository followRepository;

    private final MatchingHistoryRepository matchingHistoryRepository;

    private final OwnedHistoryRepository ownedHistoryRepository;

    private final WantedHistoryRepository wantedHistoryRepository;

    private final SimpMessageSendingOperations messagingTemplate;

    private final MatchingNotificationService matchingNotificationService;

    private final FeedRepository feedRepository;

    public MatchingService(SimpMessageSendingOperations messagingTemplate, ProfileRepository profileRepository,
            WantedRepository wantedRepository, OwnedRepository ownedRepository, ArticleRepository articleRepository,
            GouvRepository gouvRepository, MatchedRepository matchedRepository,
            MatchingHistoryRepository matchingHistoryRepository, OwnedHistoryRepository ownedHistoryRepository,
            WantedHistoryRepository wantedHistoryRepository, MatchingNotificationService matchingNotificationService,
            FollowRepository followRepository, FeedRepository feedRepository) {
        this.messagingTemplate = messagingTemplate;
        this.profileRepository = profileRepository;
        this.wantedRepository = wantedRepository;
        this.ownedRepository = ownedRepository;
        this.articleRepository = articleRepository;
        this.gouvRepository = gouvRepository;
        this.matchedRepository = matchedRepository;
        this.matchingHistoryRepository = matchingHistoryRepository;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.matchingNotificationService = matchingNotificationService;
        this.followRepository = followRepository;
        this.feedRepository = feedRepository;

    }

    public static double distance(double lat1, double lat2, double lon1, double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        double coef = 0.001;

        double distanceKM = (distance) * coef;

        return distanceKM;
    }

    public void TbedlooOwner(Owned owned, Owned wanted, Matched matchedfortbedloo,
            MatchingHistory matchingHistorytbedloo) {

        Article article = owned.getArticle();
        Profile owner = owned.getOwner();
        Article articlew = wanted.getArticle();
        Profile wantu = wanted.getOwner();

        Set<Matched> allMatched = new HashSet<>();
        Set<MatchingHistory> allHistory = new HashSet<>();
        OwnedHistory ownedHistory = ownedHistoryRepository.findOneByOwner_idAndArticle_idAndEraseDateNull(owner.getId(),
                article.getId());
        OwnedHistory wantyHistory = ownedHistoryRepository.findOneByOwner_idAndArticle_idAndEraseDateNull(wantu.getId(),
                articlew.getId());
        WantedHistory wantyy = wantedHistoryRepository.findOneByWanter_idAndArticle_idAndEraseDateNull(owner.getId(),
                articlew.getId());
        WantedHistory wantuu = wantedHistoryRepository.findOneByWanter_idAndArticle_idAndEraseDateNull(wantu.getId(),
                article.getId());
        if (ownedHistory != null) {
            List<Follow> followed = followRepository.findAllByFollowed_Id(owner.getId());
            ownedHistory.setEraseDate(Instant.now());
            ownedHistoryRepository.save(ownedHistory);
            Feed ownFeed = feedRepository.findOneByOwnedId(ownedHistory.getId());
            if (ownFeed != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(ownFeed.getId());
                
                ownFeed.setTime((Instant.now()));
                if (ownedHistory.getArticle().getCategory().getId() == 1101) {
                    ownFeed.setType(11);
                } else {
                    ownFeed.setType(99);
                }

                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            if (!visibleTo.contains(profile)) {
                                ownFeed.addVisibleTo(profile);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                        ownFeed.setVisibleTos(visibility);
                    }
                }
                feedRepository.save(ownFeed);
            }
        }
        if (wantyHistory != null) {
            List<Follow> followed = followRepository.findAllByFollowed_Id(wantu.getId());
            wantyHistory.setEraseDate(Instant.now());
            ownedHistoryRepository.save(wantyHistory);
            Feed ownFeed1 = feedRepository.findOneByOwnedId(wantyHistory.getId());
            if (ownFeed1 != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(ownFeed1.getId());
              
                ownFeed1.setTime((Instant.now()));
                if (wantyHistory.getArticle().getCategory().getId() == 1101) {
                    ownFeed1.setType(11);
                } else {
                    ownFeed1.setType(99);
                }
                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            if (!visibleTo.contains(profile)) {
                                ownFeed1.addVisibleTo(profile);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    ownFeed1.setVisibleTos(visibility);
                }
                feedRepository.save(ownFeed1);
            }
        }
        if (wantyy != null) {
            List<Follow> followed = followRepository.findAllByFollowed_Id(owner.getId());
            wantyy.setEraseDate(Instant.now());
            wantedHistoryRepository.save(wantyy);
            Feed wantFeed = feedRepository.findOneByWantedId(wantyy.getId());
            if (wantFeed != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(wantFeed.getId());
                
                wantFeed.setTime((Instant.now()));
                if (wantyy.getArticle().getCategory().getId() == 1101) {
                    wantFeed.setType(22);
                } else {
                    wantFeed.setType(88);
                }
                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            if (!visibleTo.contains(profile)) {
                                wantFeed.addVisibleTo(profile);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    wantFeed.setVisibleTos(visibility);
                }
                feedRepository.save(wantFeed);
            }
        }
        if (wantuu != null) {
            wantuu.setEraseDate(Instant.now());
            List<Follow> followed = followRepository.findAllByFollowed_Id(wantu.getId());
            wantedHistoryRepository.save(wantuu);
            Feed wantFeed2 = feedRepository.findOneByWantedId(wantuu.getId());
            if (wantFeed2 != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(wantFeed2.getId());
                wantFeed2.setTime((Instant.now()));
                if (wantuu.getArticle().getCategory().getId() == 1101) {
                    wantFeed2.setType(22);
                } else {
                    wantFeed2.setType(88);
                }
                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            if (!visibleTo.contains(profile)) {
                                wantFeed2.addVisibleTo(profile);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    wantFeed2.setVisibleTos(visibility);
                }
                feedRepository.save(wantFeed2);
            }
        }

        List<MatchingHistory> matchingsforOwner = matchingHistoryRepository
                .findAllByOwner_IdAndOwned_IdAndEraseDateNull(owner.getId(), article.getId());
        allHistory.addAll(matchingsforOwner);
        List<MatchingHistory> matchingsforWanter = matchingHistoryRepository
                .findAllByWanter_IdAndWanted_IdAndEraseDateNull(owner.getId(), article.getId());
        allHistory.addAll(matchingsforWanter);
        List<Matched> matcheds = matchedRepository.findAllByOwner_IdAndOwned_Id(owner.getId(), article.getId());
        allMatched.addAll(matcheds);
        List<Matched> matchedsw = matchedRepository.findAllByWanter_IdAndWanted_Id(owner.getId(), article.getId());
        allMatched.addAll(matchedsw);

        allHistory.remove(matchingHistorytbedloo);
        allMatched.remove(matchedfortbedloo);
        if (allHistory.size() != 0) {
            for (MatchingHistory history : allHistory) {
                String statut = owner.getPseudo() + " Tbedl already with  " + wantu.getPseudo();
                history.setStatut(statut);
                history.setEraseDate(Instant.now());
                matchingHistoryRepository.save(history);
                Feed matchedErase = feedRepository.findOneByMatchesId(history.getId());
                if (matchedErase != null) {
                    feedRepository.delete(matchedErase);
                }
            }
        }
        if (allMatched.size() != 0) {
            for (Matched matched : allMatched) {
                matchedRepository.delete(matched.getId());
            }
        }

    }

    public void deletematchingforOwned(Owned owned) {
        Article article = owned.getArticle();
        Profile owner = owned.getOwner();

        OwnedHistory ownedHistory = ownedHistoryRepository.findOneByOwner_idAndArticle_idAndEraseDateNull(owner.getId(),
                article.getId());

        if (ownedHistory != null) {
            ownedHistory.setEraseDate(Instant.now());
            ownedHistoryRepository.save(ownedHistory);
            List<Follow> followed = followRepository.findAllByFollowed_Id(owner.getId());
            Feed ownFeed1 = feedRepository.findOneByOwnedId(ownedHistory.getId());
            if (ownFeed1 != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(ownFeed1.getId());
            
                ownFeed1.setTime((Instant.now()));
                if (ownedHistory.getArticle().getCategory().getId() == 1101) {
                    ownFeed1.setType(11);
                } else {
                    ownFeed1.setType(99);
                }
                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            if (!visibleTo.contains(profile)) {
                                ownFeed1.addVisibleTo(profile);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    ownFeed1.setVisibleTos(visibility);
                }
                feedRepository.save(ownFeed1);
            }
        }

        List<MatchingHistory> matchingsforOwner = matchingHistoryRepository
                .findAllByOwner_IdAndOwned_IdAndEraseDateNull(owner.getId(), article.getId());

        List<Matched> matcheds = matchedRepository.findAllByOwner_IdAndOwned_Id(owner.getId(), article.getId());
        List<MatchingHistory> matchingsforWanter = matchingHistoryRepository
                .findAllByWanter_IdAndWanted_IdAndEraseDateNull(owner.getId(), article.getId());

        List<Matched> matchedsw = matchedRepository.findAllByWanter_IdAndWanted_Id(owner.getId(), article.getId());
        boolean testhistory = matchingsforOwner.isEmpty();
        boolean testhistory2 = matchingsforWanter.isEmpty();
        boolean testmatch = matcheds.isEmpty();
        boolean testmatch2 = matchedsw.isEmpty();
        if (!testhistory && matchingsforOwner != null) {
            for (MatchingHistory history : matchingsforWanter) {
                String statut = "the owner " + owner.getFirstName() + " does not have " + article.getTitle()
                        + " anymore";
                history.setStatut(statut);
                history.setEraseDate(Instant.now());
                matchingHistoryRepository.save(history);
                Feed matched = feedRepository.findOneByMatchesId(history.getId());
                if (matched != null) {
                    feedRepository.delete(matched);
                }
            }
        }
        if (!testmatch && matcheds != null) {
            for (Matched matched : matcheds) {
                matchedRepository.delete(matched.getId());
            }
        }
        if (!testhistory2 && matchingsforWanter != null) {
            for (MatchingHistory history : matchingsforOwner) {
                String statut = "the owner " + owner.getFirstName() + " does not have " + article.getTitle()
                        + " anymore";
                history.setStatut(statut);
                history.setEraseDate(Instant.now());
                matchingHistoryRepository.save(history);
            }
        }
        if (!testmatch2 && matchedsw != null) {
            for (Matched matched : matchedsw) {
                matchedRepository.delete(matched.getId());
            }
        }
    }

    public void deletematchingforWanted(Wanted wanted) {
        Article article = wanted.getArticle();
        Profile profile = wanted.getWanter();
        WantedHistory wantedHistory = wantedHistoryRepository
                .findOneByWanter_idAndArticle_idAndEraseDateNull(profile.getId(), article.getId());
        if (wantedHistory != null) {
            wantedHistory.setEraseDate(Instant.now());
            wantedHistoryRepository.save(wantedHistory);
            List<Follow> followed = followRepository.findAllByFollowed_Id(profile.getId());
            Feed ownFeed1 = feedRepository.findOneByWantedId(wantedHistory.getId());
            if (ownFeed1 != null) {
                Set<Profile> visibility = new HashSet<>();
                Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(ownFeed1.getId());
              
                ownFeed1.setTime((Instant.now()));
                if (wantedHistory.getArticle().getCategory().getId() == 1101) {
                    ownFeed1.setType(22);
                } else {
                    ownFeed1.setType(88);
                }
                if (visibleTo != null) {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile pr = follw.getFollower();
                            if (!visibleTo.contains(pr)) {
                                ownFeed1.addVisibleTo(pr);
                            }
                            ;
                        }
                    }
                } else {
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile pr = follw.getFollower();
                            visibility.add(pr);
                        }
                    }
                    ownFeed1.setVisibleTos(visibility);
                }
                feedRepository.save(ownFeed1);
            }
        }
        List<MatchingHistory> historyow = matchingHistoryRepository
                .findAllByOwner_IdAndWanted_IdAndEraseDateNull(profile.getId(), article.getId());
        List<MatchingHistory> historywo = matchingHistoryRepository
                .findAllByWanter_IdAndOwned_IdAndEraseDateNull(profile.getId(), article.getId());
        List<Matched> matchedsOW = matchedRepository.findAllByOwner_IdAndWanted_Id(profile.getId(), article.getId());
        List<Matched> matchedsWO = matchedRepository.findAllByWanter_idAndOwned_Id(profile.getId(), article.getId());
        boolean testhistory = historyow.isEmpty();
        boolean testhistory2 = historywo.isEmpty();
        boolean testmatch = matchedsOW.isEmpty();
        boolean testmatch2 = matchedsWO.isEmpty();
        if (historyow != null && !testhistory) {
            for (MatchingHistory history : historyow) {
                String statut = "the wanter " + profile.getFirstName() + " is not interested in " + article.getTitle()
                        + " anymore";
                history.setStatut(statut);
                history.setEraseDate(Instant.now());
                matchingHistoryRepository.save(history);
                Feed matched = feedRepository.findOneByMatchesId(history.getId());
                if (matched != null) {
                    feedRepository.delete(matched);
                }
            }
        }
        if (historywo != null && !testhistory2) {
            for (MatchingHistory history : historywo) {
                String statut = "the wanter " + profile.getFirstName() + " is not interested in " + article.getTitle()
                        + " anymore";
                history.setStatut(statut);
                history.setEraseDate(Instant.now());
                matchingHistoryRepository.save(history);
            }
        }
        if (matchedsOW != null && !testmatch) {
            for (Matched matched : matchedsOW) {
                matchedRepository.delete(matched.getId());
            }
        }
        if (matchedsWO != null && !testmatch2) {
            for (Matched matched : matchedsWO) {
                matchedRepository.delete(matched.getId());
            }
        }
    }

    public void matchingforOwned(Owned owned) {

        Profile profile = owned.getOwner();

        Article ownedArticle = owned.getArticle();

        Set<Wanted> iwantthis = new HashSet<>();
        Set<Wanted> wantedsByowner = new HashSet<>();
        List<Wanted> wanteds = wantedRepository.findAllWithEagerRelationships();
        for (Wanted wanted : wanteds) {
            Article article = wanted.getArticle();
            Profile wanteg = wanted.getWanter();
            if (ownedArticle.equals(article)) {
                iwantthis.add(wanted);
            }
            if (profile.equals(wanteg)) {
                wantedsByowner.add(wanted);
            }
        }

        for (Wanted iwantit : iwantthis) {
            Profile wanter = iwantit.getWanter();

            boolean himself = profile.getId().equals(wanter.getId());
            if (!himself) {
                List<Owned> owneds = ownedRepository.findAllWithEagerRelationships();
                Set<Owned> ownedsBywanter = new HashSet<>();
                for (Owned ownedb : owneds) {
                    Profile moulaSbewlou = ownedb.getOwner();
                    if (moulaSbewlou.equals(wanter)) {
                        ownedsBywanter.add(ownedb);
                    }
                }

                for (Owned ownedBywanter : ownedsBywanter) {
                    for (Wanted wantedByowner : wantedsByowner) {
                        //matched Object and MatchedHistory should be reInisialized on each iteration
                        MatchingHistory matchedHistory = new MatchingHistory();
                        Matched matched = new Matched();
                        long ref = Math.round(Math.random() * 100000);
                        matched.setReference(ref);
                        matched.setOwner(profile);
                        matchedHistory.setOwner(profile);
                        matchedHistory.setOwned(ownedArticle);
                        matchedHistory.setWanter(wanter);
                        matched.setOwned(ownedArticle);
                        matched.setWanter(wanter);

                        //should be here and not above, otherwise, the object would be updated and not created again
                        Article articleWanted = wantedByowner.getArticle();

                        Article articleOwned = ownedBywanter.getArticle();
                        Feed feed = new Feed();
                        Feed feedw= new Feed();
                        Gouv gouvowner = profile.getGouv();
                        Gouv gouvwanter = wanter.getGouv();
                        double latowner = gouvowner.getLatitude();
                        double latwanter = gouvwanter.getLatitude();
                        double longtowner = gouvowner.getLongitude();
                        double longtwanter = gouvwanter.getLongitude();
                        double distanceKm = distance(latowner, latwanter, longtowner, longtwanter);

                        if (articleWanted.getId().equals(articleOwned.getId())) {
                            Category categoryOw = ownedArticle.getCategory();
                            Category categoryWa = articleWanted.getCategory();
                            boolean sameCategory = categoryOw.equals(categoryWa);
                            List<Follow> followed = followRepository.findAllByFollowed_Id(profile.getId());
                            Set<Profile> visibility = new HashSet<>();
                            List<Follow> followedw = followRepository.findAllByFollowed_Id(wanter.getId());
                            Set<Profile> visibilityw = new HashSet<>();
                            if (sameCategory) {

                                matched.setWanted(articleWanted);
                                matchedHistory.setWanted(articleWanted);
                                matchedHistory.setDistance(distanceKm);
                                matched.setStatut(false);
                                String statut = "on hold";
                                matchedHistory.setStatut(statut);
                                matched.setDistance(distanceKm);
                                LocalDate today = LocalDate.now();

                                matchedHistory.setMatchingDate(Instant.now());
                                matched.setMatchingDate(Instant.now());
                                matchingHistoryRepository.save(matchedHistory);
                                //send notifications to both matchers
                                messagingTemplate.convertAndSend(
                                        "/matchingnotification/notif/" + matched.getOwner().getId(), matched);
                                messagingTemplate.convertAndSend(
                                        "/matchingnotification/notif/" + matched.getWanter().getId(), matched);
                                matchingNotificationService.sendPushNotif(matched.getOwner().getId(), matched);
                                matchingNotificationService.sendPushNotif(matched.getWanter().getId(), matched);
                                FollowEachOtherIfMatch(matched.getOwner(), matched.getWanter());
                                matchedRepository.save(matched);
                                feed.setOwner(profile);
                                feed.setMatches(matchedHistory);
                                feed.setStatut(true);
                                feed.setTime(Instant.now());
                                if (followed != null) {
                                    for (Follow follw : followed) {
                                        Profile followhim = follw.getFollower();
                                        visibility.add(followhim);
                                    }
                                }
                                feed.setVisibleTos(visibility);
                                if (categoryOw.getId() == 1101) {
                                    feed.setType(13);
                                } else {
                                    feed.setType(14);
                                }


                                feedw.setOwner(wanter);
                                feedw.setMatches(matchedHistory);
                                feedw.setStatut(true);
                                feedw.setTime(Instant.now());
                                if (followedw != null) {
                                    for (Follow follw : followedw) {
                                        Profile followhim = follw.getFollower();
                                        visibilityw.add(followhim);
                                    }
                                }
                                feedw.setVisibleTos(visibilityw);
                                if (categoryOw.getId() == 1101) {
                                    feedw.setType(13);
                                } else {
                                    feedw.setType(14);
                                }
                                feedRepository.save(feed);
                            }

                        }

                    }
                }

            }

        }

    }

    public void matchingforWanted(Wanted wanted) {

        Profile profile = wanted.getWanter();

        Article wantedArticle = wanted.getArticle();

        List<Owned> havethis = ownedRepository.findAllWithOwnerByArticle_Id(wantedArticle.getId());
        List<Owned> ownedsByWanter = ownedRepository.findAllWithArticleByOwner_Id(profile.getId());

        for (Owned haveit : havethis) {

            Profile owner = haveit.getOwner();

            List<Wanted> wantedsByowner = wantedRepository.findAllWithArticleByWanter_Id(owner.getId());

            boolean himself = profile.getId().equals(owner.getId());
            if (!himself) {
                Gouv gouvowner = owner.getGouv();
                Gouv gouvwanter = profile.getGouv();
                double latowner = gouvowner.getLatitude();
                double latwanter = gouvwanter.getLatitude();
                double longtowner = gouvowner.getLongitude();
                double longtwanter = gouvwanter.getLongitude();
                double distanceKm = distance(latowner, latwanter, longtowner, longtwanter);

                for (Wanted wantedByOwner : wantedsByowner) {
                    for (Owned ownedBywanter : ownedsByWanter) {

                        //matched Object and MatchedHistory should be reInisialized on each iteration
                        MatchingHistory matchedHistory = new MatchingHistory();
                        Matched matched = new Matched();
                        long ref = Math.round(Math.random() * 100000);
                        matched.setReference(ref);
                        matched.setWanter(profile);
                        matchedHistory.setWanter(profile);
                        matched.setOwned(wantedArticle);
                        matchedHistory.setOwned(wantedArticle);
                        Feed feed = new Feed();
                        Feed feedw = new Feed();
                        //should be here and not above, otherwise, the object would be updated and not created again
                        Article articleWanted = wantedByOwner.getArticle();
                        Article articleOwned = ownedBywanter.getArticle();
                        boolean matching = articleWanted.getId().equals(articleOwned.getId());
                        Category categoryOw = articleOwned.getCategory();
                        Category categoryWa = wantedArticle.getCategory();
                        boolean sameCategory = categoryOw.equals(categoryWa);
                        List<Follow> followed = followRepository.findAllByFollowed_Id(profile.getId());
                        List<Follow> followedw = followRepository.findAllByFollowed_Id(owner.getId());
                        Set<Profile> visibility = new HashSet<>();
                        Set<Profile> visibilityw = new HashSet<>();

                        if (matching) {
                            if (sameCategory) {

                                matched.setOwner(owner);
                                matchedHistory.setOwner(owner);
                                matchedHistory.setWanted(articleOwned);

                                matched.setWanted(articleOwned);
                                matchedHistory.setDistance(distanceKm);
                                matched.setDistance(distanceKm);
                                matched.setStatut(false);
                                String statut = "on hold";
                                matchedHistory.setStatut(statut);
                                LocalDate today = LocalDate.now();

                                matchedHistory.setMatchingDate(Instant.now());
                                matched.setMatchingDate(Instant.now());
                                if (matched.getOwned() != null) {
                                    matchingHistoryRepository.save(matchedHistory);
                                    //send notifications to both matchers
                                    messagingTemplate.convertAndSend(
                                            "/matchingnotification/notif/" + matched.getOwner().getId(), matched);
                                    messagingTemplate.convertAndSend(
                                            "/matchingnotification/notif/" + matched.getWanter().getId(), matched);
                                    matchingNotificationService.sendPushNotif(matched.getOwner().getId(), matched);
                                    matchingNotificationService.sendPushNotif(matched.getWanter().getId(), matched);
                                    FollowEachOtherIfMatch(matched.getOwner(), matched.getWanter());

                                    matchedRepository.save(matched);
                                    feed.setOwner(profile);
                                    feed.setMatches(matchedHistory);
                                    feed.setStatut(true);
                                    feed.setTime(Instant.now());
                                    if (followed != null) {
                                        for (Follow follw : followed) {
                                            Profile followhim = follw.getFollower();
                                            visibility.add(followhim);
                                        }
                                    }
                                    feed.setVisibleTos(visibility);
                                    if (categoryOw.getId() == 1101) {
                                        feed.setType(13);
                                    } else {
                                        feed.setType(14);
                                    }
                                    feedw.setOwner(owner);
                                    feedw.setMatches(matchedHistory);
                                    feedw.setStatut(true);
                                    feedw.setTime(Instant.now());
                                    if (followedw != null) {
                                        for (Follow follw : followedw) {
                                            Profile followhim = follw.getFollower();
                                            visibilityw.add(followhim);
                                        }
                                    }
                                    feedw.setVisibleTos(visibilityw);
                                    if (categoryOw.getId() == 1101) {
                                        feedw.setType(13);
                                    } else {
                                        feedw.setType(14);
                                    }
                                    feedRepository.save(feed);
                                    feedRepository.save(feedw);
                                }
                            }

                        }
                    }
                }

            }

        }

    }

    public void FollowEachOtherIfMatch(Profile profile1, Profile other) {
        //check if a following exist already

        Follow following = followRepository.findOneByFollower_IdAndFollowed_Id(profile1.getId(), other.getId());
        log.debug("check if follow the other", following);
        if (following == null) {
            Follow follow = new Follow();
            Feed feed = new Feed();
            List<Follow> followed = followRepository.findAllByFollowed_Id(profile1.getId());
            Set<Profile> visibility = new HashSet<>();
            follow.setFollower(profile1);
            follow.setFollowed(other);
            follow.setFollowTime(Instant.now());
            Follow result = followRepository.save(follow);
            new Thread() {
                @Override
                public void run() {
    
                    List<Feed> feeds = feedRepository.findAllByOwner_Id(result.getFollowed().getId());
                    if (feed != null) {
                        for (Feed feedy : feeds) {
                            Set<Profile> visibleTo = feedy.getVisibleTos();
                            visibleTo.add(result.getFollower());
                            feedRepository.save(feedy);
                        }
                    }
                }
            }.start();
            feed.setOwner(profile1);
            feed.setFollow(follow);
            feed.setStatut(true);
            feed.setTime(Instant.now());
            if (followed != null) {
                for (Follow follw : followed) {
                    Profile followhim = follw.getFollower();
                    visibility.add(followhim);
                }
            }
            feed.setVisibleTos(visibility);
            feed.setType(10);

            feedRepository.save(feed);
        }
        Follow followers = followRepository.findOneByFollower_IdAndFollowed_Id(other.getId(), profile1.getId());
        log.debug("check if follow the other", followers);
        if (followers == null) {
            List<Follow> followed = followRepository.findAllByFollowed_Id(profile1.getId());
            Set<Profile> visibility = new HashSet<>();
            Follow follow = new Follow();
            Feed feed = new Feed();
            follow.setFollower(other);
            follow.setFollowed(profile1);
            follow.setFollowTime(Instant.now());
            Follow result = followRepository.save(follow);
            new Thread() {
                @Override
                public void run() {
    
                    List<Feed> feeds = feedRepository.findAllByOwner_Id(result.getFollowed().getId());
                    if (feed != null) {
                        for (Feed feedy : feeds) {
                            Set<Profile> visibleTo = feedy.getVisibleTos();
                            visibleTo.add(result.getFollower());
                            feedRepository.save(feedy);
                        }
                    }
                }
            }.start();
            feed.setOwner(profile1);
            feed.setFollow(follow);
            feed.setStatut(true);
            feed.setTime(Instant.now());

            feed.setType(10);
            if (followed != null) {
                for (Follow follw : followed) {
                    Profile followhim = follw.getFollower();
                    visibility.add(followhim);
                }
            }

            feed.setVisibleTos(visibility);
            feedRepository.save(feed);
        }

    }

}
package com.naru.netbedloo.service;

import java.util.List;

import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Forum;
import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.domain.MatchingHistory;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.domain.Rating;
import com.naru.netbedloo.domain.Subject;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.ForumRepository;
import com.naru.netbedloo.repository.MatchedRepository;
import com.naru.netbedloo.repository.MatchingHistoryRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.RatingRepository;
import com.naru.netbedloo.repository.SubjectRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.WantedRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleDeleteService {

    private final Logger log = LoggerFactory.getLogger(ArticleDeleteService.class);
    @Autowired
    private final WantedRepository wantedRepository;
    @Autowired
    private final OwnedRepository ownedRepository;
    @Autowired
    private final ArticleRepository articleRepository;
    @Autowired
    private final MatchedRepository matchedRepository;
    @Autowired
    private final MatchingHistoryRepository matchingHistoryRepository;
    @Autowired
    private final OwnedHistoryRepository ownedHistoryRepository;
    @Autowired
    private final WantedHistoryRepository wantedHistoryRepository;
    @Autowired
    private final RatingRepository ratingRepository;
    @Autowired
    private final ForumRepository forumRepository;
    @Autowired
    private final SubjectRepository subjectRepository;

    public ArticleDeleteService(WantedRepository wantedRepository, OwnedRepository ownedRepository,
            ArticleRepository articleRepository, MatchedRepository matchedRepository,
            MatchingHistoryRepository matchingHistoryRepository, OwnedHistoryRepository ownedHistoryRepository,
            WantedHistoryRepository wantedHistoryRepository, RatingRepository ratingRepository,
            ForumRepository forumRepository, SubjectRepository subjectRepository) {
        this.articleRepository = articleRepository;
        this.wantedRepository = wantedRepository;
        this.ownedRepository = ownedRepository;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.matchingHistoryRepository = matchingHistoryRepository;
        this.matchedRepository = matchedRepository;
        this.ratingRepository = ratingRepository;
        this.forumRepository = forumRepository;
        this.subjectRepository = subjectRepository;
    }

    public void articledelete(long id) {
        Article articleToDelete = articleRepository.findOne(id);
        Forum forum = forumRepository.findOne(articleToDelete.getForum().getId());
        List<Subject> subjects = subjectRepository.findAllByForum_id(forum.getId());
        
        if (subjects != null) {
            for (Subject subject : subjects) {
                subjectRepository.delete(subject.getId());

            }

        }
        articleToDelete.setForum(null);
        articleToDelete.setProfile(null);
        articleRepository.save(articleToDelete);

        forumRepository.delete(forum);
        List<Rating> ratings = ratingRepository.findAllByRatedId(id);
        if (ratings != null) {
            for (Rating rating : ratings) {
                ratingRepository.delete(rating.getId());

            }

        }
        List<Wanted> wanteds = wantedRepository.findAllWithWanterByArticle_Id(id);
        if (wanteds != null) {
            for (Wanted wanted : wanteds) {
                wantedRepository.delete(wanted);
            }
        }
        List<WantedHistory> wantedsHistories = wantedHistoryRepository.findAllByArticle_id(id);
        if (wantedsHistories != null) {
            for (WantedHistory wantedHistory : wantedsHistories) {
                wantedHistoryRepository.delete(wantedHistory.getId());
            }
        }
        List<Owned> owneds = ownedRepository.findAllWithOwnerByArticle_Id(id);
        if (owneds != null) {
            for (Owned owned : owneds) {
                ownedRepository.delete(owned);
            }
        }
        List<OwnedHistory> ownedHistories = ownedHistoryRepository.findAllByArticle_id(id);
        if (ownedHistories != null) {
            for (OwnedHistory ownedHistory : ownedHistories) {
                ownedHistoryRepository.delete(ownedHistory.getId());
            }
        }
        List<Matched> matcheds = matchedRepository.findAllByOwned_Id(id);
        if (matcheds != null) {
            for (Matched matched : matcheds) {
                matchedRepository.delete(matched);
            }
        }
        List<Matched> matchedsW = matchedRepository.findAllByWanted_Id(id);
        if (matchedsW != null) {
            for (Matched matchedw : matchedsW) {
                matchedRepository.delete(matchedw);
            }
        }
        List<MatchingHistory> matchingHistories = matchingHistoryRepository.findAllByOwned_Id(id);
        if (matchingHistories != null) {
            for (MatchingHistory matchingHistory : matchingHistories) {
                matchingHistoryRepository.delete(matchingHistory);
            }
        }
        List<MatchingHistory> matchingHistoriesW = matchingHistoryRepository.findAllByWanted_Id(id);
        if (matchingHistoriesW != null) {
            for (MatchingHistory matchingHistoryW : matchingHistoriesW) {
                matchingHistoryRepository.delete(matchingHistoryW);
            }
        }

    }
}
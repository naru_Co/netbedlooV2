package com.naru.netbedloo.service;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.domain.MatchingHistory;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Tbedloo;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.GouvRepository;
import com.naru.netbedloo.repository.MatchedRepository;
import com.naru.netbedloo.repository.MatchingHistoryRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.TbedlooRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.WantedRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TbedlooService {

    private final Logger log = LoggerFactory.getLogger(TbedlooService.class);

    private final ProfileRepository profileRepository;

    private final WantedRepository wantedRepository;

    private final OwnedRepository ownedRepository;

    private final ArticleRepository articleRepository;

    private final GouvRepository gouvRepository;

    private final MatchedRepository matchedRepository;

    private final MatchingHistoryRepository matchingHistoryRepository;

    private final OwnedHistoryRepository ownedHistoryRepository;

    private final WantedHistoryRepository wantedHistoryRepository;

    private final MatchingService matchingService;

    private final TbedlooRepository tbedlooRepository;

    private final FeedRepository feedRepository;

    private final FollowRepository followRepository;

    public TbedlooService(ProfileRepository profileRepository, WantedRepository wantedRepository,
            OwnedRepository ownedRepository, ArticleRepository articleRepository, GouvRepository gouvRepository,
            MatchedRepository matchedRepository, MatchingHistoryRepository matchingHistoryRepository,
            OwnedHistoryRepository ownedHistoryRepository, WantedHistoryRepository wantedHistoryRepository,
            MatchingService matchingService, TbedlooRepository tbedlooRepository, FeedRepository feedRepository,
            FollowRepository followRepository) {

        this.profileRepository = profileRepository;
        this.wantedRepository = wantedRepository;
        this.ownedRepository = ownedRepository;
        this.articleRepository = articleRepository;
        this.gouvRepository = gouvRepository;
        this.matchedRepository = matchedRepository;
        this.matchingHistoryRepository = matchingHistoryRepository;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.matchingService = matchingService;
        this.tbedlooRepository = tbedlooRepository;
        this.feedRepository = feedRepository;
        this.followRepository = followRepository;

    }

    public void tbedlooSayee(Matched matched) {
        Tbedloo tbedloosayee = new Tbedloo();
        Set<Matched> matcheds = new HashSet<>();

        matcheds.add(matched);
        Profile owner = matched.getOwner();
        Profile wanter = matched.getWanter();
        Article owned = matched.getOwned();
        Article wanted = matched.getWanted();
        Set<Profile> visibility = new HashSet<>();
        Set<Profile> visibility2 = new HashSet<>();
        List<Follow> followed = followRepository.findAllByFollowed_Id(owner.getId());
        List<Follow> followedw = followRepository.findAllByFollowed_Id(wanter.getId());
        Feed feed = new Feed();
        Feed feed1 = new Feed();
        tbedloosayee.setMatcheds(matcheds);
        tbedloosayee.setTbedlooDate(Instant.now());
        MatchingHistory matchingHistory = matchingHistoryRepository
                .findOneByOwner_IdAndOwned_IdAndWanterIdAndWanted_IdAndEraseDateNull(owner.getId(), owned.getId(),
                        wanter.getId(), wanted.getId());
        Feed existentFeed = feedRepository.findOneByMatchesIdAndOwnerId(matchingHistory.getId(),owner.getId());
        Feed existentFeed1 = feedRepository.findOneByMatchesIdAndOwnerId(matchingHistory.getId(),wanter.getId());
        Owned ownedtodelete = ownedRepository.findOneByOwner_IdAndArticle_Id(owner.getId(), owned.getId());
        Owned wanty = ownedRepository.findOneByOwner_IdAndArticle_Id(wanter.getId(), wanted.getId());

        matchingService.TbedlooOwner(ownedtodelete, wanty, matched, matchingHistory);
        // ownedRepository.delete(ownedtodelete.getId());
        // ownedRepository.delete(wanty.getId());

        Wanted wantedtodelete = wantedRepository.findOneByWanter_IdAndArticle_Id(owner.getId(), wanted.getId());
        Wanted wanted2delete = wantedRepository.findOneByWanter_IdAndArticle_Id(wanter.getId(), owned.getId());
        //wantedRepository.delete(wantedtodelete.getId());
        //wantedRepository.delete(wanted2delete.getId());
        String statut = owner.getPseudo() + " who has " + owned.getTitle() + " tbedel with " + wanter.getPseudo()
                + " who has " + wanted.getTitle();
        matchedRepository.delete(matched.getId());
        matchingHistory.setStatut(statut);
        matchingHistory.setTbedlooDate(Instant.now());
        matchingHistory.setEraseDate(null);
        matchingHistoryRepository.save(matchingHistory);
        tbedlooRepository.save(tbedloosayee);
        if (existentFeed != null) {
           
            existentFeed.setMatches(matchingHistory);
            existentFeed.setTime(Instant.now());
            if (owned.getCategory().getId() == 1101) {
                existentFeed.setType(15);
            } else {
                existentFeed.setType(16);
            }
            Set<Profile> visibleTo = existentFeed.getVisibleTos();
            if (visibleTo != null) {
                if (followed != null) {
                    for (Follow follw : followed) {
                        Profile profile = follw.getFollower();
                        if (!visibleTo.contains(profile)) {
                            existentFeed.addVisibleTo(profile);
                        }
                        ;
                    }
                }

            }else{
                if (followed != null) {
                    for (Follow follw : followed) {
                        Profile profile = follw.getFollower();
                        visibility.add(profile);
                    }
                
                }
                existentFeed.setVisibleTos(visibility);
            }
            feedRepository.save(existentFeed);
        } else {
            feed.setOwner(owner);
            feed.setMatches(matchingHistory);
            feed.setTime(Instant.now());
            if (owned.getCategory().getId() == 1101) {
                feed.setType(15);
            } else {
                feed.setType(16);
            }
            if (followed != null) {
                for (Follow follw : followed) {
                    Profile profile = follw.getFollower();
                    visibility.add(profile);
                }
            }
            feed.setVisibleTos(visibility);
            feed.setStatut(true);
            feedRepository.save(feed);
        }

        if (existentFeed1 != null) {
           
            existentFeed1.setMatches(matchingHistory);
            existentFeed1.setTime(Instant.now());
            if (owned.getCategory().getId() == 1101) {
                existentFeed1.setType(15);
            } else {
                existentFeed1.setType(16);
            }
            Set<Profile> visibleTo1 = existentFeed1.getVisibleTos();
            if (visibleTo1 != null) {
                if (followedw != null) {
                    for (Follow follw : followedw) {
                        Profile profile = follw.getFollower();
                        if (!visibleTo1.contains(profile)) {
                            existentFeed1.addVisibleTo(profile);
                        }
                        ;
                    }
                }

            }else{
                if (followedw != null) {
                    for (Follow follw : followedw) {
                        Profile profile = follw.getFollower();
                        visibility2.add(profile);
                    }
                
                }
                existentFeed1.setVisibleTos(visibility2);
            }
            feedRepository.save(existentFeed1);
        } else {
            feed1.setOwner(wanter);
            feed1.setMatches(matchingHistory);
            feed1.setTime(Instant.now());
            if (owned.getCategory().getId() == 1101) {
                feed1.setType(15);
            } else {
                feed1.setType(16);
            }
            if (followedw != null) {
                for (Follow follw : followedw) {
                    Profile profile = follw.getFollower();
                    visibility2.add(profile);
                }
            }
            feed1.setVisibleTos(visibility2);
            feed1.setStatut(true);
            feedRepository.save(feed1);
        }

    }

}

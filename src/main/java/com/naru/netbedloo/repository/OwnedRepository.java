package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Owned;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Owned entity.
 */
@SuppressWarnings("unused")
public interface OwnedRepository extends JpaRepository<Owned,Long> {
      @Query("select distinct owned from Owned owned left join fetch owned.owner left join fetch owned.article")
    List<Owned> findAllWithEagerRelationships();

    List<Owned> findAllWithArticleByOwner_Id(Long id);
    List<Owned> findAllWithOwnerByArticle_Id(Long id);

    List<Owned> findAllByOwner_IdAndArticle_Id(long owner_id,long article_id);

    Owned findOneByOwner_IdAndArticle_Id(Long owner_id,Long article_id);

}

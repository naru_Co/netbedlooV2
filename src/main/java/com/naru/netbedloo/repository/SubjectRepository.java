package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Subject;

import org.springframework.data.jpa.repository.*;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data JPA repository for the Subject entity.
 */
@SuppressWarnings("unused")
public interface SubjectRepository extends JpaRepository<Subject,Long> {
 
 List<Subject> findAllByForum_id(long id);

 Subject findOneByForumIdAndProfileIdAndPublication(long forum_id,long profile_id,String publication);

List<Subject> findAllByProfileId(long id);
}

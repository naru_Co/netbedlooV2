package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Gouv;

import org.springframework.data.jpa.repository.*;

import java.util.*;

/**
 * Spring Data JPA repository for the Gouv entity.
 */
@SuppressWarnings("unused")
public interface GouvRepository extends JpaRepository<Gouv, Long> {

    List<Gouv> findAllByCountry_Id(long id);

    Gouv findTop1ByName(String name);

}

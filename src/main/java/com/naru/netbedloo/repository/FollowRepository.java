package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Follow;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Follow entity.
 */
@SuppressWarnings("unused")
public interface FollowRepository extends JpaRepository<Follow,Long> {
   List<Follow> findAllByFollower_Id(Long id);
   List<Follow> findAllByFollowed_Id(Long id);
   Follow findOneByFollower_IdAndFollowed_Id(Long id1, Long id2);

}

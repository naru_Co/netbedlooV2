package com.naru.netbedloo.repository;

import java.util.List;

import com.naru.netbedloo.domain.Tag;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the Tag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagRepository extends JpaRepository<Tag,Long> {

List<Tag> findAllByCategoryIdAndTagContainingIgnoreCase(Long category_id, String tag);

List<Tag> findAllByCategoryId(Long category_id);
List<Tag> findAllByArticles_Id(Long id);
}

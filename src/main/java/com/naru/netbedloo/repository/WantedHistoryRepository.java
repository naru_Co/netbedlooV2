package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.WantedHistory;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the WantedHistory entity.
 */
@SuppressWarnings("unused")
public interface WantedHistoryRepository extends JpaRepository<WantedHistory,Long> {

    WantedHistory findOneByWanter_idAndArticle_idAndEraseDateNull(long wanter_id,long article_id);
   List<WantedHistory> findAllByArticle_id(long id);
List<WantedHistory> findAllByWanter_Id(long id);

}

package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.OwnedHistory;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the OwnedHistory entity.
 */
@SuppressWarnings("unused")
public interface OwnedHistoryRepository extends JpaRepository<OwnedHistory,Long> {

    OwnedHistory findOneByOwner_idAndArticle_idAndEraseDateNull(long owner_id,long article_id);
     List<OwnedHistory>  findAllByArticle_id(long article_id);
	List<OwnedHistory> findAllByOwner_id(long id);
	
}

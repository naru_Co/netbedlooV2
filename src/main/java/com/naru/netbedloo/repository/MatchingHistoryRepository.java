package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.MatchingHistory;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MatchingHistory entity.
 */
@SuppressWarnings("unused")
public interface MatchingHistoryRepository extends JpaRepository<MatchingHistory,Long> {

    List<MatchingHistory> findAllByOwner_IdAndOwned_IdAndEraseDateNull(long owner_id,long owned_id);
    
    List<MatchingHistory> findAllByOwner_IdAndWanted_IdAndEraseDateNull(long owner_id,long wanted_id);

    List<MatchingHistory> findAllByWanter_IdAndOwned_IdAndEraseDateNull(long wanter_id, long owned_id);
    List<MatchingHistory> findAllByWanter_IdAndWanted_IdAndEraseDateNull(long wanter_id,long wanted_id);
    List<MatchingHistory> findAllByOwned_Id(long id);
    List<MatchingHistory> findAllByWanted_Id(long id);
    MatchingHistory findOneByOwner_IdAndOwned_IdAndWanted_IdAndEraseDateNull(long owner_id,long owned_id,long wanter_id,long wanted_id);
    List<MatchingHistory> findAllByOwner_IdOrWanter_IdAndTbedlooDateNotNull(Long id,Long id2);

    List<MatchingHistory> findAllByOwner_IdAndOwned_IdAndWanted_IdAndEraseDateNullAndTbedlooDateNull(long owner_id,long owned_id,long wanter_id,long wanted_id);

	List<MatchingHistory> findAllByOwner_Id(long id);

	List<MatchingHistory> findAllByWanter_Id(long id);

	MatchingHistory findOneByOwner_IdAndOwned_IdAndWanterIdAndWanted_IdAndEraseDateNull(Long ow_id, Long owned_id, Long wa_id,Long wante_id);

}

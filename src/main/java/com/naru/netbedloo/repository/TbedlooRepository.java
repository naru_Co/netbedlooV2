package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Tbedloo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tbedloo entity.
 */
@SuppressWarnings("unused")
public interface TbedlooRepository extends JpaRepository<Tbedloo,Long> {


}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Response;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Response entity.
 */
public interface ResponseSearchRepository extends ElasticsearchRepository<Response, Long> {
}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Owned;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Owned entity.
 */
public interface OwnedSearchRepository extends ElasticsearchRepository<Owned, Long> {
}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Okay;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Okay entity.
 */
public interface OkaySearchRepository extends ElasticsearchRepository<Okay, Long> {
}

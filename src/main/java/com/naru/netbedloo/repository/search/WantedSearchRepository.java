package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Wanted;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Wanted entity.
 */
public interface WantedSearchRepository extends ElasticsearchRepository<Wanted, Long> {
}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.OwnedHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OwnedHistory entity.
 */
public interface OwnedHistorySearchRepository extends ElasticsearchRepository<OwnedHistory, Long> {
}

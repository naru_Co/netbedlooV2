package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Tbedloo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tbedloo entity.
 */
public interface TbedlooSearchRepository extends ElasticsearchRepository<Tbedloo, Long> {
}

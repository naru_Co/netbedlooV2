package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Forum;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Forum entity.
 */
public interface ForumSearchRepository extends ElasticsearchRepository<Forum, Long> {
}

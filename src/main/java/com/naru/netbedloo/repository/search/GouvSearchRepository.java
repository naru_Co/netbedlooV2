package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Gouv;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Gouv entity.
 */
public interface GouvSearchRepository extends ElasticsearchRepository<Gouv, Long> {
}

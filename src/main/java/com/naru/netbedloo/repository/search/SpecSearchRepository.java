package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Spec;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Spec entity.
 */
public interface SpecSearchRepository extends ElasticsearchRepository<Spec, Long> {
}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.WantedHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WantedHistory entity.
 */
public interface WantedHistorySearchRepository extends ElasticsearchRepository<WantedHistory, Long> {
}

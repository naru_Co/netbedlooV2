package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Signal;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Signal entity.
 */
public interface SignalSearchRepository extends ElasticsearchRepository<Signal, Long> {
}

package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.MatchingHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MatchingHistory entity.
 */
public interface MatchingHistorySearchRepository extends ElasticsearchRepository<MatchingHistory, Long> {
}

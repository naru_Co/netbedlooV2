package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Follow;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Follow entity.
 */
public interface FollowSearchRepository extends ElasticsearchRepository<Follow, Long> {
}

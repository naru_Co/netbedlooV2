package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Feed;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Feed entity.
 */
public interface FeedSearchRepository extends ElasticsearchRepository<Feed, Long> {
}

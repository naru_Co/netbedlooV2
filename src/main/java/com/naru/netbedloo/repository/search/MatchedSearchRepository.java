package com.naru.netbedloo.repository.search;

import com.naru.netbedloo.domain.Matched;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Matched entity.
 */
public interface MatchedSearchRepository extends ElasticsearchRepository<Matched, Long> {
}

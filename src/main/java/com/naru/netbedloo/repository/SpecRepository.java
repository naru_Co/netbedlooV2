package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Spec;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the Spec entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecRepository extends JpaRepository<Spec, Long> {
    List<Spec> findAllByCategoryId(Long category_id);

    List<Spec> findAllByCategoryIdAndNameContainingIgnoreCase(Long category_id, String name);

    Spec findOneByCategoryIdAndId(long category_id, long id);
}

package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Article entity.
 */
@SuppressWarnings("unused")
public interface ArticleRepository extends JpaRepository<Article,Long> {
    List<Article> findAllByCategoryIdAndTitleContainingIgnoreCase(Long category_id,String title);
    List<Article> findAllByTitleContainingIgnoreCase(String title);
    List<Article> findAllByTitleContainingIgnoreCaseOrAuthorContainingIgnoreCase(String title,String author);
    Article findOneByTitle(String title);
    Article findOneByIdAndCategoryId(Long id,Long cat_id);
    Page<Article> findAllByCategory_Id(Long catid, Pageable pageable);
    Page<Article> findAllByCategory_IdOrderByPopularityDesc(Long catid, Pageable pageable);
    Page<Article> findAllByCategoryIdAndSpecId(Long catid,Long specid,Pageable pageable);
    List<Article> findAllByProfileId(long id);
   

}

package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Profile;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the Profile entity.
 */
@SuppressWarnings("unused")
public interface ProfileRepository extends JpaRepository<Profile,Long> {
    List<Profile> findAllByFirstNameAndLastName(String firstName,String lastName);
    List<Profile> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String firstName,String lastName);
    List<Profile> findAllByPseudoContainingIgnoreCase(String pseud);
    Set<Profile> findAllVisibleToByFeedsId(long feed);
    Profile findOneByPseudo(String pseudo);

}

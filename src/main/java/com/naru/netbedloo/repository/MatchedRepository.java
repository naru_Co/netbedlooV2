package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Matched;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.security.acl.Owner;
import java.util.List;

/**
 * Spring Data JPA repository for the Matched entity.
 */
@Repository
@SuppressWarnings("unused")
public interface MatchedRepository extends JpaRepository<Matched,Long> {
    
    List<Matched> findAllByOwner_IdAndOwned_Id(long id_owner,Long id_owned);
    List<Matched> findAllByWanter_IdAndWanted_Id(long id_wanter, long id_wanted);
    List<Matched> findAllByOwner_IdAndWanted_Id(long owner_id, long wanted_id);
    List<Matched> findAllByOwned_Id(long id);
    List<Matched> findAllByWanted_Id(long id);
    List<Matched> findAllByWanter_idAndOwned_Id(long wanter_id, long owned_id);
    List<Matched> findAllByOwner_IdAndOwned_IdAndWanter_IdAndWanted_IdAndStatutFalse(long id_owner,Long id_owned,long id_wanter, long id_wanted);
    List<Matched> findAllByStatutIsFalseAndOwner_IdOrWanter_Id(Long id,Long id2);
	List<Matched> findAllByWanter_Id(long id);
	List<Matched> findAllByOwner_Id(long id);

}

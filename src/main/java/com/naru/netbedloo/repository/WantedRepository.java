package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Article;


import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Wanted entity.
 */
@SuppressWarnings("unused")
public interface WantedRepository extends JpaRepository<Wanted,Long> {
    
        @Query("select distinct wanted from Wanted wanted left join fetch wanted.wanter left join fetch wanted.article")
    List<Wanted> findAllWithEagerRelationships();


    List<Wanted> findAllWithArticleByWanter_Id(Long id);
     List<Wanted> findAllWithWanterByArticle_Id(Long id);
     List<Wanted> findAllByWanter_IdAndArticle_Id(long wanter_id,long article_id);
    Wanted findOneByWanter_IdAndArticle_Id(Long wanter_id,Long article_id);


}

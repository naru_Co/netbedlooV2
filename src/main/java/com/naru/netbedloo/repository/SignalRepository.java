package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Signal;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Signal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SignalRepository extends JpaRepository<Signal,Long> {
    List<Signal> findAllByProfileIdAndType(Long profile_id,int type);

	List<Signal> findAllByProfileId(long id);
    
}

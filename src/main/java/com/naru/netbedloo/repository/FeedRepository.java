package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Feed;

import java.math.BigInteger;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Spring Data JPA repository for the Feed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeedRepository extends JpaRepository<Feed, Long> {
  Feed findOneByOwnedId(long owned_Id);
  Feed findOneByWantedId(long wanted_id);
  Feed findOneByFollowId(long follow_id);
  Feed findOneByMatchesId(long matches_id);
  Feed findOneByArticleIdAndOwnerIdAndContent(long article_id,long owner_id,String content);
  Feed findOneByMatchesIdAndOwnerId(long matches_id,long owner_id);
  Page<Feed> findAllByOwnerId(long owner_id,Pageable pageable);
  List<Feed> findAllByOwner_Id(long owner_id);
  Feed findOneByRatingId(long rating_id);
  Page<Feed> findAllByVisibleTosIdAndStatutIsTrueOrderByTimeDesc(long visibleto,Pageable pageable);
}

package com.naru.netbedloo.repository;

import java.util.List;

import com.naru.netbedloo.domain.Okay;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Okay entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OkayRepository extends JpaRepository<Okay, Long> {
    Long countByFeedId(long feed_id);
    List<Okay> findAllByFeedId(long feed_id);
    Okay findOneByFeedIdAndProfileId(long feed_id, long profile_id);

}

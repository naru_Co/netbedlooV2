package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Message;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Message entity.
 */
@SuppressWarnings("unused")
public interface MessageRepository extends JpaRepository<Message, Long> {

    Page<Message> findByUid(String uid, Pageable pageable);

    List<Message> findBySeenDateIsNullAndReceiver_Id(Long id);

    List<Message> findAllBySenderId(long id);

    List<Message> findAllByReceiverId(long id);

    List<Message> findAllByUidContainingIgnoreCaseOrderByIdDesc(long id);

    Message findFirstByUidOrderByIdDesc(String uid);

    List<Message> findBySeenDateIsNullAndUid(String uid);

    List<Message> findBySeenDateIsNullAndUidAndReceiver_Id(String uid, Long id);

    long countAllBySeenDateIsNullAndReceiverIdAndSenderId(long id, Long sender_id);

    List<Message> findAllByUid(String uid);
}

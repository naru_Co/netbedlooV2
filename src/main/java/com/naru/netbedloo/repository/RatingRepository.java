package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Rating;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Rating entity.
 */
@SuppressWarnings("unused")
public interface RatingRepository extends JpaRepository<Rating,Long> {
    List<Rating> findAllByRatedId(Long rated_id);
    Rating findOneByRatedIdAndRaterId(Long rated_id,Long rater_id);
	List<Rating> findAllByRaterId(long id);
}

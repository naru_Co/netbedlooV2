package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Forum;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Forum entity.
 */
@SuppressWarnings("unused")
public interface ForumRepository extends JpaRepository<Forum,Long> {

}

package com.naru.netbedloo.repository;

import com.naru.netbedloo.domain.Comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Comment entity.
 */
@SuppressWarnings("unused")
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllBySubject_id(long id);

    Long countAllByFeed_id(long feed_id);

    List<Comment> findTop5ByFeedIdOrderByPublicationTimeDesc(long id);

    Page<Comment> findAllByFeedId(long id, Pageable pageable);

}

package com.naru.netbedloo.config;

import com.naru.netbedloo.security.*;
import com.naru.netbedloo.security.jwt.*;

import io.github.jhipster.security.*;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import javax.annotation.PostConstruct;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final SessionRegistry sessionRegistry;

    private final CorsFilter corsFilter;

    public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder, UserDetailsService userDetailsService,
            TokenProvider tokenProvider, SessionRegistry sessionRegistry,
        CorsFilter corsFilter) {

        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.sessionRegistry = sessionRegistry;
        this.corsFilter = corsFilter;
    }

    @PostConstruct
    public void init() {
        try {
            authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Bean
    public Http401UnauthorizedEntryPoint http401UnauthorizedEntryPoint() {
        return new Http401UnauthorizedEntryPoint();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/bower_components/**")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement()
            .maximumSessions(32) // maximum number of concurrent sessions for one user
            .sessionRegistry(sessionRegistry)
            .and().and()
            .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling()
            .authenticationEntryPoint(http401UnauthorizedEntryPoint())
        .and()
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            
            .antMatchers("/api/register").permitAll()
            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/account/reset_password/init").permitAll()
            .antMatchers("/api/account/reset_password/finish").permitAll()
            .antMatchers("/api/profile-info").permitAll()
            .antMatchers(HttpMethod.POST,"/api/profiles").permitAll()
            .antMatchers(HttpMethod.GET,"/api/profiles").permitAll()
            .antMatchers(HttpMethod.GET,"/api/followeds/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/wanteds/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/owneds/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/ownedbooks/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/profilesbyname/**/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articlesbytitleorauthor/**/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/ownedgames/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/wantedbooks/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/wantedgames/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/matchingByprofile/**/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articlesbycategoryandspec/**/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/specsbycategory/**/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/specsofcategory/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/profiles/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/profilebypseudo/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/gouvs").permitAll()
            .antMatchers(HttpMethod.POST,"/api/gouvs").permitAll()
            .antMatchers(HttpMethod.GET,"/api/gouvsbycountry/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/ownedsbyarticle/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/wantedsbyarticle/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/subjectsbyforum/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/rateofarticle/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/gouvbyname/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/countries").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articlesbycategory/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articlebytitle/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/connectedfeeds").authenticated()
            .antMatchers("/api/booksbytitle/**").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articles").permitAll()
            .antMatchers(HttpMethod.GET,"/api/articles/**").permitAll()
            .antMatchers("/api/**").authenticated()
            .antMatchers("/websocket/tracker").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/websocket/**").permitAll()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/v2/api-docs/**").permitAll()
            .antMatchers("/swagger-resources/configuration/ui").permitAll()
            .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}

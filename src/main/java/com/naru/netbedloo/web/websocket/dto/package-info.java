/**
 * Data Access Objects used by WebSocket services.
 */
package com.naru.netbedloo.web.websocket.dto;

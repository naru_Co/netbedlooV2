package com.naru.netbedloo.web.websocket;

import java.security.Principal;
import java.time.format.DateTimeFormatter;

import com.naru.netbedloo.domain.Message;
import com.naru.netbedloo.repository.MessageRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Controller
public class NotificationService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final SimpMessageSendingOperations messagingTemplate;
    private final MessageRepository messageRepository;

    public NotificationService(SimpMessageSendingOperations messagingTemplate,MessageRepository messageRepository) {
        this.messagingTemplate = messagingTemplate;
        this.messageRepository=messageRepository;
    }
/*
         @SubscribeMapping("notification/notif/{id}")
    public void subscribeToNotif(@DestinationVariable String id,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        String login = SecurityUtils.getCurrentUserLogin();
        String ipAddress = stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString();
        log.debug("User {} subscribed to private Chat from IP {}", login, ipAddress);
       
    }
 @MessageMapping("/notification/{id}")*/
    @SendTo("/notification/notif/{id}")
    public Message sendNotif(@Payload Message message,@DestinationVariable String id,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("User sent a notification message ");

        return message;
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        // when the user disconnects, send a message saying that hey are leaving
        log.info("{} disconnected from the chat websockets", event.getUser().getName());
      
    }


}

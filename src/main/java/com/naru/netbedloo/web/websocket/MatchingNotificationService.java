package com.naru.netbedloo.web.websocket;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.repository.MessageRepository;
import com.naru.netbedloo.repository.ProfileRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Controller
public class MatchingNotificationService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(MatchingNotificationService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final SimpMessageSendingOperations messagingTemplate;
    private final MessageRepository messageRepository;
    private final ProfileRepository profileRepository;

    public MatchingNotificationService(SimpMessageSendingOperations messagingTemplate,MessageRepository messageRepository,ProfileRepository profileRepository) {
        this.messagingTemplate = messagingTemplate;
        this.messageRepository=messageRepository;
        this.profileRepository=profileRepository;
    }

    @SendTo("/matchingnotification/notif/{id}")
    public Matched sendNotif(@Payload Matched matched,@DestinationVariable String id,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("a matching notif is sent to user {}",id);

        return matched;
    }

    public void sendPushNotif(Long id, Matched matched){
 
        
        Profile profile=profileRepository.findOne(id);
        String playerId = profile.getPlayerId();
        if (playerId!=null){
            String bookList=matched.getWanted().getTitle()+" for "+matched.getOwned().getTitle();
            String matcherName ="test";
            if(id==matched.getOwner().getId()){
                 matcherName=matched.getWanter().getFirstName()+" "+matched.getWanter().getLastName();
            }else{
                 matcherName=matched.getOwner().getFirstName()+" "+matched.getOwner().getLastName();
            }
            

        try {
            String jsonResponse;
            URL url = new URL("https://onesignal.com/api/v1/notifications");
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);
         
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", "Basic MWJhMDg2MmItOTViNC00NGM4LWFjZmEtMzBlOTBkYjg5MDE5");
            con.setRequestMethod("POST");
         
            String strJsonBody = "{"
                               +   "\"app_id\": \"ea9761a2-26c1-4047-b09d-40ff05f1b6da\","
                               +   "\"include_player_ids\": [\""+playerId+"\"],"
                               +   "\"contents\": {\"en\": \" Exchange "+bookList+"\"},"
                               +    "\"heading\":{\"en\": \"You have a new match with "+matcherName+"\"}"
                               + "}";
                  
            
            System.out.println("strJsonBody:\n" + strJsonBody);
         
            byte[] sendBytes = strJsonBody.getBytes("UTF-8");
            con.setFixedLengthStreamingMode(sendBytes.length);
         
            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);
         
            int httpResponse = con.getResponseCode();
            System.out.println("httpResponse: " + httpResponse);
         
            if (  httpResponse >= HttpURLConnection.HTTP_OK
               && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
               Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
               jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
               scanner.close();
            }
            else {
               Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
               jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
               scanner.close();
            }
            System.out.println("jsonResponse:\n" + jsonResponse);
            
         } catch(Throwable t) {
            t.printStackTrace();
         }
        }

    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        // when the user disconnects, send a message saying that hey are leaving
        log.info("{} disconnected from the chat websockets", event.getUser().getName());
      
    }


}

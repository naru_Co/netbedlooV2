package com.naru.netbedloo.web.websocket;

import static com.naru.netbedloo.config.WebsocketConfiguration.IP_ADDRESS;

import java.security.Principal;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

import com.naru.netbedloo.domain.Message;
import com.naru.netbedloo.repository.MessageRepository;
import com.naru.netbedloo.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Controller
public class ChatService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(ChatService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final SimpMessageSendingOperations messagingTemplate;
    private final MessageRepository messageRepository;

    public ChatService(SimpMessageSendingOperations messagingTemplate,MessageRepository messageRepository) {
        this.messagingTemplate = messagingTemplate;
        this.messageRepository=messageRepository;
    }




     @SubscribeMapping("/privatechat/private/{id1}/{id2}")
    public void subscribeToPrivate(@DestinationVariable String id1, @DestinationVariable String id2,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        String login = SecurityUtils.getCurrentUserLogin();
        String ipAddress = stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString();
        log.debug("User {} subscribed to private Chat from IP {}", login, ipAddress);
       
    }


     @MessageMapping("/privatechat/{id1}/{id2}")
    @SendTo("/privatechat/private/{id1}/{id2}")
    public Message sendPrivateChat(@Payload Message message,@DestinationVariable String id1, @DestinationVariable String id2, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("User sent a private message ");

        return setupMessageDTO(message, stompHeaderAccessor, principal);
    }



    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        // when the user disconnects, send a message saying that hey are leaving
        log.info("{} disconnected from the chat websockets", event.getUser().getName());
      
    }

    private Message setupMessageDTO (Message message, StompHeaderAccessor stompHeaderAccessor, Principal principal) {

        message.setCreationDate(Instant.now());
        log.debug("Sending and saving user chat data {}", message);
        messageRepository.save(message);
        //trying to send a notification
        //send push notification here
        messagingTemplate.convertAndSend("/notification/notif/"+message.getReceiver().getId(), message);

        return message;
    }
}

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.WantedRepository;
import com.naru.netbedloo.repository.search.WantedSearchRepository;
import com.naru.netbedloo.service.MatchingService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Wanted.
 */
@RestController
@RequestMapping("/api")
public class WantedResource {

    private final Logger log = LoggerFactory.getLogger(WantedResource.class);

    private static final String ENTITY_NAME = "wanted";

    private final WantedRepository wantedRepository;
    private final MatchingService matchingService;
    private final WantedHistoryRepository wantedHistoryRepository;
    private final WantedSearchRepository wantedSearchRepository;
    private final ArticleRepository articleRepository;
    private final OwnedRepository ownedRepository;
    private final OwnedHistoryRepository ownedHistoryRepository;
    private final FeedRepository feedRepository;
    private final FollowRepository followRepository;
    private final ProfileRepository profileRepository;

    public WantedResource(WantedRepository wantedRepository, MatchingService matchingService,
            WantedHistoryRepository wantedHistoryRepository, WantedSearchRepository wantedSearchRepository,
            ArticleRepository articleRepository, OwnedRepository ownedRepository,
            OwnedHistoryRepository ownedHistoryRepository, FeedRepository feedRepository,
            FollowRepository followRepository, ProfileRepository profileRepository) {
        this.wantedRepository = wantedRepository;
        this.matchingService = matchingService;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.wantedSearchRepository = wantedSearchRepository;
        this.articleRepository = articleRepository;
        this.ownedRepository = ownedRepository;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.feedRepository = feedRepository;
        this.followRepository = followRepository;
        this.profileRepository = profileRepository;
    }

    /**
     * POST  /wanteds : Create a new wanted.
     *
     * @param wanted the wanted to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wanted, or with status 400 (Bad Request) if the wanted has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wanteds")
    @Timed
    public ResponseEntity<Wanted> createWanted(@RequestBody Wanted wanted) throws URISyntaxException {
        log.debug("REST request to save Wanted : {}", wanted);
        if (wanted.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new wanted cannot already have an ID"))
                    .body(null);
        }

        WantedHistory wantedHistory = new WantedHistory();
        Feed feed = new Feed();
        Profile wanter = wanted.getWanter();
        Article checker = wanted.getArticle();
        List<Wanted> check = wantedRepository.findAllByWanter_IdAndArticle_Id(wanter.getId(), checker.getId());
        Owned owned = ownedRepository.findOneByOwner_IdAndArticle_Id(wanter.getId(), checker.getId());
        OwnedHistory ownedHistory = ownedHistoryRepository
        .findOneByOwner_idAndArticle_idAndEraseDateNull(wanter.getId(), checker.getId());
        if (owned != null) {
            ownedRepository.delete(owned.getId());
        }
        if (ownedHistory != null) {
            ownedHistory.setEraseDate(Instant.now());
            ownedHistoryRepository.save(ownedHistory);
        }

        for (Wanted chiko : check) {
            deleteWanted(chiko.getId());
        }
        wanted.setCreationDate(Instant.now());
        Wanted result = wantedRepository.save(wanted);
        new Thread() {
            @Override
            public void run() {
              
                Set<Profile> visibility = new HashSet<>();
                List<Follow> followed = followRepository.findAllByFollowed_Id(wanter.getId());
                Article article = result.getArticle();
                long popularity = article.getPopularity();
                long updatedpopularity = popularity + 1;
                article.setPopularity(updatedpopularity);
                articleRepository.save(article);
                wantedHistory.setArticle(result.getArticle());
                wantedHistory.setWanter(result.getWanter());
                wantedHistory.setWantingDate(Instant.now());
                wantedHistoryRepository.save(wantedHistory);
                matchingService.matchingforWanted(result);
                feed.setOwner(result.getWanter());
                feed.setTime(Instant.now());
                if (article.getCategory().getId() == 1101) {
                    feed.setType(2);
                } else {
                    feed.setType(8);
                }
                if (followed != null) {
                    for (Follow follw : followed) {
                        Profile profile = follw.getFollower();
                        visibility.add(profile);
                    }
                }
                feed.setVisibleTos(visibility);
                feed.setStatut(true);
                feed.setWanted(wantedHistory);
                feedRepository.save(feed);
                // followSearchRepository.save(result);
            }
        }.start();

        return ResponseEntity.created(new URI("/api/wanteds/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /wanteds : Updates an existing wanted.
     *
     * @param wanted the wanted to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wanted,
     * or with status 400 (Bad Request) if the wanted is not valid,
     * or with status 500 (Internal Server Error) if the wanted couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wanteds")
    @Timed
    public ResponseEntity<Wanted> updateWanted(@RequestBody Wanted wanted) throws URISyntaxException {
        log.debug("REST request to update Wanted : {}", wanted);
        if (wanted.getId() == null) {
            return createWanted(wanted);
        }

        Wanted result = wantedRepository.save(wanted);
        // followSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wanted.getId().toString()))
                .body(result);
    }

    /**
     * GET  /wanteds : get all the wanteds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of wanteds in body
     */
    @GetMapping("/wanteds")
    @Timed
    public List<Wanted> getAllWanteds() {
        log.debug("REST request to get all Wanteds");
        List<Wanted> wanteds = wantedRepository.findAllWithEagerRelationships();
        return wanteds;
    }

    /**
    * GET  /wanteds/:id : get the "id" wanted.
    *
    * @param id the id of the wanted to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the wanted, or with status 404 (Not Found)
    */
    @GetMapping("/wanteds/{id}")
    @Timed
    public List<Wanted> getWanteds(@PathVariable Long id) {
        log.debug("REST request to get Wanted : {}", id);
        List<Wanted> wanteds = wantedRepository.findAllWithArticleByWanter_Id(id);

        return wanteds;
    }

    /**
     * GET  /wanteds/:id : get the "id" wanted.
     *
     * @param id the id of the wanted to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wanted, or with status 404 (Not Found)
     */
    @GetMapping("/wantedbooks/{id}")
    @Timed
    public List<Wanted> getBooks(@PathVariable Long id) {
        log.debug("REST request to get Wanted : {}", id);
        List<Wanted> wanteds = wantedRepository.findAllWithArticleByWanter_Id(id);
        List<Wanted> books = new ArrayList<Wanted>();
        for (Wanted wanted : wanteds) {
            Article article = wanted.getArticle();
            if (article.getCategory().getId() == 1101) {
                books.add(wanted);
            }
        }
        return books;
    }

    /**
    * GET  /wanteds/:id : get the "id" wanted.
    *
    * @param id the id of the wanted to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the wanted, or with status 404 (Not Found)
    */
    @GetMapping("/wantedgames/{id}")
    @Timed
    public List<Wanted> getGames(@PathVariable Long id) {
        log.debug("REST request to get Wanted : {}", id);
        List<Wanted> wanteds = wantedRepository.findAllWithArticleByWanter_Id(id);
        List<Wanted> games = new ArrayList<Wanted>();
        for (Wanted wanted : wanteds) {
            Article article = wanted.getArticle();
            if (article.getCategory().getId() == 1102) {
                games.add(wanted);
            }
        }
        return games;
    }

    @GetMapping("/wantedsbyarticle/{id}")
    @Timed
    public List<Wanted> getWantedByArticle(@PathVariable Long id) {
        log.debug("REST request to get Wanted : {}", id);
        List<Wanted> wanteds = wantedRepository.findAllWithWanterByArticle_Id(id);
        return wanteds;
    }

    /**
     * DELETE  /wanteds/:id : delete the "id" wanted.
     *
     * @param id the id of the wanted to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wanteds/{id}")
    @Timed
    public ResponseEntity<Void> deleteWanted(@PathVariable Long id) {
        log.debug("REST request to delete Wanted : {}", id);
        Wanted wanted = wantedRepository.findOne(id);
        wantedRepository.delete(id);
        new Thread() {
            @Override
            public void run() {
                WantedHistory wantedhistory = wantedHistoryRepository.findOneByWanter_idAndArticle_idAndEraseDateNull(
                        wanted.getWanter().getId(), wanted.getArticle().getId());
                Article article = wanted.getArticle();
                Set<Profile> visibility = new HashSet<>();
                List<Follow> followed = followRepository.findAllByFollowed_Id(wanted.getWanter().getId());
                Feed feed = new Feed();
                Feed existedfeed = feedRepository.findOneByWantedId(wantedhistory.getId());
                if (existedfeed != null) {
                    existedfeed.setTime(Instant.now());
                    if (article.getCategory().getId() == 1101) {
                        existedfeed.setType(22);
                    } else {
                        existedfeed.setType(88);
                    }
                    Set<Profile> visibleTo = profileRepository.findAllVisibleToByFeedsId(existedfeed.getId());
                    if (visibleTo != null) {
                        if (followed != null) {
                            for (Follow follw : followed) {
                                Profile profile = follw.getFollower();
                                if (!visibleTo.contains(profile)) {
                                    existedfeed.addVisibleTo(profile);
                                }
                                ;
                            }
                        }

                    } else {
                        if (followed != null) {
                            for (Follow follw : followed) {
                                Profile profile = follw.getFollower();
                                visibility.add(profile);
                            }

                        }
                        existedfeed.setVisibleTos(visibility);
                    }
                    existedfeed.setStatut(true);
                    feedRepository.save(existedfeed);
                } else {

                    feed.setOwner(wanted.getWanter());
                    feed.setTime(Instant.now());
                    if (article.getCategory().getId() == 1101) {
                        feed.setType(22);
                    } else {
                        feed.setType(88);
                    }
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    feed.setStatut(true);
                    feed.setWanted(wantedhistory);
                    feed.setVisibleTos(visibility);
                    feedRepository.save(feed);
                }
                matchingService.deletematchingforWanted(wanted);

            }
        }.start();
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wanteds?query=:query : search for the wanted corresponding
     * to the query.
     *
     * @param query the query of the wanted search 
     * @return the result of the search
     */
    @GetMapping("/_search/wanteds")
    @Timed
    public List<Wanted> searchWanteds(@RequestParam String query) {
        log.debug("REST request to search Wanteds for query {}", query);
        return StreamSupport.stream(wantedSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

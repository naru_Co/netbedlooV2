package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.ArticleImage;
import com.naru.netbedloo.domain.Forum;
import com.naru.netbedloo.domain.Tag;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.ForumRepository;
import com.naru.netbedloo.repository.TagRepository;
import com.naru.netbedloo.repository.search.ArticleSearchRepository;
import com.naru.netbedloo.service.ArticleDeleteService;
import com.naru.netbedloo.service.ImageResizeService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Article.
 */
@RestController
@RequestMapping("/api")
public class ArticleResource {

    private final Logger log = LoggerFactory.getLogger(ArticleResource.class);

    private static final String ENTITY_NAME = "article";
    @Inject
    private final ArticleRepository articleRepository;
    @Inject
    private final ArticleSearchRepository articleSearchRepository;
    @Inject
    private final TagRepository tagRepository;
    @Inject
    private final ForumRepository forumRepository;
    @Inject
    private final ImageResizeService imageResizeService;
    @Inject
    private final ArticleDeleteService articleDeleteService;
    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;

    public ArticleResource(ArticleRepository articleRepository, ArticleSearchRepository articleSearchRepository,
            TagRepository tagRepository, ForumRepository forumRepository, ImageResizeService imageResizeService,
            ArticleDeleteService articleDeleteService) {
        this.articleRepository = articleRepository;
        this.articleSearchRepository = articleSearchRepository;
        this.tagRepository = tagRepository;
        this.forumRepository = forumRepository;
        this.imageResizeService = imageResizeService;
        this.articleDeleteService = articleDeleteService;
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("user-key", "546a30d3a00a6257a77fa89a63bbc7de");
        headers.add("accept", "application/json");

    }

    /**
     * POST  /articles : Create a new article.
     *
     * @param article the article to create
     * @return the ResponseEntity with status 201 (Created) and with body the new article, or with status 400 (Bad Request) if the article has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @Autowired
    private HttpServletRequest request;

    @PostMapping("/articles")
    @Timed
    public ResponseEntity<Article> createArticle(@RequestBody ArticleImage articleImage) throws URISyntaxException {
        log.debug("REST request to save Article : {}", articleImage.getFile());
        Article article = new Article();
        if (articleImage.getFile() == null) {
        } else {

            String uploadsDir = "/app/article_covs/";
            String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);

            File file = new File("/app/");
            file.setExecutable(true);
            file.setReadable(true);
            file.setWritable(true);

            if (!new File(realPathtoUploads).exists()) {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = Math.round(Math.random() * 100000) + ""
                    + new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()) + ".jpeg";
            String filePath = realPathtoUploads + "/" + orgName;
            String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
            String mdPath = realPathtoUploads + "/" + "md_" + orgName;
            String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
            File dest = new File(filePath);
            article.setCoverImage("/app/article_covs/" + orgName);
            article.setThumbnailImage("/app/article_covs/tmbl_" + orgName);
            article.setMediumImage("/app/article_covs/md_" + orgName);
            article.setNormalImage("/app/article_covs/nr_" + orgName);
            new Thread() {
                @Override
                public void run() {
                    try {
                        new FileOutputStream(dest).write(articleImage.getFile());
                        if (articleImage.getArticle().getCategory().getId() == 1102) {
                            // if it's a game
                            imageResizeService.thumbailSizeforArticleGameImage(filePath, tmblPath);
                            
                            imageResizeService.mediumSizeforArticleGameImage(filePath, mdPath);
                           
                            imageResizeService.normalSizeforArticleGameImage(filePath, nrPath);
                            
        
                        } else {
                            imageResizeService.thumbailSizeforArticleImage(filePath, tmblPath);
                           
                            imageResizeService.mediumSizeforArticleImage(filePath, mdPath);
                        
                            imageResizeService.normalSizeforArticleImage(filePath, nrPath);
                           
                        }
        
                    } catch (Exception e) {
        
                    }
                }
            }.start();

            
        }

        Article artImg = articleImage.getArticle();

        //save tags//
        if (artImg.getTags()!= null) {
        Set<Tag> savedTags = new HashSet<Tag>();
        Set<Tag> pushedTags = artImg.getTags();
        for (Tag tag : pushedTags) {
            if (tag.getId() == null) {
                tag.setCategory(artImg.getCategory());
                tag = tagRepository.save(tag);     
            }
            savedTags.add(tag);
            
        }
        article.setTags(savedTags);
    }
        //end of tags saving//
       
        article.setTitle(artImg.getTitle());
        article.setCategory(artImg.getCategory());
        article.setProfile(artImg.getProfile());
        article.setAuthor(artImg.getAuthor());
        article.setDescription(artImg.getDescription());
        article.setCreationDate(artImg.getCreationDate());
        article.setSpec(artImg.getSpec());
        article.setAverageRate(artImg.getAverageRate());
        Forum forum = new Forum();
        forum.setProfile(article.getProfile());
        forumRepository.save(forum);
        article.setForum(forum);
        long initial = 0;
        article.setPopularity(initial);
        Article result = articleRepository.save(article);

        //articleSearchRepository.save(result);

        return ResponseEntity.created(new URI("/api/articles/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /articles : Updates an existing article.
     *
     * @param article the article to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated article,
     * or with status 400 (Bad Request) if the article is not valid,
     * or with status 500 (Internal Server Error) if the article couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/articles")
    @Timed
    public ResponseEntity<Article> updateArticle(@RequestBody ArticleImage artImage) throws URISyntaxException {
        log.debug("REST request to update Article : {}", artImage.getArticle());
        Article article = artImage.getArticle();
        if (article.getId() == null) {
            return createArticle(artImage);
        }
        if (artImage.getFile() == null) {
        } else {

            String uploadsDir = "/app/article_covs/";
            String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);

            File file = new File("/app/");
            file.setExecutable(true);
            file.setReadable(true);
            file.setWritable(true);

            if (!new File(realPathtoUploads).exists()) {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = Math.round(Math.random() * 100000) + ""
                    + new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()) + ".jpeg";
            String filePath = realPathtoUploads + "/" + orgName;
            String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
            String mdPath = realPathtoUploads + "/" + "md_" + orgName;
            String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
            File dest = new File(filePath);
            article.setCoverImage("/app/article_covs/" + orgName);
            article.setThumbnailImage("/app/article_covs/tmbl_" + orgName);
            article.setMediumImage("/app/article_covs/md_" + orgName);
            article.setNormalImage("/app/article_covs/nr_" + orgName);

            try {
                new FileOutputStream(dest).write(artImage.getFile());
                if (artImage.getArticle().getCategory().getId() == 1102) {
                    // if it's a game
                    imageResizeService.thumbailSizeforArticleGameImage(filePath, tmblPath);
                    
                    imageResizeService.mediumSizeforArticleGameImage(filePath, mdPath);
                   
                    imageResizeService.normalSizeforArticleGameImage(filePath, nrPath);
                    

                } else {
                    imageResizeService.thumbailSizeforArticleImage(filePath, tmblPath);
                   
                    imageResizeService.mediumSizeforArticleImage(filePath, mdPath);
                
                    imageResizeService.normalSizeforArticleImage(filePath, nrPath);
                   
                }

            } catch (Exception e) {

            }
         
        }
        
        if (article.getTags()!= null) {
            Set<Tag> savedTags = new HashSet<Tag>();
            Set<Tag> pushedTags = article.getTags();
          
            for (Tag tag : pushedTags) {
                if (tag.getId() == null) {
                    tag.setCategory(article.getCategory());
                    tag = tagRepository.save(tag);
                }
                savedTags.add(tag);
            }
            
            article.setTags(savedTags);
        }
       
        //end of tags saving//
        
        Article result = articleRepository.save(article);
        //articleSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, article.getId().toString()))
                .body(result);
    }

    /**
     * GET  /articles : get all the articles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of articles in body
     */

    @GetMapping("/articlesbycategoryorderbypopularity/{catid}")
    @Timed
    public ResponseEntity<List<Article>> getAllArticlesOrdredByPopularity(@PathVariable Long catid,
            @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Articles");
        Page<Article> page = articleRepository.findAllByCategory_IdOrderByPopularityDesc(catid, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
                "/api/articlesbycategoryorderbypopularity");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/articlesbycategoryandspec/{catid}/{specid}")
    @Timed
    public ResponseEntity<List<Article>> getAllArticlesByCategoryAndSpec(@PathVariable Long catid,
            @PathVariable Long specid, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Articles");
        Page<Article> page = articleRepository.findAllByCategoryIdAndSpecId(catid, specid, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/articlesbycategoryandspec");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /articles : get all the articles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of articles in body
     */

    @GetMapping("/articlesbycategory/{catid}")
    @Timed
    public ResponseEntity<List<Article>> getAllArticles(@PathVariable Long catid, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Articles");
        Page<Article> page = articleRepository.findAllByCategory_Id(catid, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/articlesbycategory");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/articles")
    @Timed
    public ResponseEntity<List<Article>> getAllArticles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Articles");
        Page<Article> page = articleRepository.findAll(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/articles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/booksbytitle/{title}")
    @Timed
    public List<Article> getAllBooksByTitle(@PathVariable String title) {
        log.debug("REST request to get all Articles of the category");
        List<Article> articles = articleRepository.findAllByTitleContainingIgnoreCase(title);
        return articles;
    }

    @GetMapping("/articlesbytitleorauthor/{title}/{author}")
    @Timed
    public List<Article> getAllBooksByTitleOrAuthor(@PathVariable String title, @PathVariable String author) {
        log.debug("REST request to Search in all Articles ");
        List<Article> articles = articleRepository.findAllByTitleContainingIgnoreCaseOrAuthorContainingIgnoreCase(title,
                author);
        return articles;
    }

    @GetMapping("/booksbytitleandcategory/{category_id}/{title}")
    @Timed
    public List<Article> getAllBooksByTitleAndCat(@PathVariable Long category_id, @PathVariable String title) {
        log.debug("REST request to get all Articles of the category", category_id);
        List<Article> articles = articleRepository.findAllByCategoryIdAndTitleContainingIgnoreCase(category_id, title);
        return articles;
    }

    /**
     * GET  /articles/:id : get the "id" article.
     *
     * @param id the id of the article to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the article, or with status 404 (Not Found)
     */
    @GetMapping("/articles/{id}")
    @Timed
    public ResponseEntity<Article> getArticle(@PathVariable Long id) {
        log.debug("REST request to get Article : {}", id);
        Article article = articleRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(article));
    }

     /**
     * GET  /articles/:id : get the "id" article.
     *
     * @param id the id of the article to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the article, or with status 404 (Not Found)
     */
    @GetMapping("/articlewithcategory/{id}/{cat_id}")
    @Timed
    public ResponseEntity<Article> getArticle(@PathVariable Long id, @PathVariable Long cat_id) {
        log.debug("REST request to get Article : {}", id);
        Article article = articleRepository.findOneByIdAndCategoryId(id,cat_id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(article));
    }
    /**
    * GET  /articles/:id : get the "id" article.
    *
    * @param title the id of the article to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the article, or with status 404 (Not Found)
    */
    @GetMapping("/articlebytitle/{title}")
    @Timed
    public ResponseEntity<Article> getArticleByTitle(@PathVariable String title) {
        log.debug("REST request to get Article : {}", title);
        Article article = articleRepository.findOneByTitle(title);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(article));
    }

    @GetMapping("/GamesIgdb/{title}")
    @Timed
    public String getGamesFromIGDB(@PathVariable String title) {

        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        ResponseEntity<String> responseEntity = rest.exchange(
                "https://api-2445582011268.apicast.io/games/?search=" + title + "&fields=*&limit=1", HttpMethod.GET,
                requestEntity, String.class);
        this.setStatus(responseEntity.getStatusCode());

        return responseEntity.getBody();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    /**
     * DELETE  /articles/:id : delete the "id" article.
     *
     * @param id the id of the article to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/articles/{id}")
    @Timed
    public ResponseEntity<Void> deleteArticle(@PathVariable Long id) {
        log.debug("REST request to delete Article : {}", id);
        articleDeleteService.articledelete(id);
        articleRepository.delete(id);

        //articleSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/articles?query=:query : search for the article corresponding
     * to the query.
     *
     * @param query the query of the article search 
     * @return the result of the search
     */
    @GetMapping("/_search/articles")
    @Timed
    public List<Article> searchArticles(@RequestParam String query) {
        log.debug("REST request to search Articles for query {}", query);
        return StreamSupport.stream(articleSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

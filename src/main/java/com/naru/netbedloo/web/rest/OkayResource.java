package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Okay;

import com.naru.netbedloo.repository.OkayRepository;
import com.naru.netbedloo.repository.search.OkaySearchRepository;

import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Okay.
 */
@RestController
@RequestMapping("/api")
public class OkayResource {

    private final Logger log = LoggerFactory.getLogger(OkayResource.class);

    private static final String ENTITY_NAME = "okay";

    private final OkayRepository okayRepository;

    private final OkaySearchRepository okaySearchRepository;

    public OkayResource(OkayRepository okayRepository, OkaySearchRepository okaySearchRepository) {
        this.okayRepository = okayRepository;
        this.okaySearchRepository = okaySearchRepository;
    }

    /**
     * POST  /okays : Create a new okay.
     *
     * @param okay the okay to create
     * @return the ResponseEntity with status 201 (Created) and with body the new okay, or with status 400 (Bad Request) if the okay has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/okays")
    @Timed
    public ResponseEntity<Okay> createOkay(@RequestBody Okay okay) throws URISyntaxException {
        log.debug("REST request to save Okay : {}", okay);
        if (okay.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new Okay cannot already have an ID"))
                    .body(null);
        }
        okay.setTime(Instant.now());
        Okay result = okayRepository.save(okay);
        //okaySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/okays/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /okays : Updates an existing okay.
     *
     * @param okay the okay to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated okay,
     * or with status 400 (Bad Request) if the okay is not valid,
     * or with status 500 (Internal Server Error) if the okay couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/okays")
    @Timed
    public ResponseEntity<Okay> updateOkay(@RequestBody Okay okay) throws URISyntaxException {
        log.debug("REST request to update Okay : {}", okay);
        if (okay.getId() == null) {
            return createOkay(okay);
        }
        Okay result = okayRepository.save(okay);
        //okaySearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, okay.getId().toString()))
                .body(result);
    }

    /**
     * GET  /okays : get all the okays.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of okays in body
     */
    @GetMapping("/okays")
    @Timed
    public List<Okay> getAllOkays() {
        log.debug("REST request to get all Okays");
        return okayRepository.findAll();
    }

    /**
     * GET  /okays/:id : get the "id" okay.
     *
     * @param id the id of the okay to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the okay, or with status 404 (Not Found)
     */
    @GetMapping("/okays/{id}")
    @Timed
    public ResponseEntity<Okay> getOkay(@PathVariable Long id) {
        log.debug("REST request to get Okay : {}", id);
        Okay okay = okayRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(okay));
    }

    /**
    * GET  /okays/:id : get the "id" okay.
    *
    * @param id the id of the okay to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the okay, or with status 404 (Not Found)
    */
    @GetMapping("/numberofokeys/{id}")
    @Timed
    public List<Long> getNumberOfOkayOfFeed(@PathVariable Long id) {
        List<Long> numberOflikes = new ArrayList<Long>();
        log.debug("REST request to get Okay : {}", id);
        Long numberOfokay = okayRepository.countByFeedId(id);
        numberOflikes.add(numberOfokay);
        return numberOflikes;
    }

    /**
     * GET  /okays : get all the okays.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of okays in body
     */
    @GetMapping("/okaysoffeed/{id}")
    @Timed
    public List<Okay> getAllOkaysOfFeed(@PathVariable Long id) {
        log.debug("REST request to get all Okays");
        List<Okay> okays = okayRepository.findAllByFeedId(id);
        return okays;
    }

    /**
    * GET  /okays/:id : get the "id" okay.
    *
    * @param id the id of the okay to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the okay, or with status 404 (Not Found)
    */
    @GetMapping("/okayprofileandfeed/{feed_id}/{profile_id}")
    @Timed
    public Okay getOkay(@PathVariable Long feed_id, @PathVariable Long profile_id) {
        log.debug("REST request to get Okay : {}");
        Okay okay = okayRepository.findOneByFeedIdAndProfileId(feed_id, profile_id);
        return okay;
    }

    /**
     * DELETE  /okays/:id : delete the "id" okay.
     *
     * @param id the id of the okay to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/okays/{id}")
    @Timed
    public ResponseEntity<Void> deleteOkay(@PathVariable Long id) {
        log.debug("REST request to delete Okay : {}", id);
        okayRepository.delete(id);
        //okaySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/okays?query=:query : search for the okay corresponding
     * to the query.
     *
     * @param query the query of the okay search
     * @return the result of the search
     */
    @GetMapping("/_search/okays")
    @Timed
    public List<Okay> searchOkays(@RequestParam String query) {
        log.debug("REST request to search Okays for query {}", query);
        return StreamSupport.stream(okaySearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

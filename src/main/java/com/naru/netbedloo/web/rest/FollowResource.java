package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.search.FollowSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Follow.
 */
@RestController
@RequestMapping("/api")
public class FollowResource {

    private final Logger log = LoggerFactory.getLogger(FollowResource.class);

    private static final String ENTITY_NAME = "follow";

    private final FollowRepository followRepository;

    private final FollowSearchRepository followSearchRepository;

    private final FeedRepository feedRepository;

    public FollowResource(FollowRepository followRepository, FollowSearchRepository followSearchRepository,
            FeedRepository feedRepository) {
        this.followRepository = followRepository;
        this.followSearchRepository = followSearchRepository;
        this.feedRepository = feedRepository;
    }

    /**
     * POST  /follows : Create a new follow.
     *
     * @param follow the follow to create
     * @return the ResponseEntity with status 201 (Created) and with body the new follow, or with status 400 (Bad Request) if the follow has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/follows")
    @Timed
    public ResponseEntity<Follow> createFollow(@RequestBody Follow follow) throws URISyntaxException {
        log.debug("REST request to save Follow : {}", follow);
        if (follow.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new follow cannot already have an ID"))
                    .body(null);
        }
        Set<Profile> visibility = new HashSet<>();
        List<Follow> followed = followRepository.findAllByFollowed_Id(follow.getFollower().getId());
        Feed feed = new Feed();
        follow.setFollowTime(Instant.now());
        Follow result = followRepository.save(follow);
        new Thread() {
            @Override
            public void run() {

                List<Feed> feeds = feedRepository.findAllByOwner_Id(follow.getFollowed().getId());
                if (feed != null) {
                    for (Feed feedy : feeds) {
                        Set<Profile> visibleTo = feedy.getVisibleTos();
                        visibleTo.add(follow.getFollower());
                        feedRepository.save(feedy);
                    }
                }
            }
        }.start();
        if (followed != null) {
            for (Follow follw : followed) {
                Profile profile = follw.getFollower();
                visibility.add(profile);
            }
        }

        feed.setVisibleTos(visibility);
        feed.setOwner(result.getFollower());
        feed.setTime(Instant.now());
        feed.setType(10);
        feed.setStatut(true);
        feed.setFollow(follow);
        feedRepository.save(feed);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/follows/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /follows : Updates an existing follow.
     *
     * @param follow the follow to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated follow,
     * or with status 400 (Bad Request) if the follow is not valid,
     * or with status 500 (Internal Server Error) if the follow couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/follows")
    @Timed
    public ResponseEntity<Follow> updateFollow(@RequestBody Follow follow) throws URISyntaxException {
        log.debug("REST request to update Follow : {}", follow);
        if (follow.getId() == null) {
            return createFollow(follow);
        }
        Follow result = followRepository.save(follow);
        // followSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, follow.getId().toString()))
                .body(result);
    }

    /**
     * GET  /follows : get all the follows.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of follows in body
     */
    @GetMapping("/follows")
    @Timed
    public ResponseEntity<List<Follow>> getAllFollows(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Follows");
        Page<Follow> page = followRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/follows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /follows/:id : get the "id" follow.
     *
     * @param id the id of the follow to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the follow, or with status 404 (Not Found)
     */
    @GetMapping("/follows/{profileId}")
    @Timed
    public List<Follow> getFollowByProfileId(@PathVariable Long profileId) {
        List<Follow> following = followRepository.findAllByFollower_Id(profileId);
        return following;
    }

    /*  @GetMapping("/follows/{profileId}/{name}")
    @Timed
    public List<Follow> getFollowByProfileIdByName(@PathVariable Long profileId,@PathVariable String name) {
        List<Follow> following = followRepository.findAllByFollower_IdAndFollowed_FirstNameContainingIgnoreCaseOrFollower_IdAndFollowed_LastNameContainingIgnoreCase(profileId,name);
        return following;
    }
    */
    @GetMapping("/followeds/{profileId}")
    @Timed
    public List<Follow> getFollowedByProfileId(@PathVariable Long profileId) {
        List<Follow> following = followRepository.findAllByFollowed_Id(profileId);
        return following;
    }

    /**
     * DELETE  /follows/:id : delete the "id" follow.
     *
     * @param id the id of the follow to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/follows/{id}")
    @Timed
    public ResponseEntity<Void> deleteFollow(@PathVariable Long id) {
        log.debug("REST request to delete Follow : {}", id);
        Feed feed = feedRepository.findOneByFollowId(id);
        if (feed != null) {
            feedRepository.delete(feed);
        }
        followRepository.delete(id);

        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/follows?query=:query : search for the follow corresponding
     * to the query.
     *
     * @param query the query of the follow search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/follows")
    @Timed
    public ResponseEntity<List<Follow>> searchFollows(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Follows for query {}", query);
        Page<Follow> page = followSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/follows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}

package com.naru.netbedloo.web.rest;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class AngularJsForwardController {
    // problem in this line
@RequestMapping(value = {"/{[path:[^\\.]*}","profile/*","article/*","video-game/*","book/*","feed/*"})
    public String redirect() {
        return "forward:/";
    }

}
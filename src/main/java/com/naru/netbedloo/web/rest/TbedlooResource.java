package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Tbedloo;

import com.naru.netbedloo.repository.TbedlooRepository;
import com.naru.netbedloo.repository.search.TbedlooSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tbedloo.
 */
@RestController
@RequestMapping("/api")
public class TbedlooResource {

    private final Logger log = LoggerFactory.getLogger(TbedlooResource.class);

    private static final String ENTITY_NAME = "tbedloo";
        
    private final TbedlooRepository tbedlooRepository;

    private final TbedlooSearchRepository tbedlooSearchRepository;

    public TbedlooResource(TbedlooRepository tbedlooRepository, TbedlooSearchRepository tbedlooSearchRepository) {
        this.tbedlooRepository = tbedlooRepository;
        this.tbedlooSearchRepository = tbedlooSearchRepository;
    }

    /**
     * POST  /tbedloos : Create a new tbedloo.
     *
     * @param tbedloo the tbedloo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tbedloo, or with status 400 (Bad Request) if the tbedloo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tbedloos")
    @Timed
    public ResponseEntity<Tbedloo> createTbedloo(@RequestBody Tbedloo tbedloo) throws URISyntaxException {
        log.debug("REST request to save Tbedloo : {}", tbedloo);
        if (tbedloo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tbedloo cannot already have an ID")).body(null);
        }
        Tbedloo result = tbedlooRepository.save(tbedloo);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tbedloos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tbedloos : Updates an existing tbedloo.
     *
     * @param tbedloo the tbedloo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tbedloo,
     * or with status 400 (Bad Request) if the tbedloo is not valid,
     * or with status 500 (Internal Server Error) if the tbedloo couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tbedloos")
    @Timed
    public ResponseEntity<Tbedloo> updateTbedloo(@RequestBody Tbedloo tbedloo) throws URISyntaxException {
        log.debug("REST request to update Tbedloo : {}", tbedloo);
        if (tbedloo.getId() == null) {
            return createTbedloo(tbedloo);
        }
        Tbedloo result = tbedlooRepository.save(tbedloo);
        // followSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tbedloo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tbedloos : get all the tbedloos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tbedloos in body
     */
    @GetMapping("/tbedloos")
    @Timed
    public List<Tbedloo> getAllTbedloos() {
        log.debug("REST request to get all Tbedloos");
        List<Tbedloo> tbedloos = tbedlooRepository.findAll();
        return tbedloos;
    }

    /**
     * GET  /tbedloos/:id : get the "id" tbedloo.
     *
     * @param id the id of the tbedloo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tbedloo, or with status 404 (Not Found)
     */
    @GetMapping("/tbedloos/{id}")
    @Timed
    public ResponseEntity<Tbedloo> getTbedloo(@PathVariable Long id) {
        log.debug("REST request to get Tbedloo : {}", id);
        Tbedloo tbedloo = tbedlooRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tbedloo));
    }

    /**
     * DELETE  /tbedloos/:id : delete the "id" tbedloo.
     *
     * @param id the id of the tbedloo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tbedloos/{id}")
    @Timed
    public ResponseEntity<Void> deleteTbedloo(@PathVariable Long id) {
        log.debug("REST request to delete Tbedloo : {}", id);
        tbedlooRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tbedloos?query=:query : search for the tbedloo corresponding
     * to the query.
     *
     * @param query the query of the tbedloo search 
     * @return the result of the search
     */
    @GetMapping("/_search/tbedloos")
    @Timed
    public List<Tbedloo> searchTbedloos(@RequestParam String query) {
        log.debug("REST request to search Tbedloos for query {}", query);
        return StreamSupport
            .stream(tbedlooSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}

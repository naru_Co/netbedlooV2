package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Comment;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Subject;
import com.naru.netbedloo.repository.CommentRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.SubjectRepository;
import com.naru.netbedloo.repository.search.CommentSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Comment.
 */
@RestController
@RequestMapping("/api")
public class CommentResource {

    private final Logger log = LoggerFactory.getLogger(CommentResource.class);

    private static final String ENTITY_NAME = "comment";

    private final CommentRepository commentRepository;

    private final CommentSearchRepository commentSearchRepository;

    private final SubjectRepository subjectRepository;

    private final FeedRepository feedRepository;

    public CommentResource(CommentRepository commentRepository, CommentSearchRepository commentSearchRepository,
            SubjectRepository subjectRepository, FeedRepository feedRepository) {
        this.commentRepository = commentRepository;
        this.commentSearchRepository = commentSearchRepository;
        this.subjectRepository = subjectRepository;
        this.feedRepository = feedRepository;
    }

    /**
     * POST  /comments : Create a new comment.
     *
     * @param comment the comment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new comment, or with status 400 (Bad Request) if the comment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comments")
    @Timed
    public ResponseEntity<Comment> createComment(@RequestBody Comment comment) throws URISyntaxException {
        log.debug("REST request to save Comment : {}", comment);
        if (comment.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new comment cannot already have an ID"))
                    .body(null);
        }
        if (comment.getFeed() != null) {
            if (comment.getFeed().getArticle() != null) {
                Subject subject = subjectRepository.findOneByForumIdAndProfileIdAndPublication(
                        comment.getFeed().getArticle().getForum().getId(), comment.getFeed().getOwner().getId(),
                        comment.getFeed().getContent());
                comment.setSubject(subject);
            }
        }
        if (comment.getSubject() != null) {
            Feed feed = feedRepository.findOneByArticleIdAndOwnerIdAndContent(
                    comment.getSubject().getForum().getArticle().getId(), comment.getSubject().getProfile().getId(),
                    comment.getSubject().getPublication());
            if (feed != null) {
                comment.setFeed(feed);
            }
        }
        comment.setPublicationTime(Instant.now());
        Comment result = commentRepository.save(comment);
        // commentSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/comments/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /comments : Updates an existing comment.
     *
     * @param comment the comment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated comment,
     * or with status 400 (Bad Request) if the comment is not valid,
     * or with status 500 (Internal Server Error) if the comment couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comments")
    @Timed
    public ResponseEntity<Comment> updateComment(@RequestBody Comment comment) throws URISyntaxException {
        log.debug("REST request to update Comment : {}", comment);
        if (comment.getId() == null) {
            return createComment(comment);
        }
        Comment result = commentRepository.save(comment);
        // commentSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, comment.getId().toString()))
                .body(result);
    }

    /**
     * GET  /comments : get all the comments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of comments in body
     */
    @GetMapping("/comments")
    @Timed
    public ResponseEntity<List<Comment>> getAllComments(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Comments");
        Page<Comment> page = commentRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /comments/:id : get the "id" comment.
     *
     * @param id the id of the comment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
     */
    @GetMapping("/comments/{id}")
    @Timed
    public ResponseEntity<Comment> getComment(@PathVariable Long id) {
        log.debug("REST request to get Comment : {}", id);
        Comment comment = commentRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(comment));
    }

    /**
    * GET  /commentsbysubject/:id : get the "id" comment.
    *
    * @param id the id of the subject  to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
    */
    @GetMapping("/commentsbysubject/{id}")
    @Timed
    public List<Comment> getCommentsBySubject(@PathVariable Long id) {
        log.debug("REST request to get Comment: {}", id);
        List<Comment> comments = commentRepository.findAllBySubject_id(id);
        return comments;
    }

    /**
    * GET  /comments/:id : get the "id" comment.
    *
    * @param id the id of the comment to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
    */
    @GetMapping("/numberofcomments/{id}")
    @Timed
    public List<Long> getNumberOfFeeds(@PathVariable Long id) {
        List<Long> numberOfcm = new ArrayList<Long>();
        log.debug("REST request to get Comment: {}", id);
        Long numberOfComments = commentRepository.countAllByFeed_id(id);
        numberOfcm.add(numberOfComments);
        return numberOfcm;
    }

    /**
    * GET  /comments/:id : get the "id" comment.
    *
    * @param id the id of the comment to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
    */
    @GetMapping("/topcommentsfeed/{id}")
    @Timed
    public List<Comment> getTop5Comments(@PathVariable Long id) {
        log.debug("REST request to get Comment: {}", id);
        List<Comment> top5Comments = commentRepository.findTop5ByFeedIdOrderByPublicationTimeDesc(id);
        return top5Comments;
    }


     /**
    * GET  /comments/:id : get the "id" comment.
    *
    * @param id the id of the comment to retrieve
    * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
    */
    @GetMapping("/commentsoffeed/{id}")
    @Timed
    public ResponseEntity<List<Comment>> getCommentsOfFeed(@PathVariable Long id,@ApiParam Pageable pageable) {
        log.debug("REST request to get Comment: {}", id);
        Page<Comment> page = commentRepository.findAllByFeedId(id,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/commentsoffeed");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * DELETE  /comments/:id : delete the "id" comment.
     *
     * @param id the id of the comment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteComment(@PathVariable Long id) {
        log.debug("REST request to delete Comment : {}", id);
        commentRepository.delete(id);
        // commentSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/comments?query=:query : search for the comment corresponding
     * to the query.
     *
     * @param query the query of the comment search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/comments")
    @Timed
    public ResponseEntity<List<Comment>> searchComments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Comments for query {}", query);
        Page<Comment> page = commentSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.ProfileImage;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.search.ProfileSearchRepository;
import com.naru.netbedloo.service.ExistedProfileService;
import com.naru.netbedloo.service.ImageResizeService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Profile.
 */
@RestController
@RequestMapping("/api")
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    private static final String ENTITY_NAME = "profile";
    @Inject
    private final ProfileRepository profileRepository;
    @Inject
    private final ProfileSearchRepository profileSearchRepository;

    @Inject
    private final ImageResizeService imageResizeService;

    @Inject
    private final ExistedProfileService existedProfileService;

    private static final int[] RGB_MASKS = { 0xFF0000, 0xFF00, 0xFF };
    private static final ColorModel RGB_OPAQUE = new DirectColorModel(32, RGB_MASKS[0], RGB_MASKS[1], RGB_MASKS[2]);

    public ProfileResource(ProfileRepository profileRepository, ProfileSearchRepository profileSearchRepository,
            ImageResizeService imageResizeService, ExistedProfileService existedProfileService) {
        this.profileRepository = profileRepository;
        this.profileSearchRepository = profileSearchRepository;
        this.imageResizeService = imageResizeService;
        this.existedProfileService = existedProfileService;

    }

    /**
     * POST  /profiles : Create a new profile.
     *
     * @param profile the profile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new profile, or with status 400 (Bad Request) if the profile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @Autowired
    private HttpServletRequest request;

    @PostMapping(path = "/profiles", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
    @Timed
    public ResponseEntity<Profile> createProfile(@RequestBody ProfileImage profilewithimage) throws URISyntaxException {
        log.debug("REST request to save Profile : {}");
        Profile profile = new Profile();
        Profile result = new Profile();
        ResponseEntity exist = existedProfileService.CheckIfExist(profilewithimage);
        Profile profileImg = profilewithimage.getProfile();
        if (profileImg.getId() != null) {
            if (profilewithimage.getFile() == null) {
                profile.setImagePath("/content/images/comm_user.jpg");
                profile.setMediumImage("/content/images/comm_user.jpg");
                profile.setNormalImage("/content/images/comm_user.jpg");
                profile.setThumbnailImage("/content/images/comm_user.jpg");
            } else {
                String uploadsDir = "/app/profil_photos/";
                String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
                File file = new File("/app/");
                file.setExecutable(true);
                file.setReadable(true);
                file.setWritable(true);
                if (!new File(realPathtoUploads).exists()) {
                    new File(realPathtoUploads).mkdir();
                }

                long random = Math.round(Math.random() * 10);
                String orgName = Math.round(Math.random() * 100000) + "_"
                        + new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()) + ".jpeg";
                String orgNameLow = random + profilewithimage.getProfile().getFirstName()
                        + profilewithimage.getProfile().getLastName() + "_" + "lowResImage" + ".jpeg";
                String filePath = realPathtoUploads + "/" + orgName;
                String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
                String mdPath = realPathtoUploads + "/" + "md_" + orgName;
                String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
                File dest = new File(filePath);
                profile.setImagePath("/app/profil_photos/" + orgName);
                profile.setThumbnailImage("/app/profil_photos/tmbl_" + orgName);
                profile.setMediumImage("/app/profil_photos/md_" + orgName);
                profile.setNormalImage("/app/profil_photos/nr_" + orgName);
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            new FileOutputStream(dest).write(profilewithimage.getFile());
                            imageResizeService.thumbailSizeforProfileImage(filePath, tmblPath);
                            imageResizeService.mediumSizeforProfileImage(filePath, mdPath);
                            imageResizeService.normalSizeforProfileImage(filePath, nrPath);
                        } catch (Exception e) {
                        }
                    }
                }.start();
            }

            profile.setId(profileImg.getId());
            profile.setPseudo(profileImg.getPseudo());
            profile.setPlayerId(profileImg.getPlayerId());
            profile.setFirstName(profileImg.getFirstName());
            profile.setLastName(profileImg.getLastName());
            profile.setBirthDate(profileImg.getBirthDate());
            profile.setSexe(profileImg.getSexe());
            profile.setAdress(profileImg.getAdress());
            profile.setZipCode(profileImg.getZipCode());
            profile.setEmail(profileImg.getEmail());
            profile.setPhoneNumber(profileImg.getPhoneNumber());
            profile.setBio(profileImg.getBio());
            profile.setGouv(profileImg.getGouv());
            profile.setCountry(profileImg.getCountry());

            result = profileRepository.save(profile);
            return ResponseEntity.created(new URI("/api/profiles/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
        } else {
            if (exist == null) {
                if (profilewithimage.getFile() == null) {
                    profile.setImagePath("/content/images/comm_user.jpg");
                    profile.setMediumImage("/content/images/comm_user.jpg");
                    profile.setNormalImage("/content/images/comm_user.jpg");
                    profile.setThumbnailImage("/content/images/comm_user.jpg");
                } else {
                    String uploadsDir = "/app/profil_photos/";
                    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
                    File file = new File("/app/");
                    file.setExecutable(true);
                    file.setReadable(true);
                    file.setWritable(true);
                    if (!new File(realPathtoUploads).exists()) {
                        new File(realPathtoUploads).mkdir();
                    }

                    long random = Math.round(Math.random() * 10);
                    String orgName = Math.round(Math.random() * 100000) + "_"
                            + new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()) + ".jpeg";
                    String orgNameLow = random + profilewithimage.getProfile().getFirstName()
                            + profilewithimage.getProfile().getLastName() + "_" + "lowResImage" + ".jpeg";
                    String filePath = realPathtoUploads + "/" + orgName;
                    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
                    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
                    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
                    File dest = new File(filePath);
                    profile.setImagePath("/app/profil_photos/" + orgName);
                    profile.setThumbnailImage("/app/profil_photos/tmbl_" + orgName);
                    profile.setMediumImage("/app/profil_photos/md_" + orgName);
                    profile.setNormalImage("/app/profil_photos/nr_" + orgName);
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                new FileOutputStream(dest).write(profilewithimage.getFile());
                                imageResizeService.thumbailSizeforProfileImage(filePath, tmblPath);
                                imageResizeService.mediumSizeforProfileImage(filePath, mdPath);
                                imageResizeService.normalSizeforProfileImage(filePath, nrPath);
                            } catch (Exception e) {
                            }
                        }
                    }.start();
                }

                if (profileImg.getId() != null) {
                    profile.setId(profileImg.getId());
                    profile.setPseudo(profileImg.getPseudo());
                } else {
                    List<Profile> profileswithnames = profileRepository.findAllByFirstNameAndLastName(
                            profileImg.getFirstName().replaceAll("[^A-Za-z0-9]", " "),
                            profileImg.getLastName().replaceAll("[^A-Za-z0-9]", " "));
                    if (profileswithnames.size() == 0) {
                        profile.setPseudo(profileImg.getFirstName().toLowerCase().replaceAll("[^A-Za-z0-9]", "") + "_"
                                + profileImg.getLastName().toLowerCase().replaceAll("[^A-Za-z0-9]", ""));
                    } else {
                        int newsize = profileswithnames.size();
                        profile.setPseudo(profileImg.getFirstName().toLowerCase().replaceAll("[^A-Za-z0-9]", "") + "_"
                                + profileImg.getLastName().toLowerCase().replaceAll("[^A-Za-z0-9]", "") + "_"
                                + newsize);
                    }
                }
                profile.setPlayerId(profileImg.getPlayerId());
                profile.setFirstName(profileImg.getFirstName());
                profile.setLastName(profileImg.getLastName());
                profile.setBirthDate(profileImg.getBirthDate());
                profile.setSexe(profileImg.getSexe());
                profile.setAdress(profileImg.getAdress());
                profile.setZipCode(profileImg.getZipCode());
                profile.setEmail(profileImg.getEmail());
                profile.setPhoneNumber(profileImg.getPhoneNumber());
                profile.setBio(profileImg.getBio());
                profile.setGouv(profileImg.getGouv());
                profile.setCountry(profileImg.getCountry());

                result = profileRepository.save(profile);
                return ResponseEntity.created(new URI("/api/profiles/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                        .body(result);
            } else {

                return exist;
            }
        }
        // followSearchRepository.save(result);

    }

    /**
     * PUT  /profiles : Updates an existing profile.
     *
     * @param profile the profile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated profile,
     * or with status 400 (Bad Request) if the profile is not valid,
     * or with status 500 (Internal Server Error) if the profile couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/profiles")
    @Timed
    public ResponseEntity<Profile> updateProfile(@RequestBody Profile profile) throws URISyntaxException {
        log.debug("REST request to update Profile : {}", profile);
        if (profile.getId() == null) {
            ProfileImage profilewithimage = new ProfileImage();
            return createProfile(profilewithimage);
        }
        Profile result = profileRepository.save(profile);
        // followSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, profile.getId().toString()))
                .body(result);
    }

    /**
     * GET  /profiles : get all the profiles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of profiles in body
     */
    @GetMapping("/profiles")
    @Timed
    public List<Profile> getAllProfiles() {
        log.debug("REST request to get all Profiles");
        List<Profile> profiles = profileRepository.findAll();
        return profiles;
    }

    @GetMapping("/profilesbyname/{lastname}/{firstname}")
    public List<Profile> getAllProfilesByName(@PathVariable String last, @PathVariable String first) {
        log.debug("REST request to get all Profiles By name");
        List<Profile> profiles = profileRepository
                .findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(last, first);
        return profiles;
    }

    @GetMapping("/profilesbypseudo/{pseudo}")
    @Timed
    public List<Profile> getAllProfilesByPseudo(@PathVariable String pseudo) {
        log.debug("REST request to get all Profiles By name");
        List<Profile> profiles = profileRepository.findAllByPseudoContainingIgnoreCase(pseudo);
        return profiles;
    }

    /**
     * GET  /profiles/:id : get the "id" profile.
     *
     * @param id the id of the profile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the profile, or with status 404 (Not Found)
     */
    @GetMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<Profile> getProfile(@PathVariable Long id) {
        log.debug("REST request to get Profile : {}", id);
        Profile profile = profileRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profile));
    }

    /**
     * GET  /profiles/:id : get the "id" profile.
     *
     * @param id the id of the profile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the profile, or with status 404 (Not Found)
     */
    @GetMapping("/profilebypseudo/{pseudo}")
    @Timed
    public ResponseEntity<Profile> getProfileByPseudo(@PathVariable String pseudo) {
        log.debug("REST request to get Profile : {}", pseudo);
        Profile profile = profileRepository.findOneByPseudo(pseudo);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profile));
    }

    /**
     * DELETE  /profiles/:id : delete the "id" profile.
     *
     * @param id the id of the profile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteProfile(@PathVariable Long id) {
        log.debug("REST request to delete Profile : {}", id);
        profileRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/profiles?query=:query : search for the profile corresponding
     * to the query.
     *
     * @param query the query of the profile search 
     * @return the result of the search
     */
    @GetMapping("/_search/profiles")
    @Timed
    public List<Profile> searchProfiles(@RequestParam String query) {
        log.debug("REST request to search Profiles for query {}", query);
        return StreamSupport.stream(profileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.search.OwnedHistorySearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing OwnedHistory.
 */
@RestController
@RequestMapping("/api")
public class OwnedHistoryResource {

    private final Logger log = LoggerFactory.getLogger(OwnedHistoryResource.class);

    private static final String ENTITY_NAME = "ownedHistory";
        
    private final OwnedHistoryRepository ownedHistoryRepository;

    private final OwnedHistorySearchRepository ownedHistorySearchRepository;

    public OwnedHistoryResource(OwnedHistoryRepository ownedHistoryRepository, OwnedHistorySearchRepository ownedHistorySearchRepository) {
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.ownedHistorySearchRepository = ownedHistorySearchRepository;
    }

    /**
     * POST  /owned-histories : Create a new ownedHistory.
     *
     * @param ownedHistory the ownedHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ownedHistory, or with status 400 (Bad Request) if the ownedHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/owned-histories")
    @Timed
    public ResponseEntity<OwnedHistory> createOwnedHistory(@RequestBody OwnedHistory ownedHistory) throws URISyntaxException {
        log.debug("REST request to save OwnedHistory : {}", ownedHistory);
        if (ownedHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ownedHistory cannot already have an ID")).body(null);
        }
        OwnedHistory result = ownedHistoryRepository.save(ownedHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/owned-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /owned-histories : Updates an existing ownedHistory.
     *
     * @param ownedHistory the ownedHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ownedHistory,
     * or with status 400 (Bad Request) if the ownedHistory is not valid,
     * or with status 500 (Internal Server Error) if the ownedHistory couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/owned-histories")
    @Timed
    public ResponseEntity<OwnedHistory> updateOwnedHistory(@RequestBody OwnedHistory ownedHistory) throws URISyntaxException {
        log.debug("REST request to update OwnedHistory : {}", ownedHistory);
        if (ownedHistory.getId() == null) {
            return createOwnedHistory(ownedHistory);
        }
        OwnedHistory result = ownedHistoryRepository.save(ownedHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ownedHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /owned-histories : get all the ownedHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ownedHistories in body
     */
    @GetMapping("/owned-histories")
    @Timed
    public ResponseEntity<List<OwnedHistory>> getAllOwnedHistories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of OwnedHistories");
        Page<OwnedHistory> page = ownedHistoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/owned-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /owned-histories/:id : get the "id" ownedHistory.
     *
     * @param id the id of the ownedHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ownedHistory, or with status 404 (Not Found)
     */
    @GetMapping("/owned-histories/{id}")
    @Timed
    public ResponseEntity<OwnedHistory> getOwnedHistory(@PathVariable Long id) {
        log.debug("REST request to get OwnedHistory : {}", id);
        OwnedHistory ownedHistory = ownedHistoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ownedHistory));
    }

    /**
     * DELETE  /owned-histories/:id : delete the "id" ownedHistory.
     *
     * @param id the id of the ownedHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/owned-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteOwnedHistory(@PathVariable Long id) {
        log.debug("REST request to delete OwnedHistory : {}", id);
        ownedHistoryRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/owned-histories?query=:query : search for the ownedHistory corresponding
     * to the query.
     *
     * @param query the query of the ownedHistory search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/owned-histories")
    @Timed
    public ResponseEntity<List<OwnedHistory>> searchOwnedHistories(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of OwnedHistories for query {}", query);
        Page<OwnedHistory> page = ownedHistorySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/owned-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Signal;
import com.naru.netbedloo.repository.SignalRepository;
import com.naru.netbedloo.repository.search.SignalSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Signal.
 */
@RestController
@RequestMapping("/api")
public class SignalResource {

    private final Logger log = LoggerFactory.getLogger(SignalResource.class);

    private static final String ENTITY_NAME = "signal";

    private final SignalRepository signalRepository;

    private final SignalSearchRepository signalSearchRepository;

    public SignalResource(SignalRepository signalRepository, SignalSearchRepository signalSearchRepository) {
        this.signalRepository = signalRepository;
        this.signalSearchRepository = signalSearchRepository;
    }

    /**
     * POST  /signals : Create a new signal.
     *
     * @param signal the signal to create
     * @return the ResponseEntity with status 201 (Created) and with body the new signal, or with status 400 (Bad Request) if the signal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/signals")
    @Timed
    public ResponseEntity<Signal> createSignal(@RequestBody Signal signal) throws URISyntaxException {
        log.debug("REST request to save Signal : {}", signal);
        if (signal.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new signal cannot already have an ID")).body(null);
        }
        signal.setDate(ZonedDateTime.now());
        Signal result = signalRepository.save(signal);
        signalSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/signals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /signals : Updates an existing signal.
     *
     * @param signal the signal to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated signal,
     * or with status 400 (Bad Request) if the signal is not valid,
     * or with status 500 (Internal Server Error) if the signal couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/signals")
    @Timed
    public ResponseEntity<Signal> updateSignal(@RequestBody Signal signal) throws URISyntaxException {
        log.debug("REST request to update Signal : {}", signal);
        if (signal.getId() == null) {
            return createSignal(signal);
        }
        Signal result = signalRepository.save(signal);
        signalSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, signal.getId().toString()))
            .body(result);
    }

    /**
     * GET  /signals : get all the signals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of signals in body
     */
    @GetMapping("/signals")
    @Timed
    public ResponseEntity<List<Signal>> getAllSignals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Signals");
        Page<Signal> page = signalRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/signals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /signals/:id : get the "id" signal.
     *
     * @param id the id of the signal to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the signal, or with status 404 (Not Found)
     */
    @GetMapping("/signals/{id}")
    @Timed
    public ResponseEntity<Signal> getSignal(@PathVariable Long id) {
        log.debug("REST request to get Signal : {}", id);
        Signal signal = signalRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(signal));
    }

    /**
     * DELETE  /signals/:id : delete the "id" signal.
     *
     * @param id the id of the signal to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/signals/{id}")
    @Timed
    public ResponseEntity<Void> deleteSignal(@PathVariable Long id) {
        log.debug("REST request to delete Signal : {}", id);
        signalRepository.delete(id);
        signalSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/signals?query=:query : search for the signal corresponding
     * to the query.
     *
     * @param query the query of the signal search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/signals")
    @Timed
    public ResponseEntity<List<Signal>> searchSignals(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Signals for query {}", query);
        Page<Signal> page = signalSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/signals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}

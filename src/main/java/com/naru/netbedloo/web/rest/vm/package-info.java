/**
 * View Models used by Spring MVC REST controllers.
 */
package com.naru.netbedloo.web.rest.vm;

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Subject;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.SubjectRepository;
import com.naru.netbedloo.repository.search.SubjectSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Subject.
 */
@RestController
@RequestMapping("/api")
public class SubjectResource {

    private final Logger log = LoggerFactory.getLogger(SubjectResource.class);

    private static final String ENTITY_NAME = "subject";

    private final SubjectRepository subjectRepository;

    private final SubjectSearchRepository subjectSearchRepository;

    private final FeedRepository feedRepository;

    public SubjectResource(SubjectRepository subjectRepository, SubjectSearchRepository subjectSearchRepository,FeedRepository feedRepository) {
        this.subjectRepository = subjectRepository;
        this.subjectSearchRepository = subjectSearchRepository;
        this.feedRepository=feedRepository;
    }

    /**
     * POST  /subjects : Create a new subject.
     *
     * @param subject the subject to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subject, or with status 400 (Bad Request) if the subject has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subjects")
    @Timed
    public ResponseEntity<Subject> createSubject(@RequestBody Subject subject) throws URISyntaxException {
        log.debug("REST request to save Subject : {}", subject);
        if (subject.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new subject cannot already have an ID"))
                    .body(null);
        }
        Feed feed = new Feed();

        Article article = subject.getForum().getArticle();
        subject.setPublicationTime(Instant.now());
        subjectRepository.save(subject);
        feed.setOwner(subject.getProfile());
        feed.setArticle(article);
        feed.setContent(subject.getPublication());
        feed.setTime(Instant.now());
        feed.setStatut(true);
        if (article.getCategory().getId() == 1101) {
            feed.setType(5);
        } else {
            feed.setType(6);
        }
        feedRepository.save(feed);

        Subject result = subjectRepository.save(subject);
        subjectSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/subjects/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /subjects : Updates an existing subject.
     *
     * @param subject the subject to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subject,
     * or with status 400 (Bad Request) if the subject is not valid,
     * or with status 500 (Internal Server Error) if the subject couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subjects")
    @Timed
    public ResponseEntity<Subject> updateSubject(@RequestBody Subject subject) throws URISyntaxException {
        log.debug("REST request to update Subject : {}", subject);
        if (subject.getId() == null) {
            return createSubject(subject);
        }
        Subject result = subjectRepository.save(subject);
        subjectSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subject.getId().toString()))
                .body(result);
    }

    /**
     * GET  /subjects : get all the subjects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subjects in body
     */
    @GetMapping("/subjects")
    @Timed
    public ResponseEntity<List<Subject>> getAllSubjects(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Subjects");
        Page<Subject> page = subjectRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subjects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /subjects/:id : get the "id" subject.
     *
     * @param id the id of the subject to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subject, or with status 404 (Not Found)
     */
    @GetMapping("/subjects/{id}")
    @Timed
    public ResponseEntity<Subject> getSubject(@PathVariable Long id) {
        log.debug("REST request to get Subject : {}", id);
        Subject subject = subjectRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subject));
    }

    @GetMapping("/subjectsbyforum/{id}")
    @Timed
    public List<Subject> getSubjectsByForum(@PathVariable Long id) {
        log.debug("REST request to get subject: {}", id);
        List<Subject> subjects = subjectRepository.findAllByForum_id(id);
        return subjects;
    }

    /**
     * DELETE  /subjects/:id : delete the "id" subject.
     *
     * @param id the id of the subject to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subjects/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubject(@PathVariable Long id) {
        log.debug("REST request to delete Subject : {}", id);
        subjectRepository.delete(id);
        subjectSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/subjects?query=:query : search for the subject corresponding
     * to the query.
     *
     * @param query the query of the subject search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/subjects")
    @Timed
    public ResponseEntity<List<Subject>> searchSubjects(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Subjects for query {}", query);
        Page<Subject> page = subjectSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/subjects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}

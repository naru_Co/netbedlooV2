package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Response;
import com.naru.netbedloo.repository.ResponseRepository;
import com.naru.netbedloo.repository.search.ResponseSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Response.
 */
@RestController
@RequestMapping("/api")
public class ResponseResource {

    private final Logger log = LoggerFactory.getLogger(ResponseResource.class);

    private static final String ENTITY_NAME = "response";
        
    private final ResponseRepository responseRepository;

    private final ResponseSearchRepository responseSearchRepository;

    public ResponseResource(ResponseRepository responseRepository, ResponseSearchRepository responseSearchRepository) {
        this.responseRepository = responseRepository;
        this.responseSearchRepository = responseSearchRepository;
    }

    /**
     * POST  /responses : Create a new response.
     *
     * @param response the response to create
     * @return the ResponseEntity with status 201 (Created) and with body the new response, or with status 400 (Bad Request) if the response has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/responses")
    @Timed
    public ResponseEntity<Response> createResponse(@RequestBody Response response) throws URISyntaxException {
        log.debug("REST request to save Response : {}", response);
        if (response.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new response cannot already have an ID")).body(null);
        }
        response.setResponseTime(Instant.now());
        Response result = responseRepository.save(response);
        responseSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/responses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /responses : Updates an existing response.
     *
     * @param response the response to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated response,
     * or with status 400 (Bad Request) if the response is not valid,
     * or with status 500 (Internal Server Error) if the response couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/responses")
    @Timed
    public ResponseEntity<Response> updateResponse(@RequestBody Response response) throws URISyntaxException {
        log.debug("REST request to update Response : {}", response);
        if (response.getId() == null) {
            return createResponse(response);
        }
        Response result = responseRepository.save(response);
        responseSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, response.getId().toString()))
            .body(result);
    }

    /**
     * GET  /responses : get all the responses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of responses in body
     */
    @GetMapping("/responses")
    @Timed
    public ResponseEntity<List<Response>> getAllResponses(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Responses");
        Page<Response> page = responseRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/responses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /responses/:id : get the "id" response.
     *
     * @param id the id of the response to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the response, or with status 404 (Not Found)
     */
    @GetMapping("/responses/{id}")
    @Timed
    public ResponseEntity<Response> getResponse(@PathVariable Long id) {
        log.debug("REST request to get Response : {}", id);
        Response response = responseRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(response));
    }
     @GetMapping("/responsesbycomment/{id}")
    @Timed
    public List<Response> getResponsesByComment(@PathVariable Long id) {
        log.debug("REST request to get Response: {}", id);
        List<Response> subjects = responseRepository.findAllByComment_Id(id);
        return subjects;
    }

    /**
     * DELETE  /responses/:id : delete the "id" response.
     *
     * @param id the id of the response to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/responses/{id}")
    @Timed
    public ResponseEntity<Void> deleteResponse(@PathVariable Long id) {
        log.debug("REST request to delete Response : {}", id);
        responseRepository.delete(id);
        responseSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/responses?query=:query : search for the response corresponding
     * to the query.
     *
     * @param query the query of the response search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/responses")
    @Timed
    public ResponseEntity<List<Response>> searchResponses(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Responses for query {}", query);
        Page<Response> page = responseSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/responses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}

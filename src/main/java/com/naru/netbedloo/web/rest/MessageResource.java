package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.EkherHkeya;
import com.naru.netbedloo.domain.Message;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.repository.MessageRepository;
import com.naru.netbedloo.repository.search.MessageSearchRepository;
import com.naru.netbedloo.service.CurrentProfileService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Message.
 */
@RestController
@RequestMapping("/api")
public class MessageResource {

    private final Logger log = LoggerFactory.getLogger(MessageResource.class);

    private static final String ENTITY_NAME = "message";

    private final MessageRepository messageRepository;

    private final MessageSearchRepository messageSearchRepository;

    private final CurrentProfileService currentProfileService;

    public MessageResource(MessageRepository messageRepository, MessageSearchRepository messageSearchRepository,
            CurrentProfileService currentProfileService) {
        this.messageRepository = messageRepository;
        this.messageSearchRepository = messageSearchRepository;
        this.currentProfileService = currentProfileService;

    }

    /**
     * POST /messages : Create a new message.
     *
     * @param message the message to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         message, or with status 400 (Bad Request) if the message has already
     *         an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/messages")
    @Timed
    public ResponseEntity<Message> createMessage(@Valid @RequestBody Message message) throws URISyntaxException {
        log.debug("REST request to save Message : {}", message);
        if (message.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new message cannot already have an ID"))
                    .body(null);
        }
        Message result = messageRepository.save(message);
        messageSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/messages/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /messages : Updates an existing message.
     *
     * @param message the message to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         message, or with status 400 (Bad Request) if the message is not
     *         valid, or with status 500 (Internal Server Error) if the message
     *         couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/messages")
    @Timed
    public ResponseEntity<Message> updateMessage(@Valid @RequestBody Message message) throws URISyntaxException {
        log.debug("REST request to update Message : {}", message);
        if (message.getId() == null) {
            return createMessage(message);
        }
        message.setSeenDate(Instant.now());
        Message result = messageRepository.save(message);
        messageSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, message.getId().toString()))
                .body(result);
    }

    /**
     * GET /messages : get all the messages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of messages in
     *         body
     */

    @GetMapping("/private-messages/{uid}")
    @Timed
    public ResponseEntity<List<Message>> getAllMessagesByUid(@PathVariable String uid, @ApiParam Pageable pageable) {
        log.debug("REST request to get all Messages");
        Page<Message> page = messageRepository.findByUid(uid, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/private-messages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }

    @GetMapping("/privateconversation/{uid}")
    @Timed
    public List<Message> getAllMessagesByUid(@PathVariable String uid) {
        log.debug("REST request to get all Messages");
        List<Message> messages = messageRepository.findAllByUid(uid);

        return messages;

    }

    @GetMapping("/see-private-messages/{uid}/{id}")
    @Timed
    public void updateMessageToSeen(@PathVariable String uid, @PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to update Message : {}");
        List<Message> unseenMessages = messageRepository.findBySeenDateIsNullAndUidAndReceiver_Id(uid, id);
        for (Message message : unseenMessages) {
            message.setSeenDate(Instant.now());
            messageRepository.save(message);
        }

    }

    @GetMapping("/talkingTo")
    @Timed
    public Set<EkherHkeya> GetAllTalkers() throws URISyntaxException {
        log.debug("REST request to update Message : {}");
        Profile profile = currentProfileService.getCurrentProfile();
        Set<EkherHkeya> people = new HashSet<>();
        List<Message> sentMessage = messageRepository.findAllBySenderId(profile.getId());
        List<Message> receivedMessage = messageRepository.findAllByReceiverId(profile.getId());
        Set<Profile> profiles = new HashSet<>();
        for (Message message : sentMessage) {
            Profile receiver = message.getReceiver();
            profiles.add(receiver);
        }
        for (Message message : receivedMessage) {
            Profile sender = message.getSender();
            profiles.add(sender);

        }
        for (Profile talker : profiles) {
            EkherHkeya hkeya = new EkherHkeya();
            hkeya.setProfile(talker);
            long unseen = messageRepository.countAllBySeenDateIsNullAndReceiverIdAndSenderId(profile.getId(),talker.getId());
            hkeya.setUnsen(unseen);
            if (talker.getId() > profile.getId()) {
                String uid = talker.getId() + "naru" + profile.getId();
                Message lastMessage = messageRepository.findFirstByUidOrderByIdDesc(uid);
                hkeya.setMessage(lastMessage);
                hkeya.setId(lastMessage.getId());
                hkeya.setCreationDate(lastMessage.getCreationDate());
                if (lastMessage.getSender().getId() == talker.getId()) {
                    hkeya.setType((long) 0);
                } else {
                    hkeya.setType((long) 1);
                }
            } else {
                String uid = profile.getId() + "naru" + talker.getId();
                Message lastMessage = messageRepository.findFirstByUidOrderByIdDesc(uid);
                hkeya.setMessage(lastMessage);
                hkeya.setCreationDate(lastMessage.getCreationDate());
                if (lastMessage.getSender().getId() == talker.getId()) {
                    hkeya.setType((long) 0);
                } else {
                    hkeya.setType((long) 1);
                }
            }

            if (!people.contains(hkeya)) {
                people.add(hkeya);
            }
        }

        return people;

    }

    @GetMapping("/lastMessage/{uid}")
    @Timed
    public Message GetLastMessage(@PathVariable String uid) throws URISyntaxException {
        log.debug("REST request to update Message : {}");
        Message message = messageRepository.findFirstByUidOrderByIdDesc(uid);
        return message;
    }

    @GetMapping("/unseen-private-messages-number/{id}")
    @Timed
    public List<Message> getNumberOfUnseenMessagesByUid(@PathVariable Long id) {
        log.debug("REST request number of unseen messages");

        List<Message> unseenMessages = messageRepository.findBySeenDateIsNullAndReceiver_Id(id);
        return unseenMessages;
    }

    @GetMapping("/messages")
    @Timed
    public ResponseEntity<List<Message>> getAllMessages(@ApiParam Pageable pageable) {
        log.debug("REST request to get all Messages");
        Page<Message> page = messageRepository.findAll(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/messages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /messages/:id : get the "id" message.
     *
     * @param id the id of the message to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the message, or
     *         with status 404 (Not Found)
     */
    @GetMapping("/messages/{id}")
    @Timed
    public ResponseEntity<Message> getMessage(@PathVariable Long id) {
        log.debug("REST request to get Message : {}", id);
        Message message = messageRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(message));
    }

    /**
     * DELETE /messages/:id : delete the "id" message.
     *
     * @param id the id of the message to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/messages/{id}")
    @Timed
    public ResponseEntity<Void> deleteMessage(@PathVariable Long id) {
        log.debug("REST request to delete Message : {}", id);
        messageRepository.delete(id);
        messageSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH /_search/messages?query=:query : search for the message corresponding
     * to the query.
     *
     * @param query the query of the message search
     * @return the result of the search
     */
    @GetMapping("/_search/messages")
    @Timed
    public List<Message> searchMessages(@RequestParam String query) {
        log.debug("REST request to search Messages for query {}", query);
        return StreamSupport.stream(messageSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

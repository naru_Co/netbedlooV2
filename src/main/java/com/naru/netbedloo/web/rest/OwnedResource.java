package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Owned;
import com.naru.netbedloo.domain.OwnedHistory;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Wanted;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.OwnedHistoryRepository;
import com.naru.netbedloo.repository.OwnedRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.WantedRepository;
import com.naru.netbedloo.repository.search.OwnedSearchRepository;
import com.naru.netbedloo.service.MatchingService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Owned.
 */
@RestController
@RequestMapping("/api")
public class OwnedResource {

    private final Logger log = LoggerFactory.getLogger(OwnedResource.class);

    private static final String ENTITY_NAME = "owned";

    private final OwnedRepository ownedRepository;

    private final MatchingService matchingService;

    private final OwnedHistoryRepository ownedHistoryRepository;

    private final OwnedSearchRepository ownedSearchRepository;

    private final ArticleRepository articleRepository;

    private final WantedRepository wantedRepository;

    private final WantedHistoryRepository wantedHistoryRepository;

    private final FeedRepository feedRepository;

    private final FollowRepository followRepository;

    private final ProfileRepository profileRepository;

    public OwnedResource(OwnedRepository ownedRepository, MatchingService matchingService,
            OwnedHistoryRepository ownedHistoryRepository, OwnedSearchRepository ownedSearchRepository,
            ArticleRepository articleRepository, WantedRepository wantedRepository,
            WantedHistoryRepository wantedHistoryRepository, FeedRepository feedRepository,
            FollowRepository followRepository, ProfileRepository profileRepository) {
        this.ownedRepository = ownedRepository;
        this.matchingService = matchingService;
        this.ownedHistoryRepository = ownedHistoryRepository;
        this.ownedSearchRepository = ownedSearchRepository;
        this.articleRepository = articleRepository;
        this.wantedRepository = wantedRepository;
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.feedRepository = feedRepository;
        this.followRepository = followRepository;
        this.profileRepository = profileRepository;
    }

    /**
     * POST /owneds : Create a new owned.
     *
     * @param owned the owned to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         owned, or with status 400 (Bad Request) if the owned has already an
     *         ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/owneds")
    @Timed
    public ResponseEntity<Owned> createOwned(@RequestBody Owned owned) throws URISyntaxException {
        log.debug("REST request to save Owned : {}", owned);
        if (owned.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new owned cannot already have an ID"))
                    .body(null);
        }

        OwnedHistory ownedHistory = new OwnedHistory();
        Profile owner = owned.getOwner();
        Article checker = owned.getArticle();
        List<Owned> check = ownedRepository.findAllByOwner_IdAndArticle_Id(owner.getId(), checker.getId());
        Wanted wanted = wantedRepository.findOneByWanter_IdAndArticle_Id(owner.getId(), checker.getId());
        WantedHistory wantedHistory = wantedHistoryRepository
                .findOneByWanter_idAndArticle_idAndEraseDateNull(owner.getId(), checker.getId());
        if (wanted != null) {
            wantedRepository.delete(wanted.getId());
        }
        if (wantedHistory != null) {
            wantedHistory.setEraseDate(Instant.now());
            wantedHistoryRepository.save(wantedHistory);
        }

        for (Owned chiko : check) {
            deleteOwned(chiko.getId());
        }
        Feed feed = new Feed();
        Set<Profile> visibility = new HashSet<>();
        owned.setCreationDate(Instant.now());
        Owned result = ownedRepository.save(owned);
        System.out.println("the saved result is " + result);
        new Thread() {
            @Override
            public void run() {
                List<Follow> followed = followRepository.findAllByFollowed_Id(owner.getId());

                Article article = result.getArticle();
                long popularity = article.getPopularity();
                long updatedpopularity = popularity + 1;
                article.setPopularity(updatedpopularity);
                articleRepository.save(article);
                ownedHistory.setOwner(result.getOwner());
                ownedHistory.setArticle(result.getArticle());
                ownedHistory.setOwningdate(Instant.now());
                ownedHistoryRepository.save(ownedHistory);
                matchingService.matchingforOwned(result);
                feed.setOwner(result.getOwner());
                if (followed != null) {
                    for (Follow follw : followed) {
                        Profile profile = follw.getFollower();
                        visibility.add(profile);
                    }
                }
                feed.setTime(Instant.now());
                if (article.getCategory().getId() == 1101) {
                    feed.setType(1);
                } else {
                    feed.setType(9);
                }

                feed.setStatut(true);
                feed.setOwned(ownedHistory);
                feed.setVisibleTos(visibility);
                feedRepository.save(feed);

            }
        }.start();

        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/owneds/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);

    }

    /**
     * PUT /owneds : Updates an existing owned.
     *
     * @param owned the owned to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         owned, or with status 400 (Bad Request) if the owned is not valid, or
     *         with status 500 (Internal Server Error) if the owned couldnt be
     *         updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/owneds")
    @Timed
    public ResponseEntity<Owned> updateOwned(@RequestBody Owned owned) throws URISyntaxException {
        log.debug("REST request to update Owned : {}", owned);
        if (owned.getId() == null) {
            return createOwned(owned);
        }
        Owned result = ownedRepository.save(owned);
        // followSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, owned.getId().toString()))
                .body(result);
    }

    /**
     * GET /owneds : get all the owneds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of owneds in
     *         body
     */
    @GetMapping("/owneds")
    @Timed
    public List<Owned> getAllOwneds() {
        log.debug("REST request to get all Owneds");
        List<Owned> owneds = ownedRepository.findAll();
        return owneds;
    }

    /**
     * GET /owneds/:id : get the "id" owned.
     *
     * @param id the id of the owned to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the owned, or
     *         with status 404 (Not Found)
     */
    @GetMapping("/owneds/{id}")
    @Timed
    public List<Owned> getOwned(@PathVariable Long id) {
        log.debug("REST request to get Owned : {}", id);
        List<Owned> owneds = ownedRepository.findAllWithArticleByOwner_Id(id);
        return owneds;
    }

    /**
     * GET /owneds/:id : get the "id" owned.
     *
     * @param id the id of the owned to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the owned, or
     *         with status 404 (Not Found)
     */
    @GetMapping("/ownedbooks/{id}")
    @Timed
    public List<Owned> getOwnedBooks(@PathVariable Long id) {
        log.debug("REST request to get Owned : {}", id);
        List<Owned> owneds = ownedRepository.findAllWithArticleByOwner_Id(id);
        List<Owned> books = new ArrayList<Owned>();
        for (Owned owned : owneds) {
            Article article = owned.getArticle();
            if (article.getCategory().getId() == 1101) {
                books.add(owned);
            }
        }
        return books;
    }

    @GetMapping("/ownedgames/{id}")
    @Timed
    public List<Owned> getOwnedGames(@PathVariable Long id) {
        log.debug("REST request to get Owned : {}", id);
        List<Owned> owneds = ownedRepository.findAllWithArticleByOwner_Id(id);
        List<Owned> games = new ArrayList<Owned>();
        for (Owned owned : owneds) {
            Article article = owned.getArticle();
            if (article.getCategory().getId() == 1102) {
                games.add(owned);
            }
        }
        return games;
    }

    /**
     * GET /owneds/:id : get the "id" owned.
     *
     * @param id the id of the owned to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the owned, or
     *         with status 404 (Not Found)
     */
    @GetMapping("/ownedsbyarticle/{id}")
    @Timed
    public List<Owned> getOwnedByArticle(@PathVariable Long id) {
        log.debug("REST request to get Owned : {}", id);
        List<Owned> owneds = ownedRepository.findAllWithOwnerByArticle_Id(id);

        return owneds;
    }

    /**
     * DELETE /owneds/:id : delete the "id" owned.
     *
     * @param id the id of the owned to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/owneds/{id}")
    @Timed
    public ResponseEntity<Void> deleteOwned(@PathVariable Long id) {
        log.debug("REST request to delete Owned : {}", id);

        Owned owned = ownedRepository.findOne(id);
        Article article = owned.getArticle();

        ownedRepository.delete(id);
        // followSearchRepository.delete(id);
        new Thread() {
            @Override
            public void run() {
                OwnedHistory ownedHistory = ownedHistoryRepository.findOneByOwner_idAndArticle_idAndEraseDateNull(
                        owned.getOwner().getId(), owned.getArticle().getId());
                matchingService.deletematchingforOwned(owned);
                Set<Profile> visibility = new HashSet<>();

                List<Follow> followed = followRepository.findAllByFollowed_Id(owned.getOwner().getId());
                Feed feed = new Feed();
                Feed existedfeed = feedRepository.findOneByOwnedId(ownedHistory.getId());
                if (existedfeed != null) {
                }

                if (existedfeed != null) {
                    Set<Profile> visibleTo = existedfeed.getVisibleTos();
                    existedfeed.setTime(Instant.now());
                    if (article.getCategory().getId() == 1101) {
                        existedfeed.setType(11);
                    } else {
                        existedfeed.setType(99);
                    }
                    if (visibleTo != null) {
                        if (followed != null) {
                            for (Follow follw : followed) {
                                Profile profile = follw.getFollower();
                                if (!visibleTo.contains(profile)) {
                                    existedfeed.addVisibleTo(profile);
                                }
                                ;
                            }
                        }

                    } else {
                        if (followed != null) {
                            for (Follow follw : followed) {
                                Profile profile = follw.getFollower();
                                visibility.add(profile);
                            }

                        }
                        existedfeed.setVisibleTos(visibility);
                    }

                    existedfeed.setStatut(true);

                    feedRepository.save(existedfeed);

                } else {

                    feed.setOwner(owned.getOwner());
                    feed.setTime(Instant.now());
                    if (article.getCategory().getId() == 1101) {
                        feed.setType(11);
                    } else {
                        feed.setType(99);
                    }
                    if (followed != null) {
                        for (Follow follw : followed) {
                            Profile profile = follw.getFollower();
                            visibility.add(profile);
                        }
                    }
                    feed.setStatut(true);
                    feed.setOwned(ownedHistory);
                    feed.setVisibleTos(visibility);
                    feedRepository.save(feed);
                }
            }
        }.start();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

    }

    /**
     * SEARCH /_search/owneds?query=:query : search for the owned corresponding to
     * the query.
     *
     * @param query the query of the owned search
     * @return the result of the search
     */
    @GetMapping("/_search/owneds")
    @Timed
    public List<Owned> searchOwneds(@RequestParam String query) {
        log.debug("REST request to search Owneds for query {}", query);
        return StreamSupport.stream(ownedSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

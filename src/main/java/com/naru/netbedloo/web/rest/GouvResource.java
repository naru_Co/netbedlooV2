package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Gouv;

import com.naru.netbedloo.repository.GouvRepository;
import com.naru.netbedloo.repository.search.GouvSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Gouv.
 */
@RestController
@RequestMapping("/api")
public class GouvResource {

    private final Logger log = LoggerFactory.getLogger(GouvResource.class);

    private static final String ENTITY_NAME = "gouv";
        
    private final GouvRepository gouvRepository;

    private final GouvSearchRepository gouvSearchRepository;

    public GouvResource(GouvRepository gouvRepository, GouvSearchRepository gouvSearchRepository) {
        this.gouvRepository = gouvRepository;
        this.gouvSearchRepository = gouvSearchRepository;
    }

    /**
     * POST  /gouvs : Create a new gouv.
     *
     * @param gouv the gouv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gouv, or with status 400 (Bad Request) if the gouv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gouvs")
    @Timed
    public ResponseEntity<Gouv> createGouv(@RequestBody Gouv gouv) throws URISyntaxException {
        log.debug("REST request to save Gouv : {}", gouv);
        if (gouv.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new gouv cannot already have an ID")).body(null);
        }
        Gouv result = gouvRepository.save(gouv);
        gouvSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/gouvs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gouvs : Updates an existing gouv.
     *
     * @param gouv the gouv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gouv,
     * or with status 400 (Bad Request) if the gouv is not valid,
     * or with status 500 (Internal Server Error) if the gouv couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gouvs")
    @Timed
    public ResponseEntity<Gouv> updateGouv(@RequestBody Gouv gouv) throws URISyntaxException {
        log.debug("REST request to update Gouv : {}", gouv);
        if (gouv.getId() == null) {
            return createGouv(gouv);
        }
        Gouv result = gouvRepository.save(gouv);
        gouvSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gouv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gouvs : get all the gouvs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of gouvs in body
     */
    @GetMapping("/gouvs")
    @Timed
    public List<Gouv> getAllGouvs() {
        log.debug("REST request to get all Gouvs");
        List<Gouv> gouvs = gouvRepository.findAll();
        return gouvs;
    }

    /**
     * GET  /gouvs/:id : get the "id" gouv.
     *
     * @param id the id of the gouv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gouv, or with status 404 (Not Found)
     */
    @GetMapping("/gouvs/{id}")
    @Timed
    public ResponseEntity<Gouv> getGouv(@PathVariable Long id) {
        log.debug("REST request to get Gouv : {}", id);
        Gouv gouv = gouvRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gouv));
    }
     @GetMapping("/gouvsbycountry/{id}")
    @Timed
    public List<Gouv> getGouvsByCountry(@PathVariable Long id) {
        log.debug("REST request to get States : {}", id);
        List<Gouv> gouvs = gouvRepository.findAllByCountry_Id(id);
        return gouvs;
    }
    @GetMapping("/gouvbyname/{name}")
    @Timed
    public Gouv getGouvsByName(@PathVariable String name) {
        Gouv gouv = gouvRepository.findTop1ByName(name); 
        return gouv;
    }


    /**
     * DELETE  /gouvs/:id : delete the "id" gouv.
     *
     * @param id the id of the gouv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gouvs/{id}")
    @Timed
    public ResponseEntity<Void> deleteGouv(@PathVariable Long id) {
        log.debug("REST request to delete Gouv : {}", id);
        gouvRepository.delete(id);
        gouvSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/gouvs?query=:query : search for the gouv corresponding
     * to the query.
     *
     * @param query the query of the gouv search 
     * @return the result of the search
     */
    @GetMapping("/_search/gouvs")
    @Timed
    public List<Gouv> searchGouvs(@RequestParam String query) {
        log.debug("REST request to search Gouvs for query {}", query);
        return StreamSupport
            .stream(gouvSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}

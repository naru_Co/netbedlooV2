package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Spec;

import com.naru.netbedloo.repository.SpecRepository;
import com.naru.netbedloo.repository.search.SpecSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Spec.
 */
@RestController
@RequestMapping("/api")
public class SpecResource {

    private final Logger log = LoggerFactory.getLogger(SpecResource.class);

    private static final String ENTITY_NAME = "spec";

    private final SpecRepository specRepository;

    private final SpecSearchRepository specSearchRepository;

    public SpecResource(SpecRepository specRepository, SpecSearchRepository specSearchRepository) {
        this.specRepository = specRepository;
        this.specSearchRepository = specSearchRepository;
    }

    /**
     * POST  /specs : Create a new spec.
     *
     * @param spec the spec to create
     * @return the ResponseEntity with status 201 (Created) and with body the new spec, or with status 400 (Bad Request) if the spec has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/specs")
    @Timed
    public ResponseEntity<Spec> createSpec(@RequestBody Spec spec) throws URISyntaxException {
        log.debug("REST request to save Spec : {}", spec);
        if (spec.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new spec cannot already have an ID")).body(null);
        }
        Spec result = specRepository.save(spec);
        specSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/specs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specs : Updates an existing spec.
     *
     * @param spec the spec to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated spec,
     * or with status 400 (Bad Request) if the spec is not valid,
     * or with status 500 (Internal Server Error) if the spec couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/specs")
    @Timed
    public ResponseEntity<Spec> updateSpec(@RequestBody Spec spec) throws URISyntaxException {
        log.debug("REST request to update Spec : {}", spec);
        if (spec.getId() == null) {
            return createSpec(spec);
        }
        Spec result = specRepository.save(spec);
        specSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, spec.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specs : get all the specs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of specs in body
     */
    @GetMapping("/specs")
    @Timed
    public List<Spec> getAllSpecs() {
        log.debug("REST request to get all Specs");
        return specRepository.findAll();
    }
     /**
     * GET  /specs : get all the specs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of specs in body
     */
    @GetMapping("/specsofcategory/{category_id}")
    @Timed
    public List<Spec> getAllSpecsByCategory(@PathVariable Long category_id) {
        log.debug("REST request to get all Specs for category" , category_id);
        List<Spec> specs = specRepository.findAllByCategoryId(category_id);
        return specs;
    }
     /**
     * GET  /specs : get all the specs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of specs in body
     */
    @GetMapping("/specsbycategory/{category_id}/{name}")
    @Timed
    public List<Spec> getAllSpecsByCategoryAndName(@PathVariable Long category_id,@PathVariable String name) {
        log.debug("REST request to get all Specs for category" , category_id);
        List<Spec> specs = specRepository.findAllByCategoryIdAndNameContainingIgnoreCase(category_id,name);
        return specs;
    }
    
      /**
     * GET  /specs : get all the specs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of specs in body
     */
    @GetMapping("/playstation")
    @Timed
    public Spec getPlaystation() {
        long category_id=1102;
        long id=37;
        log.debug("REST request to get all Specs for category" , category_id);
        Spec specs = specRepository.findOneByCategoryIdAndId(category_id,id);
        return specs;
    }

    /**
     * GET  /specs/:id : get the "id" spec.
     *
     * @param id the id of the spec to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the spec, or with status 404 (Not Found)
     */
    @GetMapping("/specs/{id}")
    @Timed
    public ResponseEntity<Spec> getSpec(@PathVariable Long id) {
        log.debug("REST request to get Spec : {}", id);
        Spec spec = specRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(spec));
    }
   

    /**
     * DELETE  /specs/:id : delete the "id" spec.
     *
     * @param id the id of the spec to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/specs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSpec(@PathVariable Long id) {
        log.debug("REST request to delete Spec : {}", id);
        specRepository.delete(id);
        specSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/specs?query=:query : search for the spec corresponding
     * to the query.
     *
     * @param query the query of the spec search
     * @return the result of the search
     */
    @GetMapping("/_search/specs")
    @Timed
    public List<Spec> searchSpecs(@RequestParam String query) {
        log.debug("REST request to search Specs for query {}", query);
        return StreamSupport
            .stream(specSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}

package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Matched;
import com.naru.netbedloo.service.TbedlooService;

import com.naru.netbedloo.repository.MatchedRepository;
import com.naru.netbedloo.repository.search.MatchedSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Matched.
 */
@RestController
@RequestMapping("/api")
public class MatchedResource {

    private final Logger log = LoggerFactory.getLogger(MatchedResource.class);

    private static final String ENTITY_NAME = "matched";
        
    private final MatchedRepository matchedRepository;

    private final MatchedSearchRepository matchedSearchRepository;

    private final TbedlooService tbedlooService;
    public MatchedResource(MatchedRepository matchedRepository, MatchedSearchRepository matchedSearchRepository,TbedlooService tbedlooService) {
        this.matchedRepository = matchedRepository;
        this.matchedSearchRepository = matchedSearchRepository;
        this.tbedlooService = tbedlooService;
    }

    /**
     * POST  /matcheds : Create a new matched.
     *
     * @param matched the matched to create
     * @return the ResponseEntity with status 201 (Created) and with body the new matched, or with status 400 (Bad Request) if the matched has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/matcheds")
    @Timed
    public ResponseEntity<Matched> createMatched(@RequestBody Matched matched) throws URISyntaxException {
        log.debug("REST request to save Matched : {}", matched);
        if (matched.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new matched cannot already have an ID")).body(null);
        }
        Matched result = matchedRepository.save(matched);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/matcheds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /matcheds : Updates an existing matched.
     *
     * @param matched the matched to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated matched,
     * or with status 400 (Bad Request) if the matched is not valid,
     * or with status 500 (Internal Server Error) if the matched couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/matcheds")
    @Timed
    public ResponseEntity<Matched> updateMatched(@RequestBody Matched matched) throws URISyntaxException {
        log.debug("REST request to update Matched : {}", matched);
       matched.setId(matched.getId());
    Matched result = matchedRepository.save(matched);
       // matchedSearchRepository.save(result);
       if(matched.isStatut()){
        tbedlooService.tbedlooSayee(result);}
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, matched.getId().toString()))
            .body(matched);
    }

    /**
     * GET  /matcheds : get all the matcheds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of matcheds in body
     */
    @GetMapping("/matcheds")
    @Timed
    public List<Matched> getAllMatcheds() {
        log.debug("REST request to get all Matcheds");
        List<Matched> matcheds = matchedRepository.findAll();
        return matcheds;
    }

    /**
     * GET  /matcheds/:id : get the "id" matched.
     *
     * @param id the id of the matched to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the matched, or with status 404 (Not Found)
     */
    @GetMapping("/matcheds/{id}")
    @Timed
    public ResponseEntity<Matched> getMatched(@PathVariable Long id) {
        log.debug("REST request to get Matched : {}", id);
        Matched matched = matchedRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(matched));
    }

    @GetMapping("/matchedsByProfile/{id}")
    @Timed
    public ResponseEntity<List<Matched>> getMatchedByProfile(@PathVariable Long id) {
        log.debug("REST request to get Matched : {}", id);
        List<Matched> matcheds = matchedRepository.findAllByStatutIsFalseAndOwner_IdOrWanter_Id(id,id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(matcheds));
    }

    /**
     * DELETE  /matcheds/:id : delete the "id" matched.
     *
     * @param id the id of the matched to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/matcheds/{id}")
    @Timed
    public ResponseEntity<Void> deleteMatched(@PathVariable Long id) {
        log.debug("REST request to delete Matched : {}", id);
        matchedRepository.delete(id);
        //matchedSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/matcheds?query=:query : search for the matched corresponding
     * to the query.
     *
     * @param query the query of the matched search 
     * @return the result of the search
     */
    @GetMapping("/_search/matcheds")
    @Timed
    public List<Matched> searchMatcheds(@RequestParam String query) {
        log.debug("REST request to search Matcheds for query {}", query);
        return StreamSupport
            .stream(matchedSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}

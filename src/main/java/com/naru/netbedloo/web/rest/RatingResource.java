package com.naru.netbedloo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Rating;
import com.naru.netbedloo.repository.ArticleRepository;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.RatingRepository;
import com.naru.netbedloo.repository.search.RatingSearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Rating.
 */
@RestController
@RequestMapping("/api")
public class RatingResource {

    private final Logger log = LoggerFactory.getLogger(RatingResource.class);

    private static final String ENTITY_NAME = "rating";

    private final RatingRepository ratingRepository;

    private final RatingSearchRepository ratingSearchRepository;

    private final ArticleRepository articleRepository;

    private final FeedRepository feedRepository;

    private final FollowRepository followRepository;
    

    public RatingResource(RatingRepository ratingRepository, RatingSearchRepository ratingSearchRepository,
            ArticleRepository articleRepository,FeedRepository feedRepository,FollowRepository followRepository) {
        this.ratingRepository = ratingRepository;
        this.ratingSearchRepository = ratingSearchRepository;
        this.articleRepository = articleRepository;
        this.feedRepository=feedRepository;
        this.followRepository=followRepository;
        
    }

    /**
     * POST  /ratings : Create a new rating.
     *
     * @param rating the rating to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rating, or with status 400 (Bad Request) if the rating has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ratings")
    @Timed
    public ResponseEntity<Rating> createRating(@RequestBody Rating rating) throws URISyntaxException {
        log.debug("REST request to save Rating : {}", rating);
        if (rating.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new rating cannot already have an ID"))
                    .body(null);
        }
        rating.setRatingTime(Instant.now());
        Rating result = ratingRepository.save(rating);
      
        Feed feed= new Feed();
        Article article = rating.getRated();
        Set<Profile> visibility = new HashSet<>();
        List<Rating> ratings = ratingRepository.findAllByRatedId(article.getId());
        List<Follow> followed = followRepository.findAllByFollowed_Id(result.getRater().getId());
        
        new Thread() {
            @Override
            public void run() {
                int type=30;
                if (followed != null) {
                    for (Follow follw : followed) {
                       Profile profile = follw.getFollower();
                        visibility.add(profile);
                    }
                }
                feed.setVisibleTos(visibility);
                feed.setOwner(result.getRater());
                feed.setRating(result);
                feed.setTime(Instant.now());
                feed.setType(type);
                feedRepository.save(feed);
               
            }
        }.start();
       
        int rate = 0; 
        double length = ratings.size() + 1;
        int newrate = result.getRate();
        for (Rating ratex : ratings) {
            int x = ratex.getRate();

            rate = x + rate;
        }
        int allrates = newrate + rate;
        double average = allrates / (double) length;

        Long popularity = article.getPopularity();
        Long newpopularity = popularity + 1;
        article.setAverageRate(average);
        article.setPopularity(newpopularity);
        articleRepository.save(article);

        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/ratings/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /ratings : Updates an existing rating.
     *
     * @param rating the rating to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rating,
     * or with status 400 (Bad Request) if the rating is not valid,
     * or with status 500 (Internal Server Error) if the rating couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ratings")
    @Timed
    public ResponseEntity<Rating> updateRating(@RequestBody Rating rating) throws URISyntaxException {
        log.debug("REST request to update Rating : {}", rating);
        if (rating.getId() == null) {
            return createRating(rating);
        }
       
        Feed feed=feedRepository.findOneByRatingId(rating.getId());
        feed.setTime(Instant.now());      
        feedRepository.save(feed);
        rating.setRatingTime(Instant.now());
        Rating result = ratingRepository.save(rating);
       
        // followSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rating.getId().toString()))
                .body(result);
    }

    /**
     * GET  /ratings : get all the ratings.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ratings in body
     */
    @GetMapping("/ratings")
    @Timed
    public List<Rating> getAllRatings() {
        log.debug("REST request to get all Ratings");
        List<Rating> ratings = ratingRepository.findAll();
        return ratings;
    }

    @GetMapping("/rateofarticle/{rated_id}")
    @Timed
    public List<Double> getTheRateOfArticle(@PathVariable Long rated_id) {
        log.debug("REST request to get the rate of article", rated_id);
        List<Double> ratex = new ArrayList<Double>();
        int rate = 0;
        List<Rating> ratings = ratingRepository.findAllByRatedId(rated_id);
        double length = ratings.size();
        double fiveStars = 0;
        double fourstars = 0;
        double threestars = 0;
        double twostars = 0;
        double onestar = 0;
        ratex.add(length);
        for (Rating rating : ratings) {
            int x = rating.getRate();
            if (x == 5) {
                fiveStars = fiveStars + 1;
            } else if (x == 4) {
                fourstars = fourstars + 1;
            } else if (x == 3) {
                threestars = threestars + 1;
            } else if (x == 2) {
                twostars = twostars + 1;
            } else if (x == 1) {
                onestar = onestar + 1;
            }
            rate = x + rate;
        }
        double result = rate / (double) length;
        ratex.add(result);
        ratex.add(fiveStars);
        ratex.add(fourstars);
        ratex.add(threestars);
        ratex.add(twostars);
        ratex.add(onestar);
        return ratex;
    }

    @GetMapping("/rateofarticlebyperson/{rated_id}/{rater_id}")
    @Timed
    public Rating getTheRateOfArticleByPerson(@PathVariable Long rated_id, @PathVariable Long rater_id) {
        log.debug("REST request to get Rating of", rater_id);
        Rating rating = ratingRepository.findOneByRatedIdAndRaterId(rated_id, rater_id);
        return rating;
    }

    /**
     * GET  /ratings/:id : get the "id" rating.
     *
     * @param id the id of the rating to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rating, or with status 404 (Not Found)
     */
    @GetMapping("/ratings/{id}")
    @Timed
    public ResponseEntity<Rating> getRating(@PathVariable Long id) {
        log.debug("REST request to get Rating : {}", id);
        Rating rating = ratingRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rating));
    }

    /**
     * DELETE  /ratings/:id : delete the "id" rating.
     *
     * @param id the id of the rating to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ratings/{id}")
    @Timed
    public ResponseEntity<Void> deleteRating(@PathVariable Long id) {
        log.debug("REST request to delete Rating : {}", id);
        ratingRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/ratings?query=:query : search for the rating corresponding
     * to the query.
     *
     * @param query the query of the rating search 
     * @return the result of the search
     */
    @GetMapping("/_search/ratings")
    @Timed
    public List<Rating> searchRatings(@RequestParam String query) {
        log.debug("REST request to search Ratings for query {}", query);
        return StreamSupport.stream(ratingSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

}

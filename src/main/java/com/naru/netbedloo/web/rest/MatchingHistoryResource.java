package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.MatchingHistory;
import com.naru.netbedloo.repository.MatchingHistoryRepository;
import com.naru.netbedloo.repository.search.MatchingHistorySearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing MatchingHistory.
 */
@RestController
@RequestMapping("/api")
public class MatchingHistoryResource {

    private final Logger log = LoggerFactory.getLogger(MatchingHistoryResource.class);

    private static final String ENTITY_NAME = "matchingHistory";
        
    private final MatchingHistoryRepository matchingHistoryRepository;

    private final MatchingHistorySearchRepository matchingHistorySearchRepository;

    public MatchingHistoryResource(MatchingHistoryRepository matchingHistoryRepository, MatchingHistorySearchRepository matchingHistorySearchRepository) {
        this.matchingHistoryRepository = matchingHistoryRepository;
        this.matchingHistorySearchRepository = matchingHistorySearchRepository;
    }

    /**
     * POST  /matching-histories : Create a new matchingHistory.
     *
     * @param matchingHistory the matchingHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new matchingHistory, or with status 400 (Bad Request) if the matchingHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/matching-histories")
    @Timed
    public ResponseEntity<MatchingHistory> createMatchingHistory(@RequestBody MatchingHistory matchingHistory) throws URISyntaxException {
        log.debug("REST request to save MatchingHistory : {}", matchingHistory);
        if (matchingHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new matchingHistory cannot already have an ID")).body(null);
        }
        MatchingHistory result = matchingHistoryRepository.save(matchingHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/matching-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /matching-histories : Updates an existing matchingHistory.
     *
     * @param matchingHistory the matchingHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated matchingHistory,
     * or with status 400 (Bad Request) if the matchingHistory is not valid,
     * or with status 500 (Internal Server Error) if the matchingHistory couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/matching-histories")
    @Timed
    public ResponseEntity<MatchingHistory> updateMatchingHistory(@RequestBody MatchingHistory matchingHistory) throws URISyntaxException {
        log.debug("REST request to update MatchingHistory : {}", matchingHistory);
        if (matchingHistory.getId() == null) {
            return createMatchingHistory(matchingHistory);
        }
        MatchingHistory result = matchingHistoryRepository.save(matchingHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, matchingHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /matching-histories : get all the matchingHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of matchingHistories in body
     */
    @GetMapping("/matching-histories")
    @Timed
    public ResponseEntity<List<MatchingHistory>> getAllMatchingHistories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MatchingHistories");
        Page<MatchingHistory> page = matchingHistoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/matching-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/matchingByprofile/{owner_id}/{wanter_id}")
    @Timed
    public List<MatchingHistory> getAllMatchingHistoriesByProfile(@PathVariable Long owner_id,@PathVariable Long wanter_id) {
        log.debug("REST request to get a page of MatchingHistories");
        List<MatchingHistory> matcheds = matchingHistoryRepository.findAllByOwner_IdOrWanter_IdAndTbedlooDateNotNull(owner_id,wanter_id);
        return matcheds;
    }

    /**
     * GET  /matching-histories/:id : get the "id" matchingHistory.
     *
     * @param id the id of the matchingHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the matchingHistory, or with status 404 (Not Found)
     */
    @GetMapping("/matching-histories/{id}")
    @Timed
    public ResponseEntity<MatchingHistory> getMatchingHistory(@PathVariable Long id) {
        log.debug("REST request to get MatchingHistory : {}", id);
        MatchingHistory matchingHistory = matchingHistoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(matchingHistory));
    }

    /**
     * DELETE  /matching-histories/:id : delete the "id" matchingHistory.
     *
     * @param id the id of the matchingHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/matching-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteMatchingHistory(@PathVariable Long id) {
        log.debug("REST request to delete MatchingHistory : {}", id);
        matchingHistoryRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/matching-histories?query=:query : search for the matchingHistory corresponding
     * to the query.
     *
     * @param query the query of the matchingHistory search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/matching-histories")
    @Timed
    public ResponseEntity<List<MatchingHistory>> searchMatchingHistories(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MatchingHistories for query {}", query);
        Page<MatchingHistory> page = matchingHistorySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/matching-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}

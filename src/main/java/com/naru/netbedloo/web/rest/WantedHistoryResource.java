package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.WantedHistory;
import com.naru.netbedloo.repository.WantedHistoryRepository;
import com.naru.netbedloo.repository.search.WantedHistorySearchRepository;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing WantedHistory.
 */
@RestController
@RequestMapping("/api")
public class WantedHistoryResource {

    private final Logger log = LoggerFactory.getLogger(WantedHistoryResource.class);

    private static final String ENTITY_NAME = "wantedHistory";
        
    private final WantedHistoryRepository wantedHistoryRepository;

    private final WantedHistorySearchRepository wantedHistorySearchRepository;

    public WantedHistoryResource(WantedHistoryRepository wantedHistoryRepository, WantedHistorySearchRepository wantedHistorySearchRepository) {
        this.wantedHistoryRepository = wantedHistoryRepository;
        this.wantedHistorySearchRepository = wantedHistorySearchRepository;
    }

    /**
     * POST  /wanted-histories : Create a new wantedHistory.
     *
     * @param wantedHistory the wantedHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wantedHistory, or with status 400 (Bad Request) if the wantedHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wanted-histories")
    @Timed
    public ResponseEntity<WantedHistory> createWantedHistory(@RequestBody WantedHistory wantedHistory) throws URISyntaxException {
        log.debug("REST request to save WantedHistory : {}", wantedHistory);
        if (wantedHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new wantedHistory cannot already have an ID")).body(null);
        }
        WantedHistory result = wantedHistoryRepository.save(wantedHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/wanted-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wanted-histories : Updates an existing wantedHistory.
     *
     * @param wantedHistory the wantedHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wantedHistory,
     * or with status 400 (Bad Request) if the wantedHistory is not valid,
     * or with status 500 (Internal Server Error) if the wantedHistory couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wanted-histories")
    @Timed
    public ResponseEntity<WantedHistory> updateWantedHistory(@RequestBody WantedHistory wantedHistory) throws URISyntaxException {
        log.debug("REST request to update WantedHistory : {}", wantedHistory);
        if (wantedHistory.getId() == null) {
            return createWantedHistory(wantedHistory);
        }
        WantedHistory result = wantedHistoryRepository.save(wantedHistory);
        // followSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wantedHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wanted-histories : get all the wantedHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wantedHistories in body
     */
    @GetMapping("/wanted-histories")
    @Timed
    public ResponseEntity<List<WantedHistory>> getAllWantedHistories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WantedHistories");
        Page<WantedHistory> page = wantedHistoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wanted-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wanted-histories/:id : get the "id" wantedHistory.
     *
     * @param id the id of the wantedHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wantedHistory, or with status 404 (Not Found)
     */
    @GetMapping("/wanted-histories/{id}")
    @Timed
    public ResponseEntity<WantedHistory> getWantedHistory(@PathVariable Long id) {
        log.debug("REST request to get WantedHistory : {}", id);
        WantedHistory wantedHistory = wantedHistoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(wantedHistory));
    }

    /**
     * DELETE  /wanted-histories/:id : delete the "id" wantedHistory.
     *
     * @param id the id of the wantedHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wanted-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteWantedHistory(@PathVariable Long id) {
        log.debug("REST request to delete WantedHistory : {}", id);
        wantedHistoryRepository.delete(id);
        // followSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wanted-histories?query=:query : search for the wantedHistory corresponding
     * to the query.
     *
     * @param query the query of the wantedHistory search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wanted-histories")
    @Timed
    public ResponseEntity<List<WantedHistory>> searchWantedHistories(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WantedHistories for query {}", query);
        Page<WantedHistory> page = wantedHistorySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wanted-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}

package com.naru.netbedloo.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.naru.netbedloo.domain.Article;
import com.naru.netbedloo.domain.Feed;
import com.naru.netbedloo.domain.Follow;
import com.naru.netbedloo.domain.Profile;
import com.naru.netbedloo.domain.Subject;
import com.naru.netbedloo.repository.FeedRepository;
import com.naru.netbedloo.repository.FollowRepository;
import com.naru.netbedloo.repository.ProfileRepository;
import com.naru.netbedloo.repository.SubjectRepository;
import com.naru.netbedloo.repository.search.FeedSearchRepository;
import com.naru.netbedloo.service.CurrentProfileService;
import com.naru.netbedloo.web.rest.util.HeaderUtil;
import com.naru.netbedloo.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Feed.
 */
@RestController
@RequestMapping("/api")
public class FeedResource {

    private final Logger log = LoggerFactory.getLogger(FeedResource.class);

    private static final String ENTITY_NAME = "feed";

    private final FeedRepository feedRepository;

    private final FeedSearchRepository feedSearchRepository;

    private final SubjectRepository subjectRepository;

    private final FollowRepository followRepository;

    private final CurrentProfileService currentProfileService;

    private final ProfileRepository profileRepository;

    public FeedResource(FeedRepository feedRepository, FeedSearchRepository feedSearchRepository,
            SubjectRepository subjectRepository,CurrentProfileService currentProfileService,FollowRepository followRepository,ProfileRepository profileRepository) {
        this.feedRepository = feedRepository;
        this.feedSearchRepository = feedSearchRepository;
        this.subjectRepository = subjectRepository;
      this.followRepository=followRepository;
        this.currentProfileService = currentProfileService;
        this.profileRepository=profileRepository;
    }

    /**
     * POST  /feeds : Create a new feed.
     *
     * @param feed the feed to create
     * @return the ResponseEntity with status 201 (Created) and with body the new feed, or with status 400 (Bad Request) if the feed has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feeds")
    @Timed
    public ResponseEntity<Feed> createFeed(@RequestBody Feed feed) throws URISyntaxException {
        log.debug("REST request to save Feed : {}", feed);
        if (feed.getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new feed cannot already have an ID"))
                    .body(null);
        }
        
        List<Follow> followed = followRepository.findAllByFollowed_Id(feed.getOwner().getId());
        Set<Profile> visibility = new HashSet<>();
        if (feed.getArticle() != null) {
            Subject subject = new Subject();
            Article article = feed.getArticle();
            subject.setProfile(feed.getOwner());
            subject.setForum(article.getForum());
            subject.setPublication(feed.getContent());
            subject.setPublicationTime(Instant.now());
            subjectRepository.save(subject);
            if (article.getCategory().getId() == 1101) {
                feed.setType(5);
            } else {
                feed.setType(6);
            }
        } else {
            feed.setType(4);
        }
        if (followed != null) {
            for (Follow follw : followed) {
                Profile profile = follw.getFollower();
                visibility.add(profile);
            }
            visibility.add(feed.getOwner());
            feed.setVisibleTos(visibility);
        }
        feed.setStatut(true);
        feed.setTime(Instant.now());
        Feed result = feedRepository.save(feed);
        //feedSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/feeds/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /feeds : Updates an existing feed.
     *
     * @param feed the feed to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated feed,
     * or with status 400 (Bad Request) if the feed is not valid,
     * or with status 500 (Internal Server Error) if the feed couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/feeds")
    @Timed
    public ResponseEntity<Feed> updateFeed(@RequestBody Feed feed) throws URISyntaxException {
        log.debug("REST request to update Feed : {}", feed);
        if (feed.getId() == null) {
            return createFeed(feed);
        }
        Set<Profile> visibility= profileRepository.findAllVisibleToByFeedsId(feed.getId());
        if(visibility!=null){
        feed.setVisibleTos(visibility);}
        Feed result = feedRepository.save(feed);
        //feedSearchRepository.save(result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, feed.getId().toString()))
                .body(result);
    }

    /**
     * GET  /feeds : get all the feeds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of feeds in body
     */
    @GetMapping("/feeds")
    @Timed
    public ResponseEntity<List<Feed>> getAllFeeds(Pageable pageable) {
        log.debug("REST request to get a page of Feeds");
        Page<Feed> page = feedRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feeds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /feeds : get all the feeds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of feeds in body
     */
    @GetMapping("/myfeeds/{id}")
    @Timed
    public ResponseEntity<List<Feed>> getMyFeeds(@PathVariable Long id,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Owner's feeds");
        Page<Feed> page = feedRepository.findAllByOwnerId(id,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/myfeeds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


     /**
     * GET  /feeds : get all the feeds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of feeds in body
     */
    @GetMapping("/connectedfeeds/{id}")
    @Timed
    public ResponseEntity<List<Feed>> getFeedsOfConnectedPerson(@PathVariable Long id,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of feeds");
        Page<Feed> page = feedRepository.findAllByVisibleTosIdAndStatutIsTrueOrderByTimeDesc(id,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/connectedfeeds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    /**
     * GET  /feeds/:id : get the "id" feed.
     *
     * @param id the id of the feed to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the feed, or with status 404 (Not Found)
     */
    @GetMapping("/feeds/{id}")
    @Timed
    public ResponseEntity<Feed> getFeed(@PathVariable Long id) {
        log.debug("REST request to get Feed : {}", id);
        Feed feed = feedRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(feed));
    }

    /**
     * DELETE  /feeds/:id : delete the "id" feed.
     *
     * @param id the id of the feed to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/feeds/{id}")
    @Timed
    public ResponseEntity<Void> deleteFeed(@PathVariable Long id) {
        log.debug("REST request to delete Feed : {}", id);
        feedRepository.delete(id);
        //feedSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/feeds?query=:query : search for the feed corresponding
     * to the query.
     *
     * @param query the query of the feed search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/feeds")
    @Timed
    public ResponseEntity<List<Feed>> searchFeeds(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Feeds for query {}", query);
        Page<Feed> page = feedSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/feeds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}

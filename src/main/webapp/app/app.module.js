(function() {
    'use strict';

    angular
        .module('netbedlooApp', [
            'ngStorage',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ngAnimate',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.router',
            'infinite-scroll',
            'ngMaterial',
            'ngMessages',
            'uiCropper',
            'luegg.directives',
            'angular-web-notification',
            'ngMeta',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'duScroll',
            '720kb.socialshare',
            'ngRateIt',
            'angularMoment'
        ])
        .run(run);

    run.$inject = ['$rootScope', 'stateHandler', 'translationHandler'];

    function run($rootScope, stateHandler, translationHandler) {
        $rootScope.isEmbedded = function() {
            if (navigator.userAgent.indexOf('EMBEDDED') != -1) {
                return true;
            };
            return false;
        };

        stateHandler.initialize();
        translationHandler.initialize();
    }
})();
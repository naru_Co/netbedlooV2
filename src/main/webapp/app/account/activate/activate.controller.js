(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ActivationController', ActivationController);

    ActivationController.$inject = ['$scope','$translate','$rootScope','$stateParams', 'Auth', 'LoginService'];

    function ActivationController ($scope,$translate,$rootScope,$stateParams, Auth, LoginService) {
        var vm = this;
        vm.updatePageDirection = updatePageDirection;
        $scope.isArab = false;
        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
        });
        updatePageDirection()
        Auth.activateAccount({key: $stateParams.key}).then(function () {
            vm.error = null;
            vm.success = 'OK';
            $rootScope.$broadcast('activated');
        }).catch(function () {
            vm.success = null;
            vm.error = 'ERROR';
        });
        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.isArab = true
            } else {
                $scope.isArab = false
            }

        }
        
    }
})();

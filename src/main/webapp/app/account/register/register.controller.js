(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['$state', 'GouvByName', 'GouvsByCountry', 'Country', 'Gouv', '$scope', '$translate', '$timeout', '$http', 'DataUtils', 'Auth', 'LoginService', 'Profile', 'User'];

    function RegisterController($state, GouvByName, GouvsByCountry, Country, Gouv, $scope, $translate, $timeout, $http, DataUtils, Auth, LoginService, Profile, User) {
        var vm = this;

        vm.maxDate = new Date();


        $scope.loading = false;
        vm.doNotMatch = null;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.success = null;
        vm.profile = {};
        vm.user = {};
        $scope.location = null;
        vm.countries = Country.query();
        $scope.change = function (country) {
            country = vm.profile.profile.country;
            $scope.newgouv = {
                country: country,
                name: '',
                longitude: null,
                latitude: null
            }
        }
        //setup before functions
        //setup before functions
        var typingTimer; //timer identifier
        var doneTypingInterval = 5000; //time in ms (5 seconds)

        //on keyup, start the countdown
        $('#gouv').keyup(function () {
            clearTimeout(typingTimer);
            if ($('#gouv').val() || $("#email:focus")) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        });

        //user is "finished typing," do something

        vm.lat = null;
        vm.long = null;
        vm.locality = null;
        vm.gouv = {};


        //user is "finished typing," do something
        function doneTyping() {

            $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + $scope.location + '&components=country:' + vm.profile.profile.country.code + '&key=AIzaSyDxX74bLnGn7AxGxH4HM7pM5Yzul2VH8Zc').success(function (data) {
                vm.lat = data.results[0].geometry.location.lat;
                vm.long = data.results[0].geometry.location.lng;
                vm.locality = data.results[0].address_components[0].long_name;




            });

        }


        $scope.myImage = '';

        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function () {

            if ($scope.myCroppedImage != '') {
                vm.profile.file = $scope.myCroppedImage.split(',')[1];
            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.profile.name = file.name;
            vm.profile.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        angular.element(document.querySelector('#fileInput2')).on('change', handleFileSelect);



        $timeout(function () {
            angular.element('#login').focus();
        });




        function register() {

            if (vm.registerAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                $scope.loading = true;
                vm.registerAccount.langKey = $translate.use();
                vm.registerAccount.firstName = vm.profile.profile.firstName;
                vm.registerAccount.lastName = vm.profile.profile.lastName;
                vm.profile.login = vm.registerAccount.login;
                vm.doNotMatch = null;
                vm.error = null;
                vm.errorUserExists = null;
                vm.errorEmailExists = null;
                vm.profile.profile.email = vm.registerAccount.email;
                function onSaveSuccess(result) {
                    $scope.$emit('netbedlooApp:profileUpdate', result);
                    $scope.enfinz = result;
                    vm.isSaving = false;
                }

                vm.gouv = GouvByName.get({
                    name: vm.locality
                }, function (result) {
                    vm.profile.profile.gouv = {};
                    vm.gouv = result;
                    var y = vm.gouv.hasOwnProperty("name");

                    if (!y) {
                        $scope.newgouv.name = vm.locality;
                        $scope.newgouv.longitude = vm.long;
                        $scope.newgouv.latitude = vm.lat
                        Gouv.save($scope.newgouv, function (result) {
                            vm.profile.profile.gouv = result;
                            Profile.save(vm.profile, function (enfinz) {

                                vm.registerAccount.profile = enfinz;
                                Auth.createAccount(vm.registerAccount).then(function () {
                                    vm.success = 'OK';

                                    $state.go('success')
                                    $scope.loading = false;

                                }).catch(function (response) {
                                    vm.success = null;
                                    $scope.loading = false;
                                    if (response.status === 400 && response.data === 'login already in use') {
                                        vm.errorUserExists = 'ERROR';
                                    } else if (response.status === 400 && response.data === 'email address already in use') {
                                        vm.errorEmailExists = 'ERROR';
                                    } else {
                                        vm.error = 'ERROR';
                                    }
                                })
                            }, function (response) {
                                vm.success = null;
                                $scope.loading = false;
                                if (response.status === 400 && response.data === 'login already in use') {
                                    vm.errorUserExists = 'ERROR';
                                    vm.registerAccount.login = '';
                                    vm.profile.login = '';
                                } else if (response.status === 400 && response.data === 'email address already in use') {
                                    vm.errorEmailExists = 'ERROR';
                                    vm.registerAccount.email = '';
                                    vm.profile.email = '';
                                } else {
                                    vm.error = 'ERROR';
                                }
                            });

                        })



                    } else {
                        vm.profile.profile.gouv = vm.gouv
                        Profile.save(vm.profile, function (enfinz) {

                            vm.registerAccount.profile = enfinz;
                            Auth.createAccount(vm.registerAccount).then(function () {
                                vm.success = 'OK';

                                $state.go('success')
                                $scope.loading = false;

                            }).catch(function (response) {
                                vm.success = null;
                                $scope.loading = false;
                                if (response.status === 400 && response.data === 'login already in use') {
                                    vm.errorUserExists = 'ERROR';
                                } else if (response.status === 400 && response.data === 'email address already in use') {
                                    vm.errorEmailExists = 'ERROR';
                                } else {
                                    vm.error = 'ERROR';
                                }
                            })
                        }, function (response) {
                            vm.success = null;
                            $scope.loading = false;
                            if (response.status === 400 && response.data === 'login already in use') {
                                vm.errorUserExists = 'ERROR';
                                vm.registerAccount.login = '';
                                vm.profile.login = '';
                            } else if (response.status === 400 && response.data === 'email address already in use') {
                                vm.errorEmailExists = 'ERROR';
                                vm.registerAccount.email = '';
                                vm.profile.email = '';
                            } else {
                                vm.error = 'ERROR';
                            }
                        });
                    };
                })


              





            }
        }
    }
})();

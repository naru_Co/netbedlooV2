﻿(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('success', {
            parent: 'account',
            url: '/success-registration',
            data: {
                authorities: [],
                pageTitle: 'register.success'
            },
            views: {
                'content@': {
                    templateUrl: 'app/account/register/sucess.html',
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('register');
                    return $translate.refresh();
                }]
            }
        });
    }
})();

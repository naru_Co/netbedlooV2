﻿(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('user-agreement', {
            parent: 'account',
            url: '/netbedloo-policy',
            data: {
                authorities: [],
                pageTitle: 'register.user'
            },
            views: {
                'content@': {
                    templateUrl: 'app/account/register/user-agreement.html',
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('register');
                    return $translate.refresh();
                }]
            }
        });
    }
})();

(function() {
    'use strict';
    /* globals SockJS, Stomp */

    angular
        .module('netbedlooApp')
        .factory('ChatService', ChatService);

    ChatService.$inject = ['$rootScope', '$window', '$cookies', '$http', '$q'];

    function ChatService($rootScope, $window, $cookies, $http, $q) {
        var stompClient = null;
        var subscriber = null;
        var listener = $q.defer();
        var connected = $q.defer();
        var alreadyConnectedOnce = false;

        var service = {
            connect: connect,
            disconnect: disconnect,
            receive: receive,
            sendMessage: sendMessage,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        return service;

        function connect() {
            //building absolute path so that websocket doesnt fail when deploying with a context path
            var loc = $window.location;
            var url = '//' + loc.host + loc.pathname + 'websocket/chat';
            var socket = new SockJS(url);
            stompClient = Stomp.over(socket);
            var stateChangeStart;
            var headers = {};
            headers[$http.defaults.xsrfHeaderName] = $cookies.get($http.defaults.xsrfCookieName);
            stompClient.connect(headers, function() {
                connected.resolve('success');
                subscribe();
                if (!alreadyConnectedOnce) {

                    alreadyConnectedOnce = true;
                }
            });
            $rootScope.$on('$destroy', function() {
                if (angular.isDefined(stateChangeStart) && stateChangeStart !== null) {
                    stateChangeStart();
                }
            });
        }

        function disconnect() {
            if (stompClient !== null) {
                stompClient.disconnect();
                stompClient = null;
            }
        }

        function receive() {
            return listener.promise;
        }

        function sendMessage(message) {
            if (stompClient !== null && stompClient.connected) {
                stompClient
                    .send('/chat', {},
                        angular.toJson({ 'message': message }));
            }
        }

        function subscribe() {
            connected.promise.then(function() {
                subscriber = stompClient.subscribe('/chat/public', function(data) {
                    listener.notify(angular.fromJson(data.body));
                });
            }, null, null);
        }

        function unsubscribe() {
            if (subscriber !== null) {
                subscriber.unsubscribe();
            }
            listener = $q.defer();
        }
    }
})();
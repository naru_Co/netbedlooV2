(function() {
    'use strict';
    /* globals SockJS, Stomp */

    angular
        .module('netbedlooApp')
        .factory('PrivateChatService', PrivateChatService);

    PrivateChatService.$inject = ['$rootScope', '$window', '$cookies', '$http', '$q', 'AuthServerProvider'];

    function PrivateChatService($rootScope, $window, $cookies, $http, $q, AuthServerProvider) {
        var stompClient = null;
        var subscriber = null;
        var listener = $q.defer();
        var connected = $q.defer();
        var alreadyConnectedOnce = false;
        

        var service = {
            connect: connect,
            disconnect: disconnect,
            receive: receive,
            sendMessage: sendMessage,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        return service;

        function connect(id1, id2) {
            //building absolute path so that websocket doesnt fail when deploying with a context path
            var loc = $window.location;
            var url = '//' + loc.host + '/websocket/privatechat/';
            var authToken = AuthServerProvider.getToken();
            if (authToken) {
                url += '?access_token=' + authToken;
            }
            var socket = new SockJS(url);
           

            stompClient = Stomp.over(socket);
            var stateChangeStart;
            var headers = {};
            stompClient.connect(headers, function() {
                connected.resolve('success');
                subscribe(id1, id2);
                if (!alreadyConnectedOnce) {

                    alreadyConnectedOnce = true;
                }
            });
            $rootScope.$on('$destroy', function() {
                if (angular.isDefined(stateChangeStart) && stateChangeStart !== null) {
                    stateChangeStart();
                }
            });
        }

        function disconnect() {
            if (stompClient !== null) {
                stompClient.disconnect();
                stompClient = null;
            }
        }

        function receive() {
            return listener.promise;
        }

        function sendMessage(message, id1, id2) {
            stompClient.debug = null
             //here it send the hole message
            if (stompClient !== null && stompClient.connected) {
                stompClient
                    .send('/privatechat/' + id1 + '/' + id2, {},
                        angular.toJson(message));
                       
            }
        }

        function subscribe(id1, id2) {

            connected.promise.then(function() {

                subscriber = stompClient.subscribe('/privatechat/private/' + id1 + '/' + id2, function(data) {

                    listener.notify(angular.fromJson(data.body));

                });
            }, null, null);
        }

        function unsubscribe() {
            if (subscriber !== null) {
                subscriber.unsubscribe();
            }
            listener = $q.defer();
        }
    }
})();
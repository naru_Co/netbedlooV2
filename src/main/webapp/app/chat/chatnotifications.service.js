(function() {
    'use strict';
    /* globals SockJS, Stomp */

    angular
        .module('netbedlooApp')
        .factory('ChatNotificationService', ChatNotificationService);

    ChatNotificationService.$inject = ['$rootScope', '$window', '$cookies', '$http', '$q', 'AuthServerProvider'];

    function ChatNotificationService($rootScope, $window, $cookies, $http, $q, AuthServerProvider) {
        var stompClient = null;
        var subscriber = null;
        var listener = $q.defer();
        var connected = $q.defer();
        var alreadyConnectedOnce = false;

        var service = {
            connect: connect,
            disconnect: disconnect,
            receive: receive,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        return service;

        function connect(id) {
            //building absolute path so that websocket doesnt fail when deploying with a context path
            var loc = $window.location;
            var url = '//' + loc.host + '/websocket/notification/';
            var authToken = AuthServerProvider.getToken();
            if (authToken) {
                url += '?access_token=' + authToken;
            }
            var socket = new SockJS(url);
            stompClient = Stomp.over(socket);
            var stateChangeStart;
            var headers = {};
            stompClient.connect(headers, function() {
                connected.resolve('success');
                subscribe(id);
                if (!alreadyConnectedOnce) {

                    alreadyConnectedOnce = true;
                }
            });
            $rootScope.$on('$destroy', function() {
                if (angular.isDefined(stateChangeStart) && stateChangeStart !== null) {
                    stateChangeStart();
                }
            });
        }

        function disconnect() {
            if (stompClient !== null) {
                stompClient.disconnect();
                stompClient = null;
            }
        }

        function receive() {
            return listener.promise;
        }


        function subscribe(id) {

            connected.promise.then(function() {

                subscriber = stompClient.subscribe('/notification/notif/' + id, function(data) {

                    listener.notify(angular.fromJson(data.body));

                });
            }, null, null);
        }

        function unsubscribe() {
            if (subscriber !== null) {
                subscriber.unsubscribe();
            }
            listener = $q.defer();
        }
    }
})();
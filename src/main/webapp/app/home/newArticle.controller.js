(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('NewArticleController', NewArticleController);

    NewArticleController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'Article', 'Category', 'Profile', 'Matched', 'Owned', 'Wanted'];

    function NewArticleController($timeout, $scope, $stateParams, $uibModalInstance, Article, Category, Profile, Matched, Owned, Wanted) {
        var vm = this;
        
        $scope.myImage = '';

        $scope.myCroppedImage = '';

        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.categories = Category.query();
        vm.profiles = Profile.query();
        vm.matcheds = Matched.query();
        vm.owneds = Owned.query();
        vm.wanteds = Wanted.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.article.id !== null) {
                Article.update(vm.article, onSaveSuccess, onSaveError);
            } else {
                Article.save(vm.article, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:articleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }

        $scope.$watch('myCroppedImage', function() {

            if ($scope.myCroppedImage != '') {
                vm.article.file = $scope.myCroppedImage.split(',')[1];
            }
        });

        angular.element(document.querySelector('#fileInputt')).on('change', handleFileSelect);


        var handleFileSelect = function(evt) {
            
            var file = evt.currentTarget.files[0];
            vm.article.name = file.name;
            vm.article.type = file.type;
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };



    }
})();
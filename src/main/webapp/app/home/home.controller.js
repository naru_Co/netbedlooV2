(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('HomeController', HomeController);


    HomeController.$inject = ['$cacheFactory','Talkers','$rootScope', '$timeout', 'PrivateChatService', 'ChatService', 'Auth', 'BookByTitle', '$uibModal', '$scope', 'Principal', 'LoginService', '$state', 'Article', 'Category', 'Wanted', 'Owned', 'Profile'];

    function HomeController($cacheFactory,Talkers,$rootScope, $timeout, PrivateChatService, ChatService, Auth, BookByTitle, $uibModal, $scope, Principal, LoginService, $state, Article, Category, Wanted, Owned, Profile) {

        var vm = this;
        vm.authenticationError = false;
        vm.credentials = {};
        vm.login = login;
        vm.password = null;
        vm.rememberMe = true;
        vm.requestResetPassword = requestResetPassword;
        vm.username = null;

        function login(event) {
            event.preventDefault();
            Auth.login({
                username: vm.username,
                password: vm.password,
                rememberMe: vm.rememberMe
            }).then(function() {
                vm.authenticationError = false;

                if ($state.current.name === 'register' || $state.current.name === 'activate' ||
                    $state.current.name === 'finishReset' || $state.current.name === 'requestReset') {

                }        
                $state.go('feed');
                $rootScope.$broadcast('authenticationSuccess');

                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is successful, go to stored previousState and clear previousState
                if (Auth.getPreviousState()) {
                    var previousState = Auth.getPreviousState();
                    Auth.resetPreviousState();
                    $state.go(previousState.name, previousState.params);
                }
            }).catch(function() {
                vm.authenticationError = true;
            });
        }
        getProfile()
        function requestResetPassword() {
            $state.go('requestReset');
        }


      





        function getProfile() {
            Principal.identity().then(function(account) {
                if (account != null) {
                   
                    vm.account = account;
                    $state.go('feed');

                }

            }).catch(function(account) {});;
        }






    }
})();
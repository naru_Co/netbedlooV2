(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('JhiLanguageController', JhiLanguageController);

    JhiLanguageController.$inject = ['amMoment','$rootScope','$translate', 'JhiLanguageService', 'tmhDynamicLocale'];

    function JhiLanguageController (amMoment,$rootScope,$translate, JhiLanguageService, tmhDynamicLocale) {
        var vm = this;

        vm.changeLanguage = changeLanguage;
        vm.languages = null;

        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function changeLanguage (languageKey) {
            
            $translate.use(languageKey);
            tmhDynamicLocale.set(languageKey);
            amMoment.changeLocale(languageKey);
           
            $rootScope.$broadcast('languageChanged')
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

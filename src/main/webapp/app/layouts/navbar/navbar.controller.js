(function () {
    'use strict';

    angular
        .module('netbedlooApp')

        .directive('scrollToBottom', scrollToBottom)
        .controller('NavbarController', NavbarController);




    scrollToBottom.$inject = ['$timeout', '$window']

    function scrollToBottom($timeout, $window) {
        return {
            scope: {
                scrollToBottom: "="
            },
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watchCollection('scrollToBottom', function (newVal) {
                    if (newVal) {
                        
                        $timeout(function () {
                            element[0].scrollTop = element[0].scrollHeight;
                        }, 0);
                    }
                   

                });
            }
        };
    }

    NavbarController.$inject = ['$rootScope','Talkers','$filter', 'PrivateConversation', '$translate', '$locale', 'ProfileByName', 'ArticleByTitleOrAuthor', '$timeout', '$mdDialog', 'Followed', 'Follow', 'MatchedByUser', 'PrivateUnseenMessage', 'MatchingNotificationService', 'Message', '$uibModal', 'BookByTitle', '$mdToast', 'ChatNotificationService', 'PrivateChatService', 'PrivateMessage', 'Profile', '$scope', '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', '$mdSidenav', 'SeePrivateMessage'];

    function NavbarController($rootScope, Talkers, $filter, PrivateConversation, $translate, $locale, ProfileByName, ArticleByTitleOrAuthor, $timeout, $mdDialog, Followed, Follow, MatchedByUser, PrivateUnseenMessage, MatchingNotificationService, Message, $uibModal, BookByTitle, $mdToast, ChatNotificationService, PrivateChatService, PrivateMessage, Profile, $scope, $state, Auth, Principal, ProfileService, LoginService, $mdSidenav, SeePrivateMessage) {
        var vm = this;
        $scope.talkers= []
        getAccount();
        $scope.gamingMode = false;
        $scope.choice = 0;
        $scope.$on('messenger', function (event, args) {
            $scope.notMessenger=false;
            $scope.gamingMode = false;   
        });
        $scope.$on('isGame', function (event, args) {
            $scope.gamingMode = true;
            $scope.onlyGames()
        });
        $scope.$on('feeds', function (event, args) {
            $scope.gamingMode = false;
            $scope.notMessenger=true;
            
        });
        $scope.$on('profile', function (event, args) {
            $scope.onlyBooks();
            $scope.gamingMode = false;
            $scope.notMessenger=true;
        });
       
        $scope.$on('book', function (event, args) {
            $scope.gamingMode = false;
            $scope.notMessenger=true;
            $scope.onlyBooks()
        });
        if ($state.includes('game-detail')) {
            $scope.gamingMode = true;
            $scope.notMessenger=true;

        }
        if ($state.includes('message')) {
            $scope.gamingMode = false;
            $scope.notMessenger=false;

        }else{
            $scope.notMessenger=true;
        }
        if ($state.includes('gaming')) {
            $scope.gamingMode = true;
            $scope.notMessenger=true;

        }else{
            $scope.gamingMode = false;
        }
        $scope.$on('gaming', function (event, args) {

            if ($state.includes('gaming')) {
                $scope.gamingMode = true;
                $scope.notMessenger=true;
                $scope.onlyGames()
            } else {
                $scope.gamingMode = false;
                $scope.notMessenger=true;
                $scope.onlyBooks()
            }
        });
        $scope.messages = [];
        vm.message = '';
        $scope.isBook = function () {
            $scope.gamingMode = false;
            $state.go('books');

        }
        vm.updatePageDirection = updatePageDirection;
        $scope.isArab = false
        updatePageDirection()
        //var userLang = navigator.language || navigator.userLanguage;

        //$translate.use(navigator.language || navigator.userLanguage);
        
        vm.theImage = 0
        vm.isNavbarCollapsed = true;
        $scope.testImages = function (URL) {
            var tester = new Image();
            tester.onload = imageFound;
            tester.onerror = imageNotFound;
            tester.src = URL;
        }

        function imageFound() {
            $scope.notFound = false
        }

        function imageNotFound() {
            $scope.notFound = true
        }

        $scope.$on('authenticationSuccess', function (event, args) {
            getAccount();
        });
        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
            var translate = $filter('translate');
            if ($scope.choice == 1) {
                $scope.concept = translate('global.menu.books')
                $scope.choose($scope.concept)
            } else if ($scope.choice == 2) {
                $scope.concept = translate('global.menu.games')
                $scope.choose($scope.concept)
            } else if ($scope.choice == 3) {
                $scope.concept = translate('global.menu.people')
                $scope.choose($scope.concept)
            }
        });
        $scope.$on('followoperation', function (event, args) {
            vm.account.profile.followings = Follow.get({
                id: vm.account.profile.id
            })
        });
        $scope.$on('imageChanged', function (event, opt) {
            $scope.profileChanged = opt.a;
            vm.account.profile = $scope.profileChanged;
        });
        $scope.$on('talkTo', function (event, opt) {
            $scope.talkTo(opt.a)
        }); 
        $scope.$on('gameadd', function (event, opt) {
            $scope.added = opt.a;
        });
        $scope.$on('bookadd', function (event, opt) {
            $scope.added = opt.a;
        });
       

        $scope.$on('deleteMatchingOperation', function (event, args) {
            $scope.deleted = args.a;
            var index = $scope.waitingMatching.indexOf($scope.deleted);
            if (index > -1) {
                $scope.waitingMatching.splice(index, 1);
            }
            getAccount();

        })
        var originatorEv;
        vm.openMenu = function ($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };

        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.isArab = true
                $scope.isFr = false
                $scope.isEng = false
                $rootScope.$broadcast('arabe');
            } else if (currentLang === "fr") {
                $scope.isArab = false
                $scope.isFr = true
                $scope.isEng = true
                $rootScope.$broadcast('notArab');
            } else {
                $scope.isArab = false
                $scope.isFr = false
                $scope.isEng = true
                $rootScope.$broadcast('notArab');
            }

        }
        //side nav for mobile

        $scope.toggleLeft = buildToggler('left');
        $scope.toggleRight = buildToggler('right');

        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            };
        }
        //


        $scope.showAlert = function (ev) {

            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Vous avez un matching')
                .textContent('Vous avez un Matching! (ceci est un test)')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
            );

        };
        $(document).click(function (event) {
            if (!$(event.target).closest('#chatBox').length) {
                if ($scope.showFriends = 1) {
                    $scope.$apply(function () {
                        $scope.showFriends = 0
                    })
                }
            }
        });

        $scope.onlyBooks = function () {
            var translate = $filter('translate');
            $scope.concept = translate('global.menu.books')
            $scope.choose($scope.concept)
            $scope.choice = 1;




        }
        $scope.onlyGames = function () {
            var translate = $filter('translate');
            $scope.concept = translate('global.menu.games');
            $scope.choose($scope.concept)
            $scope.choice = 2;


        }
        $scope.tnasnis = function () {
            var translate = $filter('translate');
            $scope.concept = translate('global.menu.people');
            $scope.choose($scope.concept)
            $scope.choice = 3;
        }
        $scope.choose = function (concept) {
            $(document).ready(function (e) {
                $('.search-panel span#search_concept').text(concept);
            });
        }

        $scope.showConfirm2 = function (ev) {
            var translate = $filter('translate');
            var confirm = $mdDialog.confirm()
                .title(translate('global.modal.well'))
                .textContent(translate('global.modal.welldone'))
                .ariaLabel('Authentificate')
                .ok(translate('global.modal.connect'))
                .cancel(translate('global.modal.register'));
            $mdDialog.show(confirm).then(function () {
                $state.go('home')

            }, function () {
                $state.go('register')

            });
        };
        $scope.goToProfile = function (profile) {
            $scope.searchText = null;
            $scope.selectedItem = null;
            $state.go('profile-detail', {
                pseudo: profile.pseudo
            });

        }
        $scope.querySearch = function (query) {
            $scope.title = query;
            return ArticleByTitleOrAuthor.get({
                title: query.toLowerCase(),
                author: query.toLowerCase()
            }).$promise;
        }

        $scope.queryProfileSearch = function (query) {
            $scope.title = query;
            return ProfileByName.get({
                pseudo: query.toLowerCase()
            }).$promise;
        }
        $scope.showConfirm4 = function (ev) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('global.modal.addtitle'))
                .textContent(translate('global.modal.addbody'))
                .ariaLabel('Add new item')
                .targetEvent(ev)
                .ok(translate('global.modal.book'))
                .cancel(translate('global.modal.game'));

            $mdDialog.show(confirm).then(function () {
                $uibModal.open({
                    templateUrl: 'app/entities/books/book-adding.html',
                    controller: 'bookAddingController',
                    animation: false,
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',

                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('article');
                            return $translate.refresh();
                        }],
                        entity: function () {
                            if (vm.account != null) {
                                return vm.account.profile;
                            }
                        }
                    }
                }).result.then(function () {
                    $uibModal.open({
                        templateUrl: 'app/entities/books/book-detail.html',
                        controller: 'booksDetailController',
                        animation: false,
                        size: 'lg',

                        resolve: {
                            translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                                $translatePartialLoader.addPart('article');
                                return $translate.refresh();
                            }],
                            entity: function () {
                                return $scope.added;
                            }
                        }
                    })


                }, function () {
                    getAccount();


                });

            }, function () {
                $uibModal.open({
                    templateUrl: 'app/entities/books/game-adding.html',
                    controller: 'gameAddingController',
                    animation: false,
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',

                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('article');
                            return $translate.refresh();
                        }],
                        entity: function () {
                            if (vm.account != null) {
                                return vm.account.profile;
                            }

                        }
                    }
                }).result.then(function () {
                    $uibModal.open({
                        templateUrl: 'app/entities/books/book-detail.html',
                        controller: 'booksDetailController',
                        animation: false,
                        size: 'lg',

                        resolve: {
                            translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                                $translatePartialLoader.addPart('article');
                                return $translate.refresh();
                            }],
                            entity: function () {
                                return $scope.added;
                            }
                        }
                    })


                }, function () {
                    getAccount();


                });


            });
        }


        vm.isNavbarCollapsed = true;


        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;



        function logout() {
            vm.profiles = {};
            vm.account = null;
            collapseNavbar();
            vm.theImage = 0;
            ChatNotificationService.disconnect();
            PrivateChatService.disconnect();
            MatchingNotificationService.disconnect();
            $scope.chatInfos.splice(0, 1);
            Auth.logout();
            $state.go('home');


        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }






        $scope.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {}, function () {});
        }

        $scope.openMatching = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/matched/matched-detail.html',
                controller: 'MatchedDetailController',
                animation: false,
                size: 'md',
                resolve: {
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {}, function () {});
        }



        $scope.account = null;
        vm.account = null;
        vm.articleInAction = null;
        vm.isAuthenticated = null;




        function register() {
            $state.go('register');
        }
        $scope.messagesNotificationsNumber = 0;
        $scope.waitingMatching = [];

        $scope.OnMatchingClick = function (profile) {
            $scope.talkTo(profile)

        }


        $scope.ValidURL = function (str) {
            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
            if (!regex.test(str)) {
                return false;
            } else {
                return true;
            }
        }
        $scope.endtalk = function (item) {
            var index = $scope.chatInfos.indexOf(item);
            $scope.chatInfos.splice(index, 1);
        }

        function getAccount() {
           
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    $scope.talkers = Talkers.get();
                    $rootScope.$broadcast('talkers', {
                        a: $scope.talkers
                    });
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    })
                    vm.account.profile.followed = Followed.query({
                        id: vm.account.profile.id
                    })

                   
                   

                    //$scope.profilepicture=account.profile.imagePath;
                    vm.isAuthenticated = Principal.isAuthenticated;
                    vm.theImage = 1;

                    //getFriendsList()

                    $scope.unseenMessages = PrivateUnseenMessage.query({
                        id: vm.account.profile.id
                    });
                    $scope.waitingMatching = MatchedByUser.query({
                        id: vm.account.profile.id
                    }, function (result) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].owner.id === vm.account.profile.id) {
                                result[i].theOtherOne = result[i].wanter;
                            } else {
                                result[i].theOtherOne = result[i].owner;
                            }
                        }
                    });

                    MatchingNotificationService.connect(vm.account.profile.id);
                    MatchingNotificationService.receive().then(null, null, function (result) {
                        $scope.playSuccessNotifAudio();
                        if (result.owner.id === vm.account.profile.id) {
                            result.theOtherOne = result.wanter;
                        } else {
                            result.theOtherOne = result.owner;
                        }
                        $scope.waitingMatching.push(result);
                        $scope.openMatching(result)


                    });
                    ChatNotificationService.connect(vm.account.profile.id);
                    ChatNotificationService.receive().then(null, null, function (pr) {
                        $scope.playNotifAudio();
                        for (var i = 0; i < $scope.talkers.length; i++) {
                            if ($scope.talkers[i].profile.id === pr.sender.id) {
                                $scope.talkers[i].message = pr
                                $scope.talkers[i].unseen =  $scope.talkers[i].unseen+1
                                $scope.talkers[i].creationDate = pr.creationDate;
                                $scope.talkers[i].id = pr.id
                                $scope.talkers[i].type = 0;
                            }
                        }
                        if ($scope.chosedProfileId != pr.sender.id) {
                            $scope.unseenMessages.push(pr);
                          

                           /*  //vm.profiles to be replaced by friendsList
                            var index = vm.profiles.map(function (e) {
                                return e.id;
                            }).indexOf(pr.sender.id);
                            vm.profiles[index].notif = vm.profiles[index].notif + 1; */

                            $mdToast.show(
                                $mdToast.simple()
                                .textContent(pr.sender.firstName + " " + pr.sender.lastName + " vous a envoyé un message")
                                .position('bottom left')
                                .hideDelay(10000)
                            );
                        }



                    });
                }
            }).catch(function (account) {

            });
        }
        $scope.playNotifAudio = function () {
            var audio = new Audio('content/audio/notif.mp3');
            audio.play();
        };
        $scope.playSuccessNotifAudio = function () {
            var audio = new Audio('content/audio/Success.mp3');
            audio.play();
        };



        $scope.showFriends = 0;





        function login() {
            //collapseNavbar();
            LoginService.open()
        }
        $scope.chatInfos = [];
        $scope.chosedProfileId = null;



        $scope.talkTo = function (profile) {

            $scope.chosedProfileId = profile.id;
            PrivateChatService.unsubscribe()
            $scope.messages = [];
            vm.message = '';
            var objDiv = document.getElementsByClassName(".chat-messages");
            objDiv.scrollTop = objDiv.scrollHeight;

            profile.notif = 0;
            //messaging part logic
            var id1 = 0;
            var id2 = 0;

            if (vm.account.profile.id > profile.id) {
                id1 = vm.account.profile.id;
                id2 = profile.id;
            } else {
                id1 = profile.id;
                id2 = vm.account.profile.id;
            }


            $scope.messages = PrivateConversation.get({
                uid: id1 + "naru" + id2
            }, function (result) {
                SeePrivateMessage.get({
                    uid: id1 + "naru" + id2,
                    id: vm.account.profile.id
                });
                $scope.messages.reverse()
                //update the notification number by seeing the unseen
                var tobeDeleted = 0;
                for (var i = 0; i < $scope.unseenMessages.length; i++) {
                    if ($scope.unseenMessages[i].sender.id === profile.id) {
                        tobeDeleted++;
                    }
                }
                $scope.unseenMessages.splice(-tobeDeleted, tobeDeleted)
                for (var i = 0; i < $scope.talkers.length; i++) {
                    if ($scope.talkers[i].profile.id === profile.id) {
                        $scope.talkers[i].unseen = 0
                    }
                }
                //end of service

                //see the messages


            })





            PrivateChatService.connect(id1, id2);
            PrivateChatService.receive().then(null, null, function (message) {
                $scope.messages.push(message);
                //addMessages to the seen
                for (var i = 0; i < $scope.talkers.length; i++) {
                    if ($scope.talkers[i].profile.id === message.receiver.id) {
                        $scope.talkers[i].message = message
                        $scope.talkers[i].unseen = $scope.talkers[i].unseen+1
                        $scope.talkers[i].creationDate = message.creationDate;
                        $scope.talkers[i].id = message.id
                    }
                }
                if (message.receiver.id === vm.account.profile.id) {
                    Message.update(message, function (result) {
                        message = result;
                    });
                }

            });

            var chatInfo = {
                receiver: profile,
                id1: id1,
                id2: id2,
                tabPos: 1
            }
           
            $scope.chatInfos[0] = chatInfo;

            $scope.seenNotifs = function () {


            }

        }

        $scope.endTalk = function (chatInfo) {
            $scope.chatInfos.splice(0, 1);
            $scope.messages = [];
            vm.message = '';

        }




        $scope.sendMessage = function (chatInfo) {

            if (vm.message.length === 0) {
                return;
            }
            var message = {};
            var sender = JSON.parse(JSON.stringify(vm.account.profile));
            delete sender.followings;
            delete sender.followed;
            message.sender = sender;
            message.receiver = chatInfo.receiver;
            message.content = vm.message;
            message.uid = chatInfo.id1 + "naru" + chatInfo.id2
            PrivateChatService.sendMessage(message, chatInfo.id1, chatInfo.id2);
            vm.message = '';

        }




        //replace with follow list when it's done

        /* function getFriendsList() {
            vm.profiles = Profile.query({}, function () {
                for (var i = 0; i < vm.profiles.length; i++) {
                    vm.profiles[i].notif = 0;
                }
            });
        } */



    }

})();

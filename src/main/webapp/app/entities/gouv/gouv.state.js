(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('gouv', {
                parent: 'entity',
                url: '/gouv',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.gouv.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/gouv/gouvs.html',
                        controller: 'GouvController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('gouv');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('gouv-detail', {
                parent: 'gouv',
                url: '/gouv/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.gouv.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/gouv/gouv-detail.html',
                        controller: 'GouvDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('gouv');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Gouv', function($stateParams, Gouv) {
                        return Gouv.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'gouv',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('gouv-detail.edit', {
                parent: 'gouv-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/gouv/gouv-dialog.html',
                        controller: 'GouvDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Gouv', function(Gouv) {
                                return Gouv.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('gouv.new', {
                parent: 'gouv',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/gouv/gouv-dialog.html',
                        controller: 'GouvDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    name: null,
                                    longitude: null,
                                    latitude: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('gouv', null, { reload: 'gouv' });
                    }, function() {
                        $state.go('gouv');
                    });
                }]
            })
            .state('gouv.edit', {
                parent: 'gouv',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/gouv/gouv-dialog.html',
                        controller: 'GouvDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Gouv', function(Gouv) {
                                return Gouv.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('gouv', null, { reload: 'gouv' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('gouv.delete', {
                parent: 'gouv',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/gouv/gouv-delete-dialog.html',
                        controller: 'GouvDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Gouv', function(Gouv) {
                                return Gouv.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('gouv', null, { reload: 'gouv' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
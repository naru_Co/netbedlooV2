(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('GouvController', GouvController);

    GouvController.$inject = ['Gouv', 'GouvSearch'];

    function GouvController(Gouv, GouvSearch) {

        var vm = this;

        vm.gouvs = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Gouv.query(function(result) {
                vm.gouvs = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            GouvSearch.query({query: vm.searchQuery}, function(result) {
                vm.gouvs = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

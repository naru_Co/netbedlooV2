(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('GouvsByCountry', GouvsByCountry)
        .factory('GouvByName', GouvByName)
        .factory('Gouv', Gouv);






    GouvsByCountry.$inject = ['$resource', 'DateUtils'];

    function GouvsByCountry($resource, DateUtils) {
        var resourceUrl = 'api/gouvsbycountry/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }

GouvByName.$inject = ['$resource'];

    function GouvByName($resource) {
        var resourceUrl = 'api/gouvbyname/:name';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: false }
              
        });
    }
    Gouv.$inject = ['$resource'];

    function Gouv($resource) {
        var resourceUrl = 'api/gouvs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
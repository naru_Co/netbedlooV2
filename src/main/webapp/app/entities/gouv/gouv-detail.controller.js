(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('GouvDetailController', GouvDetailController);

    GouvDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Gouv', 'Profile', 'Country'];

    function GouvDetailController($scope, $rootScope, $stateParams, previousState, entity, Gouv, Profile, Country) {
        var vm = this;

        vm.gouv = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:gouvUpdate', function(event, result) {
            vm.gouv = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

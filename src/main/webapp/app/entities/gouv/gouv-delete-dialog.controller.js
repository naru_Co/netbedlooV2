(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('GouvDeleteController',GouvDeleteController);

    GouvDeleteController.$inject = ['$uibModalInstance', 'entity', 'Gouv'];

    function GouvDeleteController($uibModalInstance, entity, Gouv) {
        var vm = this;

        vm.gouv = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Gouv.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

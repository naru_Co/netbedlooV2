(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('GouvSearch', GouvSearch);

    GouvSearch.$inject = ['$resource'];

    function GouvSearch($resource) {
        var resourceUrl =  'api/_search/gouvs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

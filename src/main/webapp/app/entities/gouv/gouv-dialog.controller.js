(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('GouvDialogController', GouvDialogController);

    GouvDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Gouv', 'Profile', 'Country'];

    function GouvDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Gouv, Profile, Country) {
        var vm = this;

        vm.gouv = entity;
        vm.clear = clear;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.countries = Country.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.gouv.id !== null) {
                Gouv.update(vm.gouv, onSaveSuccess, onSaveError);
            } else {
                Gouv.save(vm.gouv, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:gouvUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

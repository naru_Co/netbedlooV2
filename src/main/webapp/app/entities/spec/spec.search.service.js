(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('SpecSearch', SpecSearch);

    SpecSearch.$inject = ['$resource'];

    function SpecSearch($resource) {
        var resourceUrl =  'api/_search/specs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

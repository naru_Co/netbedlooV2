(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SpecDeleteController',SpecDeleteController);

    SpecDeleteController.$inject = ['$uibModalInstance', 'entity', 'Spec'];

    function SpecDeleteController($uibModalInstance, entity, Spec) {
        var vm = this;

        vm.spec = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Spec.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

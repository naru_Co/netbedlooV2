(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SpecDetailController', SpecDetailController);

    SpecDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Spec', 'Category', 'Article'];

    function SpecDetailController($scope, $rootScope, $stateParams, previousState, entity, Spec, Category, Article) {
        var vm = this;

        vm.spec = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:specUpdate', function(event, result) {
            vm.spec = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SpecDialogController', SpecDialogController);

    SpecDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Spec', 'Category', 'Article'];

    function SpecDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Spec, Category, Article) {
        var vm = this;

        vm.spec = entity;
        vm.clear = clear;
        vm.save = save;
        vm.categories = Category.query();
        vm.articles = Article.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.spec.id !== null) {
                Spec.update(vm.spec, onSaveSuccess, onSaveError);
            } else {
                Spec.save(vm.spec, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:specUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

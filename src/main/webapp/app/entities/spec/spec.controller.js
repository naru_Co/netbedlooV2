(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SpecController', SpecController);

    SpecController.$inject = ['Spec', 'SpecSearch'];

    function SpecController(Spec, SpecSearch) {

        var vm = this;

        vm.specs = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Spec.query(function(result) {
                vm.specs = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            SpecSearch.query({query: vm.searchQuery}, function(result) {
                vm.specs = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

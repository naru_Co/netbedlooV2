(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('spec', {
            parent: 'entity',
            url: '/spec',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'netbedlooApp.spec.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/spec/specs.html',
                    controller: 'SpecController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('spec');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('spec-detail', {
            parent: 'spec',
            url: '/spec/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'netbedlooApp.spec.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/spec/spec-detail.html',
                    controller: 'SpecDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('spec');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Spec', function($stateParams, Spec) {
                    return Spec.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'spec',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('spec-detail.edit', {
            parent: 'spec-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spec/spec-dialog.html',
                    controller: 'SpecDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Spec', function(Spec) {
                            return Spec.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('spec.new', {
            parent: 'spec',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spec/spec-dialog.html',
                    controller: 'SpecDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('spec', null, { reload: 'spec' });
                }, function() {
                    $state.go('spec');
                });
            }]
        })
        .state('spec.edit', {
            parent: 'spec',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spec/spec-dialog.html',
                    controller: 'SpecDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Spec', function(Spec) {
                            return Spec.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('spec', null, { reload: 'spec' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('spec.delete', {
            parent: 'spec',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spec/spec-delete-dialog.html',
                    controller: 'SpecDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Spec', function(Spec) {
                            return Spec.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('spec', null, { reload: 'spec' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

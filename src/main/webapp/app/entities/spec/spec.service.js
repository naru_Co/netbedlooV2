(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('SpecsByCategoryAndName', SpecsByCategoryAndName)
        .factory('SpecsByCategory', SpecsByCategory)
        .factory('Playstation', Playstation)
        .factory('Spec', Spec);
       

    SpecsByCategory.$inject = ['$resource', 'DateUtils'];

    function SpecsByCategory($resource, DateUtils) {
        var resourceUrl = 'api/specsofcategory/:category_id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
    Playstation.$inject = ['$resource', 'DateUtils'];

    function Playstation($resource, DateUtils) {
        var resourceUrl = 'api/playstation';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }

    SpecsByCategoryAndName.$inject = ['$resource', 'DateUtils'];

    function SpecsByCategoryAndName($resource, DateUtils) {
        var resourceUrl = 'api/specsbycategory/:category_id/:name';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
    Spec.$inject = ['$resource'];

    function Spec($resource) {
        var resourceUrl = 'api/specs/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT'
            }
        });
    }
})();

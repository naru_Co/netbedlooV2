(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ForumDetailController', ForumDetailController);

    ForumDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Forum', 'Subject', 'Article', 'Profile'];

    function ForumDetailController($scope, $rootScope, $stateParams, previousState, entity, Forum, Subject, Article, Profile) {
        var vm = this;

        vm.forum = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:forumUpdate', function(event, result) {
            vm.forum = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

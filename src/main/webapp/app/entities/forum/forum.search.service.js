(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('ForumSearch', ForumSearch);

    ForumSearch.$inject = ['$resource'];

    function ForumSearch($resource) {
        var resourceUrl =  'api/_search/forums/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OkayController', OkayController);

    OkayController.$inject = ['Okay', 'OkaySearch'];

    function OkayController(Okay, OkaySearch) {

        var vm = this;

        vm.okays = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Okay.query(function(result) {
                vm.okays = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            OkaySearch.query({query: vm.searchQuery}, function(result) {
                vm.okays = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Okay', Okay)
        .factory('numberOfOkay', numberOfOkay)
        .factory('likesByFeed', likesByFeed)
        .factory('likeByFeedOfPerson', likeByFeedOfPerson);


    numberOfOkay.$inject = ['$resource', 'DateUtils'];

    function numberOfOkay($resource, DateUtils) {
        var resourceUrl = 'api/numberofokeys/:id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
    likeByFeedOfPerson.$inject = ['$resource', 'DateUtils'];

    function likeByFeedOfPerson($resource, DateUtils) {
        var resourceUrl = 'api/okayprofileandfeed/:feed_id/:profile_id';
        return $resource(resourceUrl, {}, {
            method: 'GET',
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    data.time = DateUtils.convertDateTimeFromServer(data.time);
                }
                return data;
            }
        });
    }

    likesByFeed.$inject = ['$resource', 'DateUtils'];

    function likesByFeed($resource, DateUtils) {
        var resourceUrl = 'api/okaysoffeed/:id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: false
            }
        });
    }


    Okay.$inject = ['$resource', 'DateUtils'];

    function Okay($resource, DateUtils) {
        var resourceUrl = 'api/okays/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.time = DateUtils.convertDateTimeFromServer(data.time);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT'
            }
        });
    }
})();

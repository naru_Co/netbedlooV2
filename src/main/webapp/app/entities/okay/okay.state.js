(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('okay', {
            parent: 'entity',
            url: '/okay',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'netbedlooApp.okay.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/okay/okays.html',
                    controller: 'OkayController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('okay');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('okay-detail', {
            parent: 'okay',
            url: '/okay/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'netbedlooApp.okay.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/okay/okay-detail.html',
                    controller: 'OkayDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('okay');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Okay', function($stateParams, Okay) {
                    return Okay.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'okay',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('okay-detail.edit', {
            parent: 'okay-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/okay/okay-dialog.html',
                    controller: 'OkayDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Okay', function(Okay) {
                            return Okay.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('okay.new', {
            parent: 'okay',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/okay/okay-dialog.html',
                    controller: 'OkayDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                time: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('okay', null, { reload: 'okay' });
                }, function() {
                    $state.go('okay');
                });
            }]
        })
        .state('okay.edit', {
            parent: 'okay',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/okay/okay-dialog.html',
                    controller: 'OkayDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Okay', function(Okay) {
                            return Okay.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('okay', null, { reload: 'okay' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('okay.delete', {
            parent: 'okay',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/okay/okay-delete-dialog.html',
                    controller: 'OkayDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Okay', function(Okay) {
                            return Okay.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('okay', null, { reload: 'okay' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

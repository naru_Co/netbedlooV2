(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('OkaySearch', OkaySearch);

    OkaySearch.$inject = ['$resource'];

    function OkaySearch($resource) {
        var resourceUrl =  'api/_search/okays/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

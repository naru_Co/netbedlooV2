(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OkayDetailController', OkayDetailController);

    OkayDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Okay', 'Profile', 'Feed'];

    function OkayDetailController($scope, $rootScope, $stateParams, previousState, entity, Okay, Profile, Feed) {
        var vm = this;

        vm.okay = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:okayUpdate', function(event, result) {
            vm.okay = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

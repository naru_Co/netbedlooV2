(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OkayDialogController', OkayDialogController);

    OkayDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Okay', 'Profile', 'Feed'];

    function OkayDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Okay, Profile, Feed) {
        var vm = this;

        vm.okay = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.feeds = Feed.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.okay.id !== null) {
                Okay.update(vm.okay, onSaveSuccess, onSaveError);
            } else {
                Okay.save(vm.okay, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:okayUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.time = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OkayDeleteController',OkayDeleteController);

    OkayDeleteController.$inject = ['$uibModalInstance', 'entity', 'Okay'];

    function OkayDeleteController($uibModalInstance, entity, Okay) {
        var vm = this;

        vm.okay = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Okay.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

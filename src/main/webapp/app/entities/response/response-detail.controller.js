(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ResponseDetailController', ResponseDetailController);

    ResponseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Response', 'Comment', 'Profile'];

    function ResponseDetailController($scope, $rootScope, $stateParams, previousState, entity, Response, Comment, Profile) {
        var vm = this;

        vm.response = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:responseUpdate', function(event, result) {
            vm.response = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

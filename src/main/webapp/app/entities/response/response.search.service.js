(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('ResponseSearch', ResponseSearch);

    ResponseSearch.$inject = ['$resource'];

    function ResponseSearch($resource) {
        var resourceUrl =  'api/_search/responses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('ResponsesByComment', ResponsesByComment)
        .factory('Response', Response);


    ResponsesByComment.$inject = ['$resource', 'DateUtils'];

    function ResponsesByComment($resource, DateUtils) {
        var resourceUrl = 'api/responsesbycomment/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }

    Response.$inject = ['$resource', 'DateUtils'];

    function Response($resource, DateUtils) {
        var resourceUrl = 'api/responses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.responseTime = DateUtils.convertDateTimeFromServer(data.responseTime);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('OwnedSearch', OwnedSearch);

    OwnedSearch.$inject = ['$resource'];

    function OwnedSearch($resource) {
        var resourceUrl =  'api/_search/owneds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

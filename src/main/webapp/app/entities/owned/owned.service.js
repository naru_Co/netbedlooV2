(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('OwnedByArticle', OwnedByArticle)
        .factory('WantedByArticle', WantedByArticle)
        .factory('Owned', Owned)
        .factory('OwnedBooks', OwnedBooks)
        .factory('OwnedGames', OwnedGames);

    OwnedBooks.$inject = ['$resource', 'DateUtils'];

    function OwnedBooks($resource, DateUtils) {
        var resourceUrl = 'api/ownedbooks/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }
    OwnedGames.$inject = ['$resource', 'DateUtils'];

    function OwnedGames($resource, DateUtils) {
        var resourceUrl = 'api/ownedgames/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }


    OwnedByArticle.$inject = ['$resource', 'DateUtils'];

    function OwnedByArticle($resource, DateUtils) {
        var resourceUrl = 'api/ownedsbyarticle/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }

    WantedByArticle.$inject = ['$resource', 'DateUtils'];

    function WantedByArticle($resource, DateUtils) {
        var resourceUrl = 'api/wantedsbyarticle/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }



    Owned.$inject = ['$resource', 'DateUtils'];

    function Owned($resource, DateUtils) {
        var resourceUrl = 'api/owneds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': { method: 'GET', isArray: true },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedDeleteController',OwnedDeleteController);

    OwnedDeleteController.$inject = ['$uibModalInstance', 'entity', 'Owned'];

    function OwnedDeleteController($uibModalInstance, entity, Owned) {
        var vm = this;

        vm.owned = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Owned.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

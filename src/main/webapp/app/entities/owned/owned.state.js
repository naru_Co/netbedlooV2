(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('owned', {
                parent: 'entity',
                url: '/Allowned',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.owned.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/owned/owneds.html',
                        controller: 'OwnedController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('owned');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('owned-detail', {
                parent: 'owned',
                url: '/owned/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.owned.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/owned/owned-detail.html',
                        controller: 'OwnedDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('owned');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Owned', function($stateParams, Owned) {
                        return Owned.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'owned',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('owned-detail.edit', {
                parent: 'owned-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned/owned-dialog.html',
                        controller: 'OwnedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Owned', function(Owned) {
                                return Owned.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('owned.new', {
                parent: 'owned',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned/owned-dialog.html',
                        controller: 'OwnedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    creationDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('owned', null, { reload: 'owned' });
                    }, function() {
                        $state.go('owned');
                    });
                }]
            })
            .state('owned.edit', {
                parent: 'owned',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned/owned-dialog.html',
                        controller: 'OwnedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Owned', function(Owned) {
                                return Owned.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('owned', null, { reload: 'owned' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('owned.delete', {
                parent: 'owned',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned/owned-delete-dialog.html',
                        controller: 'OwnedDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Owned', function(Owned) {
                                return Owned.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('owned', null, { reload: 'owned' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
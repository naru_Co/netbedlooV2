(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedController', OwnedController);

    OwnedController.$inject = ['Owned', 'OwnedSearch'];

    function OwnedController(Owned, OwnedSearch) {

        var vm = this;

        vm.owneds = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Owned.query(function(result) {
                vm.owneds = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            OwnedSearch.query({query: vm.searchQuery}, function(result) {
                vm.owneds = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

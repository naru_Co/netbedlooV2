(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedDialogController', OwnedDialogController);

    OwnedDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Owned', 'Article', 'Profile'];

    function OwnedDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Owned, Article, Profile) {
        var vm = this;

        vm.owned = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.articles = Article.query();
        vm.profiles = Profile.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.owned.id !== null) {
                Owned.update(vm.owned, onSaveSuccess, onSaveError);
            } else {
                Owned.save(vm.owned, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:ownedUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedDetailController', OwnedDetailController);

    OwnedDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Owned', 'Article', 'Profile'];

    function OwnedDetailController($scope, $rootScope, $stateParams, previousState, entity, Owned, Article, Profile) {
        var vm = this;

        vm.owned = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:ownedUpdate', function(event, result) {
            vm.owned = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

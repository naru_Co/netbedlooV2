(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('owned-history', {
                parent: 'entity',
                url: '/owned-history',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.ownedHistory.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/owned-history/owned-histories.html',
                        controller: 'OwnedHistoryController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('ownedHistory');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('owned-history-detail', {
                parent: 'owned-history',
                url: '/owned-history/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.ownedHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/owned-history/owned-history-detail.html',
                        controller: 'OwnedHistoryDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('ownedHistory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'OwnedHistory', function($stateParams, OwnedHistory) {
                        return OwnedHistory.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'owned-history',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('owned-history-detail.edit', {
                parent: 'owned-history-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned-history/owned-history-dialog.html',
                        controller: 'OwnedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['OwnedHistory', function(OwnedHistory) {
                                return OwnedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('owned-history.new', {
                parent: 'owned-history',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned-history/owned-history-dialog.html',
                        controller: 'OwnedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    owningdate: null,
                                    eraseDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('owned-history', null, { reload: 'owned-history' });
                    }, function() {
                        $state.go('owned-history');
                    });
                }]
            })
            .state('owned-history.edit', {
                parent: 'owned-history',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned-history/owned-history-dialog.html',
                        controller: 'OwnedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['OwnedHistory', function(OwnedHistory) {
                                return OwnedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('owned-history', null, { reload: 'owned-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('owned-history.delete', {
                parent: 'owned-history',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/owned-history/owned-history-delete-dialog.html',
                        controller: 'OwnedHistoryDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['OwnedHistory', function(OwnedHistory) {
                                return OwnedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('owned-history', null, { reload: 'owned-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
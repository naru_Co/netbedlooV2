(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedHistoryDialogController', OwnedHistoryDialogController);

    OwnedHistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OwnedHistory', 'Article', 'Profile'];

    function OwnedHistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, OwnedHistory, Article, Profile) {
        var vm = this;

        vm.ownedHistory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.articles = Article.query();
        vm.profiles = Profile.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.ownedHistory.id !== null) {
                OwnedHistory.update(vm.ownedHistory, onSaveSuccess, onSaveError);
            } else {
                OwnedHistory.save(vm.ownedHistory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:ownedHistoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.owningdate = false;
        vm.datePickerOpenStatus.eraseDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

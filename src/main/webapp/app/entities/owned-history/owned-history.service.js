(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('OwnedHistory', OwnedHistory);

    OwnedHistory.$inject = ['$resource', 'DateUtils'];

    function OwnedHistory ($resource, DateUtils) {
        var resourceUrl =  'api/owned-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.owningdate = DateUtils.convertLocalDateFromServer(data.owningdate);
                        data.eraseDate = DateUtils.convertLocalDateFromServer(data.eraseDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.owningdate = DateUtils.convertLocalDateToServer(copy.owningdate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.owningdate = DateUtils.convertLocalDateToServer(copy.owningdate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

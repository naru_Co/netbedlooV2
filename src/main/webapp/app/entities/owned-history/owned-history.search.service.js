(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('OwnedHistorySearch', OwnedHistorySearch);

    OwnedHistorySearch.$inject = ['$resource'];

    function OwnedHistorySearch($resource) {
        var resourceUrl =  'api/_search/owned-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

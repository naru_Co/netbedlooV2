(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedHistoryDetailController', OwnedHistoryDetailController);

    OwnedHistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'OwnedHistory', 'Article', 'Profile'];

    function OwnedHistoryDetailController($scope, $rootScope, $stateParams, previousState, entity, OwnedHistory, Article, Profile) {
        var vm = this;

        vm.ownedHistory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:ownedHistoryUpdate', function(event, result) {
            vm.ownedHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('OwnedHistoryDeleteController',OwnedHistoryDeleteController);

    OwnedHistoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'OwnedHistory'];

    function OwnedHistoryDeleteController($uibModalInstance, entity, OwnedHistory) {
        var vm = this;

        vm.ownedHistory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            OwnedHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('SignalSearch', SignalSearch);

    SignalSearch.$inject = ['$resource'];

    function SignalSearch($resource) {
        var resourceUrl =  'api/_search/signals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Signal', Signal);

    Signal.$inject = ['$resource', 'DateUtils'];

    function Signal ($resource, DateUtils) {
        var resourceUrl =  'api/signals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertDateTimeFromServer(data.date);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

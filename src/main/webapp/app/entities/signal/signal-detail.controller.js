(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SignalDetailController', SignalDetailController);

    SignalDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Signal', 'Profile'];

    function SignalDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Signal, Profile) {
        var vm = this;

        vm.signal = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('netbedlooApp:signalUpdate', function(event, result) {
            vm.signal = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

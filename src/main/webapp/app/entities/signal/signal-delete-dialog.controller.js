(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SignalDeleteController',SignalDeleteController);

    SignalDeleteController.$inject = ['$uibModalInstance', 'entity', 'Signal'];

    function SignalDeleteController($uibModalInstance, entity, Signal) {
        var vm = this;

        vm.signal = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Signal.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

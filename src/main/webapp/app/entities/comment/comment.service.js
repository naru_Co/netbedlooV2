(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('CommentsBySubject', CommentsBySubject)
        .factory('NumberOfCommentsByFeed', NumberOfCommentsByFeed)
        .factory('Top5Comments', Top5Comments)
        .factory('CommentsOfFeed', CommentsOfFeed)
        .factory('Comment', Comment);

        CommentsOfFeed.$inject = ['$resource', 'DateUtils'];

        function CommentsOfFeed($resource, DateUtils) {
            var resourceUrl = 'api/commentsoffeed/:id';
    
            return $resource(resourceUrl, {}, {
                'get': { method: 'GET', isArray: true },
    
            });
        }

        Top5Comments.$inject = ['$resource', 'DateUtils'];

        function Top5Comments($resource, DateUtils) {
            var resourceUrl = 'api/topcommentsfeed/:id';
    
            return $resource(resourceUrl, {}, {
                'get': { method: 'GET', isArray: true },
    
            });
        }

    CommentsBySubject.$inject = ['$resource', 'DateUtils'];

    function CommentsBySubject($resource, DateUtils) {
        var resourceUrl = 'api/commentsbysubject/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }
    NumberOfCommentsByFeed.$inject = ['$resource', 'DateUtils'];

    function NumberOfCommentsByFeed($resource, DateUtils) {
        var resourceUrl = 'api/numberofcomments/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }
    Comment.$inject = ['$resource', 'DateUtils'];

    function Comment($resource, DateUtils) {
        var resourceUrl = 'api/comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.publicationTime = DateUtils.convertDateTimeFromServer(data.publicationTime);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
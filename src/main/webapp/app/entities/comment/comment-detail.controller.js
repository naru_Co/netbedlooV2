(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('CommentDetailController', CommentDetailController);

    CommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Comment', 'Subject', 'Profile', 'Response'];

    function CommentDetailController($scope, $rootScope, $stateParams, previousState, entity, Comment, Subject, Profile, Response) {
        var vm = this;

        vm.comment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:commentUpdate', function(event, result) {
            vm.comment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

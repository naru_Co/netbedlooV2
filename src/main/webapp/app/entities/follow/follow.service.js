(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Followed', Followed)
        .factory('Follow', Follow);
    Followed.$inject = ['$resource', 'DateUtils'];

    function Followed($resource, DateUtils) {
        var resourceUrl = 'api/followeds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
        });
    }

    Follow.$inject = ['$resource', 'DateUtils'];

    function Follow($resource, DateUtils) {
        var resourceUrl = 'api/follows/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                isArray: true
            },
            'update': { method: 'PUT' }
        });
    }
})();
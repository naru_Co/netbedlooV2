(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FollowDeleteController',FollowDeleteController);

    FollowDeleteController.$inject = ['$uibModalInstance', 'entity', 'Follow'];

    function FollowDeleteController($uibModalInstance, entity, Follow) {
        var vm = this;

        vm.follow = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Follow.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

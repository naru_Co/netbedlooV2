(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FollowDialogController', FollowDialogController);

    FollowDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Follow', 'Profile'];

    function FollowDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Follow, Profile) {
        var vm = this;

        vm.follow = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.follow.id !== null) {
                Follow.update(vm.follow, onSaveSuccess, onSaveError);
            } else {
                Follow.save(vm.follow, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:followUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.followTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FollowDetailController', FollowDetailController);

    FollowDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Follow', 'Profile'];

    function FollowDetailController($scope, $rootScope, $stateParams, previousState, entity, Follow, Profile) {
        var vm = this;

        vm.follow = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:followUpdate', function(event, result) {
            vm.follow = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

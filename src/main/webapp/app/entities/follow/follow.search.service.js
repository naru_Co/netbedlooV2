(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('FollowSearch', FollowSearch);

    FollowSearch.$inject = ['$resource'];

    function FollowSearch($resource) {
        var resourceUrl =  'api/_search/follows/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

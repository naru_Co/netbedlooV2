(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('follow', {
                parent: 'entity',
                url: '/follow',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.follow.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/follow/follows.html',
                        controller: 'FollowController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('follow');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('follow-detail', {
                parent: 'follow',
                url: '/follow/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.follow.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/follow/follow-detail.html',
                        controller: 'FollowDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('follow');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Follow', function($stateParams, Follow) {
                        return Follow.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'follow',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('follow-detail.edit', {
                parent: 'follow-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/follow/follow-dialog.html',
                        controller: 'FollowDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Follow', function(Follow) {
                                return Follow.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('follow.new', {
                parent: 'follow',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/follow/follow-dialog.html',
                        controller: 'FollowDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    followTime: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('follow', null, { reload: 'follow' });
                    }, function() {
                        $state.go('follow');
                    });
                }]
            })
            .state('follow.edit', {
                parent: 'follow',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/follow/follow-dialog.html',
                        controller: 'FollowDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Follow', function(Follow) {
                                return Follow.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('follow', null, { reload: 'follow' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('follow.delete', {
                parent: 'follow',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/follow/follow-delete-dialog.html',
                        controller: 'FollowDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Follow', function(Follow) {
                                return Follow.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('follow', null, { reload: 'follow' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
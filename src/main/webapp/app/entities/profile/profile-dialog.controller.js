(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ProfileDialogController', ProfileDialogController);

    ProfileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Profile', 'Article', 'Owned', 'Wanted', 'Matched', 'Gouv', 'MatchingHistory', 'OwnedHistory', 'WantedHistory'];

    function ProfileDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Profile, Article, Owned, Wanted, Matched, Gouv, MatchingHistory, OwnedHistory, WantedHistory) {
        var vm = this;

        vm.profile = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.articles = Article.query();
        vm.owneds = Owned.query();
        vm.wanteds = Wanted.query();
        vm.matcheds = Matched.query();
        vm.gouvs = Gouv.query();
        vm.matchinghistories = MatchingHistory.query();
        vm.ownedhistories = OwnedHistory.query();
        vm.wantedhistories = WantedHistory.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.profile.id !== null) {
                Profile.update(vm.profile, onSaveSuccess, onSaveError);
            } else {
                Profile.save(vm.profile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:profileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.birthDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('NewImageController', NewImageController);

    NewImageController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'Profile', 'Article', 'Owned', 'Wanted', 'Matched', 'Gouv', 'MatchingHistory', 'OwnedHistory', 'WantedHistory'];

    function NewImageController($timeout, $scope, $stateParams, $uibModalInstance, Profile, Article, Owned, Wanted, Matched, Gouv, MatchingHistory, OwnedHistory, WantedHistory) {
        var vm = this;
        vm.profileIm = {};


        $scope.myImage = '';

        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function() {

            if ($scope.myCroppedImage != '') {
                vm.profileIm.file = $scope.myCroppedImage.split(',')[1];

            }
        });

        var handleFileSelect = function(evt) {
            var file = evt.currentTarget.files[0];
            vm.profileIm.name = file.name;
            vm.profileIm.type = file.type;
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);


        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            Profile.save(vm.profileIm)
        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:profileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ProfileDetailController', ProfileDetailController);


    ProfileDetailController.$inject = ['$filter', '$translate', '$timeout', 'MatchingHistoryByProfile', 'Signal', 'OwnedBooks', 'OwnedGames', 'WantedGames', 'WantedBooks', 'Followed', 'Follow', '$mdDialog', 'ChatNotificationService', 'Message', 'PrivateChatService', 'PrivateMessage', '$state', 'Auth', 'LoginService', 'BookByTitle', 'Principal', '$uibModal', '$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Profile', 'Article', 'Owned', 'Wanted', 'Matched', 'Gouv'];

    function ProfileDetailController($filter, $translate, $timeout, MatchingHistoryByProfile, Signal, OwnedBooks, OwnedGames, WantedGames, WantedBooks, Followed, Follow, $mdDialog, ChatNotificationService, Message, PrivateChatService, PrivateMessage, $state, Auth, LoginService, BookByTitle, Principal, $uibModal, $scope, $rootScope, $stateParams, previousState, entity, Profile, Article, Owned, Wanted, Matched, Gouv) {
        $rootScope.$broadcast('profile')
        var vm = this;
        vm.profile = entity;
        vm.isSaving = false;
        vm.profileIm = {};
        $scope.editInfo = 0;
        vm.changePassword = changePassword;
        vm.doNotMatch = null;
        vm.error = null;
        vm.success = null;
        vm.password = "";
        vm.confirmPassword = "";
        vm.updatePageDirection = updatePageDirection;
        $scope.loading = false;
        $scope.isArab = false
        updatePageDirection()
        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
        });
        $scope.$on('activated', function (event, args) {
            $scope.isNotActivated=false;
        });
        $scope.$on('addown', function(event, opt) {
            $scope.ownToAdd=opt.a;
            $scope.livresPossedes.push($scope.ownToAdd);
            $scope.biblioBooks = OwnedBooks.get({
                id: entity.id
            }, function () {}, function () {})
            $scope.biblioGames = OwnedGames.get({
                id: entity.id
            }, function () {}, function () {})
                
           });
           $scope.$on('addwant', function(event, opt) {
            $scope.wantToAdd=opt.a;
            $scope.livresFavoris.push($scope.wantToAdd)
            $scope.bookswishlist = WantedBooks.get({
                id: entity.id
            }, function () {}, function () {})
            $scope.gameswishlist = WantedGames.get({
                id: entity.id
            }, function () {}, function () {})    
           });
        getAccount();
        $scope.$on('deletewant', function (event, args) {
            $scope.livresFavoris = Wanted.get({
                id: entity.id
            }, function () {}, function () {})
            $scope.bookswishlist = WantedBooks.get({
                id: entity.id
            }, function () {}, function () {})
            $scope.gameswishlist = WantedGames.get({
                id: entity.id
            }, function () {}, function () {})
           

        })
        $scope.$on('deleteown', function (event, args) {
            $scope.livresPossedes = Owned.get({
                id: vm.profile.id
            }, function () {}, function () {})
            $scope.biblioBooks = OwnedBooks.get({
                id: entity.id
            }, function () {}, function () {})
            $scope.biblioGames = OwnedGames.get({
                id: entity.id
            }, function () {}, function () {})

        })
        $scope.querySearch = function (query) {
            $scope.title = query;
            return BookByTitle.get({
                title: query.toLowerCase()
            }).$promise
        }


        vm.isNavbarCollapsed = true;


        $scope.testImages = function (URL) {
            var tester = new Image();
            tester.onload = imageFound;
            tester.onerror = imageNotFound;
            tester.src = URL;
        }

        function imageFound() {
            $scope.notFound = false
        }

        function imageNotFound() {
            $scope.notFound = true
        }
        $scope.testImages(vm.profile.imagePath);





        $scope.addNewImage = function () {
            $uibModal.open({
                templateUrl: 'app/entities/profile/newImage.html',
                controller: 'NewImageController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',

            }).result.then(function () {
                $state.go('profile-detail', null, {
                    reload: 'profile-detail'
                });
            }, function () {});

        }


        /* image stuff */

        $scope.myImage = '';

        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function () {

            if ($scope.myCroppedImage != '') {
                vm.profileIm.file = $scope.myCroppedImage.split(',')[1];
                vm.profileIm.profile = vm.profile;

            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.profileIm.name = file.name;
            vm.profileIm.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };

        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.isArab = true
            } else {
                $scope.isArab = false
            }

        }




        angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);




        /* end of image stuff */




        $scope.smt = function () {
            $scope.loading = true;
            vm.profileIm.profile = vm.profile;
            Profile.save(vm.profileIm, function (result) {
                $scope.loading = false;
                $rootScope.$broadcast('imageChanged',{a:result})
                vm.profile.normalImage = result.normalImage;
                $scope.myCroppedImage = '';
            });
        }








        vm.profile.followed = Followed.query({
            id: vm.profile.id
        })

        $scope.gouv = {
            id: null,
            name: null,
            longitude: null,
            latitude: null,
            country: {}
        }


        $scope.birthdate = vm.profile.birthDate;

        vm.previousState = previousState.name;
        if (vm.profile.gouv != null) {

            if (vm.profile.gouv.name.indexOf("/") == -1) {
                $scope.profilegouv = vm.profile.gouv.name

            } else {
                $scope.profilegouv = vm.profile.gouv.name.split('/')[0]
            }
        }


        $scope.getSecondIndex = function (index) {
            if (index - slides.length >= 0)
                return index - slides.length;
            else
                return index;
        }

        $scope.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],

                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {
                $scope.livresFavoris = Wanted.get({
                    id: entity.id
                }, function () {})
                $scope.livresPossedes = Owned.get({
                    id: vm.profile.id
                }, function () {})

            }, function () {
                $scope.livresFavoris = Wanted.get({
                    id: entity.id
                }, function () {})
                $scope.livresPossedes = Owned.get({
                    id: vm.profile.id
                }, function () {})

            });

        }




        $scope.hidden = false;
        vm.isOpen = false;
        $scope.hover = false;

        function changePassword() {
            if (vm.password !== vm.confirmPassword) {
                vm.error = null;
                vm.success = null;
                vm.doNotMatch = 'ERROR';
            } else {
                vm.doNotMatch = null;
                Auth.changePassword(vm.password).then(function () {
                    vm.error = null;
                    vm.success = 'OK';
                }).catch(function () {
                    vm.success = null;
                    vm.error = 'ERROR';
                });
            }
        }

        // On opening, add a delayed property which shows tooltips after the speed dial has opened
        // so that they have the proper position; if closing, immediately hide the tooltips
        $scope.$watch('vm.isOpen', function (isOpen) {
            if (isOpen) {
                $timeout(function () {
                    $scope.tooltipVisible = self.isOpen;
                }, 600);
            } else {
                $scope.tooltipVisible = self.isOpen;
            }
        });
        var marker;
        var tooltip = "Drag and Drop the marker to your adress";

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: {
                    lat: vm.profile.gouv.latitude,
                    lng: vm.profile.gouv.longitude
                }
            });

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                title: tooltip,
                position: {
                    lat: vm.profile.gouv.latitude,
                    lng: vm.profile.gouv.longitude
                }
            });
            google.maps.event.addListener(marker, 'dragend', changeAdress);
            marker.addListener('click', toggleBounce);
        }

        function changeAdress() {
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng()
            };
            geocoder.geocode({
                'location': latlng
            }, function (results, status) {
                if (status === 'OK') {
                    if (results[1]) {
                        $scope.gouv.country = vm.profile.country //what if the country change ?
                        $scope.gouv.longitude = marker.getPosition().lng();
                        $scope.gouv.latitude = marker.getPosition().lat();
                        $scope.gouv.name = results[1].address_components[0].long_name + " / " + vm.profile.id + " / " + vm.profile.pseudo;
                        vm.profile.adress = results[1].formatted_address;
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        }

        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
        $('#myMap').on('shown.bs.modal', function () {
            initMap();
            google.maps.event.trigger(map, "resize");
        });

        $scope.livresFavoris = Wanted.get({
            id: entity.id
        }, function () {}, function () {})
        $scope.livresPossedes = Owned.get({
            id: vm.profile.id
        }, function () {}, function () {})
        $scope.biblioBooks = OwnedBooks.get({
            id: entity.id
        }, function () {}, function () {})
        $scope.biblioGames = OwnedGames.get({
            id: entity.id
        }, function () {}, function () {})
        $scope.bookswishlist = WantedBooks.get({
            id: entity.id
        }, function () {}, function () {})
        $scope.gameswishlist = WantedGames.get({
            id: entity.id
        }, function () {}, function () {})
        $scope.tbedl = MatchingHistoryByProfile.get({
            owner_id: entity.id,
            wanter_id: entity.id
        }, function () {}, function () {})

        //idicator for if this is the page of the current user
        $scope.ownpage = 0;
        $scope.isAFiend = false;

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    if(vm.account.profile.id===vm.profile.id&&!vm.account.activated){
                        $scope.isNotActivated=true;
                    }else{
                        $scope.isNotActivated=false  
                    }
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    }, function (result) {

                        var index = result.map(function (e) {
                            return e.followed.id;
                        }).indexOf(vm.profile.id);

                        if (index > -1) {
                            $scope.isAFiend = true;
                            $scope.actualFollow = result[index];
                        }
                    }, function () {})




                    vm.isAuthenticated = Principal.isAuthenticated;
                }
            });
        }
        $scope.edit = 0;

        $scope.editBio = function () {
            $scope.edit = 1;
        }

        $scope.saveBio = function () {
            vm.isSaving = true;

            Profile.update(vm.profile, onSaveSuccess, onSaveError);

            $scope.edit = 0;






        }
        $scope.saveInfos = function () {
            vm.isSaving = true;
            Gouv.save($scope.gouv, function (result) {
                vm.profile.gouv = result;
                if (vm.profile.gouv.name.indexOf("/") == -1) {
                    $scope.profilegouv = vm.profile.gouv.name

                } else {
                    $scope.profilegouv = vm.profile.gouv.name.split('/')[0]
                }
                Profile.update(vm.profile, onSaveSuccess, onSaveError);

                vm.profile.birthDate = $scope.birthdate
                $scope.editInfo = 0;
            },function(){
                Profile.update(vm.profile, onSaveSuccess, onSaveError);
                
                                vm.profile.birthDate = $scope.birthdate
                                $scope.editInfo = 0;
            })


        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:profileUpdate', result);
            
            vm.isSaving = false;
        }
       

        function onSaveError() {
            vm.isSaving = false;
        }





        var unsubscribe = $rootScope.$on('netbedlooApp:profileUpdate', function (event, result) {
            vm.profile = result;
        });

        vm.signal = {
            id: null,
            content: null,
            type: null,
            date: null,
            profile: null
        };
        $scope.content = "";
        $scope.reported = false;

        $scope.saveReport = function () {
            $scope.header = vm.account.profile.pseudo + " wants to report " + vm.profile.fistName + " " + vm.profile.lastName + "." + "\n" + "Reason : " + "\n"
            vm.signal.content = $scope.header + $scope.content;
            vm.signal.profile = vm.account.profile;
            vm.signal.type = 2;
            Signal.save(vm.signal, function () {
                $scope.reported = true

            })
        }

        $scope.showConfirm = function (ev) {
            var translate = $filter('translate');
            translate('netbedlooApp.profile.follow')
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.profile.follow') + vm.profile.firstName + ' ' + vm.profile.lastName + '?')
                .textContent(translate('netbedlooApp.profile.rs'))
                .ariaLabel('Follow!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.profile.yes'))
                .cancel(translate('netbedlooApp.profile.no'));

            $mdDialog.show(confirm).then(function () {
                followProfile()

            }, function () {});
        };



        $scope.showConfirm2 = function (ev) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.profile.unfollow') + vm.profile.firstName + ' ' + vm.profile.lastName + '?')
                .textContent(translate('netbedlooApp.profile.nrs'))
                .ariaLabel('Unfollow!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.profile.yes'))
                .cancel(translate('netbedlooApp.profile.chance'));

            $mdDialog.show(confirm).then(function () {
                StopfollowProfile()

            }, function () {});
        };

        function followProfile() {
            var follow = {};
            follow.follower = vm.account.profile;
            follow.followed = vm.profile;
            Follow.save(follow, function () {
                $rootScope.$broadcast('followoperation');
                $scope.isAFiend = true;

            })



        }

        function StopfollowProfile() {

            Follow.delete($scope.actualFollow, function () {
                $rootScope.$broadcast('followoperation');
            })
            $scope.isAFiend = false;


        }



    }
})();

(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['Profile', 'ProfileSearch'];

    function ProfileController(Profile, ProfileSearch) {
        $(".hover").mouseleave(
            function () {
              $(this).removeClass("hover");
            }
          );

        var vm = this;

        vm.profiles = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Profile.query(function (result) {
                vm.profiles = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ProfileSearch.query({
                query: vm.searchQuery
            }, function (result) {
                vm.profiles = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }
    }
})();

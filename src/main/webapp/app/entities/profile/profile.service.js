(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Profile', Profile)
        .factory('ProfileByPseudo', ProfileByPseudo)
        .factory('ProfileByName', ProfileByName);



    ProfileByName.$inject = ['$resource', 'DateUtils'];

    function ProfileByName($resource, DateUtils) {
        var resourceUrl = 'api/profilesbypseudo/:pseudo';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }

    ProfileByPseudo.$inject = ['$resource', 'DateUtils'];

    function ProfileByPseudo($resource, DateUtils) {
        var resourceUrl = 'api/profilebypseudo/:pseudo';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.birthDate = DateUtils.convertLocalDateFromServer(data.birthDate);
                    }
                    return data;
                }
            }



        })
    }





    Profile.$inject = ['$resource', 'DateUtils'];

    function Profile($resource, DateUtils) {
        var resourceUrl = 'api/profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.birthDate = DateUtils.convertLocalDateFromServer(data.birthDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.birthDate = DateUtils.convertLocalDateToServer(copy.birthDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.birthDate = DateUtils.convertLocalDateToServer(copy.birthDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

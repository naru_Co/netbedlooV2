(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Rating', Rating)
        .factory('RateOfArticle', RateOfArticle)
        .factory('RateOfArticleByPerson', RateOfArticleByPerson);


    RateOfArticleByPerson.$inject = ['$resource', 'DateUtils'];

    function RateOfArticleByPerson($resource, DateUtils) {
        var resourceUrl = 'api/rateofarticlebyperson/:rated_id/:rater_id';
        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.ratingTime = DateUtils.convertDateTimeFromServer(data.ratingTime);
                    }
                    return data;
                }
            }
        });

    }

    RateOfArticle.$inject = ['$resource', 'DateUtils'];


    function RateOfArticle($resource, DateUtils) {
        var resourceUrl = 'api/rateofarticle/:rated_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });

    }


    Rating.$inject = ['$resource', 'DateUtils'];

    function Rating($resource, DateUtils) {
        var resourceUrl = 'api/ratings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.ratingTime = DateUtils.convertDateTimeFromServer(data.ratingTime);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('RatingController', RatingController);

    RatingController.$inject = ['Rating', 'RatingSearch'];

    function RatingController(Rating, RatingSearch) {

        var vm = this;

        vm.ratings = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Rating.query(function(result) {
                vm.ratings = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            RatingSearch.query({query: vm.searchQuery}, function(result) {
                vm.ratings = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

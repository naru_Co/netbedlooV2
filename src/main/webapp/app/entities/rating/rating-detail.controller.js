(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('RatingDetailController', RatingDetailController);

    RatingDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Rating', 'Article', 'Profile'];

    function RatingDetailController($scope, $rootScope, $stateParams, previousState, entity, Rating, Article, Profile) {
        var vm = this;

        vm.rating = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:ratingUpdate', function(event, result) {
            vm.rating = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

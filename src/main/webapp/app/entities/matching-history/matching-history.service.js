(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('MatchingHistory', MatchingHistory)
        .factory('MatchingHistoryByProfile',MatchingHistoryByProfile);


        MatchingHistoryByProfile.$inject = ['$resource', 'DateUtils'];
        
            function MatchingHistoryByProfile($resource, DateUtils) {
                var resourceUrl = 'api/matchingByprofile/:owner_id/:wanter_id';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                });
            }
    MatchingHistory.$inject = ['$resource', 'DateUtils'];

    function MatchingHistory ($resource, DateUtils) {
        var resourceUrl =  'api/matching-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.matchingDate = DateUtils.convertDateTimeFromServer(data.matchingDate);
                        data.tbedlooDate = DateUtils.convertLocalDateFromServer(data.tbedlooDate);
                        data.eraseDate = DateUtils.convertLocalDateFromServer(data.eraseDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tbedlooDate = DateUtils.convertLocalDateToServer(copy.tbedlooDate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tbedlooDate = DateUtils.convertLocalDateToServer(copy.tbedlooDate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

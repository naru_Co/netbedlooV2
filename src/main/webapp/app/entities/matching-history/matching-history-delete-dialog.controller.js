(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchingHistoryDeleteController',MatchingHistoryDeleteController);

    MatchingHistoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'MatchingHistory'];

    function MatchingHistoryDeleteController($uibModalInstance, entity, MatchingHistory) {
        var vm = this;

        vm.matchingHistory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MatchingHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchingHistoryDetailController', MatchingHistoryDetailController);

    MatchingHistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MatchingHistory', 'Profile', 'Article'];

    function MatchingHistoryDetailController($scope, $rootScope, $stateParams, previousState, entity, MatchingHistory, Profile, Article) {
        var vm = this;

        vm.matchingHistory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:matchingHistoryUpdate', function(event, result) {
            vm.matchingHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

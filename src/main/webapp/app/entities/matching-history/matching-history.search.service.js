(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('MatchingHistorySearch', MatchingHistorySearch);

    MatchingHistorySearch.$inject = ['$resource'];

    function MatchingHistorySearch($resource) {
        var resourceUrl =  'api/_search/matching-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

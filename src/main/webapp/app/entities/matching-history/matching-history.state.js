(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('matching-history', {
                parent: 'entity',
                url: '/matching-history',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.matchingHistory.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/matching-history/matching-histories.html',
                        controller: 'MatchingHistoryController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('matchingHistory');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('matching-history-detail', {
                parent: 'matching-history',
                url: '/matching-history/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.matchingHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/matching-history/matching-history-detail.html',
                        controller: 'MatchingHistoryDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('matchingHistory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MatchingHistory', function($stateParams, MatchingHistory) {
                        return MatchingHistory.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'matching-history',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('matching-history-detail.edit', {
                parent: 'matching-history-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matching-history/matching-history-dialog.html',
                        controller: 'MatchingHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['MatchingHistory', function(MatchingHistory) {
                                return MatchingHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('matching-history.new', {
                parent: 'matching-history',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matching-history/matching-history-dialog.html',
                        controller: 'MatchingHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    matchingDate: null,
                                    statut: null,
                                    distance: null,
                                    tbedlooDate: null,
                                    eraseDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('matching-history', null, { reload: 'matching-history' });
                    }, function() {
                        $state.go('matching-history');
                    });
                }]
            })
            .state('matching-history.edit', {
                parent: 'matching-history',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matching-history/matching-history-dialog.html',
                        controller: 'MatchingHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['MatchingHistory', function(MatchingHistory) {
                                return MatchingHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('matching-history', null, { reload: 'matching-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('matching-history.delete', {
                parent: 'matching-history',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matching-history/matching-history-delete-dialog.html',
                        controller: 'MatchingHistoryDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['MatchingHistory', function(MatchingHistory) {
                                return MatchingHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('matching-history', null, { reload: 'matching-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
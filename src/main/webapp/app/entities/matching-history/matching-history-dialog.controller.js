(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchingHistoryDialogController', MatchingHistoryDialogController);

    MatchingHistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MatchingHistory', 'Profile', 'Article'];

    function MatchingHistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MatchingHistory, Profile, Article) {
        var vm = this;

        vm.matchingHistory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.articles = Article.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.matchingHistory.id !== null) {
                MatchingHistory.update(vm.matchingHistory, onSaveSuccess, onSaveError);
            } else {
                MatchingHistory.save(vm.matchingHistory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:matchingHistoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.matchingDate = false;
        vm.datePickerOpenStatus.tbedlooDate = false;
        vm.datePickerOpenStatus.eraseDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

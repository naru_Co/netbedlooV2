(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('PrivateMessage', PrivateMessage)
        .factory('SeePrivateMessage', SeePrivateMessage)
        .factory('PrivateConversation', PrivateConversation)
        .factory('PrivateUnseenMessage', PrivateUnseenMessage);

        PrivateConversation.$inject = ['$resource', 'DateUtils'];
        
            function PrivateConversation($resource, DateUtils) {
                var resourceUrl = 'api/privateconversation/:uid';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true },
        
                });
            }

    PrivateUnseenMessage.$inject = ['$resource', 'DateUtils'];

    function PrivateUnseenMessage($resource, DateUtils) {
        var resourceUrl = 'api/unseen-private-messages-number/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },

        });
    }

    PrivateMessage.$inject = ['$resource', 'DateUtils'];

    function PrivateMessage($resource, DateUtils) {
        var resourceUrl = 'api/private-messages/:uid';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }

    SeePrivateMessage.$inject = ['$resource'];

    function SeePrivateMessage($resource) {
        var resourceUrl = 'api/see-private-messages/:uid/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET' }

        });
    }
})();
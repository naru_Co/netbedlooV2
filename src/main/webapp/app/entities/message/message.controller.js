(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .directive('scrollToBottomChat', scrollToBottomChat)
        .controller('MessageController', MessageController);

    scrollToBottomChat.$inject = ['$timeout', '$window']

    function scrollToBottomChat($timeout, $window) {
        return {
            scope: {
                scrollToBottomChat: "="
            },
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watchCollection('scrollToBottomChat', function (newVal) {
                    if (newVal) {
                        $timeout(function () {
                            element[0].scrollTop = element[0].scrollHeight;
                        }, 0);
                    }


                });
            }
        };
    }


    MessageController.$inject = ['ChatNotificationService', '$mdToast', 'ProfileByPseudo', 'ProfileByName', 'PrivateChatService', 'ParseLinks', 'Conversation', 'paginationConstants', '$scope', 'Message', 'MessageSearch', '$rootScope', 'Principal', 'Talkers'];

    function MessageController(ChatNotificationService, $mdToast, ProfileByPseudo, ProfileByName, PrivateChatService, ParseLinks, Conversation, paginationConstants, $scope, Message, MessageSearch, $rootScope, Principal, Talkers) {
        $rootScope.$broadcast('messenger')
        var vm = this;
        getAccount();
        vm.messages = [];
        $scope.messages = [];
        vm.message = {};
        $scope.talkers = [];
        $scope.talkTo = {};
        $scope.newMessage = '';
        $scope.noTalker = true;

        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;

        $('.messages').scroll(function () {
            if ($('.messages').scrollTop() === 0) {
                if (vm.page < vm.links['last']) {
                    vm.loadPage(vm.page + 1, $scope.talkTo)
                }
            }
        });

        vm.links = {
            last: 0
        };
        $scope.queryProfileSearch = function (query) {
            return ProfileByName.get({
                pseudo: query.toLowerCase()
            }).$promise
        }
        $scope.selectedBooks = [];
        $scope.selectedProfile = null;
        $scope.selectedProfile = null;
        $scope.searchProfile = "";
        $scope.readonly = false
        $scope.remove = false;
        $scope.transformChip = function (chip) {
            if (chip != undefined) {

                // If it is an object, it's already a known chip
                if (angular.isObject(chip)) {
                    $scope.remove = true;
                    $scope.readonly = true
                    angular.element('.md-scroll-mask').remove();
                    angular.element('body').attr('style', '');

                    return chip;

                }

            }
        }
        $scope.talkingTo = function () {

            var json = angular.toJson($scope.selectedBooks);
            $scope.slected = JSON.parse(json)
            $scope.pr = $scope.slected[0];
            $scope.talkTo = ProfileByPseudo.get({
                pseudo: $scope.pr.pseudo
            })
            $scope.loadAll($scope.talkTo)
        }
        /**
         * Search for contacts; use a random delay to simulate a remote call
         */
        $scope.searchForProfile = function (query) {

            if (typeof (query) === 'undefined' || query === "") {
                return;
            } else {
                return ProfileByName.get({
                    pseudo: query.toLowerCase(),
                }).$promise
            }



        }
        $scope.loadAll = function (profile) {
            var id1 = 0;
            var id2 = 0;
            for (var i = 0; i < $scope.talkers.length; i++) {
                if ($scope.talkers[i].profile.id === profile.id) {
                    $scope.talkers[i].unseen = 0
                }
            }

            $scope.talkTo = profile;
            $(".messages").animate({
                scrollTop: $(document).height()
            }, "fast");

            if (vm.account.profile.id > profile.id) {
                id1 = vm.account.profile.id;
                id2 = profile.id;
            } else {
                id1 = profile.id;
                id2 = vm.account.profile.id;
            }
            Conversation.get({
                uid: id1 + "naru" + id2,
                page: vm.page,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess)

            function onSuccess(data, headers) {

                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {
                    $scope.messages.push(data[i]);
                }
                $scope.messages.reverse()

            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            PrivateChatService.receive().then(null, null, function (message) {
                if (vm.message.uid != null) {
                    $scope.messages.push(vm.message);
                }
                for (var i = 0; i < $scope.talkers.length; i++) {
                    if ($scope.talkers[i].profile.id === message.receiver.id) {
                        $scope.talkers[i].message = message
                        $scope.talkers[i].creationDate = message.creationDate;
                        $scope.talkers[i].id = message.id
                        $scope.talkers[i].type = 1;
                    }
                }

                vm.message = {
                    uid: null
                }
                //addMessages to the seen


            });

        }


        $scope.reset = function (talker) {
            vm.page = 0;
            var id1 = 0;
            var id2 = 0;
            $scope.noTalker = false;
            $scope.item = talker.id
            $scope.messages = [];
            $rootScope.$broadcast('talkTo', {
                a: talker.profile
            });
            $scope.talkTo = talker.profile;
            if (vm.account.profile.id > $scope.talkTo.id) {
                id1 = vm.account.profile.id;
                id2 = $scope.talkTo.id;
            } else {
                id1 = $scope.talkTo.id;
                id2 = vm.account.profile.id;
            }
            PrivateChatService.connect(id1, id2);
            $scope.loadAll(talker.profile);

        }

        function loadPage(page, profile) {
            vm.page = page;
            $scope.loadAll(profile);
        }
        vm.predicate = 'id';
        vm.reverse = false;


        $("#profile-img").click(function () {
            $("#status-options").toggleClass("active");
        });

        /*$(".expand-button").click(function () {
            $("#profile").toggleClass("expanded");
            $("#contacts").toggleClass("expanded");
        });*/

        $("#status-options ul li").click(function () {
            $("#profile-img").removeClass();
            $("#status-online").removeClass("active");
            $("#status-away").removeClass("active");
            $("#status-busy").removeClass("active");
            $("#status-offline").removeClass("active");
            $(this).addClass("active");

            if ($("#status-online").hasClass("active")) {
                $("#profile-img").addClass("online");
            } else if ($("#status-away").hasClass("active")) {
                $("#profile-img").addClass("away");
            } else if ($("#status-busy").hasClass("active")) {
                $("#profile-img").addClass("busy");
            } else if ($("#status-offline").hasClass("active")) {
                $("#profile-img").addClass("offline");
            } else {
                $("#profile-img").removeClass();
            };

            $("#status-options").removeClass("active");
        });


        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:messageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function newMessage() {
            vm.isSaving = true;
            var id1 = 0;
            var id2 = 0;
            if (vm.account.profile.id > $scope.talkTo.id) {
                id1 = vm.account.profile.id;
                id2 = $scope.talkTo.id;
            } else {
                id1 = $scope.talkTo.id;
                id2 = vm.account.profile.id;
            }
            var message = {};
            message.uid = id1 + "naru" + id2;
            message.sender = vm.account.profile;
            message.receiver = $scope.talkTo;
            message.content = $scope.newMessage;

            PrivateChatService.sendMessage(message, id1, id2);
            vm.message = message
            $rootScope.$broadcast('newMessage');
            $scope.newMessage = ''


            $(".messages").animate({
                scrollTop: $(document).height()
            }, "fast");
        };


        $('.submit').click(function () {


            newMessage();


        });

        $(window).on('keydown', function (e) {
            if (e.which == 13) {

                newMessage();


                return false;
            }
        });


        function getAccount() {
            Principal.identity().then(function (account) {
                $scope.talkers = Talkers.get();
                vm.account = account;
                if (vm.account != null) {
                    //$scope.profilepicture=account.profile.imagePath;
                    vm.isAuthenticated = Principal.isAuthenticated;
                    ChatNotificationService.receive().then(null, null, function (pr) {
                        for (var i = 0; i < $scope.talkers.length; i++) {
                            if ($scope.talkers[i].profile.id === pr.sender.id) {
                                $scope.talkers[i].message = pr
                                $scope.talkers[i].unseen = $scope.talkers[i].unseen + 1
                                $scope.talkers[i].creationDate = pr.creationDate;
                                $scope.talkers[i].id = pr.id
                                $scope.talkers[i].type = 0;
                            }
                        }
                        if ($scope.talkTo.id != pr.sender.id) {
                            $scope.unseenMessages.push(pr);
                        }
                    });



                }
            }).catch(function (account) {

            });
        }


    }

})();

(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Message', Message)
        .factory('Talkers', Talkers)
        .factory('Conversation', Conversation);

    Talkers.$inject = ['$resource', 'DateUtils'];

    function Talkers($resource, DateUtils) {
        var resourceUrl = 'api/talkingTo';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
    Conversation.$inject = ['$resource', 'DateUtils'];

    function Conversation($resource, DateUtils) {
        var resourceUrl = 'api/private-messages/:uid';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
   



    Message.$inject = ['$resource', 'DateUtils'];

    function Message($resource, DateUtils) {
        var resourceUrl = 'api/messages/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                        data.seenDate = DateUtils.convertDateTimeFromServer(data.seenDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT'
            }
        });
    }
})();

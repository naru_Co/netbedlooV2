(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchedDeleteController',MatchedDeleteController);

    MatchedDeleteController.$inject = ['$uibModalInstance', 'entity', 'Matched'];

    function MatchedDeleteController($uibModalInstance, entity, Matched) {
        var vm = this;

        vm.matched = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Matched.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

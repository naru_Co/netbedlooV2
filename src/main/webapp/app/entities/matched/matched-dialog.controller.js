(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchedDialogController', MatchedDialogController);

    MatchedDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Matched', 'Profile', 'Article', 'Tbedloo'];

    function MatchedDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Matched, Profile, Article, Tbedloo) {
        var vm = this;

        vm.matched = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.articles = Article.query();
        vm.tbedloos = Tbedloo.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.matched.id !== null) {
                Matched.update(vm.matched, onSaveSuccess, onSaveError);
            } else {
                Matched.save(vm.matched, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:matchedUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.matchingDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

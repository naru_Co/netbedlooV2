(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Matched', Matched)
        .factory('MatchedByUser', MatchedByUser);
    Matched.$inject = ['$resource', 'DateUtils'];

    function MatchedByUser($resource, DateUtils) {
        var resourceUrl = 'api/matchedsByProfile/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },

        });
    }

    MatchedByUser.$inject = ['$resource', 'DateUtils'];

    function Matched($resource, DateUtils) {
        var resourceUrl = 'api/matcheds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
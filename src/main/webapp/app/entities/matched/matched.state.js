(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('matched', {
                parent: 'entity',
                url: '/Allmatched',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.matched.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/matched/matcheds.html',
                        controller: 'MatchedController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('matched');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('matched-detail', {
                parent: 'matched',
                url: '/matched/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.matched.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/matched/matched-detail.html',
                        controller: 'MatchedDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('matched');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Matched', function($stateParams, Matched) {
                        return Matched.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'matched',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('matched-detail.edit', {
                parent: 'matched-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matched/matched-dialog.html',
                        controller: 'MatchedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Matched', function(Matched) {
                                return Matched.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('matched.new', {
                parent: 'matched',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matched/matched-dialog.html',
                        controller: 'MatchedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    matchingDate: null,
                                    statut: null,
                                    reference: null,
                                    distance: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('matched', null, { reload: 'matched' });
                    }, function() {
                        $state.go('matched');
                    });
                }]
            })
            .state('matched.edit', {
                parent: 'matched',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matched/matched-dialog.html',
                        controller: 'MatchedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Matched', function(Matched) {
                                return Matched.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('matched', null, { reload: 'matched' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('matched.delete', {
                parent: 'matched',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/matched/matched-delete-dialog.html',
                        controller: 'MatchedDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Matched', function(Matched) {
                                return Matched.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('matched', null, { reload: 'matched' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
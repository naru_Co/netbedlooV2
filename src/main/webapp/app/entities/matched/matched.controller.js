(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchedController', MatchedController);

    MatchedController.$inject = ['Matched', 'MatchedSearch'];

    function MatchedController(Matched, MatchedSearch) {

        var vm = this;

        vm.matcheds = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Matched.query(function(result) {
                vm.matcheds = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            MatchedSearch.query({query: vm.searchQuery}, function(result) {
                vm.matcheds = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

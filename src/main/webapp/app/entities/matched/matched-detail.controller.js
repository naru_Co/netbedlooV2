(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('MatchedDetailController', MatchedDetailController);

    MatchedDetailController.$inject = ['$translate','Principal', 'PrivateMessage', 'PrivateChatService', '$uibModalInstance', '$uibModal', '$state', '$scope', '$rootScope', '$stateParams', 'entity', 'Matched', 'Profile', 'Article', 'Tbedloo'];

    function MatchedDetailController($translate,Principal, PrivateMessage, PrivateChatService, $uibModalInstance, $uibModal, $state, $scope, $rootScope, $stateParams, entity, Matched, Profile, Article, Tbedloo) {
        var vm = this;
        $scope.messages = [];
        vm.message = '';
        vm.updatePageDirection = updatePageDirection;
        $scope.isArab=false
        updatePageDirection()
        getAccount();
        $scope.$on('languageChanged', function(event, args) {
            updatePageDirection()
        });
        $scope.matched = entity;
         
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.confirmExchange = function(element) {
            element.statut = true;           
            
            Matched.update(element, function() {
                $uibModalInstance.close();
                location.reload();
            });

        }

        $scope.deleteExchange = function(theId) {
            Matched.delete({ id: theId }, function() {
                $rootScope.$broadcast('deleteMatchingOperation',{ a: $scope.matched})
                $uibModalInstance.close();
            });
        }

        $scope.openDetails = function(element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',
                resolve: {
                    entity: function() {
                        return element;
                    }
                }
            }).result.then(function() {}, function() {});
        }

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                $scope.account = account;
                //$scope.profilepicture=account.profile.imagePath;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        $scope.isOpen = 0;
        $scope.talkTo = function(profile) {
            $scope.chosedProfileId = profile.id;
            PrivateChatService.unsubscribe()
            $scope.isOpen = 1;
            $scope.messages = [];
            vm.message = '';
            var objDiv = document.getElementsByClassName(".chat-messages");
            objDiv.scrollTop = objDiv.scrollHeight;

            profile.notif = 0;
            //messaging part logic
            var id1 = 0;
            var id2 = 0;

            if (vm.account.profile.id > profile.id) {
                id1 = vm.account.profile.id;
                id2 = profile.id;
            } else {
                id1 = profile.id;
                id2 = vm.account.profile.id;
            }


            $scope.messages = PrivateMessage.get({ uid: id1 + "naru" + id2 }, function() {
               
            });

            PrivateChatService.connect(id1, id2);
            PrivateChatService.receive().then(null, null, function(message) {
                $scope.messages.push(message);
                //addMessages to the seen
                if (message.receiver.id === vm.account.profile.id) {
                    Message.update(message);
                }

            });

            $scope.chatInfo = { receiver: profile, id1: id1, id2: id2, tabPos: 1 }


        }
        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if(currentLang==="ar-ly"){
                $scope.isArab=true
            }else{
                $scope.isArab=false
            }
            
          }
        $scope.sendMessage = function(Sentmessage) {
            if (Sentmessage.length === 0) {
                return;
            }
            var message = {};
            message.sender = vm.account.profile;
            message.receiver = $scope.chatInfo.receiver;
            message.content = Sentmessage;
            message.uid = $scope.chatInfo.id1 + "naru" + $scope.chatInfo.id2
            PrivateChatService.sendMessage(message, $scope.chatInfo.id1, $scope.chatInfo.id2);
            $scope.Sentmessage = '';

        }
    }
})();
(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('WantedSearch', WantedSearch);

    WantedSearch.$inject = ['$resource'];

    function WantedSearch($resource) {
        var resourceUrl =  'api/_search/wanteds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Wanted', Wanted)
        .factory('WantedBooks', WantedBooks)
        .factory('WantedGames', WantedGames);

        WantedBooks.$inject = ['$resource', 'DateUtils'];
        
            function WantedBooks($resource, DateUtils) {
                var resourceUrl = 'api/wantedbooks/:id';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                })
            }
            WantedGames.$inject = ['$resource', 'DateUtils'];
            
                function WantedGames($resource, DateUtils) {
                    var resourceUrl = 'api/wantedgames/:id';
            
                    return $resource(resourceUrl, {}, {
                        'get': { method: 'GET', isArray: true }
                    })
                }



    Wanted.$inject = ['$resource', 'DateUtils'];

    function Wanted($resource, DateUtils) {
        var resourceUrl = 'api/wanteds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': { method: 'GET', isArray: true },
            'update': {
                method: 'PUT',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('wanted', {
                parent: 'entity',
                url: '/Allwanted',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.wanted.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/wanted/wanteds.html',
                        controller: 'WantedController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('wanted');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('wanted-detail', {
                parent: 'wanted',
                url: '/wanted/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.wanted.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/wanted/wanted-detail.html',
                        controller: 'WantedDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('wanted');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Wanted', function($stateParams, Wanted) {
                        return Wanted.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'wanted',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('wanted-detail.edit', {
                parent: 'wanted-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted/wanted-dialog.html',
                        controller: 'WantedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Wanted', function(Wanted) {
                                return Wanted.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('wanted.new', {
                parent: 'wanted',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted/wanted-dialog.html',
                        controller: 'WantedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    creationDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('wanted', null, { reload: 'wanted' });
                    }, function() {
                        $state.go('wanted');
                    });
                }]
            })
            .state('wanted.edit', {
                parent: 'wanted',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted/wanted-dialog.html',
                        controller: 'WantedDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Wanted', function(Wanted) {
                                return Wanted.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('wanted', null, { reload: 'wanted' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('wanted.delete', {
                parent: 'wanted',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted/wanted-delete-dialog.html',
                        controller: 'WantedDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Wanted', function(Wanted) {
                                return Wanted.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('wanted', null, { reload: 'wanted' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
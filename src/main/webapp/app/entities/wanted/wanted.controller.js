(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedController', WantedController);

    WantedController.$inject = ['Wanted', 'WantedSearch'];

    function WantedController(Wanted, WantedSearch) {

        var vm = this;

        vm.wanteds = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Wanted.query(function(result) {
                vm.wanteds = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            WantedSearch.query({ query: vm.searchQuery }, function(result) {
                vm.wanteds = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }
    }
})();
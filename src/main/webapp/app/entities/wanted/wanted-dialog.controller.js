(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedDialogController', WantedDialogController);

    WantedDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Wanted', 'Profile', 'Article'];

    function WantedDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Wanted, Profile, Article) {
        var vm = this;

        vm.wanted = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.articles = Article.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.wanted.id !== null) {
                Wanted.update(vm.wanted, onSaveSuccess, onSaveError);
            } else {
                Wanted.save(vm.wanted, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:wantedUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

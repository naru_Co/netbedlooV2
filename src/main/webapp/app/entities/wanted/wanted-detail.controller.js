(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedDetailController', WantedDetailController);

    WantedDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Wanted', 'Profile', 'Article'];

    function WantedDetailController($scope, $rootScope, $stateParams, previousState, entity, Wanted, Profile, Article) {
        var vm = this;

        vm.wanted = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:wantedUpdate', function(event, result) {
            vm.wanted = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

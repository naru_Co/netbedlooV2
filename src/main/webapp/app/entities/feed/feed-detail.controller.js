(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FeedDetailController', FeedDetailController);

    FeedDetailController.$inject = ['$translate','amMoment','Comment','$filter','$mdDialog','ParseLinks', 'CommentsOfFeed', 'Okay', 'likeByFeedOfPerson', 'NumberOfCommentsByFeed', 'numberOfOkay', '$uibModal', 'Principal', '$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Feed', 'MatchingHistory', 'OwnedHistory', 'WantedHistory', 'Follow', 'Profile', 'Article'];

    function FeedDetailController($translate,amMoment,Comment,$filter,$mdDialog,ParseLinks, CommentsOfFeed, Okay, likeByFeedOfPerson, NumberOfCommentsByFeed, numberOfOkay, $uibModal, Principal, $scope, $rootScope, $stateParams, previousState, entity, Feed, MatchingHistory, OwnedHistory, WantedHistory, Follow, Profile, Article) {
        var vm = this;

        vm.feed = entity;
        vm.previousState = previousState.name;
        vm.loadPage = loadPage;
        vm.itemsPerPage = 5;
        vm.comments = [];
        vm.comment={};
        $scope.commenter='';
        vm.okay = {};
        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.updatePageDirection = updatePageDirection;
        updatePageDirection()
        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            amMoment.changeLocale(currentLang);
            if (currentLang === "ar-ly") {
                
                $scope.isArab = true;

            } else {
                
                $scope.isArab = false;
            }

        }
        $scope.$on('languageChanged', function (event, args){
            updatePageDirection()
        })
        vm.predicate = 'id';
        vm.reset = reset;
        vm.reverse = true;
        vm.clear = clear;
        vm.loadAll = loadAll;

        getAccount();
        loadAll()
        loadFeedPropreties(vm.feed)

        /**************************************************************
         * ------------- Get Account of the connected person----------*
         **************************************************************/
        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.account != null) {
                    
                    var likeit = likeByFeedOfPerson.get({
                        feed_id: vm.feed.id,
                        profile_id: vm.account.profile.id
                    }, function (data) {

                        if (data.id != null) {
                            vm.feed.account = true
                            $scope.okayTodelete = data
                        } else {
                            vm.feed.account = false
                        }
                    }, function () {
                        vm.feed.account = false
                    })
                }
            });
        }
        $scope.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {


            }, function () {



            });

        }
        $scope.openGame = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {


            }, function () {



            });

        }
        /**************************************************************
         * ----------------------- Load Feed Propreties ------------------------*
         **************************************************************/
        function abbrNum(number, decPlaces) {
            // 2 decimal places => 100, 3 => 1000, etc
            decPlaces = Math.pow(10, decPlaces);

            // Enumerate number abbreviations
            var abbrev = [" k", " m", " b", " t"];

            // Go through the array backwards, so we do the largest first
            for (var i = abbrev.length - 1; i >= 0; i--) {

                // Convert array index to "1000", "1000000", etc
                var size = Math.pow(10, (i + 1) * 3);

                // If the number is bigger or equal do the abbreviation
                if (size <= number) {
                    // Here, we multiply by decPlaces, round, and then divide by decPlaces.
                    // This gives us nice rounding to a particular decimal place.
                    number = Math.round(number * decPlaces / size) / decPlaces;

                    // Handle special case where we round up to the next abbreviation
                    if ((number == 1000) && (i < abbrev.length - 1)) {
                        number = 1;
                        i++;
                    }

                    // Add the letter for the abbreviation
                    number += abbrev[i];

                    // We are done... stop
                    break;
                }
            }

            return number;
        }

        function loadFeedPropreties(feed) {
            var numberOk = numberOfOkay.get({
                id: feed.id
            }, function (data) {
                feed.numberOfOkay = abbrNum(data[0], 2)
            });
            var numberCm = NumberOfCommentsByFeed.get({
                id: feed.id
            }, function (data) {
                feed.numberOfCm = abbrNum(data[0], 2)
            });

        }
        $scope.deleteOkay = function () {
            vm.isSaving = true;
            Okay.delete({
                id: $scope.okayTodelete.id
            }, function (result) {
                loadFeedPropreties(vm.feed);
                vm.feed.account = false;
            }, function (result) {

            })
        }
        $scope.showConfirm2 = function (ev, comment) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.feed.eraseComment'))
                .textContent(translate('netbedlooApp.feed.confirmErase'))
                .ariaLabel('delete comment!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.feed.deleteit'))
                .cancel(translate('netbedlooApp.feed.keep'));

            $mdDialog.show(confirm).then(function () {
                $scope.deleteComment(comment)

            }, function () {});
        };
        $scope.deleteComment = function (comment) {
            vm.isSaving = true;

            Comment.delete({
                id: comment.id
            }, function (result) {
               loadFeedPropreties(vm.feed);
            }, function (result) {

            })




        }
        $scope.saveComment = function () {
            vm.isSaving = true;
            vm.comment.feed = vm.feed;
            vm.comment.profile = vm.account.profile
            vm.comment.comment = $scope.commenter
            Comment.save(vm.comment, function (result) {
                vm.comment = {};
                loadFeedPropreties(vm.feed);
                vm.comments.push(result);
                $('#comment').val('');
            }, function (result) {
                $('#comment').val('');

            })

        }

        $scope.saveOkay = function () {

            vm.isSaving = true;
            vm.okay.feed = vm.feed;
            vm.okay.profile = vm.account.profile
            Okay.save(vm.okay, function (result) {
                $scope.okayTodelete =result
                loadFeedPropreties(vm.feed);
                vm.feed.account = true;
            }, function (result) {

            })

        }

        function loadAll() {

            CommentsOfFeed.query({
                id: vm.feed.id,
                page: vm.page,
                size: vm.itemsPerPage,

            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {
                    vm.comments.push(data[i]);
                }

            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset() {
            vm.page = 0;
            vm.feeds = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear() {
            vm.feeds = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;

            loadAll();
        }



        var unsubscribe = $rootScope.$on('netbedlooApp:feedUpdate', function (event, result) {
            vm.feed = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

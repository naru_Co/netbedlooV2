(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FeedDeleteController',FeedDeleteController);

    FeedDeleteController.$inject = ['$uibModalInstance', 'entity', 'Feed'];

    function FeedDeleteController($uibModalInstance, entity, Feed) {
        var vm = this;

        vm.feed = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Feed.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .directive('hires3', hires3)
        .controller('FeedController', FeedController);


    hires3.$inject = []

    function hires3() {
        return {
            restrict: 'A',
            scope: {
                hires3: '@'
            },
            link: function (scope, element, attrs) {
                element.one('load', function () {
                    element.attr('src', scope.hires3);
                });
            }
        };
    }


    FeedController.$inject = ['$cookies', 'Talkers', 'amMoment', '$state', '$rootScope', '$uibModal', '$mdDialog', '$filter', '$translate', 'Top5Comments', 'NumberOfCommentsByFeed', 'Comment', '$timeout', 'ConnectedFeeds', '$scope', 'ArticleByTitleAndCategory', 'Follow', 'Principal', 'Feed', 'ParseLinks', 'AlertService', 'paginationConstants', 'likeByFeedOfPerson', 'likesByFeed', 'numberOfOkay', 'Okay'];

    function FeedController($cookies, Talkers, amMoment, $state, $rootScope, $uibModal, $mdDialog, $filter, $translate, Top5Comments, NumberOfCommentsByFeed, Comment, $timeout, ConnectedFeeds, $scope, ArticleByTitleAndCategory, Follow, Principal, Feed, ParseLinks, AlertService, paginationConstants, likeByFeedOfPerson, likesByFeed, numberOfOkay, Okay) {
        $rootScope.$broadcast('feeds')
        var vm = this;
        $scope.isArab = false;
        vm.feed = {};
        vm.okay = {};
        $scope.followings = [];
        $scope.goToFeed = function (feed) {
            $state.go('feed-detail', {
                id: feed.id
            });

        }

        vm.updatePageDirection = updatePageDirection;
        updatePageDirection()
        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
        })

        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            amMoment.changeLocale(currentLang);
            if (currentLang === "ar-ly") {
                document.getElementById("mytextarea").dir = "rtl";
                document.getElementById("title").dir = "rtl";
                document.getElementById("one").dir = "rtl";
                $scope.isArab = true;

            } else {
                document.getElementById("mytextarea").dir = "ltr";
                document.getElementById("title").dir = "ltr";
                document.getElementById("one").dir = "ltr";
                $scope.isArab = false;
            }

        }

        $scope.sameWantedBooks = [];
        $scope.sameOwnedBooks = [];
        $scope.sameOwnedGames = [];
        $scope.sameWantedGames = [];
        $scope.people = [];
        vm.comment = {}
        vm.feeds = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reset = reset;
        vm.reverse = true;
        vm.clear = clear;
        vm.save = save;
        vm.loadAll = loadAll;
        vm.getAccount = getAccount;
        $scope.chooseBook = 0;
        $scope.chooseGame = 0;


        $scope.openGame = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {


            }, function () {



            });

        }


        getAccount()


        /**************************************************************
         * ------------- Get Account of the connected person----------*
         **************************************************************/
        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                loadAll();
                $scope.followings = Follow.get({
                    id: vm.account.profile.id
                })
            });
        }


        /**************************************************************
         * --------------------- Open Modal for new feed----------------*
         **************************************************************/
        $('.bt').click(function () {
            var buttonId = $(this).attr('id');
            $('#modal-container').removeAttr('class').addClass(buttonId);
            $('body').addClass('modal-active');
            $('#mytextarea').focus();

        })
        $('.btg').click(function () {
            var buttonId = $(this).attr('id');
            $('#modal-container').removeAttr('class').addClass(buttonId);
            $('body').addClass('modal-active');
            $('#mytextarea').focus();
            $scope.chooseGame = 1
            $scope.chooseBook = 0;
            $scope.selectedBooks = [];
            $scope.slected = []
            $scope.readonly = false
            $scope.selectedBook = null;
            vm.searchBook = "";

        })
        $('.btb').click(function () {
            $scope.chooseBook = 1;
            $scope.chooseGame = 0;
            $scope.selectedBooks = [];
            $scope.slected = []
            $scope.readonly = false
            $scope.selectedBook = null;
            vm.searchBook = "";
            var buttonId = $(this).attr('id');
            $('#modal-container').removeAttr('class').addClass(buttonId);
            $('body').addClass('modal-active');
            $('#mytextarea').focus();

        })

        $('.close').click(function () {
            $('#mytextarea').trigger('blur');
            $('#modal-container').addClass('out');
            $('body').removeClass('modal-active');
        });

        /**************************************************************
         * ----------------------- Load Feed Propreties ------------------------*
         **************************************************************/
        function abbrNum(number, decPlaces) {
            // 2 decimal places => 100, 3 => 1000, etc
            decPlaces = Math.pow(10, decPlaces);

            // Enumerate number abbreviations
            var abbrev = [" k", " m", " b", " t"];

            // Go through the array backwards, so we do the largest first
            for (var i = abbrev.length - 1; i >= 0; i--) {

                // Convert array index to "1000", "1000000", etc
                var size = Math.pow(10, (i + 1) * 3);

                // If the number is bigger or equal do the abbreviation
                if (size <= number) {
                    // Here, we multiply by decPlaces, round, and then divide by decPlaces.
                    // This gives us nice rounding to a particular decimal place.
                    number = Math.round(number * decPlaces / size) / decPlaces;

                    // Handle special case where we round up to the next abbreviation
                    if ((number == 1000) && (i < abbrev.length - 1)) {
                        number = 1;
                        i++;
                    }

                    // Add the letter for the abbreviation
                    number += abbrev[i];

                    // We are done... stop
                    break;
                }
            }

            return number;
        }

        $scope.loadFeedPropreties = function (feed) {
            var numberOk = numberOfOkay.get({
                id: feed.id
            }, function (data) {
                feed.numberOfOkay = abbrNum(data[0], 2)
            }, function () {
                feed.numberOfOkay = 0
            });
            var numberCm = NumberOfCommentsByFeed.get({
                id: feed.id
            }, function (data) {
                feed.numberOfCm = abbrNum(data[0], 2)
            }, function () {
                feed.numberCm = 0
            });

            var top5Cm = Top5Comments.get({
                id: feed.id
            }, function (data) {
                feed.topComments = data;
            });

            if (vm.account != null) {
                var likeit = likeByFeedOfPerson.get({
                    feed_id: feed.id,
                    profile_id: vm.account.profile.id
                }, function (data) {

                    if (data.id != null) {
                        feed.account = true
                    } else {
                        feed.account = false
                    }
                }, function () {
                    feed.account = false
                })
            }
            if (feed.type === 10) {
               $scope.followings = Follow.get({
                    id: vm.account.profile.id
                }, function (result) {

                    var index = result.map(function (e) {
                        return e.followed.id;
                    }).indexOf(feed.follow.followed.id);

                    if (index > -1) {
                        feed.isAfriend = true;
                      
                    }else{
                        feed.isAfriend=false;
                    }
                }, function () {})
            }
        }


        /**************************************************************
         * ----------------------- Save New Feed ------------------------*
         **************************************************************/

        $scope.transformChip = function (chip) {
            if (chip != undefined) {

                // If it is an object, it's already a known chip
                if (angular.isObject(chip)) {
                    $scope.readonly = true
                    angular.element('.md-scroll-mask').remove();
                    angular.element('body').attr('style', '');

                    return chip;
                }
            }
        }
        $scope.choose = function (id) {
            $('#mytextarea').trigger('blur');
            if (id === 1) {
                $scope.chooseBook = 1;
            } else {
                $scope.chooseGame = 1
            }


        }
        $scope.clear = function () {
            angular.element('.md-scroll-mask').remove();
            angular.element('body').attr('style', '');
            $scope.readonly = false;
            $scope.selectedBooks = [];
            $scope.selectedBook = null;
            vm.searchBook = "";
        }
        $scope.selectedBooks = [];
        $scope.slected = []
        $scope.readonly = false
        $scope.selectedBook = null;
        vm.searchBook = "";
        $scope.querySearchBook = function (text) {
            if (vm.searchBook.length > 1) {
                return ArticleByTitleAndCategory.query({
                    category_id: 1101,
                    title: text.toLowerCase()
                }).$promise
            }
        }
        $scope.querySearchGame = function (text) {
            return ArticleByTitleAndCategory.query({
                category_id: 1102,
                title: text.toLowerCase()
            }).$promise
        }
        $scope.saveOkay = function (feed) {
            vm.isSaving = true;
            vm.okay.feed = feed;
            vm.okay.profile = vm.account.profile
            Okay.save(vm.okay, function (result) {
                $scope.loadFeedPropreties(feed);
                vm.okay = {}

            }, function (result) {

            })

        }


        $scope.showConfirm2 = function (ev, comment, feed) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.feed.eraseComment'))
                .textContent(translate('netbedlooApp.feed.confirmErase'))
                .ariaLabel('delete comment!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.feed.deleteit'))
                .cancel(translate('netbedlooApp.feed.keep'));

            $mdDialog.show(confirm).then(function () {
                $scope.deleteComment(comment, feed)

            }, function () {});
        };
        $scope.showConfirm = function (ev, feed) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.feed.deletehead'))
                .textContent(translate('netbedlooApp.feed.delety'))
                .ariaLabel('delete post!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.feed.deleteit'))
                .cancel(translate('netbedlooApp.feed.keep'));

            $mdDialog.show(confirm).then(function () {
                $scope.delete(feed)

            }, function () {});
        };
        $scope.deleteComment = function (comment, feed) {
            vm.isSaving = true;

            Comment.delete({
                id: comment.id
            }, function (result) {
                $scope.loadFeedPropreties(feed);
            }, function (result) {

            })




        }
        $scope.deleteOkay = function (feed) {
            vm.isSaving = true;
            var okayy = likeByFeedOfPerson.get({
                feed_id: feed.id,
                profile_id: vm.account.profile.id
            }, function (data) {

                $scope.okayTodelete = data
                Okay.delete({
                    id: $scope.okayTodelete.id
                }, function (result) {
                    $scope.loadFeedPropreties(feed);
                }, function (result) {

                })

            }, function () {
                feed.account = false
            })


        }
        $scope.talkTo = function (person) {
            $rootScope.$broadcast('talkTo',{a:person})
        }
        $scope.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {


            }, function () {



            });

        }
        $scope.secondSpace = function (string, character, n) {
            var count = 0,
                i = 0;
            if (string != undefined) {
                while (count < n && (i = string.indexOf(character, i) + 1)) {
                    count++;
                }
            }
            if (count == n) {
                return i - 1;
            } else {
                return 20
            }

        }

        $scope.followProfile = function (feed) {
            var follow = {};
            follow.follower = vm.account.profile;
            follow.followed = feed.follow.followed;
            Follow.save(follow, function (result) {
                $scope.followings.push(result)
                $rootScope.$broadcast('followoperation');
                feed.isAfriend = true;
            })
        }

        $scope.saveComment = function (feed) {
            vm.isSaving = true;
            vm.comment.feed = feed;
            vm.comment.profile = vm.account.profile
            vm.comment.comment = feed.comment;
            Comment.save(vm.comment, function (result) {
                vm.comment = {};
                $('#comment').val('');
                $scope.loadFeedPropreties(feed);
            }, function (result) {
                $('#comment').val('');

            })

        }

        function save() {
            vm.isSaving = true;
            var json = angular.toJson($scope.selectedBooks);
            $scope.slected = JSON.parse(json)
            vm.feed.owner = vm.account.profile;
            vm.feed.article = $scope.slected[0];
            Feed.save(vm.feed, function (result) {
                $('#mytextarea').trigger('blur');
                $('#modal-container').addClass('out');
                $('body').removeClass('modal-active');
                $scope.loadFeedPropreties(result);
                vm.feeds.push(result);
                $scope.selectedBooks = [];
                $scope.slected = []
                $scope.readonly = false
                $scope.selectedBook = null;
                vm.searchBook = "";
                $scope.chooseBook = 0;
                $scope.chooseGame = 0;
                vm.feed = {};



            }, function (result) {
                $scope.selectedBooks = [];
                $scope.slected = []
                $scope.readonly = false
                $scope.selectedBook = null;
                vm.searchBook = "";
                $scope.chooseBook = 0;
                $scope.chooseGame = 0;
            })






        }

        $scope.delete = function (feed) {
            Feed.get({
                id: feed.id
            }, function (result) {
                var index = vm.feeds.indexOf(feed)
                vm.feeds.splice(index, 1);
                result.statut = false;
                Feed.update(result, function (result) {

                })
            })

        }
        /**************************************************************
         * ----------------------- Load All Feeds ---------------------*
         **************************************************************/
        function concatinateFeeds(i, data, output, x, array) {
            for (var j = 0; j < data.length; j++) {

                if (data[i].owner.id === data[j].owner.id) {
                    if (typeof array != 'undefined') {


                    } else {
                        array = [];
                    }
                    array.push(data[i].x)
                    array.push(data[j].x)
                    data.splice(data[j], 1)

                } else {
                    output.push(data[j])
                }
            }
            var d = new Date();
            var n = d.getDate();
            if (array.length > 0) {
                for (var j = 1; j < array.length; j++) {
                    if (data[i].type === 1 || data[i].type === 9) {
                        var t = new Date(array[j].owningdate)
                    } else {
                        var t = new Date(array[j].wantingDate)
                    }
                    var r = t.getDate();
                    if (r != n) {
                        var lala = output.map(function (e) {
                            return e.x.id;
                        }).indexOf(array[j].id)
                        if (lala === -1) {
                            var xd = data.map(function (e) {
                                return e.x.id;
                            }).indexOf(array[j].id)
                            output.push(data[xd])
                            data.splice(data[xd], i)
                        }
                        array.splice(array[j], 1)

                    }

                }
                output.push(data[i])
                data.splice(data[i])
            } else {
                output.push(data[i])
                data.splice(data[i])
            }


        }

        function loadAll() {

            ConnectedFeeds.query({
                id: vm.account.profile.id,
                page: vm.page,
                size: 6,

            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {

                    /* if (data[i].type === 1) {
                         $scope.sameOwnedBooks.push(data[i]);
                     } else if (data[i].type === 9) {
                         $scope.sameOwnedGames.push(data[i]);
                     } else if (data[i].type === 2) {
                         $scope.sameWantedBooks.push(data[i]);
                     } else if (data[i].type === 8) {
                         $scope.sameWantedGames.push(data[i]);
                     } else {
                         
                        
                     }*/
                    $scope.loadFeedPropreties(data[i])
                    vm.feeds.push(data[i]);
                }

            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset() {
            vm.page = 0;
            vm.feeds = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear() {
            vm.feeds = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.searchQuery = null;
            vm.currentSearch = null;
            vm.loadAll();
        }


    }
})();

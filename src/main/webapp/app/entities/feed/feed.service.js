(function () {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Feed', Feed)
        .factory('ConnectedFeeds', ConnectedFeeds);



        ConnectedFeeds.$inject = ['$resource', 'DateUtils'];

    function ConnectedFeeds($resource, DateUtils) {
        var resourceUrl = 'api/connectedfeeds/:id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }
    Feed.$inject = ['$resource', 'DateUtils'];

    function Feed($resource, DateUtils) {
        var resourceUrl = 'api/feeds/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.time = DateUtils.convertDateTimeFromServer(data.time);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT'
            }
        });
    }
})();

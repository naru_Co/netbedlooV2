(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('feed', {
            parent: 'entity',
            url: '/feed',
            data: {
                authorities: ['ROLE_USER','ROLE_ADMIN'],
                pageTitle: 'netbedlooApp.feed.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/feed/feeds.html',
                    controller: 'FeedController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('feed');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('feed-detail', {
            parent: 'feed',
            url: '/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'netbedlooApp.feed.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/feed/feed-detail.html',
                    controller: 'FeedDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('feed');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Feed', function($stateParams, Feed) {
                    return Feed.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'feed',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('feed-detail.edit', {
            parent: 'feed-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/feed/feed-dialog.html',
                    controller: 'FeedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Feed', function(Feed) {
                            return Feed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('feed.new', {
            parent: 'feed',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/feed/feed-dialog.html',
                    controller: 'FeedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                time: null,
                                type: null,
                                statut: null,
                                content: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('feed', null, { reload: 'feed' });
                }, function() {
                    $state.go('feed');
                });
            }]
        })
        .state('feed.edit', {
            parent: 'feed',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/feed/feed-dialog.html',
                    controller: 'FeedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Feed', function(Feed) {
                            return Feed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('feed', null, { reload: 'feed' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('feed.delete', {
            parent: 'feed',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/feed/feed-delete-dialog.html',
                    controller: 'FeedDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Feed', function(Feed) {
                            return Feed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('feed', null, { reload: 'feed' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('FeedDialogController', FeedDialogController);

    FeedDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Feed', 'MatchingHistory', 'OwnedHistory', 'WantedHistory', 'Follow', 'Profile', 'Article'];

    function FeedDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Feed, MatchingHistory, OwnedHistory, WantedHistory, Follow, Profile, Article) {
        var vm = this;

        vm.feed = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.matchinghistories = MatchingHistory.query();
        vm.owneds = OwnedHistory.query({filter: 'feed-is-null'});
        $q.all([vm.feed.$promise, vm.owneds.$promise]).then(function() {
            if (!vm.feed.owned || !vm.feed.owned.id) {
                return $q.reject();
            }
            return OwnedHistory.get({id : vm.feed.owned.id}).$promise;
        }).then(function(owned) {
            vm.owneds.push(owned);
        });
        vm.wanteds = WantedHistory.query({filter: 'feed-is-null'});
        $q.all([vm.feed.$promise, vm.wanteds.$promise]).then(function() {
            if (!vm.feed.wanted || !vm.feed.wanted.id) {
                return $q.reject();
            }
            return WantedHistory.get({id : vm.feed.wanted.id}).$promise;
        }).then(function(wanted) {
            vm.wanteds.push(wanted);
        });
        vm.follows = Follow.query({filter: 'feed-is-null'});
        $q.all([vm.feed.$promise, vm.follows.$promise]).then(function() {
            if (!vm.feed.follow || !vm.feed.follow.id) {
                return $q.reject();
            }
            return Follow.get({id : vm.feed.follow.id}).$promise;
        }).then(function(follow) {
            vm.follows.push(follow);
        });
        vm.profiles = Profile.query();
        vm.articles = Article.query({filter: 'feed-is-null'});
        $q.all([vm.feed.$promise, vm.articles.$promise]).then(function() {
            if (!vm.feed.article || !vm.feed.article.id) {
                return $q.reject();
            }
            return Article.get({id : vm.feed.article.id}).$promise;
        }).then(function(article) {
            vm.articles.push(article);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.feed.id !== null) {
                Feed.update(vm.feed, onSaveSuccess, onSaveError);
            } else {
                Feed.save(vm.feed, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:feedUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.time = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

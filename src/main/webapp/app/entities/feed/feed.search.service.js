(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('FeedSearch', FeedSearch);

    FeedSearch.$inject = ['$resource'];

    function FeedSearch($resource) {
        var resourceUrl =  'api/_search/feeds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

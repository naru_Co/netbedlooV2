(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('SubjectDetailController', SubjectDetailController);

    SubjectDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Subject', 'Forum', 'Comment', 'Profile'];

    function SubjectDetailController($scope, $rootScope, $stateParams, previousState, entity, Subject, Forum, Comment, Profile) {
        var vm = this;

        vm.subject = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:subjectUpdate', function(event, result) {
            vm.subject = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

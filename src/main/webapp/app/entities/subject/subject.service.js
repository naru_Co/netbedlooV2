(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('SubjectsByForum', SubjectsByForum)
        .factory('Subject', Subject);

    SubjectsByForum.$inject = ['$resource', 'DateUtils'];

    function SubjectsByForum($resource, DateUtils) {
        var resourceUrl = 'api/subjectsbyforum/:id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }

    Subject.$inject = ['$resource', 'DateUtils'];

    function Subject($resource, DateUtils) {
        var resourceUrl = 'api/subjects/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.publicationTime = DateUtils.convertDateTimeFromServer(data.publicationTime);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();
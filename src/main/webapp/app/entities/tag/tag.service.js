(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Tag', Tag)
        .factory('TagsByCategory',TagsByCategory)
        .factory('TagsOfArticle',TagsOfArticle);

        TagsOfArticle.$inject = ['$resource', 'DateUtils'];
        
            function TagsOfArticle($resource, DateUtils) {
                var resourceUrl = 'api/tagsbyarticle/:art_id';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                });
            }
            TagsByCategory.$inject = ['$resource', 'DateUtils'];
            
                function TagsByCategory($resource, DateUtils) {
                    var resourceUrl = 'api/tagsbycategoryandtag/:category_id/:tag';
            
                    return $resource(resourceUrl, {}, {
                        'get': { method: 'GET', isArray: true }
                    });
                }
        Tag.$inject = ['$resource'];

    function Tag ($resource) {
        var resourceUrl =  'api/tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

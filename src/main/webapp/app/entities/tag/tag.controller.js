(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('TagController', TagController);

    TagController.$inject = ['Tag', 'TagSearch'];

    function TagController(Tag, TagSearch) {

        var vm = this;

        vm.tags = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Tag.query(function(result) {
                vm.tags = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            TagSearch.query({query: vm.searchQuery}, function(result) {
                vm.tags = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

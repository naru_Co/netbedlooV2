(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('article', {
                parent: 'entity',
                url: '/Allarticles',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.article.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/article/articles.html',
                        controller: 'ArticleController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('book-detail', {
                parent: 'entity',
                url: '/book/{id}',
                data: {
                    pageTitle: 'netbedlooApp.article.detail.book'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/article/article-detail.html',
                        controller: 'ArticleDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ArticleWithCategory', function ($stateParams, ArticleWithCategory) {
                        return ArticleWithCategory.get({
                            id: $stateParams.id,
                            cat_id: 1101
                        }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'book',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('game-detail', {
                parent: 'entity',
                url: '/video-game/{id}',
                data: {
                    pageTitle: 'netbedlooApp.article.detail.game'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/article/article-detail.html',
                        controller: 'ArticleDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ArticleWithCategory', function ($stateParams, ArticleWithCategory) {
                        return ArticleWithCategory.get({
                            id: $stateParams.id,
                            cat_id: 1102
                        }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'article',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('article-detail', {
                parent: 'entity',
                url: '/article/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.article.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/article/article-detail.html',
                        controller: 'ArticleDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Article', function ($stateParams, Article) {
                        return Article.get({
                            id: $stateParams.id
                        }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'article',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('article-detail.edit', {
                parent: 'article-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/article/article-dialog.html',
                        controller: 'ArticleDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Article', function (Article) {
                                return Article.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {
                            reload: false
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('article.new', {
                parent: 'article',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/article/article-dialog.html',
                        controller: 'ArticleDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    author: null,
                                    coverImage: null,
                                    description: null,
                                    creationDate: null,
                                    popularity: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('article', null, {
                            reload: 'article'
                        });
                    }, function () {
                        $state.go('article');
                    });
                }]
            })
            .state('article.edit', {
                parent: 'article',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/article/article-dialog.html',
                        controller: 'ArticleDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Article', function (Article) {
                                return Article.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('article', null, {
                            reload: 'article'
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('article.delete', {
                parent: 'article',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/article/article-delete-dialog.html',
                        controller: 'ArticleDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Article', function (Article) {
                                return Article.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('article', null, {
                            reload: 'article'
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();

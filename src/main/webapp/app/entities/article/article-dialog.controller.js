(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ArticleDialogController', ArticleDialogController);

    ArticleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Article', 'Category', 'Profile', 'Matched', 'Owned', 'Wanted', 'MatchingHistory', 'OwnedHistory', 'WantedHistory', 'Rating', 'Tag'];

    function ArticleDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Article, Category, Profile, Matched, Owned, Wanted, MatchingHistory, OwnedHistory, WantedHistory, Rating, Tag) {
        var vm = this;

        vm.article = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.categories = Category.query();
        vm.profiles = Profile.query();
        vm.matcheds = Matched.query();
        vm.owneds = Owned.query();
        vm.wanteds = Wanted.query();
        vm.matchinghistories = MatchingHistory.query();
        vm.ownedhistories = OwnedHistory.query();
        vm.wantedhistories = WantedHistory.query();
        vm.ratings = Rating.query();
        vm.tags = Tag.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.article.id !== null) {
                Article.update(vm.article, onSaveSuccess, onSaveError);
            } else {
                Article.save(vm.article, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:articleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }


    }
})();
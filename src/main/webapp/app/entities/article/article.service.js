(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Article', Article)
        .factory('ArticleByCategory', ArticleByCategory)
        .factory('ArticleWithCategory', ArticleWithCategory)
        .factory('BookByTitle', BookByTitle)
        .factory('ArticleByTitleAndCategory', ArticleByTitleAndCategory)
        .factory('ArticleByTitle', ArticleByTitle)
        .factory('ArticleByTitleOrAuthor', ArticleByTitleOrAuthor)
        .factory('ArticleByCategoryFiltred',ArticleByCategoryFiltred)
        .factory('Sbewlou',Sbewlou);
        
        Sbewlou.$inject = ['$resource', 'DateUtils'];
        
            function Sbewlou($resource, DateUtils) {
                var resourceUrl = 'api/GamesIgdb/:title';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                });
            }

        ArticleByTitleOrAuthor.$inject = ['$resource', 'DateUtils'];
        
            function ArticleByTitleOrAuthor($resource, DateUtils) {
                var resourceUrl = 'api/articlesbytitleorauthor/:title/:author';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                });
            }
        

        ArticleByCategoryFiltred.$inject = ['$resource', 'DateUtils'];
        
            function ArticleByCategoryFiltred($resource, DateUtils) {
                var resourceUrl = 'api/articlesbycategoryandspec/:category_id/:spec_id';
        
                return $resource(resourceUrl, {}, {
                    'get': { method: 'GET', isArray: true }
                });
            }
        
        
            ArticleWithCategory.$inject = ['$resource', 'DateUtils'];
        
            function ArticleWithCategory($resource, DateUtils) {
                var resourceUrl = 'api/articlewithcategory/:id/:cat_id';
        
                return $resource(resourceUrl, {}, {
                    'get': {
                        method: 'GET',
                        transformResponse: function(data) {
                            if (data) {
                                data = angular.fromJson(data);
                                data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                            }
                            return data;
                        }
                    }
                });
            }
        ArticleByTitle.$inject = ['$resource', 'DateUtils'];
        
            function ArticleByTitle($resource, DateUtils) {
                var resourceUrl = 'api/articlebytitle/:title';
        
                return $resource(resourceUrl, {}, {
                    'get': {
                        method: 'GET',
                        transformResponse: function(data) {
                            if (data) {
                                data = angular.fromJson(data);
                                data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                            }
                            return data;
                        }
                    }
                });
            }
    
            ArticleByTitleAndCategory.$inject = ['$resource', 'DateUtils'];
            
                function ArticleByTitleAndCategory($resource, DateUtils) {
                    var resourceUrl = 'api/booksbytitleandcategory/:category_id/:title';
            
                    return $resource(resourceUrl, {}, {
                        'get': { method: 'GET', isArray: true }
                    });
                }
    
        BookByTitle.$inject = ['$resource', 'DateUtils'];

    function BookByTitle($resource, DateUtils) {
        var resourceUrl = 'api/booksbytitle/:title';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    ArticleByCategory.$inject = ['$resource', 'DateUtils'];

    function ArticleByCategory($resource, DateUtils) {
        var resourceUrl = 'api/articlesbycategory/:catid';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }


    Article.$inject = ['$resource', 'DateUtils'];

    function Article($resource, DateUtils) {
        var resourceUrl = 'api/articles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function(data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
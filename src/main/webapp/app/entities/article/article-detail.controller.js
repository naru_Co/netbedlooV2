(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ArticleDetailController', ArticleDetailController);

    ArticleDetailController.$inject = ['$filter', '$translate', '$state', '$timeout', 'RateOfArticleByPerson', 'RateOfArticle', 'Signal', '$mdDialog', 'Principal', '$scope', '$rootScope', '$stateParams', 'CommentsBySubject', 'SubjectsByForum', 'ResponsesByComment', 'WantedByArticle', 'OwnedByArticle', 'previousState', 'entity', 'Article', 'Category', 'Profile', 'Matched', 'Owned', 'Wanted', 'MatchingHistory', 'OwnedHistory', 'WantedHistory', 'Forum', 'Comment', 'Subject', 'Response', 'Rating'];

    function ArticleDetailController($filter, $translate, $state, $timeout, RateOfArticleByPerson, RateOfArticle, Signal, $mdDialog, Principal, $scope, $rootScope, $stateParams, CommentsBySubject, SubjectsByForum, ResponsesByComment, WantedByArticle, OwnedByArticle, previousState, entity, Article, Category, Profile, Matched, Owned, Wanted, MatchingHistory, OwnedHistory, WantedHistory, Forum, Comment, Subject, Response, Rating) {
        var vm = this;

        vm.article = entity;
        $scope.rateit = 0;
        if (vm.article.category.id == 1102) {

            $rootScope.$broadcast('isGame')
        }
        setTitle(vm.article.title);
        getAccount()
        vm.signal = {
            id: null,
            content: null,
            type: null,
            date: null,
            profile: null
        };
        $scope.isArab = false
        $scope.arabia = false
        updatePageDirection()
        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
        });
        vm.previousState = previousState.name;
        if (vm.article.description != null) {
            var index = vm.article.description.indexOf(".");

            if (index > 200) {
                $scope.shortdesc = vm.article.description.substr(0, index);
            } else {
                $scope.shortdesc = vm.article.description.split(" ").splice(0, 50).join(" ");
            }
        }
        $scope.isGame = false;
        if (vm.article.category.id == '1102') {
            $scope.isGame = true;
        }




        $scope.owneds = [];
        $scope.wanteds = [];
        $scope.subjects = [];
        $scope.rateit = 0;

        vm.loadAllSubjects = loadAllSubjects;
        $scope.content = "";
        $scope.reported = false;
        $scope.rate = 0;

        $scope.isOpen = false;
        $scope.isOpen2 = false;
        $scope.hidden2 = false;
        $scope.hover2 = false;

        $scope.hidden = false;
        $scope.hover = false;

        // On opening, add a delayed property which shows tooltips after the speed dial has opened
        // so that they have the proper position; if closing, immediately hide the tooltips
        $scope.$watch('isOpen', function (isOpen) {
            if (isOpen) {
                $timeout(function () {
                    $scope.tooltipVisible = $scope.isOpen;
                }, 600);
            } else {
                $scope.tooltipVisible = $scope.isOpen;
            }
        });
        $scope.$watch('isOpen2', function (isOpen2) {
            if (isOpen2) {
                $timeout(function () {
                    $scope.tooltipVisible2 = $scope.isOpen2;
                }, 600);
            } else {
                $scope.tooltipVisible2 = $scope.isOpen2;
            }
        });
        $scope.connectedFrom = null
        $.getJSON('https://freegeoip.net/json/?callback=?', function (data) {
            $scope.connectedFrom = JSON.stringify(data.country_name, null, 2)
        });
        $scope.showConfirm4 = function (ev) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.article.detail.hello') + ' ' + $scope.connectedFrom + ' ? ' + translate('netbedlooApp.article.detail.welcome'))
                .textContent(translate('netbedlooApp.article.detail.auth'))
                .ariaLabel('Welcome')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.article.detail.connect'))
                .cancel(translate('netbedlooApp.article.detail.register'));

            $mdDialog.show(confirm).then(function () {
                $state.go('home')

            }, function () {
                $state.go('register')
            });
        };


        vm.rates = RateOfArticle.get({
            rated_id: vm.article.id
        }, function (data) {
            if (data[0] != 0) {
                $scope.rate = data[1].toFixed(1)
                $scope.raters = data[0]
            } else {
                $scope.rate = 0;
                $scope.raters = data[0]
            }

        });




        $scope.max = 5;
        $scope.isReadonly = false;

        $scope.hoveringOver = function (value) {
            $scope.overStar = value;
            $scope.percent = value;
        };


        $scope.changeWantState = function () {
            if ($scope.want === 1) {
                Wanted.delete({
                    id: $scope.wanteds[$scope.wantIndx].id
                }, function () {
                    loadAll();

                });


                $scope.want = 0

            } else {
                $scope.want = 1;
                vm.wanted = {};
                vm.wanted.wanter = vm.account.profile
                vm.wanted.article = vm.article
                Wanted.save(vm.wanted, function () {
                    loadAll(vm.account);

                });



            }

        }

        $scope.changeOwnState = function () {
            if ($scope.own === 1) {
                Owned.delete({
                    id: $scope.owneds[$scope.ownIndx].id
                }, function () {
                    loadAll();

                });



                $scope.own = 0

            } else {
                $scope.own = 1;
                vm.owned = {};
                vm.owned.owner = vm.account.profile
                vm.owned.article = vm.article
                Owned.save(vm.owned, function () {
                    loadAll(vm.account);
                });
            }
        }

        $scope.change = function (rating) {

            if ($scope.rating.id != null) {
                $scope.rating.rate = rating;
                Rating.update($scope.rating,
                    function () {
                        vm.rates = RateOfArticle.get({
                            rated_id: vm.article.id
                        }, function (data) {

                            $scope.rate = data[1].toFixed(1)
                            $scope.raters = data[0]
                        })
                        $scope.rateit = rating;
                    })
            } else {
                $scope.rating.rate = rating;
                $scope.rating.rated = vm.article;
                $scope.rating.rater = vm.account.profile

                Rating.save($scope.rating, function () {
                    vm.rates = RateOfArticle.get({
                        rated_id: vm.article.id
                    }, function (data) {

                        $scope.rate = data[1].toFixed(1)
                        $scope.raters = data[0]
                    })
                    $scope.rateit = rating;
                })
            }

        }

        function setTitle(extra) {
            document.title = location.pathname.split('/').map(function (v) {
                return v.replace(/\.(html|php)/, '')
                    .replace(/^\w/, function (a) {
                        return a.toUpperCase();
                    });
            }).join(' | ') + (extra && ' | ' + extra);
        }
        $scope.saveReport = function () {
            $scope.header = vm.account.profile.pseudo + " wants to report " + vm.article.title + " by " + vm.article.author + ", Published By " + vm.article.profile.firstName + vm.article.profile.lastName + " at " + vm.article.creationDate + "." + "\n" + "Reason : " + "\n"
            vm.signal.content = $scope.header + $scope.content;
            vm.signal.profile = vm.account.profile;
            vm.signal.type = 1;
            Signal.save(vm.signal, function () {
                $scope.reported = true

            })
        }

        function loadAllSubjects() {
            $scope.subjects = SubjectsByForum.get({
                id: vm.article.forum.id
            }, function (result) {

            }, function () {

            })
        }
        $scope.groups = [{
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            },
            {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }
        ];

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.addItem = function () {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
        };

        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
            isFirstDisabled: false
        };




        $scope.showConfirm = function (ev, pub) {
            var translate = $filter('translate');
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('netbedlooApp.article.detail.erase'))
                .textContent(translate('netbedlooApp.article.detail.confirm'))
                .ariaLabel('Delete!')
                .targetEvent(ev)
                .ok(translate('netbedlooApp.article.detail.yes'))
                .cancel(translate('netbedlooApp.article.detail.no'));

            $mdDialog.show(confirm).then(function () {
                DeletePublication(pub)

            }, function () {});
        };

        function DeletePublication(pub) {
            Subject.delete({
                id: pub
            }, function () {
                loadAllSubjects()

            })

        }




        vm.loadAllWithoutAccount = loadAllWithoutAccount;



        function loadAllWithoutAccount() {

            $scope.owneds = OwnedByArticle.get({
                id: vm.article.id
            })
            $scope.wanteds = WantedByArticle.get({
                id: vm.article.id
            })


        }



        $scope.showDelete = function (ev) {
            var translate = $filter('translate');
            translate('netbedlooApp.profile.follow')
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('entity.delete.title'))
                .textContent(translate('netbedlooApp.article.delete.question'))
                .ariaLabel('Delete!')
                .targetEvent(ev)
                .ok(translate('entity.action.delete'))
                .cancel(translate('entity.action.cancel'));

            $mdDialog.show(confirm).then(function () {
                confirmDelete()

            }, function () {});
        };

        function confirmDelete() {
            
            Article.delete({
                    id: vm.article.id
                },
                function () {
                    if($scope.isGame){
                        $state.go('gaming')
                    }else{
                        $state.go('books')  
                    }
                   
                });
        }
        $scope.showDeleteGame = function (ev) {
            var translate = $filter('translate');
            translate('netbedlooApp.profile.follow')
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(translate('entity.delete.title'))
                .textContent(translate('netbedlooApp.article.delete.question'))
                .ariaLabel('Delete!')
                .targetEvent(ev)
                .ok(translate('entity.action.delete'))
                .cancel(translate('entity.action.cancel'));

            $mdDialog.show(confirm).then(function () {
                confirmDeleteGame()

            }, function () {});
        };

        function confirmDeleteGame() {
            Article.delete({
                    id: vm.article.id
                },
                function () {
                    $state.go('gaming')
                });
        }

        function updatePageDirection() {
            var arabic = /[\u0600-\u06FF]/;
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.arabia = true;
            }

            if (arabic.test(vm.article.title)) {
                $scope.isArab = true
            } else {
                $scope.isArab = false
            }

        }
        $scope.IfArab = function (text) {
            var arabic = /[\u0600-\u06FF]/;
            if (arabic.test(text)) {
                return true
            } else {
                return false
            }
        }


        function loadAll(account) {

            $scope.owneds = OwnedByArticle.get({
                id: vm.article.id
            }, function (result) {
                $scope.ownIndx = result.map(function (e) {
                    return e.owner.id;
                }).indexOf(account.profile.id)
                if ($scope.ownIndx > -1) {
                    $scope.own = 1;

                } else {
                    $scope.own = 0;
                }

            })
            $scope.wanteds = WantedByArticle.get({
                id: vm.article.id
            }, function (result) {

                $scope.wantIndx = result.map(function (e) {
                    return e.wanter.id;
                }).indexOf(account.profile.id)
                if ($scope.wantIndx > -1) {
                    $scope.want = 1;

                } else {
                    $scope.want = 0;

                }
            })

        }



        // Saving Subject //


        vm.subject = {};
        vm.clearSubject = clearSubject;
        vm.saveSubject = saveSubject;


        function clearSubject() {
            vm.subject = {}
        }


        function saveSubject() {
            vm.isSaving = true;
            vm.subject.profile = vm.account.profile;
            vm.subject.forum = vm.article.forum;

            Subject.save(vm.subject, onSaveSubjectSuccess, onSaveSubjectError);

        }

        function onSaveSubjectSuccess(result) {
            $scope.$emit('netbedlooApp:subjectUpdate', result);
            vm.subject = {}
            vm.isSaving = false;
            loadAllSubjects()

        }

        function onSaveSubjectError() {
            vm.isSaving = false;
        }
        // Saving Comments //
        vm.comment = {};
        vm.clearComment = clearComment;
        vm.saveComment = saveComment;

        function clearComment() {
            vm.comment = {}
        }


        function saveComment(subject) {
            vm.isSaving = true;
            vm.comment.profile = vm.account.profile;
            vm.comment.subject = subject;
            Comment.save(vm.comment, onSaveCommentSuccess, onSaveCommentError);

        }

        function onSaveCommentSuccess(result) {
            $scope.$emit('netbedlooApp:commentUpdate', result);

            vm.isSaving = false;
        }

        function onSaveCommentError() {
            vm.isSaving = false;
        }

        // Saving Replies //
        vm.reply = {};
        vm.clearResponse = clearResponse;
        vm.saveResponse = saveResponse;

        function clearResponse() {
            vm.subject = {}
        }


        function saveResponse(comment) {
            vm.isSaving = true;
            vm.reply.profile = vm.account.profile;
            vm.reply.comment = comment;
            Response.save(vm.reply, onSaveResponseSuccess, onSaveResponseError);

        }

        function onSaveResponseSuccess(result) {
            $scope.$emit('netbedlooApp:responseUpdate', result);

            vm.isSaving = false;
        }

        function onSaveResponseError() {
            vm.isSaving = false;
        }


        function getAccount() {

            Principal.identity().then(function (account) {
                vm.account = account;


                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.account != null) {
                    loadAll(account);
                    loadAllSubjects()
                    $scope.thisPersonRate = RateOfArticleByPerson.get({
                        rated_id: vm.article.id,
                        rater_id: vm.account.profile.id
                    }, function (result) {
                        if (result.rate != null) {
                            $scope.rating = result
                            $scope.rateit = result.rate
                        } else {
                            $scope.rating = {
                                id: null,
                                rate: 0,
                                rater: {},
                                rated: {}
                            };
                            $scope.rateit = 0;
                        }
                    }, function () {

                        $scope.rating = {
                            id: null,
                            rate: 0,
                            rater: {},
                            rated: {}
                        };
                        $scope.rateit = 0;
                    })

                } else {
                    $scope.own = 0;
                    $scope.want = 0;
                    loadAllWithoutAccount()
                }



            }).then(function () {
                $scope.own = 0;
                $scope.want = 0;
                loadAllWithoutAccount()
            })
        }





        var unsubscribe = $rootScope.$on('netbedlooApp:articleUpdate', function (event, result) {
            vm.article = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

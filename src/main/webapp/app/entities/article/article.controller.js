(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('ArticleController', ArticleController);

    ArticleController.$inject = ['Article', 'ArticleSearch'];

    function ArticleController(Article, ArticleSearch) {

        var vm = this;

        vm.articles = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();
       
        function loadAll() {
            Article.query(function(result) {
                vm.articles = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ArticleSearch.query({query: vm.searchQuery}, function(result) {
                vm.articles = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

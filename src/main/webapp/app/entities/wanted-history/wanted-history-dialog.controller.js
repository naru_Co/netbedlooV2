(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedHistoryDialogController', WantedHistoryDialogController);

    WantedHistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'WantedHistory', 'Article', 'Profile'];

    function WantedHistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, WantedHistory, Article, Profile) {
        var vm = this;

        vm.wantedHistory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.articles = Article.query();
        vm.profiles = Profile.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.wantedHistory.id !== null) {
                WantedHistory.update(vm.wantedHistory, onSaveSuccess, onSaveError);
            } else {
                WantedHistory.save(vm.wantedHistory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:wantedHistoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.wantingDate = false;
        vm.datePickerOpenStatus.eraseDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedHistoryDeleteController',WantedHistoryDeleteController);

    WantedHistoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'WantedHistory'];

    function WantedHistoryDeleteController($uibModalInstance, entity, WantedHistory) {
        var vm = this;

        vm.wantedHistory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            WantedHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

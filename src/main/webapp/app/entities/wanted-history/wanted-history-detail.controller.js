(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('WantedHistoryDetailController', WantedHistoryDetailController);

    WantedHistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'WantedHistory', 'Article', 'Profile'];

    function WantedHistoryDetailController($scope, $rootScope, $stateParams, previousState, entity, WantedHistory, Article, Profile) {
        var vm = this;

        vm.wantedHistory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:wantedHistoryUpdate', function(event, result) {
            vm.wantedHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('WantedHistorySearch', WantedHistorySearch);

    WantedHistorySearch.$inject = ['$resource'];

    function WantedHistorySearch($resource) {
        var resourceUrl =  'api/_search/wanted-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('wanted-history', {
                parent: 'entity',
                url: '/wanted-history',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.wantedHistory.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/wanted-history/wanted-histories.html',
                        controller: 'WantedHistoryController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('wantedHistory');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('wanted-history-detail', {
                parent: 'wanted-history',
                url: '/wanted-history/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.wantedHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/wanted-history/wanted-history-detail.html',
                        controller: 'WantedHistoryDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('wantedHistory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'WantedHistory', function($stateParams, WantedHistory) {
                        return WantedHistory.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'wanted-history',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('wanted-history-detail.edit', {
                parent: 'wanted-history-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted-history/wanted-history-dialog.html',
                        controller: 'WantedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['WantedHistory', function(WantedHistory) {
                                return WantedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('wanted-history.new', {
                parent: 'wanted-history',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted-history/wanted-history-dialog.html',
                        controller: 'WantedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    wantingDate: null,
                                    eraseDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('wanted-history', null, { reload: 'wanted-history' });
                    }, function() {
                        $state.go('wanted-history');
                    });
                }]
            })
            .state('wanted-history.edit', {
                parent: 'wanted-history',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted-history/wanted-history-dialog.html',
                        controller: 'WantedHistoryDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['WantedHistory', function(WantedHistory) {
                                return WantedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('wanted-history', null, { reload: 'wanted-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('wanted-history.delete', {
                parent: 'wanted-history',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/wanted-history/wanted-history-delete-dialog.html',
                        controller: 'WantedHistoryDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['WantedHistory', function(WantedHistory) {
                                return WantedHistory.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('wanted-history', null, { reload: 'wanted-history' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
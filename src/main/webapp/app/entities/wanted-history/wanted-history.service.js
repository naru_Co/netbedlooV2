(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('WantedHistory', WantedHistory);

    WantedHistory.$inject = ['$resource', 'DateUtils'];

    function WantedHistory ($resource, DateUtils) {
        var resourceUrl =  'api/wanted-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.wantingDate = DateUtils.convertLocalDateFromServer(data.wantingDate);
                        data.eraseDate = DateUtils.convertLocalDateFromServer(data.eraseDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.wantingDate = DateUtils.convertLocalDateToServer(copy.wantingDate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.wantingDate = DateUtils.convertLocalDateToServer(copy.wantingDate);
                    copy.eraseDate = DateUtils.convertLocalDateToServer(copy.eraseDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

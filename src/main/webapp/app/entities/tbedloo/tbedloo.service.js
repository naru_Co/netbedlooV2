(function() {
    'use strict';
    angular
        .module('netbedlooApp')
        .factory('Tbedloo', Tbedloo);

    Tbedloo.$inject = ['$resource', 'DateUtils'];

    function Tbedloo ($resource, DateUtils) {
        var resourceUrl =  'api/tbedloos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.tbedlooDate = DateUtils.convertLocalDateFromServer(data.tbedlooDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tbedlooDate = DateUtils.convertLocalDateToServer(copy.tbedlooDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tbedlooDate = DateUtils.convertLocalDateToServer(copy.tbedlooDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

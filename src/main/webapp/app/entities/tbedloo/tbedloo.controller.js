(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('TbedlooController', TbedlooController);

    TbedlooController.$inject = ['Tbedloo', 'TbedlooSearch'];

    function TbedlooController(Tbedloo, TbedlooSearch) {

        var vm = this;

        vm.tbedloos = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Tbedloo.query(function(result) {
                vm.tbedloos = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            TbedlooSearch.query({query: vm.searchQuery}, function(result) {
                vm.tbedloos = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();

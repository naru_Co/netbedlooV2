(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('TbedlooDeleteController',TbedlooDeleteController);

    TbedlooDeleteController.$inject = ['$uibModalInstance', 'entity', 'Tbedloo'];

    function TbedlooDeleteController($uibModalInstance, entity, Tbedloo) {
        var vm = this;

        vm.tbedloo = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Tbedloo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

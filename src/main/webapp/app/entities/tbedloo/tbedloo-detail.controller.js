(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('TbedlooDetailController', TbedlooDetailController);

    TbedlooDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Tbedloo', 'Matched'];

    function TbedlooDetailController($scope, $rootScope, $stateParams, previousState, entity, Tbedloo, Matched) {
        var vm = this;

        vm.tbedloo = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('netbedlooApp:tbedlooUpdate', function(event, result) {
            vm.tbedloo = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

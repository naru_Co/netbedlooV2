(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .factory('TbedlooSearch', TbedlooSearch);

    TbedlooSearch.$inject = ['$resource'];

    function TbedlooSearch($resource) {
        var resourceUrl =  'api/_search/tbedloos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

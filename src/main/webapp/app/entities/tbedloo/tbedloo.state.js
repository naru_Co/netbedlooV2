(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('tbedloo', {
                parent: 'entity',
                url: '/tbedloo',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.tbedloo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/tbedloo/tbedloos.html',
                        controller: 'TbedlooController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tbedloo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tbedloo-detail', {
                parent: 'tbedloo',
                url: '/tbedloo/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'netbedlooApp.tbedloo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/tbedloo/tbedloo-detail.html',
                        controller: 'TbedlooDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tbedloo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tbedloo', function($stateParams, Tbedloo) {
                        return Tbedloo.get({ id: $stateParams.id }).$promise;
                    }],
                    previousState: ["$state", function($state) {
                        var currentStateData = {
                            name: $state.current.name || 'tbedloo',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('tbedloo-detail.edit', {
                parent: 'tbedloo-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/tbedloo/tbedloo-dialog.html',
                        controller: 'TbedlooDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Tbedloo', function(Tbedloo) {
                                return Tbedloo.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('tbedloo.new', {
                parent: 'tbedloo',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/tbedloo/tbedloo-dialog.html',
                        controller: 'TbedlooDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                    tbedlooDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('tbedloo', null, { reload: 'tbedloo' });
                    }, function() {
                        $state.go('tbedloo');
                    });
                }]
            })
            .state('tbedloo.edit', {
                parent: 'tbedloo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/tbedloo/tbedloo-dialog.html',
                        controller: 'TbedlooDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Tbedloo', function(Tbedloo) {
                                return Tbedloo.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('tbedloo', null, { reload: 'tbedloo' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('tbedloo.delete', {
                parent: 'tbedloo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/tbedloo/tbedloo-delete-dialog.html',
                        controller: 'TbedlooDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Tbedloo', function(Tbedloo) {
                                return Tbedloo.get({ id: $stateParams.id }).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('tbedloo', null, { reload: 'tbedloo' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('TbedlooDialogController', TbedlooDialogController);

    TbedlooDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tbedloo', 'Matched'];

    function TbedlooDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Tbedloo, Matched) {
        var vm = this;

        vm.tbedloo = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.matcheds = Matched.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tbedloo.id !== null) {
                Tbedloo.update(vm.tbedloo, onSaveSuccess, onSaveError);
            } else {
                Tbedloo.save(vm.tbedloo, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('netbedlooApp:tbedlooUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.tbedlooDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

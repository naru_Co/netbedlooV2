(function() {
    'use strict';

    angular
        .module('netbedlooApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('books', {
                parent: 'app',
                url: '/books',
                data: {
                    authorities: [],
                    pageTitle: 'global.titleb'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/books/books.html',
                        controller: 'BooksController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('home');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('gaming', {
                parent: 'app',
                url: '/video-games',
                data: {
                    authorities: [],
                    pageTitle: 'global.titleg'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/books/gaming.html',
                        controller: 'GamingController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('home');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
    }



})();
(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('bookAddingController', bookAddingController)
        .directive('customOnChange', customOnChange)

    function customOnChange() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    };


    bookAddingController.$inject = ['$rootScope','$translate','SpecsByCategoryAndName', 'TagsByCategory', 'Tag', 'ArticleByTitleAndCategory', '$http', '$uibModalInstance', 'WantedByArticle', 'Category', 'OwnedByArticle', 'entity', 'Profile', '$scope', 'Principal', 'Article', 'Wanted', 'Owned'];

    function bookAddingController( $rootScope,$translate,SpecsByCategoryAndName, TagsByCategory, Tag, ArticleByTitleAndCategory, $http, $uibModalInstance, WantedByArticle, Category, OwnedByArticle, entity, Profile, $scope, Principal, Article, Wanted, Owned) {
        var vm = this;
$scope.error=null
        $scope.creator = entity;
        vm.isSaving = false;
        vm.updatePageDirection = updatePageDirection;
        $scope.isArab = false
        $scope.loading = false;
        updatePageDirection()
        $scope.$on('languageChanged', function(event, args) {
            updatePageDirection()
        });
        $scope.currentCat = Category.get({
            id: 1101
        });

        $scope.cat = 1;

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        $scope.sbewlou = false;
        $scope.hide=false;
        $scope.description = "";
        $scope.author = "";
        $scope.averageRate = 0;
        $scope.title = "";
        $scope.want = 0;
        $scope.own = 0;
        $scope.livresFavoris = {};
        $scope.livresPossedes = {};
        $scope.coverImage = "content/images/NaruLogoposter2.jpg"
        vm.article = {
            name: null,
            type: null,
            file: null,
            article: {}
        };



        $scope.querySearch = function (query) {
            $scope.title = query;
            $scope.sbewlou = true;
            return ArticleByTitleAndCategory.get({
                category_id: 1101,
                title: query.toLowerCase()
            }).$promise
        }

        $scope.avilable = 0;
        $scope.updateInfo = function (item) {
            $scope.chosenOne = item;

            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                return e.article.id;
            }).indexOf(item.id);
            if ($scope.wantIndx > -1) {
                $scope.want = 1;
            }
            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                return e.article.id;
            }).indexOf(item.id)
            if ($scope.ownIndx > -1) {
                $scope.own = 1;
            }

            $scope.title = item.title;
            $scope.Tid = item.id;
            $scope.author = item.author;
            $scope.description = item.description;
            $scope.coverImage = item.coverImage;
            $scope.myCroppedImage = item.coverImage;
            $scope.avilable = 1;

        }
        getAccount()

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;

                $scope.livresFavoris = Wanted.get({
                    id: vm.account.profile.id
                }, function () {
                    $scope.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    }, function () {



                    })

                })
            });
        }

        /**************************************************************
         * ----------------------- Tags Chips ------------------------*
         **************************************************************/

        $scope.transformChipTag = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return { id: null, tag: chip }
        }

        $scope.selectedTags = [];
        vm.selectedTag = null;
        vm.searchTag = null;

        $scope.querySearchTag = function (text) {
            return TagsByCategory.query({
                category_id: 1101,
                tag: text.toLowerCase()
            }).$promise
        }

          /**************************************************************
         * ----------------------- Web Crawlers ------------------------*
         **************************************************************/

        $scope.getData = function (data) {
            $scope.$apply(function () {
                $scope.description = data.items[0].volumeInfo.description;
                $scope.title = data.items[0].volumeInfo.title;
                $scope.author = data.items[0].volumeInfo.authors[0];
                $scope.averageRate = data.items[0].volumeInfo.averageRating;
                $scope.hide=true;

            });



        }
        $scope.getInformations = function () {
            $.getJSON('https://www.googleapis.com/books/v1/volumes?q=' + $scope.title +' '+ $scope.author + '&maxResults=4&key=AIzaSyCC_ZbyFKXJMabDcgFzs5tvxOFUEwHiJEk').then(function (data) {
                $scope.getData(data)
            

            })
            $scope.getImageFromGoogle()

        }
        $scope.getImageFromGoogle = function () {
            $.getJSON('https://www.googleapis.com/customsearch/v1?key=AIzaSyCC_ZbyFKXJMabDcgFzs5tvxOFUEwHiJEk&cx=016301181575101414302:idcaho1v7ly&num=1&searchType=image&q=' + $scope.title + '' + $scope.author + ' book').then(function (res) {
                $scope.$apply(function () {
                    $scope.myImage = res.items[0].link;
                })
                $('#myModal').modal('show');

            })
        }

       

        /**************************************************************
         * ----------------------- Specification Autocomplete---------*
         **************************************************************/
        $scope.selectedLanguage = null;
        vm.searchLg = null;

        $scope.querySearchLanguage = function (text) {
            return SpecsByCategoryAndName.query({
                category_id: 1101,
                name: text.toLowerCase()
            }).$promise
        }

        $scope.selectedItemChange = function (item) {
            vm.article.article.spec = item;
        }
        function updatePageDirection() {
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.isArab = true
            } else {
                $scope.isArab = false
            }

        }

        /**************************************************************
         * ----------------------- Wanted and Owned Stuffs---------*
         **************************************************************/
        $scope.changeWantState = function () {
            if ($scope.want === 1) {
                Wanted.delete({
                    id: $scope.livresFavoris[$scope.wantIndx].id
                }, function () {

                });


                $scope.want = 0

            } else {
                $scope.want = 1;
                vm.wanted = {};
                vm.wanted.wanter = vm.account.profile
                vm.wanted.article = $scope.chosenOne
                Wanted.save(vm.wanted, function () {
                    $scope.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {

                            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id);
                            if ($scope.wantIndx > -1) {
                                $scope.want = 1;
                            }
                            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id)
                            if ($scope.ownIndx > -1) {
                                $scope.own = 1;
                            }



                        })

                    })



                });



            }

        }

        $scope.changeOwnState = function () {
            if ($scope.own === 1) {
                Owned.delete({
                    id: $scope.livresPossedes[$scope.ownIndx].id
                }, function () {

                });



                $scope.own = 0

            } else {
                $scope.own = 1;
                vm.owned = {};
                vm.owned.owner = vm.account.profile
                vm.owned.article = $scope.chosenOne
                Owned.save(vm.owned, function () {
                    $scope.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {

                            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id);
                            if ($scope.wantIndx > -1) {
                                $scope.want = 1;
                            }
                            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id)
                            if ($scope.ownIndx > -1) {
                                $scope.own = 1;
                            }



                        })

                    })



                });


            }

        }

        $scope.clear = function () {
            $uibModalInstance.dismiss('cancel');
        }
        

        $scope.save = function () {
            vm.isSaving = true;
            $scope.loading = true;
            vm.article.article.description = $scope.description

            vm.article.article.author = $scope.author

            vm.article.article.title = $scope.title;
            vm.article.article.creationDate = new Date()
            vm.article.article.category = $scope.currentCat;
            vm.article.article.profile = $scope.creator;
            vm.article.article.tags = $scope.selectedTags;
            vm.article.article.averageRate=$scope.averageRate;
            Article.save(vm.article, onSaveSuccess, onSaveError);


        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:articleUpdate', result);
            $scope.loading = false;
            vm.isSaving = false;
            $rootScope.$broadcast('bookadd',{ a: result});
            $uibModalInstance.close(result)
        }

        function onSaveError() {
            $scope.error="ERROR"
            $scope.loading = false;
            vm.isSaving = false;

        }
       /**/

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }


        $scope.myImage = '';

        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function () {
            if ($scope.myCroppedImage != '') {
                vm.article.file = $scope.myCroppedImage.split(',')[1];
               

            }
        });



        $scope.handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];

            vm.article.name = file.name;
            vm.article.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        }






        /* end of image stuff */

    }
})();
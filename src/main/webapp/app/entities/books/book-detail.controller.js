(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('booksDetailController', booksDetailController);


    booksDetailController.$inject = ['ProfileByName', 'TagsOfArticle', 'TagsByCategory', '$rootScope', '$translate', '$state', '$mdDialog', 'Signal', 'RateOfArticleByPerson', '$timeout', 'RateOfArticle', 'Rating', '$uibModalInstance', 'WantedByArticle', 'OwnedByArticle', 'entity', 'Profile', '$scope', 'Principal', 'Article', 'Wanted', 'Owned'];

    function booksDetailController(ProfileByName, TagsOfArticle, TagsByCategory, $rootScope, $translate, $state, $mdDialog, Signal, RateOfArticleByPerson, $timeout, RateOfArticle, Rating, $uibModalInstance, WantedByArticle, OwnedByArticle, entity, Profile, $scope, Principal, Article, Wanted, Owned) {
        $scope.book = entity;
        var vm = this;
        getAccount()
        vm.signal = {
            id: null,
            content: null,
            type: null,
            date: null,
            profile: null
        };
        vm.rates = RateOfArticle.get({
            rated_id: $scope.book.id
        }, function (data) {
            if (data[0] != 0) {
                $scope.rate = data[1].toFixed(1)
                $scope.raters = data[0]
            } else {
                $scope.rate = 0;
                $scope.raters = data[0]
            }

        });


        function updatePageDirection() {
            var arabic = /[\u0600-\u06FF]/;
            var currentLang = $translate.proposedLanguage() || $translate.use();
            if (currentLang === "ar-ly") {
                $scope.isArab = true
            } else {
                $scope.isArab = false
            }
            if (arabic.test($scope.book.title)) {
                $scope.arabBook = true
            } else {
                $scope.arabBook = false
            }

        }
        TagsOfArticle.get({
            art_id: $scope.book.id
        }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $scope.selectedBookTags.push(result[i]);
            }
            var json = angular.toJson($scope.selectedBookTags);
            $scope.book.tags = JSON.parse(json)
        }, function () {})
        vm.updatePageDirection = updatePageDirection;
        $scope.arabBook = false;
        $scope.owners = [];
        $scope.wanters = [];
        $scope.owneds = {};
        $scope.wanteds = {};
        $scope.myImage = '';
        $scope.addtags = 0;
        $scope.edit = 0;
        $scope.owny = 0;
        $scope.wanty = 0;
        $scope.selectedBookTags = []


        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function () {

            if ($scope.myCroppedImage != '') {
                vm.articleImage.file = $scope.myCroppedImage.split(',')[1];
                vm.articleImage.article = $scope.book;

            }
        });
        updatePageDirection()
        $scope.connectedFrom = null
        $.getJSON('https://freegeoip.net/json/?callback=?', function (data) {
            $scope.connectedFrom = JSON.stringify(data.country_name, null, 2)

        });

        $scope.$on('languageChanged', function (event, args) {
            updatePageDirection()
        });
        $scope.content = "";
        $scope.rate = 0;
        $scope.isOpen = false;
        $scope.hidden = false;
        $scope.hover = false;

        /**************************************************************
         * ----------------------- Tags Chips ------------------------*
         **************************************************************/

        $scope.transformChipTag = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return {
                id: null,
                tag: chip
            }
        }

        $scope.selectedBookTag = {}
        $scope.searchBookTag = "";
        $scope.querySearchBookTag = function (text) {
            var promise
            if (typeof (text) === 'undefined' || text==="") {
                return promise;
            }
            if ($scope.addtags === 1) {
                
                  return TagsByCategory.query({
                        category_id: 1101,
                        tag: text.toLowerCase()
                    }).$promise
                
                }
            

        }
        $scope.querySearchGameTag = function (text) {
           
            if (typeof (text) === 'undefined' || text==="") {
                return;
            }
            if ($scope.addtags === 1) {
               
                    return TagsByCategory.query({
                        category_id: 1101,
                        tag: text.toLowerCase()
                    }).$promise
                

            }
         
        }
        $scope.saveTags = function () {
            vm.isSaving = true;
            var json = angular.toJson($scope.selectedBookTags);
            $scope.book.tags = JSON.parse(json)
            vm.articleImage.file = null;
            vm.articleImage.name = null;
            vm.articleImage.type = null;
            vm.articleImage.article = $scope.book;

            Article.update(vm.articleImage, function (result) {
                $scope.addtags = 0;
                $scope.book.tags = result.tags
                $scope.selectedBookTags = result.tags



            }, function (result) {
                $scope.addtags = 1;
            })
        }
        // On opening, add a delayed property which shows tooltips after the speed dial has opened
        // so that they have the proper position; if closing, immediately hide the tooltips
        $scope.$watch('isOpen', function (isOpen) {
            if (isOpen) {
                $timeout(function () {
                    $scope.tooltipVisible = $scope.isOpen;
                }, 600);
            } else {
                $scope.tooltipVisible = $scope.isOpen;
            }
        });
        vm.articleImage = {}

        $scope.handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.articleImage.name = file.name;
            vm.articleImage.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        $scope.selectedProfile = null;
        $scope.searchProfile = "";
        $scope.transformProfileChip = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

        }

        /**
         * Search for contacts; use a random delay to simulate a remote call
         */
        $scope.searchForProfile = function (query) {
         
                if (typeof (query) === 'undefined' || query==="") {
                    return;
                } else {
                    return ProfileByName.get({
                        pseudo: query.toLowerCase(),
                    }).$promise
                }

            
          
        }
        $scope.newOwners = function () {
            var json = angular.toJson($scope.owners);
            $scope.owners = JSON.parse(json)
            OwnedByArticle.get({
                id: $scope.book.id
            }, function (result) {

                for (var i = 0; i < $scope.owners.length; i++) {
                    $scope.ownIndx = result.map(function (e) {
                        return e.owner.id;
                    }).indexOf($scope.owners[i].id)
                    if ($scope.ownIndx > -1) {

                    } else {
                        vm.owned = {};
                        vm.owned.owner = $scope.owners[i]
                        vm.owned.article = $scope.book
                        Owned.save(vm.owned, function (result) {
                            $rootScope.$broadcast('addown', {
                                a: result
                            })
                            $scope.owneds.push(result);

                            loadAll(vm.account);
                            $scope.want = 0;
                            $scope.own = 1;

                        })

                    }
                }
                $scope.owny = 0;
            })



        }

        $scope.newWanters = function () {

            var json = angular.toJson($scope.wanters);
            $scope.wanters = JSON.parse(json)
            WantedByArticle.get({
                id: $scope.book.id
            }, function (result) {

                for (var i = 0; i < $scope.wanters.length; i++) {
                    $scope.wantIndx = $scope.wanteds.map(function (e) {
                        return e.wanter.id;
                    }).indexOf($scope.wanters[i].id)
                    if ($scope.wantIndx > -1) {

                    } else {
                        vm.wanted = {};
                        vm.wanted.wanter = $scope.wanters[i]
                        vm.wanted.article = $scope.book
                        Wanted.save(vm.wanted, function (result) {
                            $rootScope.$broadcast('addwant', {
                                a: result
                            })
                            $scope.wanteds.push(result);

                            loadAll(vm.account);
                            $scope.own = 0;
                            $scope.want = 1;

                        })

                    }
                }
                $scope.wanty = 0;
            })



        }
        $scope.addWanter = function () {
            $scope.wanty = 1;
        }
        $scope.add = function () {
            $scope.addtags = 1;
        }

        $scope.addOwner = function () {
            $scope.owny = 1;
        }


        $scope.editBio = function () {
            $scope.edit = 1;
        }

        $scope.saveDescription = function () {
            vm.isSaving = true;
            vm.articleImage.file = null;
            vm.articleImage.name = null;
            vm.articleImage.type = null;
            vm.articleImage.article = $scope.book;

            Article.update(vm.articleImage, function (result) {
                $scope.edit = 0;
                var index = result.description.indexOf(".");
                if (index > 50) {
                    $scope.shortdesc = result.description.substr(0, index);
                } else {
                    $scope.shortdesc = result.description.split(" ").splice(0, 20).join(" ");
                }

            }, function (result) {
                $scope.edit = 1;
            })








        }

        $scope.smt = function () {
            $scope.loading = true;
            vm.articleImage.article = $scope.book;
            Article.update(vm.articleImage, function (result) {
                $scope.loading = false;
                $rootScope.$broadcast('imageUpdated', {
                    a: result
                })
                $scope.book.normalImage = result.normalImage;
                $scope.myCroppedImage = '';
            });
        }
      
        $scope.max = 5;
        $scope.isReadonly = false;

        $scope.hoveringOver = function (value) {
            $scope.overStar = value;
            $scope.percent = value;
        };



        $scope.change = function (rating) {
            if ($scope.rating.id != null) {
                $scope.rating.rate = rating;
                Rating.update($scope.rating,
                    function () {
                        vm.rates = RateOfArticle.get({
                            rated_id: $scope.book.id
                        }, function (data) {

                            $scope.rate = data[1].toFixed(1)
                            $scope.raters = data[0]
                        })
                       vm.rate = rating;
                    })
            } else {
                $scope.rating.rate = rating;
                $scope.rating.rater = vm.account.profile;
                $scope.rating.rated = $scope.book;
                Rating.save($scope.rating, function () {
                    vm.rates = RateOfArticle.get({
                        rated_id: $scope.book.id
                    }, function (data) {

                        $scope.rate = data[1].toFixed(1)
                        $scope.raters = data[0]
                    })
                   vm.rate = rating;
                })
            }

        }



        $scope.isGame = false;
        if ($scope.book.category.id == '1102') {
            $scope.isGame = true;
        }
        if ($scope.book.description != null) {
            var index = $scope.book.description.indexOf(".");
            if (index > 50) {
                $scope.shortdesc = $scope.book.description.substr(0, index);
            } else {
                $scope.shortdesc = $scope.book.description.split(" ").splice(0, 20).join(" ");
            }
        }
        $scope.reported = false;


        $scope.saveReport = function () {
            $scope.header = vm.account.profile.pseudo + " wants to report " + $scope.book.title + " by " + $scope.book.author + ", Published By " + $scope.book.profile.firstName + $scope.book.profile.lastName + " at " + $scope.book.creationDate + "." + "\n" + "Reason : " + "\n"
            vm.signal.content = $scope.header + $scope.content;
            vm.signal.profile = vm.account.profile;
            vm.signal.type = 1;
            Signal.save(vm.signal, function () {
                $scope.reported = true

            })
        }


        function loadAll(account) {

            $scope.owneds = OwnedByArticle.get({
                id: $scope.book.id
            }, function (result) {
                for (var i = 0; i < result.length; i++) {
                    $scope.owners.push(result[i].owner);
                }

                $scope.ownIndx = $scope.owneds.map(function (e) {
                    return e.owner.id;
                }).indexOf(account.profile.id)
                if ($scope.ownIndx > -1) {
                    $scope.own = 1;

                } else {
                    $scope.own = 0;
                }

            })
            $scope.wanteds = WantedByArticle.get({
                id: $scope.book.id
            }, function (result) {
                for (var i = 0; i < result.length; i++) {
                    $scope.wanters.push(result[i].wanter);
                }

                $scope.wantIndx = $scope.wanteds.map(function (e) {
                    return e.wanter.id;
                }).indexOf(account.profile.id)
                if ($scope.wantIndx > -1) {
                    $scope.want = 1;
                } else {
                    $scope.want = 0;

                }
            })

        }



        $scope.changeWantState = function () {
            if ($scope.want === 1) {
                Wanted.delete({
                    id: $scope.wanteds[$scope.wantIndx].id
                }, function () {
                    loadAll(vm.account);
                    $rootScope.$broadcast('deletewant')


                });


                $scope.want = 0

            } else {
                $scope.want = 1;
                vm.wanted = {};
                vm.wanted.wanter = vm.account.profile
                vm.wanted.article = $scope.book
                Wanted.save(vm.wanted, function (result) {
                    $rootScope.$broadcast('addwant', {
                        a: result
                    })
                    loadAll(vm.account);
                    $scope.own = 0;

                });



            }

        }

        $scope.changeOwnState = function () {
            if ($scope.own === 1) {

                Owned.delete({
                    id: $scope.owneds[$scope.ownIndx].id
                }, function () {
                    loadAll(vm.account);
                    $rootScope.$broadcast('deleteown')
                });



                $scope.own = 0

            } else {
                $scope.own = 1;
                vm.owned = {};
                vm.owned.owner = vm.account.profile
                vm.owned.article = $scope.book
                Owned.save(vm.owned, function (result) {
                    $rootScope.$broadcast('addown', {
                        a: result
                    })
                    loadAll(vm.account);
                    $scope.want = 0;
                });
            }
        }

        $scope.hasAccount = false

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                if (account === null) {
                    $scope.owneds = OwnedByArticle.get({
                        id: $scope.book.id
                    })
                    $scope.wanteds = WantedByArticle.get({
                        id: $scope.book.id
                    })
                }
                if (account != null) {
                    $scope.hasAccount = true
                    loadAll(vm.account);
                    vm.isAuthenticated = Principal.isAuthenticated;
                    $scope.thisPersonRate = RateOfArticleByPerson.get({
                        rated_id: $scope.book.id,
                        rater_id: vm.account.profile.id
                    }, function (result) {
                        if (result.rate != null) {
                            $scope.rating = result
                           vm.rate = result.rate
                        } else {
                            $scope.rating = {
                                id: null,
                                rate: 0,
                                rater: {},
                                rated: {}
                            };
                           vm.rate = 0;
                        }
                    }, function () {

                        $scope.rating = {
                            id: null,
                            rate: 0,
                            rater: {},
                            rated: {}
                        };
                       vm.rate = 0;
                    })

                }
            })


        }

        $scope.clear = function () {
            $uibModalInstance.dismiss('cancel');
        }
        $scope.clearModal = function (id) {
            if (id === 1) {
                $uibModalInstance.dismiss('cancel');
                $('#ownedModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            } else {
                $uibModalInstance.dismiss('cancel');
                $('#wantedModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

            }

        }




    }
})();

(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .directive('hires', hires)
        .controller('BooksController', BooksController);


    hires.$inject = []

    function hires() {
        return {
            restrict: 'A',
            scope: {
                hires: '@'
            },
            link: function (scope, element, attrs) {
                element.one('load', function () {
                    element.attr('src', scope.hires);
                });
            }
        };
    }


    BooksController.$inject = ['$translatePartialLoader', '$translate', 'SpecsByCategory', 'ArticleByCategoryFiltred', 'ArticleByCategory', 'Follow', 'MatchedByUser', '$mdToast', 'ChatNotificationService', 'Auth', 'BookByTitle', '$timeout', 'ParseLinks', 'paginationConstants', 'Profile', '$uibModal', '$scope', 'Principal', 'LoginService', '$state', 'Article', 'Category', 'Wanted', 'Owned', '$rootScope', '$stateParams'];

    function BooksController($translatePartialLoader, $translate, SpecsByCategory, ArticleByCategoryFiltred, ArticleByCategory, Follow, MatchedByUser, $mdToast, ChatNotificationService, Auth, BookByTitle, $timeout, ParseLinks, paginationConstants, Profile, $uibModal, $scope, Principal, LoginService, $state, Article, Category, Wanted, Owned, $rootScope, $stateParams) {
        var vm = this;
        $rootScope.$broadcast('book')
        $scope.booksCont = {};
        $scope.booksCont.myImage = '';
        getAccount()
        $scope.x=4
        $scope.y=4
        $scope.$on('bookadd', function (event, opt) {
            $scope.added = opt.a;
        });
        $scope.$on('addown', function (event, opt) {
            $scope.ownToAdd = opt.a;
            $scope.booksCont.livresPossedes.push($scope.ownToAdd)

        });
        $scope.$on('addwant', function (event, opt) {
            $scope.wantToAdd = opt.a;
            $scope.booksCont.livresFavoris.push($scope.wantToAdd)

        });
        vm.languages = SpecsByCategory.get({
            category_id: 1101
        })
        $scope.booksCont.openMatching = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/matched/matched-detail.html',
                controller: 'MatchedDetailController',
                animation: false,
                size: 'md',
                resolve: {
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {}, function () {});
        }





     








        $scope.booksCont.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {
                

            }, function () {
                


            });

        }

        $scope.booksCont.addBook = function () {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-adding.html',
                controller: 'bookAddingController',
                animation: false,
                backdrop: 'static',
                keyboard: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        if (vm.account != null) {
                            return vm.account.profile;
                        }
                    }
                }
            }).result.then(function () {
                $uibModal.open({
                    templateUrl: 'app/entities/books/book-detail.html',
                    controller: 'booksDetailController',
                    animation: false,
                    size: 'lg',

                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('article');
                            return $translate.refresh();
                        }],
                        entity: function () {
                            return $scope.added;
                        }
                    }
                })


            }, function () {
                getAccount();


            });

        }





        $scope.booksCont.changeWantState = function (book) {
            if (book.want === 1) {
                var index = $scope.booksCont.livresFavoris.map(function (e) {
                    return e.article.id;
                }).indexOf(book.id);
                Wanted.delete({
                    id: $scope.booksCont.livresFavoris[index].id
                }, function () {
                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    })
                    $scope.booksCont.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    })
                });


                book.want = 0

            } else {
                book.want = 1;
                vm.wanted = {};
                vm.wanted.wanter = vm.account.profile
                vm.wanted.article = book
                Wanted.save(vm.wanted, function () {
                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    })
                    $scope.booksCont.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    })
                    book.got = 0;
                });


            }

        }


        $scope.booksCont.changeGotState = function (book) {
            if (book.got === 1) {
                var index = $scope.booksCont.livresPossedes.map(function (e) {
                    return e.article.id;
                }).indexOf(book.id);
                Owned.delete({
                    id: $scope.booksCont.livresPossedes[index].id
                }, function () {
                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    })
                    $scope.booksCont.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    })
                });



                book.got = 0

            } else {
                book.got = 1;
                vm.owned = {};
                vm.owned.owner = vm.account.profile
                vm.owned.article = book
                Owned.save(vm.owned, function () {

                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    })
                    $scope.booksCont.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    }, function () {

                    })
                    book.want = 0;
                });


            }

        }

        vm.articles = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        getAccountAfterAdd();


        function onSuccess(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            $scope.busy = false;
            for (var i = 0; i < data.length; i++) {
                vm.articles.push(data[i]);
            }


            for (var i = 0; i < vm.articles.length; i++) {
                var art = vm.articles[i];
                art.got = 0;
                art.want = 0;
            }
            if ($scope.booksCont.livresFavoris != undefined) {
                for (var i = 0; i < $scope.booksCont.livresFavoris.length; i++) {
                    var livre = $scope.booksCont.livresFavoris[i];
                    var index = vm.articles.map(function (e) {
                        return e.id;
                    }).indexOf(livre.article.id);
                    if (index > -1) {
                        vm.articles[index].want = 1;
                    }

                }
                for (var i = 0; i < $scope.booksCont.livresPossedes.length; i++) {
                    var livre = $scope.booksCont.livresPossedes[i];
                    var index = vm.articles.map(function (e) {
                        return e.id;
                    }).indexOf(livre.article.id);
                    if (index > -1) {
                        vm.articles[index].got = 1;
                    }

                }
            }

        }

        $scope.busy = false;


        function loadPage(id, page) {
            $scope.busy = true;
            vm.page = page;
            loadAll(id);
        }


        vm.loadAll = loadAll;

        function loadAllWA() {

            for (var i = 0; i < vm.articles.length; i++) {
                var art = vm.articles[i]
                art.got = 0;
                art.want = 0;
            }
            vm.searchQuery = null;
            for (var i = 0; i < $scope.booksCont.livresFavoris.length; i++) {
                var livre = $scope.booksCont.livresFavoris[i];
                var index = vm.articles.map(function (e) {
                    return e.id;
                }).indexOf(livre.article.id);
                if (index > -1) {
                    vm.articles[index].want = 1;
                }

            }
            for (var i = 0; i < $scope.booksCont.livresPossedes.length; i++) {
                var livre = $scope.booksCont.livresPossedes[i];
                var index = vm.articles.map(function (e) {
                    return e.id;
                }).indexOf(livre.article.id);
                if (index > -1) {
                    vm.articles[index].got = 1;
                }

            }
        }
        $scope.loadMoreWish=function(){
            $scope.y=$scope.booksCont.livresFavoris.length
            $scope.x=4
        }
        $scope.loadMoreOwn=function(){
            $scope.x=$scope.booksCont.livresPossedes.length
            $scope.y=4
        }
        $scope.selectedLanguage = null;
        $scope.selectedItemChange = function (item) {
            if (item != null) {
                vm.spec = item.id
                vm.articles = [];
                loadAll(vm.spec)
            } else {
                vm.spec = null
                vm.articles = [];
                loadAll(vm.spec)
            }
        }
        vm.reset = reset

        function reset(id) {
            vm.page = 0;
            vm.articles = [];
            loadAll(id);
        }

        vm.predicate = 'popularity';

        vm.reverse = false;

        function loadAll(id) {
            if (id != null) {
                ArticleByCategoryFiltred.get({
                    category_id: 1101,
                    spec_id: id,
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess)
            } else {
                ArticleByCategory.query({
                    catid: 1101,
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
        }



        $scope.$on('authenticationSuccess', function () {
            getAccountAfterAdd();
        });








        function getAccount() {
            vm.spec = null;
            Principal.identity().then(function (account) {
                vm.account = account;
                
                if (account != null) {

                    $scope.booksCont.waitingMatching = MatchedByUser.query({
                        id: vm.account.profile.id
                    }, function (result) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].owner.id === vm.account.profile.id) {
                                result[i].theOtherOne = result[i].wanter;
                            } else {
                                result[i].theOtherOne = result[i].owner;
    
                            }
                        }
                    });
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    })

                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {


                            loadAllWA();

                        })

                    })
                }
            });

        }


        function getAccountAfterAdd() {
            vm.spec = null;
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    })

                    vm.isAuthenticated = Principal.isAuthenticated;

                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {


                            loadAll(vm.spec);


                        })

                    })
                } else {
                    loadAll(vm.spec)

                }
            });
        }


    }
})();

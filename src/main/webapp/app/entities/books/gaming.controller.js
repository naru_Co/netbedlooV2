(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .directive('hires2', hires2)
        .controller('GamingController', GamingController);


    hires2.$inject = []

    function hires2() {
        return {
            restrict: 'A',
            scope: {
                hires2: '@'
            },
            link: function (scope, element, attrs) {
                element.one('load', function () {
                    element.attr('src', scope.hires2);
                });
            }
        };
    }




    GamingController.$inject = ['$translate', '$translatePartialLoader', 'ArticleByCategoryFiltred', 'SpecsByCategory', 'ArticleByCategory', 'Follow', 'MatchedByUser', '$mdToast', 'ChatNotificationService', 'Auth', 'BookByTitle', '$timeout', 'ParseLinks', 'paginationConstants', 'Profile', '$uibModal', '$scope', 'Principal', 'LoginService', '$state', 'Article', 'Category', 'Wanted', 'Owned', '$rootScope', '$stateParams'];

    function GamingController($translate, $translatePartialLoader, ArticleByCategoryFiltred, SpecsByCategory, ArticleByCategory, Follow, MatchedByUser, $mdToast, ChatNotificationService, Auth, BookByTitle, $timeout, ParseLinks, paginationConstants, Profile, $uibModal, $scope, Principal, LoginService, $state, Article, Category, Wanted, Owned, $rootScope, $stateParams) {
        $rootScope.$broadcast('gaming', {
            a: 1102
        })
        var vm = this;
        $scope.booksCont = {};
        $scope.booksCont.myImage = '';
        vm.articles = [];
        $scope.x = 4
        $scope.y = 4
        vm.loadPage = loadPage;
        $scope.$on('gameadd', function (event, opt) {
            $scope.added = opt.a;
        });
        vm.consoles = SpecsByCategory.get({
            category_id: 1102
        })
        $scope.booksCont.openMatching = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/matched/matched-detail.html',
                controller: 'MatchedDetailController',
                animation: false,
                size: 'md',
                resolve: {
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () { }, function () { });
        }












        $scope.loadMoreWish = function () {
            $scope.y = $scope.booksCont.livresFavoris.length
            $scope.x = 4
        }
        $scope.loadMoreOwn = function () {
            $scope.x = $scope.booksCont.livresPossedes.length
            $scope.y = 4
        }



        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        getAccountAfterAdd();


        function onSuccess(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            $scope.busy = false;
            for (var i = 0; i < data.length; i++) {
                vm.articles.push(data[i]);
            }


            for (var i = 0; i < vm.articles.length; i++) {
                var art = vm.articles[i];
                art.got = 0;
                art.want = 0;
            }
            if ($scope.booksCont.livresFavoris != undefined) {
                for (var i = 0; i < $scope.booksCont.livresFavoris.length; i++) {
                    var livre = $scope.booksCont.livresFavoris[i];
                    var index = vm.articles.map(function (e) {
                        return e.id;
                    }).indexOf(livre.article.id);
                    if (index > -1) {
                        vm.articles[index].want = 1;
                    }

                }
                for (var i = 0; i < $scope.booksCont.livresPossedes.length; i++) {
                    var livre = $scope.booksCont.livresPossedes[i];
                    var index = vm.articles.map(function (e) {
                        return e.id;
                    }).indexOf(livre.article.id);
                    if (index > -1) {
                        vm.articles[index].got = 1;
                    }

                }
            }

        }

        $scope.busy = false;
        vm.profiles = Profile.query({});

        function loadPage(id, page) {
            $scope.busy = true;
            vm.page = page;
            loadAll(id);
        }

        $scope.booksCont.openDetails = function (element) {
            $uibModal.open({
                templateUrl: 'app/entities/books/book-detail.html',
                controller: 'booksDetailController',
                animation: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return element;
                    }
                }
            }).result.then(function () {
                

            }, function () {
               


            });

        }

        $scope.booksCont.addBook = function () {
            $uibModal.open({
                templateUrl: 'app/entities/books/game-adding.html',
                controller: 'gameAddingController',
                animation: false,
                backdrop: 'static',
                keyboard: false,
                size: 'lg',

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('article');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        if (vm.account != null) {
                            return vm.account.profile;
                        }

                    }
                }
            }).result.then(function () {
                $uibModal.open({
                    templateUrl: 'app/entities/books/book-detail.html',
                    controller: 'booksDetailController',
                    animation: false,
                    size: 'lg',

                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('article');
                            return $translate.refresh();
                        }],
                        entity: function () {
                            return $scope.added;
                        }
                    }
                })


            }, function () {
                getAccount();


            });

        }





        $scope.booksCont.changeWantState = function (book) {
            if (vm.account != null) {
                if (book.want === 1) {
                    var index = $scope.booksCont.livresFavoris.map(function (e) {
                        return e.article.id;
                    }).indexOf(book.id);
                    Wanted.delete({
                        id: $scope.booksCont.livresFavoris[index].id
                    }, function () {
                        $scope.booksCont.livresFavoris = Wanted.get({
                            id: vm.account.profile.id
                        })
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        })
                    });


                    book.want = 0

                } else {
                    book.want = 1;
                    vm.wanted = {};
                    vm.wanted.wanter = vm.account.profile
                    vm.wanted.article = book
                    Wanted.save(vm.wanted, function () {
                        $scope.booksCont.livresFavoris = Wanted.get({
                            id: vm.account.profile.id
                        })
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        })
                    });


                }
            }

        }


        $scope.booksCont.changeGotState = function (book) {
            if (vm.account != null) {
                if (book.got === 1) {
                    var index = $scope.booksCont.livresPossedes.map(function (e) {
                        return e.article.id;
                    }).indexOf(book.id);
                    Owned.delete({
                        id: $scope.booksCont.livresPossedes[index].id
                    }, function () {
                        $scope.booksCont.livresFavoris = Wanted.get({
                            id: vm.account.profile.id
                        })
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        })
                    });



                    book.got = 0

                } else {
                    book.got = 1;
                    vm.owned = {};
                    vm.owned.owner = vm.account.profile
                    vm.owned.article = book
                    Owned.save(vm.owned, function () {

                        $scope.booksCont.livresFavoris = Wanted.get({
                            id: vm.account.profile.id
                        })
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {

                        })
                    });


                }

            }
        }






        vm.loadAll = loadAll;

        function loadAllWA() {

            for (var i = 0; i < vm.articles.length; i++) {
                var art = vm.articles[i]
                art.got = 0;
                art.want = 0;
            }
            vm.searchQuery = null;
            for (var i = 0; i < $scope.booksCont.livresFavoris.length; i++) {
                var livre = $scope.booksCont.livresFavoris[i];
                var index = vm.articles.map(function (e) {
                    return e.id;
                }).indexOf(livre.article.id);
                if (index > -1) {
                    vm.articles[index].want = 1;
                }

            }
            for (var i = 0; i < $scope.booksCont.livresPossedes.length; i++) {
                var livre = $scope.booksCont.livresPossedes[i];
                var index = vm.articles.map(function (e) {
                    return e.id;
                }).indexOf(livre.article.id);
                if (index > -1) {
                    vm.articles[index].got = 1;
                }

            }
        }


        /**************************************************************
         * ----------------------- Specification Autocomplete---------*
         **************************************************************/
        $scope.selectedConsole = null;
        $scope.selectedItemChange = function (item) {
            if (item != null) {
                vm.spec = item.id
                vm.articles = [];
                loadAll(vm.spec)
            } else {
                vm.spec = null
                vm.articles = [];
                loadAll(vm.spec)
            }
        }

        function loadAll(id) {
            if (id != null) {
                ArticleByCategoryFiltred.get({
                    category_id: 1102,
                    spec_id: id,
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess)
            } else {
                ArticleByCategory.query({
                    catid: 1102,
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

        }
        vm.reset = reset

        function reset(id) {
            vm.page = 0;
            vm.articles = [];
            loadAll(id);
        }

        vm.predicate = 'popularity';

        vm.reverse = false;

        $scope.$on('authenticationSuccess', function () {
            getAccountAfterAdd();
        });








        function getAccount() {
            Principal.identity().then(function (account) {

                vm.account = account;
                if (vm.account != null) {
                    vm.isAuthenticated = Principal.isAuthenticated;
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    })
                    $scope.booksCont.waitingMatching = MatchedByUser.query({
                        id: vm.account.profile.id
                    }, function (result) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].owner.id === vm.account.profile.id) {
                                result[i].theOtherOne = result[i].wanter;
                            } else {
                                result[i].theOtherOne = result[i].owner;
                            }
                        }
                    });



                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {


                            loadAllWA();

                        })

                    })
                }
            });
        }

        function getAccountAfterAdd() {
            Principal.identity().then(function (account) {
                vm.spec = null
                vm.account = account;
                if (vm.account != null) {
                    vm.account.profile.followings = Follow.get({
                        id: vm.account.profile.id
                    })

                    vm.isAuthenticated = Principal.isAuthenticated;

                    $scope.booksCont.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.booksCont.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {


                            loadAll(vm.spec);


                        })

                    })
                } else {
                    loadAll(vm.spec)

                }
            });
        }


    }
})();

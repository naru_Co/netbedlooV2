(function () {
    'use strict';

    angular
        .module('netbedlooApp')
        .controller('gameAddingController', gameAddingController)
        .directive('customOnChange', customOnChange)
        .directive('customChip', customChip)

    function customChip() {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                var chipClass = scope.$chip.style;
                var mdChip = elem.parent().parent();
                mdChip.addClass(chipClass);
            }
        }
    };

    function customOnChange() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    };


    gameAddingController.$inject = ['$rootScope','Sbewlou', 'Playstation', 'TagsByCategory', 'TagSearch', 'ArticleByTitleAndCategory', '$http', '$uibModalInstance', 'WantedByArticle', 'Category', 'OwnedByArticle', 'entity', 'Profile', '$scope', 'Principal', 'Article', 'Wanted', 'Owned'];

    function gameAddingController($rootScope,Sbewlou, Playstation, TagsByCategory, TagSearch, ArticleByTitleAndCategory, $http, $uibModalInstance, WantedByArticle, Category, OwnedByArticle, entity, Profile, $scope, Principal, Article, Wanted, Owned) {
        var vm = this;
        $scope.loading = false;
        $scope.error=null;
        $scope.creator = entity;
        vm.isSaving = false;
        $scope.currentCat = Category.get({
            id: 1102
        });

        $scope.cat = 2;

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;


        $scope.description = "";
        $scope.author = "";
        $scope.selectedArticle = null;
        $scope.title = "";
        $scope.want = 0;
        $scope.own = 0;
        $scope.livresFavoris = {};
        $scope.livresPossedes = {};
        $scope.coverImage = "content/images/NaryLogDVD2.png"
        vm.article = {
            name: null,
            type: null,
            file: null,
            article: {}
        };


        $scope.sbewlou = false
        $scope.querySearch = function (query) {
            $scope.title = query;
            $scope.sbewlou = true
            return ArticleByTitleAndCategory.get({
                category_id: 1102,
                title: query.toLowerCase()
            }).$promise
        }

        $scope.avilable = 0;
        $scope.updateInfo = function (item) {
            $scope.chosenOne = item;

            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                return e.article.id;
            }).indexOf(item.id);
            if ($scope.wantIndx > -1) {
                $scope.want = 1;
            }
            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                return e.article.id;
            }).indexOf(item.id)
            if ($scope.ownIndx > -1) {
                $scope.own = 1;
            }

            $scope.title = item.title;
            $scope.Tid = item.id;
            $scope.author = item.author;
            $scope.description = item.description;
            $scope.selectedConsole = item.spec.name

            $scope.selectedTags = item.tags
            $scope.coverImage = item.coverImage;
            $scope.myCroppedImage = item.coverImage;
            $scope.avilable = 1;

        }
        getAccount()

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;

                $scope.livresFavoris = Wanted.get({
                    id: vm.account.profile.id
                }, function () {
                    $scope.livresPossedes = Owned.get({
                        id: vm.account.profile.id
                    }, function () {



                    })

                })
            });
        }


        /**************************************************************
  * ----------------------- Tags Chips ------------------------*
  **************************************************************/

        $scope.transformChipTag = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return { id: null, tag: chip }
        }

        $scope.selectedTags = [];
        vm.selectedTag = null;
        vm.searchTag = null;

        $scope.querySearchTag = function (text) {
            return TagsByCategory.query({
                category_id: 1102,
                tag: text.toLowerCase()
            }).$promise
        }
        $scope.disabled = true;
        $scope.hide = false;

        $scope.url = "https://images.igdb.com/igdb/image/upload/t_1080p/";
        $scope.hash = "";
        $scope.autoFill = function () {
           
            Sbewlou.get({
                title: $scope.title
            }, function (data) {
               
                $scope.hash = data[0].cover.cloudinary_id;
                $scope.title = data[0].name;
                $scope.description = data[0].summary;
                $scope.averageRate = (data[0].rating / 20).toFixed(1);

                $scope.myImage = $scope.url + $scope.hash + ".jpg";
                $('#myModal2').modal('show');
                $scope.hide = true;

            })
            /*  */

        }








        /**************************************************************
        * ----------------------- Specification Autocomplete---------*
        **************************************************************/
        $scope.selectedConsole = Playstation.get();
        $scope.changeWantState = function () {
            if ($scope.want === 1) {
                Wanted.delete({
                    id: $scope.livresFavoris[$scope.wantIndx].id
                }, function () {

                });


                $scope.want = 0

            } else {
                $scope.want = 1;
                vm.wanted = {};
                vm.wanted.wanter = vm.account.profile
                vm.wanted.article = $scope.chosenOne
                Wanted.save(vm.wanted, function () {
                    $scope.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {

                            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id);
                            if ($scope.wantIndx > -1) {
                                $scope.want = 1;
                            }
                            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id)
                            if ($scope.ownIndx > -1) {
                                $scope.own = 1;
                            }



                        })

                    })



                });



            }

        }

        $scope.changeOwnState = function () {
            if ($scope.own === 1) {
                Owned.delete({
                    id: $scope.livresPossedes[$scope.ownIndx].id
                }, function () {

                });



                $scope.own = 0

            } else {
                $scope.own = 1;
                vm.owned = {};
                vm.owned.owner = vm.account.profile
                vm.owned.article = $scope.chosenOne
                Owned.save(vm.owned, function () {
                    $scope.livresFavoris = Wanted.get({
                        id: vm.account.profile.id
                    }, function () {
                        $scope.livresPossedes = Owned.get({
                            id: vm.account.profile.id
                        }, function () {

                            $scope.wantIndx = $scope.livresFavoris.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id);
                            if ($scope.wantIndx > -1) {
                                $scope.want = 1;
                            }
                            $scope.ownIndx = $scope.livresPossedes.map(function (e) {
                                return e.article.id;
                            }).indexOf($scope.chosenOne.id)
                            if ($scope.ownIndx > -1) {
                                $scope.own = 1;
                            }



                        })

                    })



                });


            }

        }

        $scope.clear = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.save = function () {
            $scope.loading = true;
            vm.isSaving = true;
            vm.article.article.description = $scope.description
            vm.article.article.author = $scope.author
            vm.article.article.title = $scope.title;
            vm.article.article.spec = $scope.selectedConsole;
            vm.article.article.creationDate = new Date()
            vm.article.article.category = $scope.currentCat;
            vm.article.article.profile = $scope.creator;
            vm.article.article.tags = vm.selectedTags;
            Article.save(vm.article, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:articleUpdate', result);
            $scope.loading = false;
            $uibModalInstance.close(result);
            $rootScope.$broadcast('gameadd',{ a: result});
            vm.isSaving = false;


        }

        function onSaveError() {
            $scope.error="ERROR";
            $scope.loading = false;
            vm.isSaving = false;

        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }


        $scope.myImage = '';

        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function () {
            if ($scope.myCroppedImage != '') {
                vm.article.file = $scope.myCroppedImage.split(',')[1];

            }
        });



        $scope.handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];

            vm.article.name = file.name;
            vm.article.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        }






        /* end of image stuff */

    }
})();
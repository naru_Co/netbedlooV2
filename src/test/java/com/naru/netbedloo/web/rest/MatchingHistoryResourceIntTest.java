package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the MatchingHistoryResource REST controller.
 *
 * @see MatchingHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class MatchingHistoryResourceIntTest {

    // private static final Instant DEFAULT_MATCHING_DATE = Instant.ofEpochMilli(0L);
    // private static final Instant UPDATED_MATCHING_DATE = Instant.now();

    // private static final String DEFAULT_STATUT = "AAAAAAAAAA";
    // private static final String UPDATED_STATUT = "BBBBBBBBBB";

    // private static final Double DEFAULT_DISTANCE = 1D;
    // private static final Double UPDATED_DISTANCE = 2D;

    // private static final Instant DEFAULT_TBEDLOO_DATE = Instant.ofEpochMilli(0L);
    // private static final Instant UPDATED_TBEDLOO_DATE = Instant.now();

    // private static final Instant DEFAULT_ERASE_DATE = Instant.ofEpochMilli(0L);
    // private static final Instant UPDATED_ERASE_DATE = Instant.now();

    // @Autowired
    // private MatchingHistoryRepository matchingHistoryRepository;

    // @Autowired
    // private MatchingHistorySearchRepository matchingHistorySearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restMatchingHistoryMockMvc;

    // private MatchingHistory matchingHistory;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     MatchingHistoryResource matchingHistoryResource = new MatchingHistoryResource(matchingHistoryRepository, matchingHistorySearchRepository);
    //     this.restMatchingHistoryMockMvc = MockMvcBuilders.standaloneSetup(matchingHistoryResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // /** 
    // public static MatchingHistory createEntity(EntityManager em) {
    //     MatchingHistory matchingHistory = new MatchingHistory()
    //         .matchingDate(DEFAULT_MATCHING_DATE)
    //         .statut(DEFAULT_STATUT)
    //         .distance(DEFAULT_DISTANCE)
    //         .tbedlooDate(DEFAULT_TBEDLOO_DATE)
    //         .eraseDate(DEFAULT_ERASE_DATE);
    //     return matchingHistory;
    // }

    // @Before
    // public void initTest() {
    //     matchingHistorySearchRepository.deleteAll();
    //     matchingHistory = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createMatchingHistory() throws Exception {
    //     int databaseSizeBeforeCreate = matchingHistoryRepository.findAll().size();

    //     // Create the MatchingHistory
    //     restMatchingHistoryMockMvc.perform(post("/api/matching-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matchingHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the MatchingHistory in the database
    //     List<MatchingHistory> matchingHistoryList = matchingHistoryRepository.findAll();
    //     assertThat(matchingHistoryList).hasSize(databaseSizeBeforeCreate + 1);
    //     MatchingHistory testMatchingHistory = matchingHistoryList.get(matchingHistoryList.size() - 1);
    //     assertThat(testMatchingHistory.getMatchingDate()).isEqualTo(DEFAULT_MATCHING_DATE);
    //     assertThat(testMatchingHistory.getStatut()).isEqualTo(DEFAULT_STATUT);
    //     assertThat(testMatchingHistory.getDistance()).isEqualTo(DEFAULT_DISTANCE);
    //     assertThat(testMatchingHistory.getTbedlooDate()).isEqualTo(DEFAULT_TBEDLOO_DATE);
    //     assertThat(testMatchingHistory.getEraseDate()).isEqualTo(DEFAULT_ERASE_DATE);

    //     // Validate the MatchingHistory in Elasticsearch
    //     MatchingHistory matchingHistoryEs = matchingHistorySearchRepository.findOne(testMatchingHistory.getId());
    //     assertThat(matchingHistoryEs).isEqualToComparingFieldByField(testMatchingHistory);
    // }

    // @Test
    // @Transactional
    // public void createMatchingHistoryWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = matchingHistoryRepository.findAll().size();

    //     // Create the MatchingHistory with an existing ID
    //     matchingHistory.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restMatchingHistoryMockMvc.perform(post("/api/matching-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matchingHistory)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<MatchingHistory> matchingHistoryList = matchingHistoryRepository.findAll();
    //     assertThat(matchingHistoryList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    
    // public void getAllMatchingHistories() throws Exception {
    //     // Initialize the database
    //     matchingHistoryRepository.saveAndFlush(matchingHistory);

    //     // Get all the matchingHistoryList
    //     restMatchingHistoryMockMvc.perform(get("/api/matching-histories?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(matchingHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].matchingDate").value(hasItem(sameInstant(DEFAULT_MATCHING_DATE))))
    //         .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
    //         .andExpect(jsonPath("$.[*].distance").value(hasItem(DEFAULT_DISTANCE.doubleValue())))
    //         .andExpect(jsonPath("$.[*].tbedlooDate").value(hasItem(DEFAULT_TBEDLOO_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    //    public void getMatchingHistory() throws Exception {
    //     // Initialize the database
    //     matchingHistoryRepository.saveAndFlush(matchingHistory);

    //     // Get the matchingHistory
    //     restMatchingHistoryMockMvc.perform(get("/api/matching-histories/{id}", matchingHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(matchingHistory.getId().intValue()))
    //         .andExpect(jsonPath("$.matchingDate").value(sameInstant(DEFAULT_MATCHING_DATE)))
    //         .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()))
    //         .andExpect(jsonPath("$.distance").value(DEFAULT_DISTANCE.doubleValue()))
    //         .andExpect(jsonPath("$.tbedlooDate").value(DEFAULT_TBEDLOO_DATE.toString()))
    //         .andExpect(jsonPath("$.eraseDate").value(DEFAULT_ERASE_DATE.toString()));
    // }
    

    // @Test
    // @Transactional
    // public void getNonExistingMatchingHistory() throws Exception {
    //     // Get the matchingHistory
    //     restMatchingHistoryMockMvc.perform(get("/api/matching-histories/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateMatchingHistory() throws Exception {
    //     // Initialize the database
    //     matchingHistoryRepository.saveAndFlush(matchingHistory);
    //     matchingHistorySearchRepository.save(matchingHistory);
    //     int databaseSizeBeforeUpdate = matchingHistoryRepository.findAll().size();

    //     // Update the matchingHistory
    //     MatchingHistory updatedMatchingHistory = matchingHistoryRepository.findOne(matchingHistory.getId());
    //     updatedMatchingHistory
    //         .matchingDate(UPDATED_MATCHING_DATE)
    //         .statut(UPDATED_STATUT)
    //         .distance(UPDATED_DISTANCE)
    //         .tbedlooDate(UPDATED_TBEDLOO_DATE)
    //         .eraseDate(UPDATED_ERASE_DATE);

    //     restMatchingHistoryMockMvc.perform(put("/api/matching-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedMatchingHistory)))
    //         .andExpect(status().isOk());

    //     // Validate the MatchingHistory in the database
    //     List<MatchingHistory> matchingHistoryList = matchingHistoryRepository.findAll();
    //     assertThat(matchingHistoryList).hasSize(databaseSizeBeforeUpdate);
    //     MatchingHistory testMatchingHistory = matchingHistoryList.get(matchingHistoryList.size() - 1);
    //     assertThat(testMatchingHistory.getMatchingDate()).isEqualTo(UPDATED_MATCHING_DATE);
    //     assertThat(testMatchingHistory.getStatut()).isEqualTo(UPDATED_STATUT);
    //     assertThat(testMatchingHistory.getDistance()).isEqualTo(UPDATED_DISTANCE);
    //     assertThat(testMatchingHistory.getTbedlooDate()).isEqualTo(UPDATED_TBEDLOO_DATE);
    //     assertThat(testMatchingHistory.getEraseDate()).isEqualTo(UPDATED_ERASE_DATE);

    //     // Validate the MatchingHistory in Elasticsearch
    //     MatchingHistory matchingHistoryEs = matchingHistorySearchRepository.findOne(testMatchingHistory.getId());
    //     assertThat(matchingHistoryEs).isEqualToComparingFieldByField(testMatchingHistory);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingMatchingHistory() throws Exception {
    //     int databaseSizeBeforeUpdate = matchingHistoryRepository.findAll().size();

    //     // Create the MatchingHistory

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restMatchingHistoryMockMvc.perform(put("/api/matching-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matchingHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the MatchingHistory in the database
    //     List<MatchingHistory> matchingHistoryList = matchingHistoryRepository.findAll();
    //     assertThat(matchingHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteMatchingHistory() throws Exception {
    //     // Initialize the database
    //     matchingHistoryRepository.saveAndFlush(matchingHistory);
    //     matchingHistorySearchRepository.save(matchingHistory);
    //     int databaseSizeBeforeDelete = matchingHistoryRepository.findAll().size();

    //     // Get the matchingHistory
    //     restMatchingHistoryMockMvc.perform(delete("/api/matching-histories/{id}", matchingHistory.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean matchingHistoryExistsInEs = matchingHistorySearchRepository.exists(matchingHistory.getId());
    //     assertThat(matchingHistoryExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<MatchingHistory> matchingHistoryList = matchingHistoryRepository.findAll();
    //     assertThat(matchingHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchMatchingHistory() throws Exception {
    //     // Initialize the database
    //     matchingHistoryRepository.saveAndFlush(matchingHistory);
    //     matchingHistorySearchRepository.save(matchingHistory);

    //     // Search the matchingHistory
    //     restMatchingHistoryMockMvc.perform(get("/api/_search/matching-histories?query=id:" + matchingHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(matchingHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].matchingDate").value(hasItem(sameInstant(DEFAULT_MATCHING_DATE))))
    //         .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
    //         .andExpect(jsonPath("$.[*].distance").value(hasItem(DEFAULT_DISTANCE.doubleValue())))
    //         .andExpect(jsonPath("$.[*].tbedlooDate").value(hasItem(DEFAULT_TBEDLOO_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(MatchingHistory.class);
    // }*/
}

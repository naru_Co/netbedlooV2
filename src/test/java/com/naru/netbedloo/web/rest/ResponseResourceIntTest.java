package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the ResponseResource REST controller.
 *
 * @see ResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class ResponseResourceIntTest {

    // private static final String DEFAULT_RESPONSE = "AAAAAAAAAA";
    // private static final String UPDATED_RESPONSE = "BBBBBBBBBB";

    // private static final ZonedDateTime DEFAULT_RESPONSE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    // private static final ZonedDateTime UPDATED_RESPONSE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    // @Autowired
    // private ResponseRepository responseRepository;

    // @Autowired
    // private ResponseSearchRepository responseSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restResponseMockMvc;

    // private Response response;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     ResponseResource responseResource = new ResponseResource(responseRepository, responseSearchRepository);
    //     this.restResponseMockMvc = MockMvcBuilders.standaloneSetup(responseResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Response createEntity(EntityManager em) {
    //     Response response = new Response()
    //         .response(DEFAULT_RESPONSE)
    //         .responseTime(DEFAULT_RESPONSE_TIME);
    //     return response;
    // }

    // @Before
    // public void initTest() {
    //     responseSearchRepository.deleteAll();
    //     response = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createResponse() throws Exception {
    //     int databaseSizeBeforeCreate = responseRepository.findAll().size();

    //     // Create the Response
    //     restResponseMockMvc.perform(post("/api/responses")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(response)))
    //         .andExpect(status().isCreated());

    //     // Validate the Response in the database
    //     List<Response> responseList = responseRepository.findAll();
    //     assertThat(responseList).hasSize(databaseSizeBeforeCreate + 1);
    //     Response testResponse = responseList.get(responseList.size() - 1);
    //     assertThat(testResponse.getResponse()).isEqualTo(DEFAULT_RESPONSE);
    //     assertThat(testResponse.getResponseTime()).isEqualTo(DEFAULT_RESPONSE_TIME);

    //     // Validate the Response in Elasticsearch
    //     Response responseEs = responseSearchRepository.findOne(testResponse.getId());
    //     assertThat(responseEs).isEqualToComparingFieldByField(testResponse);
    // }

    // @Test
    // @Transactional
    // public void createResponseWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = responseRepository.findAll().size();

    //     // Create the Response with an existing ID
    //     response.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restResponseMockMvc.perform(post("/api/responses")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(response)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Response> responseList = responseRepository.findAll();
    //     assertThat(responseList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllResponses() throws Exception {
    //     // Initialize the database
    //     responseRepository.saveAndFlush(response);

    //     // Get all the responseList
    //     restResponseMockMvc.perform(get("/api/responses?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(response.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].response").value(hasItem(DEFAULT_RESPONSE.toString())))
    //         .andExpect(jsonPath("$.[*].responseTime").value(hasItem(sameInstant(DEFAULT_RESPONSE_TIME))));
    // }

    // @Test
    // @Transactional
    // public void getResponse() throws Exception {
    //     // Initialize the database
    //     responseRepository.saveAndFlush(response);

    //     // Get the response
    //     restResponseMockMvc.perform(get("/api/responses/{id}", response.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(response.getId().intValue()))
    //         .andExpect(jsonPath("$.response").value(DEFAULT_RESPONSE.toString()))
    //         .andExpect(jsonPath("$.responseTime").value(sameInstant(DEFAULT_RESPONSE_TIME)));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingResponse() throws Exception {
    //     // Get the response
    //     restResponseMockMvc.perform(get("/api/responses/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateResponse() throws Exception {
    //     // Initialize the database
    //     responseRepository.saveAndFlush(response);
    //     responseSearchRepository.save(response);
    //     int databaseSizeBeforeUpdate = responseRepository.findAll().size();

    //     // Update the response
    //     Response updatedResponse = responseRepository.findOne(response.getId());
    //     updatedResponse
    //         .response(UPDATED_RESPONSE)
    //         .responseTime(UPDATED_RESPONSE_TIME);

    //     restResponseMockMvc.perform(put("/api/responses")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedResponse)))
    //         .andExpect(status().isOk());

    //     // Validate the Response in the database
    //     List<Response> responseList = responseRepository.findAll();
    //     assertThat(responseList).hasSize(databaseSizeBeforeUpdate);
    //     Response testResponse = responseList.get(responseList.size() - 1);
    //     assertThat(testResponse.getResponse()).isEqualTo(UPDATED_RESPONSE);
    //     assertThat(testResponse.getResponseTime()).isEqualTo(UPDATED_RESPONSE_TIME);

    //     // Validate the Response in Elasticsearch
    //     Response responseEs = responseSearchRepository.findOne(testResponse.getId());
    //     assertThat(responseEs).isEqualToComparingFieldByField(testResponse);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingResponse() throws Exception {
    //     int databaseSizeBeforeUpdate = responseRepository.findAll().size();

    //     // Create the Response

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restResponseMockMvc.perform(put("/api/responses")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(response)))
    //         .andExpect(status().isCreated());

    //     // Validate the Response in the database
    //     List<Response> responseList = responseRepository.findAll();
    //     assertThat(responseList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteResponse() throws Exception {
    //     // Initialize the database
    //     responseRepository.saveAndFlush(response);
    //     responseSearchRepository.save(response);
    //     int databaseSizeBeforeDelete = responseRepository.findAll().size();

    //     // Get the response
    //     restResponseMockMvc.perform(delete("/api/responses/{id}", response.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean responseExistsInEs = responseSearchRepository.exists(response.getId());
    //     assertThat(responseExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Response> responseList = responseRepository.findAll();
    //     assertThat(responseList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchResponse() throws Exception {
    //     // Initialize the database
    //     responseRepository.saveAndFlush(response);
    //     responseSearchRepository.save(response);

    //     // Search the response
    //     restResponseMockMvc.perform(get("/api/_search/responses?query=id:" + response.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(response.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].response").value(hasItem(DEFAULT_RESPONSE.toString())))
    //         .andExpect(jsonPath("$.[*].responseTime").value(hasItem(sameInstant(DEFAULT_RESPONSE_TIME))));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Response.class);
    // }*/
}

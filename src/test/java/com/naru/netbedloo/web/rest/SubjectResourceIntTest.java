package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the SubjectResource REST controller.
 *
 * @see SubjectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class SubjectResourceIntTest {

    // private static final String DEFAULT_PUBLICATION = "AAAAAAAAAA";
    // private static final String UPDATED_PUBLICATION = "BBBBBBBBBB";

    // private static final ZonedDateTime DEFAULT_PUBLICATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    // private static final ZonedDateTime UPDATED_PUBLICATION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    // @Autowired
    // private SubjectRepository subjectRepository;

    // @Autowired
    // private SubjectSearchRepository subjectSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restSubjectMockMvc;

    // private Subject subject;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     SubjectResource subjectResource = new SubjectResource(subjectRepository, subjectSearchRepository);
    //     this.restSubjectMockMvc = MockMvcBuilders.standaloneSetup(subjectResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    
    // public static Subject createEntity(EntityManager em) {
    //     Subject subject = new Subject()
    //         .publication(DEFAULT_PUBLICATION)
    //         .publicationTime(DEFAULT_PUBLICATION_TIME);
    //     return subject;
    // }

    // @Before
    // public void initTest() {
    //     subjectSearchRepository.deleteAll();
    //     subject = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createSubject() throws Exception {
    //     int databaseSizeBeforeCreate = subjectRepository.findAll().size();

    //     // Create the Subject
    //     restSubjectMockMvc.perform(post("/api/subjects")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(subject)))
    //         .andExpect(status().isCreated());

    //     // Validate the Subject in the database
    //     List<Subject> subjectList = subjectRepository.findAll();
    //     assertThat(subjectList).hasSize(databaseSizeBeforeCreate + 1);
    //     Subject testSubject = subjectList.get(subjectList.size() - 1);
    //     assertThat(testSubject.getPublication()).isEqualTo(DEFAULT_PUBLICATION);
    //     assertThat(testSubject.getPublicationTime()).isEqualTo(DEFAULT_PUBLICATION_TIME);

    //     // Validate the Subject in Elasticsearch
    //     Subject subjectEs = subjectSearchRepository.findOne(testSubject.getId());
    //     assertThat(subjectEs).isEqualToComparingFieldByField(testSubject);
    // }

    // @Test
    // @Transactional
    // public void createSubjectWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = subjectRepository.findAll().size();

    //     // Create the Subject with an existing ID
    //     subject.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restSubjectMockMvc.perform(post("/api/subjects")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(subject)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Subject> subjectList = subjectRepository.findAll();
    //     assertThat(subjectList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllSubjects() throws Exception {
    //     // Initialize the database
    //     subjectRepository.saveAndFlush(subject);

    //     // Get all the subjectList
    //     restSubjectMockMvc.perform(get("/api/subjects?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(subject.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].publication").value(hasItem(DEFAULT_PUBLICATION.toString())))
    //         .andExpect(jsonPath("$.[*].publicationTime").value(hasItem(sameInstant(DEFAULT_PUBLICATION_TIME))));
    // }

    // @Test
    // @Transactional
    // public void getSubject() throws Exception {
    //     // Initialize the database
    //     subjectRepository.saveAndFlush(subject);

    //     // Get the subject
    //     restSubjectMockMvc.perform(get("/api/subjects/{id}", subject.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(subject.getId().intValue()))
    //         .andExpect(jsonPath("$.publication").value(DEFAULT_PUBLICATION.toString()))
    //         .andExpect(jsonPath("$.publicationTime").value(sameInstant(DEFAULT_PUBLICATION_TIME)));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingSubject() throws Exception {
    //     // Get the subject
    //     restSubjectMockMvc.perform(get("/api/subjects/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateSubject() throws Exception {
    //     // Initialize the database
    //     subjectRepository.saveAndFlush(subject);
    //     subjectSearchRepository.save(subject);
    //     int databaseSizeBeforeUpdate = subjectRepository.findAll().size();

    //     // Update the subject
    //     Subject updatedSubject = subjectRepository.findOne(subject.getId());
    //     updatedSubject
    //         .publication(UPDATED_PUBLICATION)
    //         .publicationTime(UPDATED_PUBLICATION_TIME);

    //     restSubjectMockMvc.perform(put("/api/subjects")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedSubject)))
    //         .andExpect(status().isOk());

    //     // Validate the Subject in the database
    //     List<Subject> subjectList = subjectRepository.findAll();
    //     assertThat(subjectList).hasSize(databaseSizeBeforeUpdate);
    //     Subject testSubject = subjectList.get(subjectList.size() - 1);
    //     assertThat(testSubject.getPublication()).isEqualTo(UPDATED_PUBLICATION);
    //     assertThat(testSubject.getPublicationTime()).isEqualTo(UPDATED_PUBLICATION_TIME);

    //     // Validate the Subject in Elasticsearch
    //     Subject subjectEs = subjectSearchRepository.findOne(testSubject.getId());
    //     assertThat(subjectEs).isEqualToComparingFieldByField(testSubject);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingSubject() throws Exception {
    //     int databaseSizeBeforeUpdate = subjectRepository.findAll().size();

    //     // Create the Subject

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restSubjectMockMvc.perform(put("/api/subjects")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(subject)))
    //         .andExpect(status().isCreated());

    //     // Validate the Subject in the database
    //     List<Subject> subjectList = subjectRepository.findAll();
    //     assertThat(subjectList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteSubject() throws Exception {
    //     // Initialize the database
    //     subjectRepository.saveAndFlush(subject);
    //     subjectSearchRepository.save(subject);
    //     int databaseSizeBeforeDelete = subjectRepository.findAll().size();

    //     // Get the subject
    //     restSubjectMockMvc.perform(delete("/api/subjects/{id}", subject.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean subjectExistsInEs = subjectSearchRepository.exists(subject.getId());
    //     assertThat(subjectExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Subject> subjectList = subjectRepository.findAll();
    //     assertThat(subjectList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchSubject() throws Exception {
    //     // Initialize the database
    //     subjectRepository.saveAndFlush(subject);
    //     subjectSearchRepository.save(subject);

    //     // Search the subject
    //     restSubjectMockMvc.perform(get("/api/_search/subjects?query=id:" + subject.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(subject.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].publication").value(hasItem(DEFAULT_PUBLICATION.toString())))
    //         .andExpect(jsonPath("$.[*].publicationTime").value(hasItem(sameInstant(DEFAULT_PUBLICATION_TIME))));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Subject.class);
    // } */
}

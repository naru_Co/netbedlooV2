package com.naru.netbedloo.web.rest;

import static com.naru.netbedloo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import com.naru.netbedloo.NetbedlooApp;
import com.naru.netbedloo.domain.Signal;
import com.naru.netbedloo.repository.SignalRepository;
import com.naru.netbedloo.repository.search.SignalSearchRepository;
import com.naru.netbedloo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the SignalResource REST controller.
 *
 * @see SignalResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class SignalResourceIntTest {

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    @Autowired
    private SignalRepository signalRepository;

    @Autowired
    private SignalSearchRepository signalSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSignalMockMvc;

    private Signal signal;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SignalResource signalResource = new SignalResource(signalRepository, signalSearchRepository);
        this.restSignalMockMvc = MockMvcBuilders.standaloneSetup(signalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Signal createEntity(EntityManager em) {
        Signal signal = new Signal()
            .content(DEFAULT_CONTENT)
            .date(DEFAULT_DATE)
            .type(DEFAULT_TYPE);
        return signal;
    }

    @Before
    public void initTest() {
        signalSearchRepository.deleteAll();
        signal = createEntity(em);
    }

    @Test
    @Transactional
    public void createSignal() throws Exception {
        int databaseSizeBeforeCreate = signalRepository.findAll().size();

        // Create the Signal
        restSignalMockMvc.perform(post("/api/signals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signal)))
            .andExpect(status().isCreated());

        // Validate the Signal in the database
        List<Signal> signalList = signalRepository.findAll();
        assertThat(signalList).hasSize(databaseSizeBeforeCreate + 1);
        Signal testSignal = signalList.get(signalList.size() - 1);
        assertThat(testSignal.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testSignal.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testSignal.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the Signal in Elasticsearch
        Signal signalEs = signalSearchRepository.findOne(testSignal.getId());
        assertThat(signalEs).isEqualToComparingFieldByField(testSignal);
    }

    @Test
    @Transactional
    public void createSignalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = signalRepository.findAll().size();

        // Create the Signal with an existing ID
        signal.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSignalMockMvc.perform(post("/api/signals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signal)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Signal> signalList = signalRepository.findAll();
        assertThat(signalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSignals() throws Exception {
        // Initialize the database
        signalRepository.saveAndFlush(signal);

        // Get all the signalList
        restSignalMockMvc.perform(get("/api/signals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(signal.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }

    @Test
    @Transactional
    public void getSignal() throws Exception {
        // Initialize the database
        signalRepository.saveAndFlush(signal);

        // Get the signal
        restSignalMockMvc.perform(get("/api/signals/{id}", signal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(signal.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }

    @Test
    @Transactional
    public void getNonExistingSignal() throws Exception {
        // Get the signal
        restSignalMockMvc.perform(get("/api/signals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSignal() throws Exception {
        // Initialize the database
        signalRepository.saveAndFlush(signal);
        signalSearchRepository.save(signal);
        int databaseSizeBeforeUpdate = signalRepository.findAll().size();

        // Update the signal
        Signal updatedSignal = signalRepository.findOne(signal.getId());
        updatedSignal
            .content(UPDATED_CONTENT)
            .date(UPDATED_DATE)
            .type(UPDATED_TYPE);

        restSignalMockMvc.perform(put("/api/signals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSignal)))
            .andExpect(status().isOk());

        // Validate the Signal in the database
        List<Signal> signalList = signalRepository.findAll();
        assertThat(signalList).hasSize(databaseSizeBeforeUpdate);
        Signal testSignal = signalList.get(signalList.size() - 1);
        assertThat(testSignal.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testSignal.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testSignal.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the Signal in Elasticsearch
        Signal signalEs = signalSearchRepository.findOne(testSignal.getId());
        assertThat(signalEs).isEqualToComparingFieldByField(testSignal);
    }

    @Test
    @Transactional
    public void updateNonExistingSignal() throws Exception {
        int databaseSizeBeforeUpdate = signalRepository.findAll().size();

        // Create the Signal

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSignalMockMvc.perform(put("/api/signals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signal)))
            .andExpect(status().isCreated());

        // Validate the Signal in the database
        List<Signal> signalList = signalRepository.findAll();
        assertThat(signalList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSignal() throws Exception {
        // Initialize the database
        signalRepository.saveAndFlush(signal);
        signalSearchRepository.save(signal);
        int databaseSizeBeforeDelete = signalRepository.findAll().size();

        // Get the signal
        restSignalMockMvc.perform(delete("/api/signals/{id}", signal.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean signalExistsInEs = signalSearchRepository.exists(signal.getId());
        assertThat(signalExistsInEs).isFalse();

        // Validate the database is empty
        List<Signal> signalList = signalRepository.findAll();
        assertThat(signalList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSignal() throws Exception {
        // Initialize the database
        signalRepository.saveAndFlush(signal);
        signalSearchRepository.save(signal);

        // Search the signal
        restSignalMockMvc.perform(get("/api/_search/signals?query=id:" + signal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(signal.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Signal.class);
        Signal signal1 = new Signal();
        signal1.setId(1L);
        Signal signal2 = new Signal();
        signal2.setId(signal1.getId());
        assertThat(signal1).isEqualTo(signal2);
        signal2.setId(2L);
        assertThat(signal1).isNotEqualTo(signal2);
        signal1.setId(null);
        assertThat(signal1).isNotEqualTo(signal2);
    }
}

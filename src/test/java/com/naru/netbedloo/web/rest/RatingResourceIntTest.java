package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the RatingResource REST controller.
 *
 * @see RatingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class RatingResourceIntTest {

    // private static final Integer DEFAULT_RATE = 1;
    // private static final Integer UPDATED_RATE = 2;

    // private static final ZonedDateTime DEFAULT_RATING_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    // private static final ZonedDateTime UPDATED_RATING_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    // @Autowired
    // private RatingRepository ratingRepository;

    // @Autowired
    // private RatingSearchRepository ratingSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restRatingMockMvc;

    // private Rating rating;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     RatingResource ratingResource = new RatingResource(ratingRepository, ratingSearchRepository);
    //     this.restRatingMockMvc = MockMvcBuilders.standaloneSetup(ratingResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Rating createEntity(EntityManager em) {
    //     Rating rating = new Rating()
    //         .rate(DEFAULT_RATE)
    //         .ratingTime(DEFAULT_RATING_TIME);
    //     return rating;
    // }

    // @Before
    // public void initTest() {
    //     ratingSearchRepository.deleteAll();
    //     rating = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createRating() throws Exception {
    //     int databaseSizeBeforeCreate = ratingRepository.findAll().size();

    //     // Create the Rating
    //     restRatingMockMvc.perform(post("/api/ratings")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(rating)))
    //         .andExpect(status().isCreated());

    //     // Validate the Rating in the database
    //     List<Rating> ratingList = ratingRepository.findAll();
    //     assertThat(ratingList).hasSize(databaseSizeBeforeCreate + 1);
    //     Rating testRating = ratingList.get(ratingList.size() - 1);
    //     assertThat(testRating.getRate()).isEqualTo(DEFAULT_RATE);
    //     assertThat(testRating.getRatingTime()).isEqualTo(DEFAULT_RATING_TIME);

    //     // Validate the Rating in Elasticsearch
    //     Rating ratingEs = ratingSearchRepository.findOne(testRating.getId());
    //     assertThat(ratingEs).isEqualToComparingFieldByField(testRating);
    // }

    // @Test
    // @Transactional
    // public void createRatingWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = ratingRepository.findAll().size();

    //     // Create the Rating with an existing ID
    //     rating.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restRatingMockMvc.perform(post("/api/ratings")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(rating)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Rating> ratingList = ratingRepository.findAll();
    //     assertThat(ratingList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllRatings() throws Exception {
    //     // Initialize the database
    //     ratingRepository.saveAndFlush(rating);

    //     // Get all the ratingList
    //     restRatingMockMvc.perform(get("/api/ratings?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(rating.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE)))
    //         .andExpect(jsonPath("$.[*].ratingTime").value(hasItem(sameInstant(DEFAULT_RATING_TIME))));
    // }

    // @Test
    // @Transactional
    // public void getRating() throws Exception {
    //     // Initialize the database
    //     ratingRepository.saveAndFlush(rating);

    //     // Get the rating
    //     restRatingMockMvc.perform(get("/api/ratings/{id}", rating.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(rating.getId().intValue()))
    //         .andExpect(jsonPath("$.rate").value(DEFAULT_RATE))
    //         .andExpect(jsonPath("$.ratingTime").value(sameInstant(DEFAULT_RATING_TIME)));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingRating() throws Exception {
    //     // Get the rating
    //     restRatingMockMvc.perform(get("/api/ratings/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateRating() throws Exception {
    //     // Initialize the database
    //     ratingRepository.saveAndFlush(rating);
    //     ratingSearchRepository.save(rating);
    //     int databaseSizeBeforeUpdate = ratingRepository.findAll().size();

    //     // Update the rating
    //     Rating updatedRating = ratingRepository.findOne(rating.getId());
    //     updatedRating
    //         .rate(UPDATED_RATE)
    //         .ratingTime(UPDATED_RATING_TIME);

    //     restRatingMockMvc.perform(put("/api/ratings")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedRating)))
    //         .andExpect(status().isOk());

    //     // Validate the Rating in the database
    //     List<Rating> ratingList = ratingRepository.findAll();
    //     assertThat(ratingList).hasSize(databaseSizeBeforeUpdate);
    //     Rating testRating = ratingList.get(ratingList.size() - 1);
    //     assertThat(testRating.getRate()).isEqualTo(UPDATED_RATE);
    //     assertThat(testRating.getRatingTime()).isEqualTo(UPDATED_RATING_TIME);

    //     // Validate the Rating in Elasticsearch
    //     Rating ratingEs = ratingSearchRepository.findOne(testRating.getId());
    //     assertThat(ratingEs).isEqualToComparingFieldByField(testRating);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingRating() throws Exception {
    //     int databaseSizeBeforeUpdate = ratingRepository.findAll().size();

    //     // Create the Rating

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restRatingMockMvc.perform(put("/api/ratings")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(rating)))
    //         .andExpect(status().isCreated());

    //     // Validate the Rating in the database
    //     List<Rating> ratingList = ratingRepository.findAll();
    //     assertThat(ratingList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteRating() throws Exception {
    //     // Initialize the database
    //     ratingRepository.saveAndFlush(rating);
    //     ratingSearchRepository.save(rating);
    //     int databaseSizeBeforeDelete = ratingRepository.findAll().size();

    //     // Get the rating
    //     restRatingMockMvc.perform(delete("/api/ratings/{id}", rating.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean ratingExistsInEs = ratingSearchRepository.exists(rating.getId());
    //     assertThat(ratingExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Rating> ratingList = ratingRepository.findAll();
    //     assertThat(ratingList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchRating() throws Exception {
    //     // Initialize the database
    //     ratingRepository.saveAndFlush(rating);
    //     ratingSearchRepository.save(rating);

    //     // Search the rating
    //     restRatingMockMvc.perform(get("/api/_search/ratings?query=id:" + rating.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(rating.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE)))
    //         .andExpect(jsonPath("$.[*].ratingTime").value(hasItem(sameInstant(DEFAULT_RATING_TIME))));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Rating.class);
    // }*/
}

package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the WantedResource REST controller.
 *
 * @see WantedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class WantedResourceIntTest {

    // private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    // @Autowired
    // private WantedRepository wantedRepository;
    // @Autowired
    // private ArticleRepository articleRepository;
    // @Autowired
    // private WantedSearchRepository wantedSearchRepository;
    // @Autowired
    // private MatchingService matchingService;
    // @Autowired
    // private WantedHistoryRepository wantedHistoryRepository;
    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restWantedMockMvc;

    // private Wanted wanted;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     WantedResource wantedResource = new WantedResource(wantedRepository,matchingService,wantedHistoryRepository,wantedSearchRepository,articleRepository);

    //     this.restWantedMockMvc = MockMvcBuilders.standaloneSetup(wantedResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // /** 
    // public static Wanted createEntity(EntityManager em) {
    //     Wanted wanted = new Wanted()
    //         .creationDate(DEFAULT_CREATION_DATE);
    //     return wanted;
    // }
    

    // @Before
    // public void initTest() {
    //     wantedSearchRepository.deleteAll();
    //     wanted = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createWanted() throws Exception {
    //     int databaseSizeBeforeCreate = wantedRepository.findAll().size();

    //     // Create the Wanted
    //     restWantedMockMvc.perform(post("/api/wanteds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wanted)))
    //         .andExpect(status().isCreated());

    //     // Validate the Wanted in the database
    //     List<Wanted> wantedList = wantedRepository.findAll();
    //     assertThat(wantedList).hasSize(databaseSizeBeforeCreate + 1);
    //     Wanted testWanted = wantedList.get(wantedList.size() - 1);
    //     assertThat(testWanted.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);

    //     // Validate the Wanted in Elasticsearch
    //     Wanted wantedEs = wantedSearchRepository.findOne(testWanted.getId());
    //     assertThat(wantedEs).isEqualToComparingFieldByField(testWanted);
    // }

    // @Test
    // @Transactional
    // public void createWantedWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = wantedRepository.findAll().size();

    //     // Create the Wanted with an existing ID
    //     wanted.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restWantedMockMvc.perform(post("/api/wanteds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wanted)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Wanted> wantedList = wantedRepository.findAll();
    //     assertThat(wantedList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllWanteds() throws Exception {
    //     // Initialize the database
    //     wantedRepository.saveAndFlush(wanted);

    //     // Get all the wantedList
    //     restWantedMockMvc.perform(get("/api/wanteds?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(wanted.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getWanted() throws Exception {
    //     // Initialize the database
    //     wantedRepository.saveAndFlush(wanted);

    //     // Get the wanted
    //     restWantedMockMvc.perform(get("/api/wanteds/{id}", wanted.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(wanted.getId().intValue()))
    //         .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingWanted() throws Exception {
    //     // Get the wanted
    //     restWantedMockMvc.perform(get("/api/wanteds/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateWanted() throws Exception {
    //     // Initialize the database
    //     wantedRepository.saveAndFlush(wanted);
    //     wantedSearchRepository.save(wanted);
    //     int databaseSizeBeforeUpdate = wantedRepository.findAll().size();

    //     // Update the wanted
    //     Wanted updatedWanted = wantedRepository.findOne(wanted.getId());
    //     updatedWanted
    //         .creationDate(UPDATED_CREATION_DATE);

    //     restWantedMockMvc.perform(put("/api/wanteds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedWanted)))
    //         .andExpect(status().isOk());

    //     // Validate the Wanted in the database
    //     List<Wanted> wantedList = wantedRepository.findAll();
    //     assertThat(wantedList).hasSize(databaseSizeBeforeUpdate);
    //     Wanted testWanted = wantedList.get(wantedList.size() - 1);
    //     assertThat(testWanted.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);

    //     // Validate the Wanted in Elasticsearch
    //     Wanted wantedEs = wantedSearchRepository.findOne(testWanted.getId());
    //     assertThat(wantedEs).isEqualToComparingFieldByField(testWanted);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingWanted() throws Exception {
    //     int databaseSizeBeforeUpdate = wantedRepository.findAll().size();

    //     // Create the Wanted

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restWantedMockMvc.perform(put("/api/wanteds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wanted)))
    //         .andExpect(status().isCreated());

    //     // Validate the Wanted in the database
    //     List<Wanted> wantedList = wantedRepository.findAll();
    //     assertThat(wantedList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteWanted() throws Exception {
    //     // Initialize the database
    //     wantedRepository.saveAndFlush(wanted);
    //     wantedSearchRepository.save(wanted);
    //     int databaseSizeBeforeDelete = wantedRepository.findAll().size();

    //     // Get the wanted
    //     restWantedMockMvc.perform(delete("/api/wanteds/{id}", wanted.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean wantedExistsInEs = wantedSearchRepository.exists(wanted.getId());
    //     assertThat(wantedExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Wanted> wantedList = wantedRepository.findAll();
    //     assertThat(wantedList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchWanted() throws Exception {
    //     // Initialize the database
    //     wantedRepository.saveAndFlush(wanted);
    //     wantedSearchRepository.save(wanted);

    //     // Search the wanted
    //     restWantedMockMvc.perform(get("/api/_search/wanteds?query=id:" + wanted.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(wanted.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Wanted.class);
    //     Wanted wanted1 = new Wanted();
    //     wanted1.setId(1L);
    //     Wanted wanted2 = new Wanted();
    //     wanted2.setId(wanted1.getId());
    //     assertThat(wanted1).isEqualTo(wanted2);
    //     wanted2.setId(2L);
    //     assertThat(wanted1).isNotEqualTo(wanted2);
    //     wanted1.setId(null);
    //     assertThat(wanted1).isNotEqualTo(wanted2);
    // }*/
}

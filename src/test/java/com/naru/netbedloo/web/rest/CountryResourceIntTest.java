package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the CountryResource REST controller.
 *
 * @see CountryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class CountryResourceIntTest {

    // private static final String DEFAULT_NAME = "AAAAAAAAAA";
    // private static final String UPDATED_NAME = "BBBBBBBBBB";

    // private static final String DEFAULT_FLAG = "AAAAAAAAAA";
    // private static final String UPDATED_FLAG = "BBBBBBBBBB";

    // private static final String DEFAULT_CODE = "AAAAAAAAAA";
    // private static final String UPDATED_CODE = "BBBBBBBBBB";

    // @Autowired
    // private CountryRepository countryRepository;

    // @Autowired
    // private CountrySearchRepository countrySearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restCountryMockMvc;

    // private Country country;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     CountryResource countryResource = new CountryResource(countryRepository, countrySearchRepository);
    //     this.restCountryMockMvc = MockMvcBuilders.standaloneSetup(countryResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Country createEntity(EntityManager em) {
    //     Country country = new Country()
    //         .name(DEFAULT_NAME)
    //         .flag(DEFAULT_FLAG)
    //         .code(DEFAULT_CODE);
    //     return country;
    // }

    // @Before
    // public void initTest() {
    //     countrySearchRepository.deleteAll();
    //     country = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createCountry() throws Exception {
    //     int databaseSizeBeforeCreate = countryRepository.findAll().size();

    //     // Create the Country
    //     restCountryMockMvc.perform(post("/api/countries")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(country)))
    //         .andExpect(status().isCreated());

    //     // Validate the Country in the database
    //     List<Country> countryList = countryRepository.findAll();
    //     assertThat(countryList).hasSize(databaseSizeBeforeCreate + 1);
    //     Country testCountry = countryList.get(countryList.size() - 1);
    //     assertThat(testCountry.getName()).isEqualTo(DEFAULT_NAME);
    //     assertThat(testCountry.getFlag()).isEqualTo(DEFAULT_FLAG);
    //     assertThat(testCountry.getCode()).isEqualTo(DEFAULT_CODE);

    //     // Validate the Country in Elasticsearch
    //     Country countryEs = countrySearchRepository.findOne(testCountry.getId());
    //     assertThat(countryEs).isEqualToComparingFieldByField(testCountry);
    // }

    // @Test
    // @Transactional
    // public void createCountryWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = countryRepository.findAll().size();

    //     // Create the Country with an existing ID
    //     country.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restCountryMockMvc.perform(post("/api/countries")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(country)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Country> countryList = countryRepository.findAll();
    //     assertThat(countryList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllCountries() throws Exception {
    //     // Initialize the database
    //     countryRepository.saveAndFlush(country);

    //     // Get all the countryList
    //     restCountryMockMvc.perform(get("/api/countries?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.toString())))
    //         .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getCountry() throws Exception {
    //     // Initialize the database
    //     countryRepository.saveAndFlush(country);

    //     // Get the country
    //     restCountryMockMvc.perform(get("/api/countries/{id}", country.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(country.getId().intValue()))
    //         .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
    //         .andExpect(jsonPath("$.flag").value(DEFAULT_FLAG.toString()))
    //         .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingCountry() throws Exception {
    //     // Get the country
    //     restCountryMockMvc.perform(get("/api/countries/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateCountry() throws Exception {
    //     // Initialize the database
    //     countryRepository.saveAndFlush(country);
    //     countrySearchRepository.save(country);
    //     int databaseSizeBeforeUpdate = countryRepository.findAll().size();

    //     // Update the country
    //     Country updatedCountry = countryRepository.findOne(country.getId());
    //     updatedCountry
    //         .name(UPDATED_NAME)
    //         .flag(UPDATED_FLAG)
    //         .code(UPDATED_CODE);

    //     restCountryMockMvc.perform(put("/api/countries")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedCountry)))
    //         .andExpect(status().isOk());

    //     // Validate the Country in the database
    //     List<Country> countryList = countryRepository.findAll();
    //     assertThat(countryList).hasSize(databaseSizeBeforeUpdate);
    //     Country testCountry = countryList.get(countryList.size() - 1);
    //     assertThat(testCountry.getName()).isEqualTo(UPDATED_NAME);
    //     assertThat(testCountry.getFlag()).isEqualTo(UPDATED_FLAG);
    //     assertThat(testCountry.getCode()).isEqualTo(UPDATED_CODE);

    //     // Validate the Country in Elasticsearch
    //     Country countryEs = countrySearchRepository.findOne(testCountry.getId());
    //     assertThat(countryEs).isEqualToComparingFieldByField(testCountry);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingCountry() throws Exception {
    //     int databaseSizeBeforeUpdate = countryRepository.findAll().size();

    //     // Create the Country

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restCountryMockMvc.perform(put("/api/countries")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(country)))
    //         .andExpect(status().isCreated());

    //     // Validate the Country in the database
    //     List<Country> countryList = countryRepository.findAll();
    //     assertThat(countryList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteCountry() throws Exception {
    //     // Initialize the database
    //     countryRepository.saveAndFlush(country);
    //     countrySearchRepository.save(country);
    //     int databaseSizeBeforeDelete = countryRepository.findAll().size();

    //     // Get the country
    //     restCountryMockMvc.perform(delete("/api/countries/{id}", country.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean countryExistsInEs = countrySearchRepository.exists(country.getId());
    //     assertThat(countryExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Country> countryList = countryRepository.findAll();
    //     assertThat(countryList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchCountry() throws Exception {
    //     // Initialize the database
    //     countryRepository.saveAndFlush(country);
    //     countrySearchRepository.save(country);

    //     // Search the country
    //     restCountryMockMvc.perform(get("/api/_search/countries?query=id:" + country.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.toString())))
    //         .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Country.class);
    // }
}

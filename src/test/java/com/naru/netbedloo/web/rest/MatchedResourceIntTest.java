package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the MatchedResource REST controller.
 *
 * @see MatchedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class MatchedResourceIntTest {

    // private static final LocalDate DEFAULT_MATCHING_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_MATCHING_DATE = LocalDate.now(ZoneId.systemDefault());

    // private static final Boolean DEFAULT_STATUT = false;
    // private static final Boolean UPDATED_STATUT = true;

    // private static final Long DEFAULT_REFERENCE = 1L;
    // private static final Long UPDATED_REFERENCE = 2L;

    // private static final Double DEFAULT_DISTANCE = 1D;
    // private static final Double UPDATED_DISTANCE = 2D;

    // @Autowired
    // private MatchedRepository matchedRepository;

    // @Autowired
    // private TbedlooService tbedlooService;
    // @Autowired
    // private MatchedSearchRepository matchedSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restMatchedMockMvc;

    // private Matched matched;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     MatchedResource matchedResource = new MatchedResource(matchedRepository, matchedSearchRepository,tbedlooService);
    //     this.restMatchedMockMvc = MockMvcBuilders.standaloneSetup(matchedResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Matched createEntity(EntityManager em) {
    //     Matched matched = new Matched()
    //         .matchingDate(DEFAULT_MATCHING_DATE)
    //         .statut(DEFAULT_STATUT)
    //         .reference(DEFAULT_REFERENCE)
    //         .distance(DEFAULT_DISTANCE);
    //     return matched;
    // }

    // @Before
    // public void initTest() {
    //     matchedSearchRepository.deleteAll();
    //     matched = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createMatched() throws Exception {
    //     int databaseSizeBeforeCreate = matchedRepository.findAll().size();

    //     // Create the Matched
    //     restMatchedMockMvc.perform(post("/api/matcheds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matched)))
    //         .andExpect(status().isCreated());

    //     // Validate the Matched in the database
    //     List<Matched> matchedList = matchedRepository.findAll();
    //     assertThat(matchedList).hasSize(databaseSizeBeforeCreate + 1);
    //     Matched testMatched = matchedList.get(matchedList.size() - 1);
    //     assertThat(testMatched.getMatchingDate()).isEqualTo(DEFAULT_MATCHING_DATE);
    //     assertThat(testMatched.isStatut()).isEqualTo(DEFAULT_STATUT);
    //     assertThat(testMatched.getReference()).isEqualTo(DEFAULT_REFERENCE);
    //     assertThat(testMatched.getDistance()).isEqualTo(DEFAULT_DISTANCE);

    //     // Validate the Matched in Elasticsearch
    //     Matched matchedEs = matchedSearchRepository.findOne(testMatched.getId());
    //     assertThat(matchedEs).isEqualToComparingFieldByField(testMatched);
    // }

    // @Test
    // @Transactional
    // public void createMatchedWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = matchedRepository.findAll().size();

    //     // Create the Matched with an existing ID
    //     matched.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restMatchedMockMvc.perform(post("/api/matcheds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matched)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Matched> matchedList = matchedRepository.findAll();
    //     assertThat(matchedList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllMatcheds() throws Exception {
    //     // Initialize the database
    //     matchedRepository.saveAndFlush(matched);

    //     // Get all the matchedList
    //     restMatchedMockMvc.perform(get("/api/matcheds?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(matched.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].matchingDate").value(hasItem(DEFAULT_MATCHING_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.booleanValue())))
    //         .andExpect(jsonPath("$.[*].reference").value(hasItem(DEFAULT_REFERENCE.intValue())))
    //         .andExpect(jsonPath("$.[*].distance").value(hasItem(DEFAULT_DISTANCE.doubleValue())));
    // }

    // @Test
    // @Transactional
    // public void getMatched() throws Exception {
    //     // Initialize the database
    //     matchedRepository.saveAndFlush(matched);

    //     // Get the matched
    //     restMatchedMockMvc.perform(get("/api/matcheds/{id}", matched.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(matched.getId().intValue()))
    //         .andExpect(jsonPath("$.matchingDate").value(DEFAULT_MATCHING_DATE.toString()))
    //         .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.booleanValue()))
    //         .andExpect(jsonPath("$.reference").value(DEFAULT_REFERENCE.intValue()))
    //         .andExpect(jsonPath("$.distance").value(DEFAULT_DISTANCE.doubleValue()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingMatched() throws Exception {
    //     // Get the matched
    //     restMatchedMockMvc.perform(get("/api/matcheds/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateMatched() throws Exception {
    //     // Initialize the database
    //     matchedRepository.saveAndFlush(matched);
    //     matchedSearchRepository.save(matched);
    //     int databaseSizeBeforeUpdate = matchedRepository.findAll().size();

    //     // Update the matched
    //     Matched updatedMatched = matchedRepository.findOne(matched.getId());
    //     updatedMatched
    //         .matchingDate(UPDATED_MATCHING_DATE)
    //         .statut(UPDATED_STATUT)
    //         .reference(UPDATED_REFERENCE)
    //         .distance(UPDATED_DISTANCE);

    //     restMatchedMockMvc.perform(put("/api/matcheds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedMatched)))
    //         .andExpect(status().isOk());

    //     // Validate the Matched in the database
    //     List<Matched> matchedList = matchedRepository.findAll();
    //     assertThat(matchedList).hasSize(databaseSizeBeforeUpdate);
    //     Matched testMatched = matchedList.get(matchedList.size() - 1);
    //     assertThat(testMatched.getMatchingDate()).isEqualTo(UPDATED_MATCHING_DATE);
    //     assertThat(testMatched.isStatut()).isEqualTo(UPDATED_STATUT);
    //     assertThat(testMatched.getReference()).isEqualTo(UPDATED_REFERENCE);
    //     assertThat(testMatched.getDistance()).isEqualTo(UPDATED_DISTANCE);

    //     // Validate the Matched in Elasticsearch
    //     Matched matchedEs = matchedSearchRepository.findOne(testMatched.getId());
    //     assertThat(matchedEs).isEqualToComparingFieldByField(testMatched);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingMatched() throws Exception {
    //     int databaseSizeBeforeUpdate = matchedRepository.findAll().size();

    //     // Create the Matched

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restMatchedMockMvc.perform(put("/api/matcheds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(matched)))
    //         .andExpect(status().isCreated());

    //     // Validate the Matched in the database
    //     List<Matched> matchedList = matchedRepository.findAll();
    //     assertThat(matchedList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteMatched() throws Exception {
    //     // Initialize the database
    //     matchedRepository.saveAndFlush(matched);
    //     matchedSearchRepository.save(matched);
    //     int databaseSizeBeforeDelete = matchedRepository.findAll().size();

    //     // Get the matched
    //     restMatchedMockMvc.perform(delete("/api/matcheds/{id}", matched.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean matchedExistsInEs = matchedSearchRepository.exists(matched.getId());
    //     assertThat(matchedExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Matched> matchedList = matchedRepository.findAll();
    //     assertThat(matchedList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchMatched() throws Exception {
    //     // Initialize the database
    //     matchedRepository.saveAndFlush(matched);
    //     matchedSearchRepository.save(matched);

    //     // Search the matched
    //     restMatchedMockMvc.perform(get("/api/_search/matcheds?query=id:" + matched.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(matched.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].matchingDate").value(hasItem(DEFAULT_MATCHING_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.booleanValue())))
    //         .andExpect(jsonPath("$.[*].reference").value(hasItem(DEFAULT_REFERENCE.intValue())))
    //         .andExpect(jsonPath("$.[*].distance").value(hasItem(DEFAULT_DISTANCE.doubleValue())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Matched.class);
    // }*/
}

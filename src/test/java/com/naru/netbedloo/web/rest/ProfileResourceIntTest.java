package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the ProfileResource REST controller.
 *
 * @see ProfileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class ProfileResourceIntTest {

    // private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    // private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    // private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    // private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    // private static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_BIRTH_DATE = LocalDate.now(ZoneId.systemDefault());

    // private static final String DEFAULT_IMAGE_PATH = "AAAAAAAAAA";
    // private static final String UPDATED_IMAGE_PATH = "BBBBBBBBBB";

    // private static final String DEFAULT_SEXE = "AAAAAAAAAA";
    // private static final String UPDATED_SEXE = "BBBBBBBBBB";

    // private static final String DEFAULT_ADRESS = "AAAAAAAAAA";
    // private static final String UPDATED_ADRESS = "BBBBBBBBBB";

    // private static final Integer DEFAULT_ZIP_CODE = 1;
    // private static final Integer UPDATED_ZIP_CODE = 2;

    // private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    // private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    // private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    // private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    // private static final String DEFAULT_BIO = "AAAAAAAAAA";
    // private static final String UPDATED_BIO = "BBBBBBBBBB";

    // @Autowired
    // private ProfileRepository profileRepository;

    // @Autowired
    // private ProfileSearchRepository profileSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restProfileMockMvc;

    // private Profile profile;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     ProfileResource profileResource = new ProfileResource(profileRepository, profileSearchRepository);
    //     this.restProfileMockMvc = MockMvcBuilders.standaloneSetup(profileResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Profile createEntity(EntityManager em) {
    //     Profile profile = new Profile()
    //         .firstName(DEFAULT_FIRST_NAME)
    //         .lastName(DEFAULT_LAST_NAME)
    //         .birthDate(DEFAULT_BIRTH_DATE)
    //         .imagePath(DEFAULT_IMAGE_PATH)
    //         .sexe(DEFAULT_SEXE)
    //         .adress(DEFAULT_ADRESS)
    //         .zipCode(DEFAULT_ZIP_CODE)
    //         .email(DEFAULT_EMAIL)
    //         .phoneNumber(DEFAULT_PHONE_NUMBER)
    //         .bio(DEFAULT_BIO);
    //     return profile;
    // }

    // @Before
    // public void initTest() {
    //     profileSearchRepository.deleteAll();
    //     profile = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createProfile() throws Exception {
    //     int databaseSizeBeforeCreate = profileRepository.findAll().size();

    //     // Create the Profile
    //     restProfileMockMvc.perform(post("/api/profiles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(profile)))
    //         .andExpect(status().isCreated());

    //     // Validate the Profile in the database
    //     List<Profile> profileList = profileRepository.findAll();
    //     assertThat(profileList).hasSize(databaseSizeBeforeCreate + 1);
    //     Profile testProfile = profileList.get(profileList.size() - 1);
    //     assertThat(testProfile.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
    //     assertThat(testProfile.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
    //     assertThat(testProfile.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
    //     assertThat(testProfile.getImagePath()).isEqualTo(DEFAULT_IMAGE_PATH);
    //     assertThat(testProfile.getSexe()).isEqualTo(DEFAULT_SEXE);
    //     assertThat(testProfile.getAdress()).isEqualTo(DEFAULT_ADRESS);
    //     assertThat(testProfile.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
    //     assertThat(testProfile.getEmail()).isEqualTo(DEFAULT_EMAIL);
    //     assertThat(testProfile.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    //     assertThat(testProfile.getBio()).isEqualTo(DEFAULT_BIO);

    //     // Validate the Profile in Elasticsearch
    //     Profile profileEs = profileSearchRepository.findOne(testProfile.getId());
    //     assertThat(profileEs).isEqualToComparingFieldByField(testProfile);
    // }

    // @Test
    // @Transactional
    // public void createProfileWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = profileRepository.findAll().size();

    //     // Create the Profile with an existing ID
    //     profile.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restProfileMockMvc.perform(post("/api/profiles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(profile)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Profile> profileList = profileRepository.findAll();
    //     assertThat(profileList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllProfiles() throws Exception {
    //     // Initialize the database
    //     profileRepository.saveAndFlush(profile);

    //     // Get all the profileList
    //     restProfileMockMvc.perform(get("/api/profiles?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(profile.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())))
    //         .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())))
    //         .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())))
    //         .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
    //         .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
    //         .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
    //         .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO.toString())));
    // }

    // @Test
    // @Transactional
    // public void getProfile() throws Exception {
    //     // Initialize the database
    //     profileRepository.saveAndFlush(profile);

    //     // Get the profile
    //     restProfileMockMvc.perform(get("/api/profiles/{id}", profile.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(profile.getId().intValue()))
    //         .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
    //         .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
    //         .andExpect(jsonPath("$.birthDate").value(DEFAULT_BIRTH_DATE.toString()))
    //         .andExpect(jsonPath("$.imagePath").value(DEFAULT_IMAGE_PATH.toString()))
    //         .andExpect(jsonPath("$.sexe").value(DEFAULT_SEXE.toString()))
    //         .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS.toString()))
    //         .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE))
    //         .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
    //         .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()))
    //         .andExpect(jsonPath("$.bio").value(DEFAULT_BIO.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingProfile() throws Exception {
    //     // Get the profile
    //     restProfileMockMvc.perform(get("/api/profiles/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateProfile() throws Exception {
    //     // Initialize the database
    //     profileRepository.saveAndFlush(profile);
    //     profileSearchRepository.save(profile);
    //     int databaseSizeBeforeUpdate = profileRepository.findAll().size();

    //     // Update the profile
    //     Profile updatedProfile = profileRepository.findOne(profile.getId());
    //     updatedProfile
    //         .firstName(UPDATED_FIRST_NAME)
    //         .lastName(UPDATED_LAST_NAME)
    //         .birthDate(UPDATED_BIRTH_DATE)
    //         .imagePath(UPDATED_IMAGE_PATH)
    //         .sexe(UPDATED_SEXE)
    //         .adress(UPDATED_ADRESS)
    //         .zipCode(UPDATED_ZIP_CODE)
    //         .email(UPDATED_EMAIL)
    //         .phoneNumber(UPDATED_PHONE_NUMBER)
    //         .bio(UPDATED_BIO);

    //     restProfileMockMvc.perform(put("/api/profiles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedProfile)))
    //         .andExpect(status().isOk());

    //     // Validate the Profile in the database
    //     List<Profile> profileList = profileRepository.findAll();
    //     assertThat(profileList).hasSize(databaseSizeBeforeUpdate);
    //     Profile testProfile = profileList.get(profileList.size() - 1);
    //     assertThat(testProfile.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
    //     assertThat(testProfile.getLastName()).isEqualTo(UPDATED_LAST_NAME);
    //     assertThat(testProfile.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
    //     assertThat(testProfile.getImagePath()).isEqualTo(UPDATED_IMAGE_PATH);
    //     assertThat(testProfile.getSexe()).isEqualTo(UPDATED_SEXE);
    //     assertThat(testProfile.getAdress()).isEqualTo(UPDATED_ADRESS);
    //     assertThat(testProfile.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
    //     assertThat(testProfile.getEmail()).isEqualTo(UPDATED_EMAIL);
    //     assertThat(testProfile.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    //     assertThat(testProfile.getBio()).isEqualTo(UPDATED_BIO);

    //     // Validate the Profile in Elasticsearch
    //     Profile profileEs = profileSearchRepository.findOne(testProfile.getId());
    //     assertThat(profileEs).isEqualToComparingFieldByField(testProfile);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingProfile() throws Exception {
    //     int databaseSizeBeforeUpdate = profileRepository.findAll().size();

    //     // Create the Profile

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restProfileMockMvc.perform(put("/api/profiles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(profile)))
    //         .andExpect(status().isCreated());

    //     // Validate the Profile in the database
    //     List<Profile> profileList = profileRepository.findAll();
    //     assertThat(profileList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteProfile() throws Exception {
    //     // Initialize the database
    //     profileRepository.saveAndFlush(profile);
    //     profileSearchRepository.save(profile);
    //     int databaseSizeBeforeDelete = profileRepository.findAll().size();

    //     // Get the profile
    //     restProfileMockMvc.perform(delete("/api/profiles/{id}", profile.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean profileExistsInEs = profileSearchRepository.exists(profile.getId());
    //     assertThat(profileExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Profile> profileList = profileRepository.findAll();
    //     assertThat(profileList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchProfile() throws Exception {
    //     // Initialize the database
    //     profileRepository.saveAndFlush(profile);
    //     profileSearchRepository.save(profile);

    //     // Search the profile
    //     restProfileMockMvc.perform(get("/api/_search/profiles?query=id:" + profile.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(profile.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())))
    //         .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())))
    //         .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())))
    //         .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
    //         .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
    //         .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
    //         .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Profile.class);
    // }
}

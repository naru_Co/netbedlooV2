package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the OwnedResource REST controller.
 *
 * @see OwnedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class OwnedResourceIntTest {

    // private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    // @Autowired
    // private OwnedRepository ownedRepository;
    // @Autowired
    // private ArticleRepository articleRepository;
    // @Autowired
    // private OwnedSearchRepository ownedSearchRepository;
    // @Autowired
    // private MatchingService matchingService;
    // @Autowired
    // private OwnedHistoryRepository ownedHistoryRepository;
    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restOwnedMockMvc;

    // private Owned owned;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     OwnedResource ownedResource = new OwnedResource(ownedRepository,matchingService ,ownedHistoryRepository,ownedSearchRepository,articleRepository);

    //     this.restOwnedMockMvc = MockMvcBuilders.standaloneSetup(ownedResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Owned createEntity(EntityManager em) {
    //     Owned owned = new Owned()
    //         .creationDate(DEFAULT_CREATION_DATE);
    //     return owned;
    // }

    // @Before
    // public void initTest() {
    //     ownedSearchRepository.deleteAll();
    //     owned = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createOwned() throws Exception {
    //     int databaseSizeBeforeCreate = ownedRepository.findAll().size();

    //     // Create the Owned
    //     restOwnedMockMvc.perform(post("/api/owneds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(owned)))
    //         .andExpect(status().isCreated());

    //     // Validate the Owned in the database
    //     List<Owned> ownedList = ownedRepository.findAll();
    //     assertThat(ownedList).hasSize(databaseSizeBeforeCreate + 1);
    //     Owned testOwned = ownedList.get(ownedList.size() - 1);
    //     assertThat(testOwned.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);

    //     // Validate the Owned in Elasticsearch
    //     Owned ownedEs = ownedSearchRepository.findOne(testOwned.getId());
    //     assertThat(ownedEs).isEqualToComparingFieldByField(testOwned);
    // }

    // @Test
    // @Transactional
    // public void createOwnedWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = ownedRepository.findAll().size();

    //     // Create the Owned with an existing ID
    //     owned.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restOwnedMockMvc.perform(post("/api/owneds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(owned)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Owned> ownedList = ownedRepository.findAll();
    //     assertThat(ownedList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllOwneds() throws Exception {
    //     // Initialize the database
    //     ownedRepository.saveAndFlush(owned);

    //     // Get all the ownedList
    //     restOwnedMockMvc.perform(get("/api/owneds?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(owned.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getOwned() throws Exception {
    //     // Initialize the database
    //     ownedRepository.saveAndFlush(owned);

    //     // Get the owned
    //     restOwnedMockMvc.perform(get("/api/owneds/{id}", owned.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(owned.getId().intValue()))
    //         .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingOwned() throws Exception {
    //     // Get the owned
    //     restOwnedMockMvc.perform(get("/api/owneds/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateOwned() throws Exception {
    //     // Initialize the database
    //     ownedRepository.saveAndFlush(owned);
    //     ownedSearchRepository.save(owned);
    //     int databaseSizeBeforeUpdate = ownedRepository.findAll().size();

    //     // Update the owned
    //     Owned updatedOwned = ownedRepository.findOne(owned.getId());
    //     updatedOwned
    //         .creationDate(UPDATED_CREATION_DATE);

    //     restOwnedMockMvc.perform(put("/api/owneds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedOwned)))
    //         .andExpect(status().isOk());

    //     // Validate the Owned in the database
    //     List<Owned> ownedList = ownedRepository.findAll();
    //     assertThat(ownedList).hasSize(databaseSizeBeforeUpdate);
    //     Owned testOwned = ownedList.get(ownedList.size() - 1);
    //     assertThat(testOwned.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);

    //     // Validate the Owned in Elasticsearch
    //     Owned ownedEs = ownedSearchRepository.findOne(testOwned.getId());
    //     assertThat(ownedEs).isEqualToComparingFieldByField(testOwned);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingOwned() throws Exception {
    //     int databaseSizeBeforeUpdate = ownedRepository.findAll().size();

    //     // Create the Owned

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restOwnedMockMvc.perform(put("/api/owneds")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(owned)))
    //         .andExpect(status().isCreated());

    //     // Validate the Owned in the database
    //     List<Owned> ownedList = ownedRepository.findAll();
    //     assertThat(ownedList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteOwned() throws Exception {
    //     // Initialize the database
    //     ownedRepository.saveAndFlush(owned);
    //     ownedSearchRepository.save(owned);
    //     int databaseSizeBeforeDelete = ownedRepository.findAll().size();

    //     // Get the owned
    //     restOwnedMockMvc.perform(delete("/api/owneds/{id}", owned.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean ownedExistsInEs = ownedSearchRepository.exists(owned.getId());
    //     assertThat(ownedExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Owned> ownedList = ownedRepository.findAll();
    //     assertThat(ownedList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchOwned() throws Exception {
    //     // Initialize the database
    //     ownedRepository.saveAndFlush(owned);
    //     ownedSearchRepository.save(owned);

    //     // Search the owned
    //     restOwnedMockMvc.perform(get("/api/_search/owneds?query=id:" + owned.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(owned.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Owned.class);
    //     Owned owned1 = new Owned();
    //     owned1.setId(1L);
    //     Owned owned2 = new Owned();
    //     owned2.setId(owned1.getId());
    //     assertThat(owned1).isEqualTo(owned2);
    //     owned2.setId(2L);
    //     assertThat(owned1).isNotEqualTo(owned2);
    //     owned1.setId(null);
    //     assertThat(owned1).isNotEqualTo(owned2);
    // }*/
}

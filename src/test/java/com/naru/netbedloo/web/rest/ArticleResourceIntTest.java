package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the ArticleResource REST controller.
 *
 * @see ArticleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class ArticleResourceIntTest {

    // private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    // private static final String UPDATED_TITLE = "BBBBBBBBBB";

    // private static final String DEFAULT_AUTHOR = "AAAAAAAAAA";
    // private static final String UPDATED_AUTHOR = "BBBBBBBBBB";

    // private static final String DEFAULT_COVER_IMAGE = "AAAAAAAAAA";
    // private static final String UPDATED_COVER_IMAGE = "BBBBBBBBBB";

    // private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    // private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    // private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    // private static final Long DEFAULT_POPULARITY = 1L;
    // private static final Long UPDATED_POPULARITY = 2L;

    // @Autowired
    // private ArticleRepository articleRepository;

    // @Autowired
    // private ArticleSearchRepository articleSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restArticleMockMvc;

    // private Article article;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     ArticleResource articleResource = new ArticleResource(articleRepository, articleSearchRepository);
    //     this.restArticleMockMvc = MockMvcBuilders.standaloneSetup(articleResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Article createEntity(EntityManager em) {
    //     Article article = new Article()
    //         .title(DEFAULT_TITLE)
    //         .author(DEFAULT_AUTHOR)
    //         .coverImage(DEFAULT_COVER_IMAGE)
    //         .description(DEFAULT_DESCRIPTION)
    //         .creationDate(DEFAULT_CREATION_DATE)
    //         .popularity(DEFAULT_POPULARITY);
    //     return article;
    // }

    // @Before
    // public void initTest() {
    //     articleSearchRepository.deleteAll();
    //     article = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createArticle() throws Exception {
    //     int databaseSizeBeforeCreate = articleRepository.findAll().size();

    //     // Create the Article
    //     restArticleMockMvc.perform(post("/api/articles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(article)))
    //         .andExpect(status().isCreated());

    //     // Validate the Article in the database
    //     List<Article> articleList = articleRepository.findAll();
    //     assertThat(articleList).hasSize(databaseSizeBeforeCreate + 1);
    //     Article testArticle = articleList.get(articleList.size() - 1);
    //     assertThat(testArticle.getTitle()).isEqualTo(DEFAULT_TITLE);
    //     assertThat(testArticle.getAuthor()).isEqualTo(DEFAULT_AUTHOR);
    //     assertThat(testArticle.getCoverImage()).isEqualTo(DEFAULT_COVER_IMAGE);
    //     assertThat(testArticle.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    //     assertThat(testArticle.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
    //     assertThat(testArticle.getPopularity()).isEqualTo(DEFAULT_POPULARITY);

    //     // Validate the Article in Elasticsearch
    //     Article articleEs = articleSearchRepository.findOne(testArticle.getId());
    //     assertThat(articleEs).isEqualToComparingFieldByField(testArticle);
    // }

    // @Test
    // @Transactional
    // public void createArticleWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = articleRepository.findAll().size();

    //     // Create the Article with an existing ID
    //     article.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restArticleMockMvc.perform(post("/api/articles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(article)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Article> articleList = articleRepository.findAll();
    //     assertThat(articleList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllArticles() throws Exception {
    //     // Initialize the database
    //     articleRepository.saveAndFlush(article);

    //     // Get all the articleList
    //     restArticleMockMvc.perform(get("/api/articles?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(article.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
    //         .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
    //         .andExpect(jsonPath("$.[*].coverImage").value(hasItem(DEFAULT_COVER_IMAGE.toString())))
    //         .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].popularity").value(hasItem(DEFAULT_POPULARITY.intValue())));
    // }

    // @Test
    // @Transactional
    // public void getArticle() throws Exception {
    //     // Initialize the database
    //     articleRepository.saveAndFlush(article);

    //     // Get the article
    //     restArticleMockMvc.perform(get("/api/articles/{id}", article.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(article.getId().intValue()))
    //         .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
    //         .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()))
    //         .andExpect(jsonPath("$.coverImage").value(DEFAULT_COVER_IMAGE.toString()))
    //         .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
    //         .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
    //         .andExpect(jsonPath("$.popularity").value(DEFAULT_POPULARITY.intValue()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingArticle() throws Exception {
    //     // Get the article
    //     restArticleMockMvc.perform(get("/api/articles/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateArticle() throws Exception {
    //     // Initialize the database
    //     articleRepository.saveAndFlush(article);
    //     articleSearchRepository.save(article);
    //     int databaseSizeBeforeUpdate = articleRepository.findAll().size();

    //     // Update the article
    //     Article updatedArticle = articleRepository.findOne(article.getId());
    //     updatedArticle
    //         .title(UPDATED_TITLE)
    //         .author(UPDATED_AUTHOR)
    //         .coverImage(UPDATED_COVER_IMAGE)
    //         .description(UPDATED_DESCRIPTION)
    //         .creationDate(UPDATED_CREATION_DATE)
    //         .popularity(UPDATED_POPULARITY);

    //     restArticleMockMvc.perform(put("/api/articles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedArticle)))
    //         .andExpect(status().isOk());

    //     // Validate the Article in the database
    //     List<Article> articleList = articleRepository.findAll();
    //     assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    //     Article testArticle = articleList.get(articleList.size() - 1);
    //     assertThat(testArticle.getTitle()).isEqualTo(UPDATED_TITLE);
    //     assertThat(testArticle.getAuthor()).isEqualTo(UPDATED_AUTHOR);
    //     assertThat(testArticle.getCoverImage()).isEqualTo(UPDATED_COVER_IMAGE);
    //     assertThat(testArticle.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    //     assertThat(testArticle.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
    //     assertThat(testArticle.getPopularity()).isEqualTo(UPDATED_POPULARITY);

    //     // Validate the Article in Elasticsearch
    //     Article articleEs = articleSearchRepository.findOne(testArticle.getId());
    //     assertThat(articleEs).isEqualToComparingFieldByField(testArticle);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingArticle() throws Exception {
    //     int databaseSizeBeforeUpdate = articleRepository.findAll().size();

    //     // Create the Article

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restArticleMockMvc.perform(put("/api/articles")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(article)))
    //         .andExpect(status().isCreated());

    //     // Validate the Article in the database
    //     List<Article> articleList = articleRepository.findAll();
    //     assertThat(articleList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteArticle() throws Exception {
    //     // Initialize the database
    //     articleRepository.saveAndFlush(article);
    //     articleSearchRepository.save(article);
    //     int databaseSizeBeforeDelete = articleRepository.findAll().size();

    //     // Get the article
    //     restArticleMockMvc.perform(delete("/api/articles/{id}", article.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean articleExistsInEs = articleSearchRepository.exists(article.getId());
    //     assertThat(articleExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Article> articleList = articleRepository.findAll();
    //     assertThat(articleList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchArticle() throws Exception {
    //     // Initialize the database
    //     articleRepository.saveAndFlush(article);
    //     articleSearchRepository.save(article);

    //     // Search the article
    //     restArticleMockMvc.perform(get("/api/_search/articles?query=id:" + article.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(article.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
    //         .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
    //         .andExpect(jsonPath("$.[*].coverImage").value(hasItem(DEFAULT_COVER_IMAGE.toString())))
    //         .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
    //         .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].popularity").value(hasItem(DEFAULT_POPULARITY.intValue())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Article.class);
    // }
}

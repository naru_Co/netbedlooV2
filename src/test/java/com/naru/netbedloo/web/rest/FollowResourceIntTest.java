package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the FollowResource REST controller.
 *
 * @see FollowResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class FollowResourceIntTest {

    // private static final ZonedDateTime DEFAULT_FOLLOW_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    // private static final ZonedDateTime UPDATED_FOLLOW_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    // @Autowired
    // private FollowRepository followRepository;

    // @Autowired
    // private FollowSearchRepository followSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restFollowMockMvc;

    // private Follow follow;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     FollowResource followResource = new FollowResource(followRepository, followSearchRepository);
    //     this.restFollowMockMvc = MockMvcBuilders.standaloneSetup(followResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Follow createEntity(EntityManager em) {
    //     Follow follow = new Follow()
    //         .followTime(DEFAULT_FOLLOW_TIME);
    //     return follow;
    // }

    // @Before
    // public void initTest() {
    //     followSearchRepository.deleteAll();
    //     follow = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createFollow() throws Exception {
    //     int databaseSizeBeforeCreate = followRepository.findAll().size();

    //     // Create the Follow
    //     restFollowMockMvc.perform(post("/api/follows")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(follow)))
    //         .andExpect(status().isCreated());

    //     // Validate the Follow in the database
    //     List<Follow> followList = followRepository.findAll();
    //     assertThat(followList).hasSize(databaseSizeBeforeCreate + 1);
    //     Follow testFollow = followList.get(followList.size() - 1);
    //     assertThat(testFollow.getFollowTime()).isEqualTo(DEFAULT_FOLLOW_TIME);

    //     // Validate the Follow in Elasticsearch
    //     Follow followEs = followSearchRepository.findOne(testFollow.getId());
    //     assertThat(followEs).isEqualToComparingFieldByField(testFollow);
    // }

    // @Test
    // @Transactional
    // public void createFollowWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = followRepository.findAll().size();

    //     // Create the Follow with an existing ID
    //     follow.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restFollowMockMvc.perform(post("/api/follows")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(follow)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Follow> followList = followRepository.findAll();
    //     assertThat(followList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllFollows() throws Exception {
    //     // Initialize the database
    //     followRepository.saveAndFlush(follow);

    //     // Get all the followList
    //     restFollowMockMvc.perform(get("/api/follows?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(follow.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].followTime").value(hasItem(sameInstant(DEFAULT_FOLLOW_TIME))));
    // }

    // @Test
    // @Transactional
    // public void getFollow() throws Exception {
    //     // Initialize the database
    //     followRepository.saveAndFlush(follow);

    //     // Get the follow
    //     restFollowMockMvc.perform(get("/api/follows/{id}", follow.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(follow.getId().intValue()))
    //         .andExpect(jsonPath("$.followTime").value(sameInstant(DEFAULT_FOLLOW_TIME)));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingFollow() throws Exception {
    //     // Get the follow
    //     restFollowMockMvc.perform(get("/api/follows/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateFollow() throws Exception {
    //     // Initialize the database
    //     followRepository.saveAndFlush(follow);
    //     followSearchRepository.save(follow);
    //     int databaseSizeBeforeUpdate = followRepository.findAll().size();

    //     // Update the follow
    //     Follow updatedFollow = followRepository.findOne(follow.getId());
    //     updatedFollow
    //         .followTime(UPDATED_FOLLOW_TIME);

    //     restFollowMockMvc.perform(put("/api/follows")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedFollow)))
    //         .andExpect(status().isOk());

    //     // Validate the Follow in the database
    //     List<Follow> followList = followRepository.findAll();
    //     assertThat(followList).hasSize(databaseSizeBeforeUpdate);
    //     Follow testFollow = followList.get(followList.size() - 1);
    //     assertThat(testFollow.getFollowTime()).isEqualTo(UPDATED_FOLLOW_TIME);

    //     // Validate the Follow in Elasticsearch
    //     Follow followEs = followSearchRepository.findOne(testFollow.getId());
    //     assertThat(followEs).isEqualToComparingFieldByField(testFollow);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingFollow() throws Exception {
    //     int databaseSizeBeforeUpdate = followRepository.findAll().size();

    //     // Create the Follow

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restFollowMockMvc.perform(put("/api/follows")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(follow)))
    //         .andExpect(status().isCreated());

    //     // Validate the Follow in the database
    //     List<Follow> followList = followRepository.findAll();
    //     assertThat(followList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteFollow() throws Exception {
    //     // Initialize the database
    //     followRepository.saveAndFlush(follow);
    //     followSearchRepository.save(follow);
    //     int databaseSizeBeforeDelete = followRepository.findAll().size();

    //     // Get the follow
    //     restFollowMockMvc.perform(delete("/api/follows/{id}", follow.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean followExistsInEs = followSearchRepository.exists(follow.getId());
    //     assertThat(followExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Follow> followList = followRepository.findAll();
    //     assertThat(followList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchFollow() throws Exception {
    //     // Initialize the database
    //     followRepository.saveAndFlush(follow);
    //     followSearchRepository.save(follow);

    //     // Search the follow
    //     restFollowMockMvc.perform(get("/api/_search/follows?query=id:" + follow.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(follow.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].followTime").value(hasItem(sameInstant(DEFAULT_FOLLOW_TIME))));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Follow.class);
    // }
    // */
}

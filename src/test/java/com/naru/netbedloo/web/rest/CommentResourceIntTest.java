package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the CommentResource REST controller.
 *
 * @see CommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class CommentResourceIntTest {

    // private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    // private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    // private static final ZonedDateTime DEFAULT_PUBLICATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    // private static final ZonedDateTime UPDATED_PUBLICATION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    // @Autowired
    // private CommentRepository commentRepository;

    // @Autowired
    // private CommentSearchRepository commentSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restCommentMockMvc;

    // private Comment comment;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     CommentResource commentResource = new CommentResource(commentRepository, commentSearchRepository);
    //     this.restCommentMockMvc = MockMvcBuilders.standaloneSetup(commentResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Comment createEntity(EntityManager em) {
    //     Comment comment = new Comment()
    //         .comment(DEFAULT_COMMENT)
    //         .publicationTime(DEFAULT_PUBLICATION_TIME);
    //     return comment;
    // }

    // @Before
    // public void initTest() {
    //     commentSearchRepository.deleteAll();
    //     comment = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createComment() throws Exception {
    //     int databaseSizeBeforeCreate = commentRepository.findAll().size();

    //     // Create the Comment
    //     restCommentMockMvc.perform(post("/api/comments")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(comment)))
    //         .andExpect(status().isCreated());

    //     // Validate the Comment in the database
    //     List<Comment> commentList = commentRepository.findAll();
    //     assertThat(commentList).hasSize(databaseSizeBeforeCreate + 1);
    //     Comment testComment = commentList.get(commentList.size() - 1);
    //     assertThat(testComment.getComment()).isEqualTo(DEFAULT_COMMENT);
    //     assertThat(testComment.getPublicationTime()).isEqualTo(DEFAULT_PUBLICATION_TIME);

    //     // Validate the Comment in Elasticsearch
    //     Comment commentEs = commentSearchRepository.findOne(testComment.getId());
    //     assertThat(commentEs).isEqualToComparingFieldByField(testComment);
    // }

    // @Test
    // @Transactional
    // public void createCommentWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = commentRepository.findAll().size();

    //     // Create the Comment with an existing ID
    //     comment.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restCommentMockMvc.perform(post("/api/comments")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(comment)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Comment> commentList = commentRepository.findAll();
    //     assertThat(commentList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllComments() throws Exception {
    //     // Initialize the database
    //     commentRepository.saveAndFlush(comment);

    //     // Get all the commentList
    //     restCommentMockMvc.perform(get("/api/comments?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(comment.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
    //         .andExpect(jsonPath("$.[*].publicationTime").value(hasItem(sameInstant(DEFAULT_PUBLICATION_TIME))));
    // }

    // @Test
    // @Transactional
    // public void getComment() throws Exception {
    //     // Initialize the database
    //     commentRepository.saveAndFlush(comment);

    //     // Get the comment
    //     restCommentMockMvc.perform(get("/api/comments/{id}", comment.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(comment.getId().intValue()))
    //         .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
    //         .andExpect(jsonPath("$.publicationTime").value(sameInstant(DEFAULT_PUBLICATION_TIME)));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingComment() throws Exception {
    //     // Get the comment
    //     restCommentMockMvc.perform(get("/api/comments/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateComment() throws Exception {
    //     // Initialize the database
    //     commentRepository.saveAndFlush(comment);
    //     commentSearchRepository.save(comment);
    //     int databaseSizeBeforeUpdate = commentRepository.findAll().size();

    //     // Update the comment
    //     Comment updatedComment = commentRepository.findOne(comment.getId());
    //     updatedComment
    //         .comment(UPDATED_COMMENT)
    //         .publicationTime(UPDATED_PUBLICATION_TIME);

    //     restCommentMockMvc.perform(put("/api/comments")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedComment)))
    //         .andExpect(status().isOk());

    //     // Validate the Comment in the database
    //     List<Comment> commentList = commentRepository.findAll();
    //     assertThat(commentList).hasSize(databaseSizeBeforeUpdate);
    //     Comment testComment = commentList.get(commentList.size() - 1);
    //     assertThat(testComment.getComment()).isEqualTo(UPDATED_COMMENT);
    //     assertThat(testComment.getPublicationTime()).isEqualTo(UPDATED_PUBLICATION_TIME);

    //     // Validate the Comment in Elasticsearch
    //     Comment commentEs = commentSearchRepository.findOne(testComment.getId());
    //     assertThat(commentEs).isEqualToComparingFieldByField(testComment);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingComment() throws Exception {
    //     int databaseSizeBeforeUpdate = commentRepository.findAll().size();

    //     // Create the Comment

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restCommentMockMvc.perform(put("/api/comments")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(comment)))
    //         .andExpect(status().isCreated());

    //     // Validate the Comment in the database
    //     List<Comment> commentList = commentRepository.findAll();
    //     assertThat(commentList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteComment() throws Exception {
    //     // Initialize the database
    //     commentRepository.saveAndFlush(comment);
    //     commentSearchRepository.save(comment);
    //     int databaseSizeBeforeDelete = commentRepository.findAll().size();

    //     // Get the comment
    //     restCommentMockMvc.perform(delete("/api/comments/{id}", comment.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean commentExistsInEs = commentSearchRepository.exists(comment.getId());
    //     assertThat(commentExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Comment> commentList = commentRepository.findAll();
    //     assertThat(commentList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchComment() throws Exception {
    //     // Initialize the database
    //     commentRepository.saveAndFlush(comment);
    //     commentSearchRepository.save(comment);

    //     // Search the comment
    //     restCommentMockMvc.perform(get("/api/_search/comments?query=id:" + comment.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(comment.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
    //         .andExpect(jsonPath("$.[*].publicationTime").value(hasItem(sameInstant(DEFAULT_PUBLICATION_TIME))));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Comment.class);
    // }*/
}

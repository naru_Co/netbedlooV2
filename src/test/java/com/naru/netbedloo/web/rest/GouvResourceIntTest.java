package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the GouvResource REST controller.
 *
 * @see GouvResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class GouvResourceIntTest {

    // private static final String DEFAULT_NAME = "AAAAAAAAAA";
    // private static final String UPDATED_NAME = "BBBBBBBBBB";

    // private static final Double DEFAULT_LONGITUDE = 1D;
    // private static final Double UPDATED_LONGITUDE = 2D;

    // private static final Double DEFAULT_LATITUDE = 1D;
    // private static final Double UPDATED_LATITUDE = 2D;

    // @Autowired
    // private GouvRepository gouvRepository;

    // @Autowired
    // private GouvSearchRepository gouvSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restGouvMockMvc;

    // private Gouv gouv;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     GouvResource gouvResource = new GouvResource(gouvRepository, gouvSearchRepository);
    //     this.restGouvMockMvc = MockMvcBuilders.standaloneSetup(gouvResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Gouv createEntity(EntityManager em) {
    //     Gouv gouv = new Gouv()
    //         .name(DEFAULT_NAME)
    //         .longitude(DEFAULT_LONGITUDE)
    //         .latitude(DEFAULT_LATITUDE);
    //     return gouv;
    // }

    // @Before
    // public void initTest() {
    //     gouvSearchRepository.deleteAll();
    //     gouv = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createGouv() throws Exception {
    //     int databaseSizeBeforeCreate = gouvRepository.findAll().size();

    //     // Create the Gouv
    //     restGouvMockMvc.perform(post("/api/gouvs")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(gouv)))
    //         .andExpect(status().isCreated());

    //     // Validate the Gouv in the database
    //     List<Gouv> gouvList = gouvRepository.findAll();
    //     assertThat(gouvList).hasSize(databaseSizeBeforeCreate + 1);
    //     Gouv testGouv = gouvList.get(gouvList.size() - 1);
    //     assertThat(testGouv.getName()).isEqualTo(DEFAULT_NAME);
    //     assertThat(testGouv.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
    //     assertThat(testGouv.getLatitude()).isEqualTo(DEFAULT_LATITUDE);

    //     // Validate the Gouv in Elasticsearch
    //     Gouv gouvEs = gouvSearchRepository.findOne(testGouv.getId());
    //     assertThat(gouvEs).isEqualToComparingFieldByField(testGouv);
    // }

    // @Test
    // @Transactional
    // public void createGouvWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = gouvRepository.findAll().size();

    //     // Create the Gouv with an existing ID
    //     gouv.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restGouvMockMvc.perform(post("/api/gouvs")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(gouv)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Gouv> gouvList = gouvRepository.findAll();
    //     assertThat(gouvList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllGouvs() throws Exception {
    //     // Initialize the database
    //     gouvRepository.saveAndFlush(gouv);

    //     // Get all the gouvList
    //     restGouvMockMvc.perform(get("/api/gouvs?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(gouv.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
    //         .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())));
    // }

    // @Test
    // @Transactional
    // public void getGouv() throws Exception {
    //     // Initialize the database
    //     gouvRepository.saveAndFlush(gouv);

    //     // Get the gouv
    //     restGouvMockMvc.perform(get("/api/gouvs/{id}", gouv.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(gouv.getId().intValue()))
    //         .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
    //         .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
    //         .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingGouv() throws Exception {
    //     // Get the gouv
    //     restGouvMockMvc.perform(get("/api/gouvs/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateGouv() throws Exception {
    //     // Initialize the database
    //     gouvRepository.saveAndFlush(gouv);
    //     gouvSearchRepository.save(gouv);
    //     int databaseSizeBeforeUpdate = gouvRepository.findAll().size();

    //     // Update the gouv
    //     Gouv updatedGouv = gouvRepository.findOne(gouv.getId());
    //     updatedGouv
    //         .name(UPDATED_NAME)
    //         .longitude(UPDATED_LONGITUDE)
    //         .latitude(UPDATED_LATITUDE);

    //     restGouvMockMvc.perform(put("/api/gouvs")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedGouv)))
    //         .andExpect(status().isOk());

    //     // Validate the Gouv in the database
    //     List<Gouv> gouvList = gouvRepository.findAll();
    //     assertThat(gouvList).hasSize(databaseSizeBeforeUpdate);
    //     Gouv testGouv = gouvList.get(gouvList.size() - 1);
    //     assertThat(testGouv.getName()).isEqualTo(UPDATED_NAME);
    //     assertThat(testGouv.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
    //     assertThat(testGouv.getLatitude()).isEqualTo(UPDATED_LATITUDE);

    //     // Validate the Gouv in Elasticsearch
    //     Gouv gouvEs = gouvSearchRepository.findOne(testGouv.getId());
    //     assertThat(gouvEs).isEqualToComparingFieldByField(testGouv);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingGouv() throws Exception {
    //     int databaseSizeBeforeUpdate = gouvRepository.findAll().size();

    //     // Create the Gouv

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restGouvMockMvc.perform(put("/api/gouvs")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(gouv)))
    //         .andExpect(status().isCreated());

    //     // Validate the Gouv in the database
    //     List<Gouv> gouvList = gouvRepository.findAll();
    //     assertThat(gouvList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteGouv() throws Exception {
    //     // Initialize the database
    //     gouvRepository.saveAndFlush(gouv);
    //     gouvSearchRepository.save(gouv);
    //     int databaseSizeBeforeDelete = gouvRepository.findAll().size();

    //     // Get the gouv
    //     restGouvMockMvc.perform(delete("/api/gouvs/{id}", gouv.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean gouvExistsInEs = gouvSearchRepository.exists(gouv.getId());
    //     assertThat(gouvExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Gouv> gouvList = gouvRepository.findAll();
    //     assertThat(gouvList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchGouv() throws Exception {
    //     // Initialize the database
    //     gouvRepository.saveAndFlush(gouv);
    //     gouvSearchRepository.save(gouv);

    //     // Search the gouv
    //     restGouvMockMvc.perform(get("/api/_search/gouvs?query=id:" + gouv.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(gouv.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
    //         .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
    //         .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Gouv.class);
    // }
}

package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the ForumResource REST controller.
 *
 * @see ForumResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class ForumResourceIntTest {

    // @Autowired
    // private ForumRepository forumRepository;

    // @Autowired
    // private ForumSearchRepository forumSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restForumMockMvc;

    // private Forum forum;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     ForumResource forumResource = new ForumResource(forumRepository, forumSearchRepository);
    //     this.restForumMockMvc = MockMvcBuilders.standaloneSetup(forumResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Forum createEntity(EntityManager em) {
    //     Forum forum = new Forum();
    //     return forum;
    // }

    // @Before
    // public void initTest() {
    //     forumSearchRepository.deleteAll();
    //     forum = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createForum() throws Exception {
    //     int databaseSizeBeforeCreate = forumRepository.findAll().size();

    //     // Create the Forum
    //     restForumMockMvc.perform(post("/api/forums")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(forum)))
    //         .andExpect(status().isCreated());

    //     // Validate the Forum in the database
    //     List<Forum> forumList = forumRepository.findAll();
    //     assertThat(forumList).hasSize(databaseSizeBeforeCreate + 1);
    //     Forum testForum = forumList.get(forumList.size() - 1);

    //     // Validate the Forum in Elasticsearch
    //     Forum forumEs = forumSearchRepository.findOne(testForum.getId());
    //     assertThat(forumEs).isEqualToComparingFieldByField(testForum);
    // }

    // @Test
    // @Transactional
    // public void createForumWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = forumRepository.findAll().size();

    //     // Create the Forum with an existing ID
    //     forum.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restForumMockMvc.perform(post("/api/forums")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(forum)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Forum> forumList = forumRepository.findAll();
    //     assertThat(forumList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllForums() throws Exception {
    //     // Initialize the database
    //     forumRepository.saveAndFlush(forum);

    //     // Get all the forumList
    //     restForumMockMvc.perform(get("/api/forums?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(forum.getId().intValue())));
    // }

    // @Test
    // @Transactional
    // public void getForum() throws Exception {
    //     // Initialize the database
    //     forumRepository.saveAndFlush(forum);

    //     // Get the forum
    //     restForumMockMvc.perform(get("/api/forums/{id}", forum.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(forum.getId().intValue()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingForum() throws Exception {
    //     // Get the forum
    //     restForumMockMvc.perform(get("/api/forums/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateForum() throws Exception {
    //     // Initialize the database
    //     forumRepository.saveAndFlush(forum);
    //     forumSearchRepository.save(forum);
    //     int databaseSizeBeforeUpdate = forumRepository.findAll().size();

    //     // Update the forum
    //     Forum updatedForum = forumRepository.findOne(forum.getId());

    //     restForumMockMvc.perform(put("/api/forums")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedForum)))
    //         .andExpect(status().isOk());

    //     // Validate the Forum in the database
    //     List<Forum> forumList = forumRepository.findAll();
    //     assertThat(forumList).hasSize(databaseSizeBeforeUpdate);
    //     Forum testForum = forumList.get(forumList.size() - 1);

    //     // Validate the Forum in Elasticsearch
    //     Forum forumEs = forumSearchRepository.findOne(testForum.getId());
    //     assertThat(forumEs).isEqualToComparingFieldByField(testForum);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingForum() throws Exception {
    //     int databaseSizeBeforeUpdate = forumRepository.findAll().size();

    //     // Create the Forum

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restForumMockMvc.perform(put("/api/forums")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(forum)))
    //         .andExpect(status().isCreated());

    //     // Validate the Forum in the database
    //     List<Forum> forumList = forumRepository.findAll();
    //     assertThat(forumList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteForum() throws Exception {
    //     // Initialize the database
    //     forumRepository.saveAndFlush(forum);
    //     forumSearchRepository.save(forum);
    //     int databaseSizeBeforeDelete = forumRepository.findAll().size();

    //     // Get the forum
    //     restForumMockMvc.perform(delete("/api/forums/{id}", forum.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean forumExistsInEs = forumSearchRepository.exists(forum.getId());
    //     assertThat(forumExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Forum> forumList = forumRepository.findAll();
    //     assertThat(forumList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchForum() throws Exception {
    //     // Initialize the database
    //     forumRepository.saveAndFlush(forum);
    //     forumSearchRepository.save(forum);

    //     // Search the forum
    //     restForumMockMvc.perform(get("/api/_search/forums?query=id:" + forum.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(forum.getId().intValue())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Forum.class);
    // }
}

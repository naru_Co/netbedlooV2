package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the OwnedHistoryResource REST controller.
 *
 * @see OwnedHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class OwnedHistoryResourceIntTest {

    // private static final LocalDate DEFAULT_OWNINGDATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_OWNINGDATE = LocalDate.now(ZoneId.systemDefault());

    // private static final LocalDate DEFAULT_ERASE_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_ERASE_DATE = LocalDate.now(ZoneId.systemDefault());

    // @Autowired
    // private OwnedHistoryRepository ownedHistoryRepository;

    // @Autowired
    // private OwnedHistorySearchRepository ownedHistorySearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restOwnedHistoryMockMvc;

    // private OwnedHistory ownedHistory;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     OwnedHistoryResource ownedHistoryResource = new OwnedHistoryResource(ownedHistoryRepository, ownedHistorySearchRepository);
    //     this.restOwnedHistoryMockMvc = MockMvcBuilders.standaloneSetup(ownedHistoryResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // /** 
    // public static OwnedHistory createEntity(EntityManager em) {
    //     OwnedHistory ownedHistory = new OwnedHistory()
    //         .owningdate(DEFAULT_OWNINGDATE)
    //         .eraseDate(DEFAULT_ERASE_DATE);
    //     return ownedHistory;
    // }

    // @Before
    // public void initTest() {
    //     ownedHistorySearchRepository.deleteAll();
    //     ownedHistory = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createOwnedHistory() throws Exception {
    //     int databaseSizeBeforeCreate = ownedHistoryRepository.findAll().size();

    //     // Create the OwnedHistory
    //     restOwnedHistoryMockMvc.perform(post("/api/owned-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(ownedHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the OwnedHistory in the database
    //     List<OwnedHistory> ownedHistoryList = ownedHistoryRepository.findAll();
    //     assertThat(ownedHistoryList).hasSize(databaseSizeBeforeCreate + 1);
    //     OwnedHistory testOwnedHistory = ownedHistoryList.get(ownedHistoryList.size() - 1);
    //     assertThat(testOwnedHistory.getOwningdate()).isEqualTo(DEFAULT_OWNINGDATE);
    //     assertThat(testOwnedHistory.getEraseDate()).isEqualTo(DEFAULT_ERASE_DATE);

    //     // Validate the OwnedHistory in Elasticsearch
    //     OwnedHistory ownedHistoryEs = ownedHistorySearchRepository.findOne(testOwnedHistory.getId());
    //     assertThat(ownedHistoryEs).isEqualToComparingFieldByField(testOwnedHistory);
    // }

    // @Test
    // @Transactional
    // public void createOwnedHistoryWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = ownedHistoryRepository.findAll().size();

    //     // Create the OwnedHistory with an existing ID
    //     ownedHistory.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restOwnedHistoryMockMvc.perform(post("/api/owned-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(ownedHistory)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<OwnedHistory> ownedHistoryList = ownedHistoryRepository.findAll();
    //     assertThat(ownedHistoryList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllOwnedHistories() throws Exception {
    //     // Initialize the database
    //     ownedHistoryRepository.saveAndFlush(ownedHistory);

    //     // Get all the ownedHistoryList
    //     restOwnedHistoryMockMvc.perform(get("/api/owned-histories?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(ownedHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].owningdate").value(hasItem(DEFAULT_OWNINGDATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getOwnedHistory() throws Exception {
    //     // Initialize the database
    //     ownedHistoryRepository.saveAndFlush(ownedHistory);

    //     // Get the ownedHistory
    //     restOwnedHistoryMockMvc.perform(get("/api/owned-histories/{id}", ownedHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(ownedHistory.getId().intValue()))
    //         .andExpect(jsonPath("$.owningdate").value(DEFAULT_OWNINGDATE.toString()))
    //         .andExpect(jsonPath("$.eraseDate").value(DEFAULT_ERASE_DATE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingOwnedHistory() throws Exception {
    //     // Get the ownedHistory
    //     restOwnedHistoryMockMvc.perform(get("/api/owned-histories/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateOwnedHistory() throws Exception {
    //     // Initialize the database
    //     ownedHistoryRepository.saveAndFlush(ownedHistory);
    //     ownedHistorySearchRepository.save(ownedHistory);
    //     int databaseSizeBeforeUpdate = ownedHistoryRepository.findAll().size();

    //     // Update the ownedHistory
    //     OwnedHistory updatedOwnedHistory = ownedHistoryRepository.findOne(ownedHistory.getId());
    //     updatedOwnedHistory
    //         .owningdate(UPDATED_OWNINGDATE)
    //         .eraseDate(UPDATED_ERASE_DATE);

    //     restOwnedHistoryMockMvc.perform(put("/api/owned-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedOwnedHistory)))
    //         .andExpect(status().isOk());

    //     // Validate the OwnedHistory in the database
    //     List<OwnedHistory> ownedHistoryList = ownedHistoryRepository.findAll();
    //     assertThat(ownedHistoryList).hasSize(databaseSizeBeforeUpdate);
    //     OwnedHistory testOwnedHistory = ownedHistoryList.get(ownedHistoryList.size() - 1);
    //     assertThat(testOwnedHistory.getOwningdate()).isEqualTo(UPDATED_OWNINGDATE);
    //     assertThat(testOwnedHistory.getEraseDate()).isEqualTo(UPDATED_ERASE_DATE);

    //     // Validate the OwnedHistory in Elasticsearch
    //     OwnedHistory ownedHistoryEs = ownedHistorySearchRepository.findOne(testOwnedHistory.getId());
    //     assertThat(ownedHistoryEs).isEqualToComparingFieldByField(testOwnedHistory);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingOwnedHistory() throws Exception {
    //     int databaseSizeBeforeUpdate = ownedHistoryRepository.findAll().size();

    //     // Create the OwnedHistory

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restOwnedHistoryMockMvc.perform(put("/api/owned-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(ownedHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the OwnedHistory in the database
    //     List<OwnedHistory> ownedHistoryList = ownedHistoryRepository.findAll();
    //     assertThat(ownedHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteOwnedHistory() throws Exception {
    //     // Initialize the database
    //     ownedHistoryRepository.saveAndFlush(ownedHistory);
    //     ownedHistorySearchRepository.save(ownedHistory);
    //     int databaseSizeBeforeDelete = ownedHistoryRepository.findAll().size();

    //     // Get the ownedHistory
    //     restOwnedHistoryMockMvc.perform(delete("/api/owned-histories/{id}", ownedHistory.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean ownedHistoryExistsInEs = ownedHistorySearchRepository.exists(ownedHistory.getId());
    //     assertThat(ownedHistoryExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<OwnedHistory> ownedHistoryList = ownedHistoryRepository.findAll();
    //     assertThat(ownedHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchOwnedHistory() throws Exception {
    //     // Initialize the database
    //     ownedHistoryRepository.saveAndFlush(ownedHistory);
    //     ownedHistorySearchRepository.save(ownedHistory);

    //     // Search the ownedHistory
    //     restOwnedHistoryMockMvc.perform(get("/api/_search/owned-histories?query=id:" + ownedHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(ownedHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].owningdate").value(hasItem(DEFAULT_OWNINGDATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(OwnedHistory.class);
    // }*/
}

package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import com.naru.netbedloo.domain.Okay;
import com.naru.netbedloo.repository.OkayRepository;
import com.naru.netbedloo.repository.search.OkaySearchRepository;
import com.naru.netbedloo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OkayResource REST controller.
 *
 * @see OkayResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class OkayResourceIntTest {

    private static final Instant DEFAULT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private OkayRepository okayRepository;

    @Autowired
    private OkaySearchRepository okaySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOkayMockMvc;

    private Okay okay;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OkayResource okayResource = new OkayResource(okayRepository, okaySearchRepository);
        this.restOkayMockMvc = MockMvcBuilders.standaloneSetup(okayResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Okay createEntity(EntityManager em) {
        Okay okay = new Okay()
            .time(DEFAULT_TIME);
        return okay;
    }

    @Before
    public void initTest() {
        okaySearchRepository.deleteAll();
        okay = createEntity(em);
    }

    @Test
    @Transactional
    public void createOkay() throws Exception {
        int databaseSizeBeforeCreate = okayRepository.findAll().size();

        // Create the Okay
        restOkayMockMvc.perform(post("/api/okays")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(okay)))
            .andExpect(status().isCreated());

        // Validate the Okay in the database
        List<Okay> okayList = okayRepository.findAll();
        assertThat(okayList).hasSize(databaseSizeBeforeCreate + 1);
        Okay testOkay = okayList.get(okayList.size() - 1);
        assertThat(testOkay.getTime()).isEqualTo(DEFAULT_TIME);

        // Validate the Okay in Elasticsearch
        Okay okayEs = okaySearchRepository.findOne(testOkay.getId());
        assertThat(okayEs).isEqualToIgnoringGivenFields(testOkay);
    }

    @Test
    @Transactional
    public void createOkayWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = okayRepository.findAll().size();

        // Create the Okay with an existing ID
        okay.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOkayMockMvc.perform(post("/api/okays")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(okay)))
            .andExpect(status().isBadRequest());

        // Validate the Okay in the database
        List<Okay> okayList = okayRepository.findAll();
        assertThat(okayList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllOkays() throws Exception {
        // Initialize the database
        okayRepository.saveAndFlush(okay);

        // Get all the okayList
        restOkayMockMvc.perform(get("/api/okays?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(okay.getId().intValue())))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())));
    }

    @Test
    @Transactional
    public void getOkay() throws Exception {
        // Initialize the database
        okayRepository.saveAndFlush(okay);

        // Get the okay
        restOkayMockMvc.perform(get("/api/okays/{id}", okay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(okay.getId().intValue()))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOkay() throws Exception {
        // Get the okay
        restOkayMockMvc.perform(get("/api/okays/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOkay() throws Exception {
        // Initialize the database
        okayRepository.saveAndFlush(okay);
        okaySearchRepository.save(okay);
        int databaseSizeBeforeUpdate = okayRepository.findAll().size();

        // Update the okay
        Okay updatedOkay = okayRepository.findOne(okay.getId());
        // Disconnect from session so that the updates on updatedOkay are not directly saved in db
        em.detach(updatedOkay);
        updatedOkay
            .time(UPDATED_TIME);

        restOkayMockMvc.perform(put("/api/okays")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOkay)))
            .andExpect(status().isOk());

        // Validate the Okay in the database
        List<Okay> okayList = okayRepository.findAll();
        assertThat(okayList).hasSize(databaseSizeBeforeUpdate);
        Okay testOkay = okayList.get(okayList.size() - 1);
        assertThat(testOkay.getTime()).isEqualTo(UPDATED_TIME);

        // Validate the Okay in Elasticsearch
        Okay okayEs = okaySearchRepository.findOne(testOkay.getId());
        assertThat(okayEs).isEqualToIgnoringGivenFields(testOkay);
    }

    @Test
    @Transactional
    public void updateNonExistingOkay() throws Exception {
        int databaseSizeBeforeUpdate = okayRepository.findAll().size();

        // Create the Okay

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOkayMockMvc.perform(put("/api/okays")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(okay)))
            .andExpect(status().isCreated());

        // Validate the Okay in the database
        List<Okay> okayList = okayRepository.findAll();
        assertThat(okayList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOkay() throws Exception {
        // Initialize the database
        okayRepository.saveAndFlush(okay);
        okaySearchRepository.save(okay);
        int databaseSizeBeforeDelete = okayRepository.findAll().size();

        // Get the okay
        restOkayMockMvc.perform(delete("/api/okays/{id}", okay.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean okayExistsInEs = okaySearchRepository.exists(okay.getId());
        assertThat(okayExistsInEs).isFalse();

        // Validate the database is empty
        List<Okay> okayList = okayRepository.findAll();
        assertThat(okayList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchOkay() throws Exception {
        // Initialize the database
        okayRepository.saveAndFlush(okay);
        okaySearchRepository.save(okay);

        // Search the okay
        restOkayMockMvc.perform(get("/api/_search/okays?query=id:" + okay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(okay.getId().intValue())))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Okay.class);
        Okay okay1 = new Okay();
        okay1.setId(1L);
        Okay okay2 = new Okay();
        okay2.setId(okay1.getId());
        assertThat(okay1).isEqualTo(okay2);
        okay2.setId(2L);
        assertThat(okay1).isNotEqualTo(okay2);
        okay1.setId(null);
        assertThat(okay1).isNotEqualTo(okay2);
    }
}

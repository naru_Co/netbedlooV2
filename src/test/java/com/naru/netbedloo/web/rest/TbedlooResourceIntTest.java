package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the TbedlooResource REST controller.
 *
 * @see TbedlooResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class TbedlooResourceIntTest {

    // private static final LocalDate DEFAULT_TBEDLOO_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_TBEDLOO_DATE = LocalDate.now(ZoneId.systemDefault());

    // @Autowired
    // private TbedlooRepository tbedlooRepository;

    // @Autowired
    // private TbedlooSearchRepository tbedlooSearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restTbedlooMockMvc;

    // private Tbedloo tbedloo;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     TbedlooResource tbedlooResource = new TbedlooResource(tbedlooRepository, tbedlooSearchRepository);
    //     this.restTbedlooMockMvc = MockMvcBuilders.standaloneSetup(tbedlooResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
     
    // public static Tbedloo createEntity(EntityManager em) {
    //     Tbedloo tbedloo = new Tbedloo()
    //         .tbedlooDate(DEFAULT_TBEDLOO_DATE);
    //     return tbedloo;
    // }

    // @Before
    // public void initTest() {
    //     tbedlooSearchRepository.deleteAll();
    //     tbedloo = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createTbedloo() throws Exception {
    //     int databaseSizeBeforeCreate = tbedlooRepository.findAll().size();

    //     // Create the Tbedloo
    //     restTbedlooMockMvc.perform(post("/api/tbedloos")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(tbedloo)))
    //         .andExpect(status().isCreated());

    //     // Validate the Tbedloo in the database
    //     List<Tbedloo> tbedlooList = tbedlooRepository.findAll();
    //     assertThat(tbedlooList).hasSize(databaseSizeBeforeCreate + 1);
    //     Tbedloo testTbedloo = tbedlooList.get(tbedlooList.size() - 1);
    //     assertThat(testTbedloo.getTbedlooDate()).isEqualTo(DEFAULT_TBEDLOO_DATE);

    //     // Validate the Tbedloo in Elasticsearch
    //     Tbedloo tbedlooEs = tbedlooSearchRepository.findOne(testTbedloo.getId());
    //     assertThat(tbedlooEs).isEqualToComparingFieldByField(testTbedloo);
    // }

    // @Test
    // @Transactional
    // public void createTbedlooWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = tbedlooRepository.findAll().size();

    //     // Create the Tbedloo with an existing ID
    //     tbedloo.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restTbedlooMockMvc.perform(post("/api/tbedloos")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(tbedloo)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Tbedloo> tbedlooList = tbedlooRepository.findAll();
    //     assertThat(tbedlooList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllTbedloos() throws Exception {
    //     // Initialize the database
    //     tbedlooRepository.saveAndFlush(tbedloo);

    //     // Get all the tbedlooList
    //     restTbedlooMockMvc.perform(get("/api/tbedloos?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(tbedloo.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].tbedlooDate").value(hasItem(DEFAULT_TBEDLOO_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getTbedloo() throws Exception {
    //     // Initialize the database
    //     tbedlooRepository.saveAndFlush(tbedloo);

    //     // Get the tbedloo
    //     restTbedlooMockMvc.perform(get("/api/tbedloos/{id}", tbedloo.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(tbedloo.getId().intValue()))
    //         .andExpect(jsonPath("$.tbedlooDate").value(DEFAULT_TBEDLOO_DATE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingTbedloo() throws Exception {
    //     // Get the tbedloo
    //     restTbedlooMockMvc.perform(get("/api/tbedloos/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateTbedloo() throws Exception {
    //     // Initialize the database
    //     tbedlooRepository.saveAndFlush(tbedloo);
    //     tbedlooSearchRepository.save(tbedloo);
    //     int databaseSizeBeforeUpdate = tbedlooRepository.findAll().size();

    //     // Update the tbedloo
    //     Tbedloo updatedTbedloo = tbedlooRepository.findOne(tbedloo.getId());
    //     updatedTbedloo
    //         .tbedlooDate(UPDATED_TBEDLOO_DATE);

    //     restTbedlooMockMvc.perform(put("/api/tbedloos")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedTbedloo)))
    //         .andExpect(status().isOk());

    //     // Validate the Tbedloo in the database
    //     List<Tbedloo> tbedlooList = tbedlooRepository.findAll();
    //     assertThat(tbedlooList).hasSize(databaseSizeBeforeUpdate);
    //     Tbedloo testTbedloo = tbedlooList.get(tbedlooList.size() - 1);
    //     assertThat(testTbedloo.getTbedlooDate()).isEqualTo(UPDATED_TBEDLOO_DATE);

    //     // Validate the Tbedloo in Elasticsearch
    //     Tbedloo tbedlooEs = tbedlooSearchRepository.findOne(testTbedloo.getId());
    //     assertThat(tbedlooEs).isEqualToComparingFieldByField(testTbedloo);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingTbedloo() throws Exception {
    //     int databaseSizeBeforeUpdate = tbedlooRepository.findAll().size();

    //     // Create the Tbedloo

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restTbedlooMockMvc.perform(put("/api/tbedloos")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(tbedloo)))
    //         .andExpect(status().isCreated());

    //     // Validate the Tbedloo in the database
    //     List<Tbedloo> tbedlooList = tbedlooRepository.findAll();
    //     assertThat(tbedlooList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteTbedloo() throws Exception {
    //     // Initialize the database
    //     tbedlooRepository.saveAndFlush(tbedloo);
    //     tbedlooSearchRepository.save(tbedloo);
    //     int databaseSizeBeforeDelete = tbedlooRepository.findAll().size();

    //     // Get the tbedloo
    //     restTbedlooMockMvc.perform(delete("/api/tbedloos/{id}", tbedloo.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean tbedlooExistsInEs = tbedlooSearchRepository.exists(tbedloo.getId());
    //     assertThat(tbedlooExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<Tbedloo> tbedlooList = tbedlooRepository.findAll();
    //     assertThat(tbedlooList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchTbedloo() throws Exception {
    //     // Initialize the database
    //     tbedlooRepository.saveAndFlush(tbedloo);
    //     tbedlooSearchRepository.save(tbedloo);

    //     // Search the tbedloo
    //     restTbedlooMockMvc.perform(get("/api/_search/tbedloos?query=id:" + tbedloo.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(tbedloo.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].tbedlooDate").value(hasItem(DEFAULT_TBEDLOO_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Tbedloo.class);
    //     Tbedloo tbedloo1 = new Tbedloo();
    //     tbedloo1.setId(1L);
    //     Tbedloo tbedloo2 = new Tbedloo();
    //     tbedloo2.setId(tbedloo1.getId());
    //     assertThat(tbedloo1).isEqualTo(tbedloo2);
    //     tbedloo2.setId(2L);
    //     assertThat(tbedloo1).isNotEqualTo(tbedloo2);
    //     tbedloo1.setId(null);
    //     assertThat(tbedloo1).isNotEqualTo(tbedloo2);
    // }*/
}

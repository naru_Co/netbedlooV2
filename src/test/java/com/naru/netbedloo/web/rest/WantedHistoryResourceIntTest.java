package com.naru.netbedloo.web.rest;

import com.naru.netbedloo.NetbedlooApp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for the WantedHistoryResource REST controller.
 *
 * @see WantedHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NetbedlooApp.class)
public class WantedHistoryResourceIntTest {

    // private static final LocalDate DEFAULT_WANTING_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_WANTING_DATE = LocalDate.now(ZoneId.systemDefault());

    // private static final LocalDate DEFAULT_ERASE_DATE = LocalDate.ofEpochDay(0L);
    // private static final LocalDate UPDATED_ERASE_DATE = LocalDate.now(ZoneId.systemDefault());

    // @Autowired
    // private WantedHistoryRepository wantedHistoryRepository;

    // @Autowired
    // private WantedHistorySearchRepository wantedHistorySearchRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restWantedHistoryMockMvc;

    // private WantedHistory wantedHistory;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     WantedHistoryResource wantedHistoryResource = new WantedHistoryResource(wantedHistoryRepository, wantedHistorySearchRepository);
    //     this.restWantedHistoryMockMvc = MockMvcBuilders.standaloneSetup(wantedHistoryResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // /** 
    // public static WantedHistory createEntity(EntityManager em) {
    //     WantedHistory wantedHistory = new WantedHistory()
    //         .wantingDate(DEFAULT_WANTING_DATE)
    //         .eraseDate(DEFAULT_ERASE_DATE);
    //     return wantedHistory;
    // }

    // @Before
    // public void initTest() {
    //     wantedHistorySearchRepository.deleteAll();
    //     wantedHistory = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createWantedHistory() throws Exception {
    //     int databaseSizeBeforeCreate = wantedHistoryRepository.findAll().size();

    //     // Create the WantedHistory
    //     restWantedHistoryMockMvc.perform(post("/api/wanted-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wantedHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the WantedHistory in the database
    //     List<WantedHistory> wantedHistoryList = wantedHistoryRepository.findAll();
    //     assertThat(wantedHistoryList).hasSize(databaseSizeBeforeCreate + 1);
    //     WantedHistory testWantedHistory = wantedHistoryList.get(wantedHistoryList.size() - 1);
    //     assertThat(testWantedHistory.getWantingDate()).isEqualTo(DEFAULT_WANTING_DATE);
    //     assertThat(testWantedHistory.getEraseDate()).isEqualTo(DEFAULT_ERASE_DATE);

    //     // Validate the WantedHistory in Elasticsearch
    //     WantedHistory wantedHistoryEs = wantedHistorySearchRepository.findOne(testWantedHistory.getId());
    //     assertThat(wantedHistoryEs).isEqualToComparingFieldByField(testWantedHistory);
    // }

    // @Test
    // @Transactional
    // public void createWantedHistoryWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = wantedHistoryRepository.findAll().size();

    //     // Create the WantedHistory with an existing ID
    //     wantedHistory.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restWantedHistoryMockMvc.perform(post("/api/wanted-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wantedHistory)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<WantedHistory> wantedHistoryList = wantedHistoryRepository.findAll();
    //     assertThat(wantedHistoryList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllWantedHistories() throws Exception {
    //     // Initialize the database
    //     wantedHistoryRepository.saveAndFlush(wantedHistory);

    //     // Get all the wantedHistoryList
    //     restWantedHistoryMockMvc.perform(get("/api/wanted-histories?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(wantedHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].wantingDate").value(hasItem(DEFAULT_WANTING_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void getWantedHistory() throws Exception {
    //     // Initialize the database
    //     wantedHistoryRepository.saveAndFlush(wantedHistory);

    //     // Get the wantedHistory
    //     restWantedHistoryMockMvc.perform(get("/api/wanted-histories/{id}", wantedHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(wantedHistory.getId().intValue()))
    //         .andExpect(jsonPath("$.wantingDate").value(DEFAULT_WANTING_DATE.toString()))
    //         .andExpect(jsonPath("$.eraseDate").value(DEFAULT_ERASE_DATE.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingWantedHistory() throws Exception {
    //     // Get the wantedHistory
    //     restWantedHistoryMockMvc.perform(get("/api/wanted-histories/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateWantedHistory() throws Exception {
    //     // Initialize the database
    //     wantedHistoryRepository.saveAndFlush(wantedHistory);
    //     wantedHistorySearchRepository.save(wantedHistory);
    //     int databaseSizeBeforeUpdate = wantedHistoryRepository.findAll().size();

    //     // Update the wantedHistory
    //     WantedHistory updatedWantedHistory = wantedHistoryRepository.findOne(wantedHistory.getId());
    //     updatedWantedHistory
    //         .wantingDate(UPDATED_WANTING_DATE)
    //         .eraseDate(UPDATED_ERASE_DATE);

    //     restWantedHistoryMockMvc.perform(put("/api/wanted-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedWantedHistory)))
    //         .andExpect(status().isOk());

    //     // Validate the WantedHistory in the database
    //     List<WantedHistory> wantedHistoryList = wantedHistoryRepository.findAll();
    //     assertThat(wantedHistoryList).hasSize(databaseSizeBeforeUpdate);
    //     WantedHistory testWantedHistory = wantedHistoryList.get(wantedHistoryList.size() - 1);
    //     assertThat(testWantedHistory.getWantingDate()).isEqualTo(UPDATED_WANTING_DATE);
    //     assertThat(testWantedHistory.getEraseDate()).isEqualTo(UPDATED_ERASE_DATE);

    //     // Validate the WantedHistory in Elasticsearch
    //     WantedHistory wantedHistoryEs = wantedHistorySearchRepository.findOne(testWantedHistory.getId());
    //     assertThat(wantedHistoryEs).isEqualToComparingFieldByField(testWantedHistory);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingWantedHistory() throws Exception {
    //     int databaseSizeBeforeUpdate = wantedHistoryRepository.findAll().size();

    //     // Create the WantedHistory

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restWantedHistoryMockMvc.perform(put("/api/wanted-histories")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(wantedHistory)))
    //         .andExpect(status().isCreated());

    //     // Validate the WantedHistory in the database
    //     List<WantedHistory> wantedHistoryList = wantedHistoryRepository.findAll();
    //     assertThat(wantedHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteWantedHistory() throws Exception {
    //     // Initialize the database
    //     wantedHistoryRepository.saveAndFlush(wantedHistory);
    //     wantedHistorySearchRepository.save(wantedHistory);
    //     int databaseSizeBeforeDelete = wantedHistoryRepository.findAll().size();

    //     // Get the wantedHistory
    //     restWantedHistoryMockMvc.perform(delete("/api/wanted-histories/{id}", wantedHistory.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate Elasticsearch is empty
    //     boolean wantedHistoryExistsInEs = wantedHistorySearchRepository.exists(wantedHistory.getId());
    //     assertThat(wantedHistoryExistsInEs).isFalse();

    //     // Validate the database is empty
    //     List<WantedHistory> wantedHistoryList = wantedHistoryRepository.findAll();
    //     assertThat(wantedHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void searchWantedHistory() throws Exception {
    //     // Initialize the database
    //     wantedHistoryRepository.saveAndFlush(wantedHistory);
    //     wantedHistorySearchRepository.save(wantedHistory);

    //     // Search the wantedHistory
    //     restWantedHistoryMockMvc.perform(get("/api/_search/wanted-histories?query=id:" + wantedHistory.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(wantedHistory.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].wantingDate").value(hasItem(DEFAULT_WANTING_DATE.toString())))
    //         .andExpect(jsonPath("$.[*].eraseDate").value(hasItem(DEFAULT_ERASE_DATE.toString())));
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(WantedHistory.class);
    // } */
}

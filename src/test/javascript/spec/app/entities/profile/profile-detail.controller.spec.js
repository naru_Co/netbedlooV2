'use strict';

describe('Controller Tests', function() {

    describe('Profile Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockProfile, MockArticle, MockOwned, MockWanted, MockMatched, MockGouv, MockMatchingHistory, MockOwnedHistory, MockWantedHistory, MockRating;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockProfile = jasmine.createSpy('MockProfile');
            MockArticle = jasmine.createSpy('MockArticle');
            MockOwned = jasmine.createSpy('MockOwned');
            MockWanted = jasmine.createSpy('MockWanted');
            MockMatched = jasmine.createSpy('MockMatched');
            MockGouv = jasmine.createSpy('MockGouv');
            MockMatchingHistory = jasmine.createSpy('MockMatchingHistory');
            MockOwnedHistory = jasmine.createSpy('MockOwnedHistory');
            MockWantedHistory = jasmine.createSpy('MockWantedHistory');
            MockRating = jasmine.createSpy('MockRating');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Profile': MockProfile,
                'Article': MockArticle,
                'Owned': MockOwned,
                'Wanted': MockWanted,
                'Matched': MockMatched,
                'Gouv': MockGouv,
                'MatchingHistory': MockMatchingHistory,
                'OwnedHistory': MockOwnedHistory,
                'WantedHistory': MockWantedHistory,
                'Rating': MockRating
            };
            createController = function() {
                $injector.get('$controller')("ProfileDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'netbedlooApp:profileUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

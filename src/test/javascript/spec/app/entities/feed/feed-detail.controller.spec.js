'use strict';

describe('Controller Tests', function() {

    describe('Feed Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockFeed, MockMatchingHistory, MockOwnedHistory, MockWantedHistory, MockFollow, MockProfile, MockArticle;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockFeed = jasmine.createSpy('MockFeed');
            MockMatchingHistory = jasmine.createSpy('MockMatchingHistory');
            MockOwnedHistory = jasmine.createSpy('MockOwnedHistory');
            MockWantedHistory = jasmine.createSpy('MockWantedHistory');
            MockFollow = jasmine.createSpy('MockFollow');
            MockProfile = jasmine.createSpy('MockProfile');
            MockArticle = jasmine.createSpy('MockArticle');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Feed': MockFeed,
                'MatchingHistory': MockMatchingHistory,
                'OwnedHistory': MockOwnedHistory,
                'WantedHistory': MockWantedHistory,
                'Follow': MockFollow,
                'Profile': MockProfile,
                'Article': MockArticle
            };
            createController = function() {
                $injector.get('$controller')("FeedDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'netbedlooApp:feedUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

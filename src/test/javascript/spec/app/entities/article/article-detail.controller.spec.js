'use strict';

describe('Controller Tests', function() {

    describe('Article Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockArticle, MockCategory, MockProfile, MockMatched, MockOwned, MockWanted, MockMatchingHistory, MockOwnedHistory, MockWantedHistory, MockRating, MockTag;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockArticle = jasmine.createSpy('MockArticle');
            MockCategory = jasmine.createSpy('MockCategory');
            MockProfile = jasmine.createSpy('MockProfile');
            MockMatched = jasmine.createSpy('MockMatched');
            MockOwned = jasmine.createSpy('MockOwned');
            MockWanted = jasmine.createSpy('MockWanted');
            MockMatchingHistory = jasmine.createSpy('MockMatchingHistory');
            MockOwnedHistory = jasmine.createSpy('MockOwnedHistory');
            MockWantedHistory = jasmine.createSpy('MockWantedHistory');
            MockRating = jasmine.createSpy('MockRating');
            MockTag = jasmine.createSpy('MockTag');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Article': MockArticle,
                'Category': MockCategory,
                'Profile': MockProfile,
                'Matched': MockMatched,
                'Owned': MockOwned,
                'Wanted': MockWanted,
                'MatchingHistory': MockMatchingHistory,
                'OwnedHistory': MockOwnedHistory,
                'WantedHistory': MockWantedHistory,
                'Rating': MockRating,
                'Tag': MockTag
            };
            createController = function() {
                $injector.get('$controller')("ArticleDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'netbedlooApp:articleUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

'use strict';

describe('Controller Tests', function() {

    describe('Matched Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockMatched, MockProfile, MockArticle, MockTbedloo;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockMatched = jasmine.createSpy('MockMatched');
            MockProfile = jasmine.createSpy('MockProfile');
            MockArticle = jasmine.createSpy('MockArticle');
            MockTbedloo = jasmine.createSpy('MockTbedloo');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Matched': MockMatched,
                'Profile': MockProfile,
                'Article': MockArticle,
                'Tbedloo': MockTbedloo
            };
            createController = function() {
                $injector.get('$controller')("MatchedDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'netbedlooApp:matchedUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
